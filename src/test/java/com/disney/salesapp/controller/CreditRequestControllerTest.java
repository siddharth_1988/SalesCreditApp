package com.disney.salesapp.controller;

import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.createStrictMock;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.disney.salesapp.bo.BusinessAreaBO;
import com.disney.salesapp.bo.BusinessGroupBO;
import com.disney.salesapp.bo.CompanyBO;
import com.disney.salesapp.bo.CountryBO;
import com.disney.salesapp.bo.RegionBO;
import com.disney.salesapp.bo.SegmentBO;
import com.disney.salesapp.services.CreditSubmit;

public class CreditRequestControllerTest {
    private CreditSubmit creditSubmit = createStrictMock(CreditSubmit.class);
    private CreditRequestController creditRequestController;

    @Before
    public void createCreditRequestController() throws Exception {
        creditRequestController = new CreditRequestController();
        creditRequestController.setCreditSubmit(creditSubmit);
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetAllSegments() {
        List<SegmentBO> segmentBOs = createSegmentBOs();
        EasyMock.expect(creditSubmit.getAllSegments()).andReturn(segmentBOs);
        EasyMock.replay(creditSubmit);
        
        creditRequestController.getAllSegments();
        
        EasyMock.verify(creditSubmit);
        
        assertTrue(segmentBOs.size() == 4);
        for (SegmentBO segmentBO : segmentBOs) {
            if (segmentBO.getSegmentId().equals(1)) {
                assertTrue(segmentBO.getSegmentName().equals("CONSUMER PRODUCTS"));
            }
            if (segmentBO.getSegmentId().equals(2)) {
                assertTrue(segmentBO.getSegmentName().equals("DISNEY INTERACTIVE MEDIA GROUP"));
            }
            if (segmentBO.getSegmentId().equals(3)) {
                assertTrue(segmentBO.getSegmentName().equals("MEDIA NETWORKS"));
            }
            if (segmentBO.getSegmentId().equals(4)) {
                assertTrue(segmentBO.getSegmentName().equals("PARKS AND RESORTS"));
            }
        }
    }

    private List<SegmentBO> createSegmentBOs() {
        List<SegmentBO> segmentBOs = new ArrayList<SegmentBO>();
        SegmentBO segment = new SegmentBO();
        segment.setSegmentId(1);
        segment.setSegmentName("CONSUMER PRODUCTS");
        segmentBOs.add(segment);
        
        segment = new SegmentBO();
        segment.setSegmentId(2);
        segment.setSegmentName("DISNEY INTERACTIVE MEDIA GROUP");
        segmentBOs.add(segment);
        
        segment = new SegmentBO();
        segment.setSegmentId(3);
        segment.setSegmentName("MEDIA NETWORKS");
        segmentBOs.add(segment);
        
        segment = new SegmentBO();
        segment.setSegmentId(4);
        segment.setSegmentName("PARKS AND RESORTS");
        segmentBOs.add(segment);
        return segmentBOs;
    }
    
    @Test
    public void testGetRegionBySegment() throws Exception {
    	 List<RegionBO> regionBOs = createRegionBOs();
        EasyMock.expect(creditSubmit.getRegionBySegment(3)).andReturn(regionBOs);
        EasyMock.replay(creditSubmit);
        
        creditRequestController.getRegionBySegment(3);
        
        EasyMock.verify(creditSubmit);
        assertTrue(regionBOs.size() == 5);
    }

    private List<RegionBO> createRegionBOs() {
        List<RegionBO> regionBOs = new ArrayList<RegionBO>();
        RegionBO region = new RegionBO();
        region.setRegionId(1);
        region.setRegionName("North America");
        regionBOs.add(region);
        
        region = new RegionBO();
        region.setRegionId(2);
        region.setRegionName("Latin America");
        regionBOs.add(region);
        
        region = new RegionBO();
        region.setRegionId(3);
        region.setRegionName("Asia");
        regionBOs.add(region);
        
        region = new RegionBO();
        region.setRegionId(4);
        region.setRegionName("Europe");
        regionBOs.add(region);
        
        region = new RegionBO();
        region.setRegionId(5);
        region.setRegionName("Australia");
        regionBOs.add(region);
        
        return regionBOs;
    }
    
    @Test
    public void testGetCompanyCodeByRegion() throws Exception {
    	List<CompanyBO> companyBOs = createCompanyBOs();
       EasyMock.expect(creditSubmit.getCompanyBySegmentRegion(3, 1)).andReturn(companyBOs);
       EasyMock.replay(creditSubmit);
       
       creditRequestController.getCompanyCodeByRegion(3, 1);
       
       EasyMock.verify(creditSubmit);
       assertTrue(companyBOs.size() == 5);
    }

    private List<CompanyBO> createCompanyBOs() {
        List<CompanyBO> companyBOs = new ArrayList<CompanyBO>();
        CompanyBO company = new CompanyBO();
        company.setCompanyId(66);
        company.setCompanyCode("1213");
        company.setCompanyName("ESPN Affiliate Sales, Inc");
        companyBOs.add(company);
        
        company = new CompanyBO();
        company.setCompanyId(65);
        company.setCompanyCode("1212");
        company.setCompanyName("ESPN Adv Sales, Inc");
        companyBOs.add(company);
        
        company = new CompanyBO();
        company.setCompanyId(61);
        company.setCompanyCode("1274");
        company.setCompanyName("KGO Television, Inc");
        companyBOs.add(company);
        
        company = new CompanyBO();
        company.setCompanyId(1);
        company.setCompanyCode("1218");
        company.setCompanyName("ESPN Enterprises, Inc");
        companyBOs.add(company);
        
        company = new CompanyBO();
        company.setCompanyId(40);
        company.setCompanyCode("1504");
        company.setCompanyName("Am Brd Companies, Inc");
        companyBOs.add(company);
        
        return companyBOs;
    }
    
    @Test
    public void testGetBusinessAreaByCompany() throws Exception {
    	List<BusinessAreaBO> businessAreaBOs = createBusinessAreaBOs();
        EasyMock.expect(creditSubmit.getBABySegementRegionCompany(3, 1, 1212)).andReturn(businessAreaBOs);
        EasyMock.replay(creditSubmit);
        
        creditRequestController.getBusinessAreaByCompany(3, 1, 1212);
        
        EasyMock.verify(creditSubmit);
        assertTrue(businessAreaBOs.size() == 1);
    }

    private List<BusinessAreaBO> createBusinessAreaBOs() {
        List<BusinessAreaBO> businessAreaBOs = new ArrayList<BusinessAreaBO>();
        BusinessAreaBO businessArea = new BusinessAreaBO();
        businessArea.setBusinessAreaId(1);
        businessArea.setBusinessAreaCode("113");
        businessArea.setBusinessAreaName("ESPN Inc.");
        businessAreaBOs.add(businessArea);
        
        return businessAreaBOs;
    }
    
    @Test
    public void testGetBusinessGroup() throws Exception {
    	BusinessGroupBO businessGroupBO = new BusinessGroupBO();
    	businessGroupBO.setBusinessGroupId(1);
    	businessGroupBO.setBusinessGroupName("AdSales (No SetUps)");
    	
        EasyMock.expect(creditSubmit.getBusinessGroup(65, 1)).andReturn(businessGroupBO);
        EasyMock.replay(creditSubmit);
        
        creditRequestController.getBusinessGroup(65, 1);
        
        EasyMock.verify(creditSubmit);
        assertTrue(businessGroupBO != null);
    }

    @Test
    public void testGetCountryList() throws Exception {
    	List<CountryBO> countryBOs = createCountryBOs();
        EasyMock.expect(creditSubmit.getAllCountries()).andReturn(countryBOs);
        EasyMock.replay(creditSubmit);
        
        creditRequestController.getCountryList();
        
        EasyMock.verify(creditSubmit);
        assertTrue(countryBOs.size() == 7);
    }
    
    private List<CountryBO> createCountryBOs() {
        List<CountryBO> countryBOs = new ArrayList<CountryBO>();
        CountryBO country = new CountryBO();
        country.setCountryId(9);
        country.setCountryName("Australia");
        countryBOs.add(country);
        
        country = new CountryBO();
        country.setCountryId(62);
        country.setCountryName("Germany");
        countryBOs.add(country);
        
        country = new CountryBO();
        country.setCountryId(74);
        country.setCountryName("India");
        countryBOs.add(country);
        
        country = new CountryBO();
        country.setCountryId(107);
        country.setCountryName("Mexico");
        countryBOs.add(country);
        
        country = new CountryBO();
        country.setCountryId(119);
        country.setCountryName("New Zealand");
        countryBOs.add(country);
        
        country = new CountryBO();
        country.setCountryId(139);
        country.setCountryName("Russia");
        countryBOs.add(country);
        
        country = new CountryBO();
        country.setCountryId(180);
        country.setCountryName("US");
        countryBOs.add(country);
        return countryBOs;
    }

    @Test
    public void testGetCreditRequestSubmission() throws Exception {
        //throw new RuntimeException("not yet implemented");
    }

    @Test
    public void testGetXMLCreditRequestSubmission() throws Exception {
        //throw new RuntimeException("not yet implemented");
    }

}  