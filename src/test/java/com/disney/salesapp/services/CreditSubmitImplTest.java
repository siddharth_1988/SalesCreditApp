package com.disney.salesapp.services;

import static junit.framework.Assert.assertTrue;
import static org.easymock.EasyMock.createStrictMock;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.disney.salesapp.dao.BaccMapDao;
import com.disney.salesapp.dao.BusinessAreaDao;
import com.disney.salesapp.dao.BusinessGroupDao;
import com.disney.salesapp.dao.BusinessGroupMapDao;
import com.disney.salesapp.dao.CompanyDao;
import com.disney.salesapp.dao.CreditSubmissionDao;
import com.disney.salesapp.dao.RegionDao;
import com.disney.salesapp.dao.SegmentDao;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.BusinessGroup;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Country;
import com.disney.salesapp.model.entities.Region;
import com.disney.salesapp.model.entities.Segment;

public class CreditSubmitImplTest {
	private BaccMapDao baccMapDao = createStrictMock(BaccMapDao.class);

	private BusinessAreaDao businessAreaDao = createStrictMock(BusinessAreaDao.class);

	private BusinessGroupDao businessGroupDao = createStrictMock(BusinessGroupDao.class);

	private BusinessGroupMapDao businessGroupMapDao = createStrictMock(BusinessGroupMapDao.class);

	private CompanyDao companyDao = createStrictMock(CompanyDao.class);

	private CreditSubmissionDao creditSubmissionDao = createStrictMock(CreditSubmissionDao.class);

	private RegionDao regionDao = createStrictMock(RegionDao.class);

	private SegmentDao segmentDao = createStrictMock(SegmentDao.class);
	
	private CreditSubmitImpl creditSubmitImpl;

	@Before
	public void createCreditSubmitImpl() throws Exception {
		creditSubmitImpl = new CreditSubmitImpl();
		creditSubmitImpl.setBaccMapDao(baccMapDao);
		creditSubmitImpl.setBusinessAreaDao(businessAreaDao);
		creditSubmitImpl.setBusinessGroupDao(businessGroupDao);
		creditSubmitImpl.setBusinessGroupMapDao(businessGroupMapDao);
		creditSubmitImpl.setCompanyDao(companyDao);
		creditSubmitImpl.setCreditSubmissionDao(creditSubmissionDao);
		creditSubmitImpl.setRegionDao(regionDao);
		creditSubmitImpl.setSegmentDao(segmentDao);
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
    public void testGetAllSegments() {
		List<Segment> segments = createSegments();
        EasyMock.expect(segmentDao.findAllSegments()).andReturn(segments);
        EasyMock.replay(segmentDao);
        
        creditSubmitImpl.getAllSegments();
        
        EasyMock.verify(segmentDao);
        
        assertTrue(segments.size() == 4);
    }
	
	private List<Segment> createSegments() {
        List<Segment> segments = new ArrayList<Segment>();
        Segment segment = new Segment();
        segment.setSegmentId(1);
        segment.setSegmentName("CONSUMER PRODUCTS");
        segments.add(segment);
        
        segment = new Segment();
        segment.setSegmentId(2);
        segment.setSegmentName("DISNEY INTERACTIVE MEDIA GROUP");
        segments.add(segment);
        
        segment = new Segment();
        segment.setSegmentId(3);
        segment.setSegmentName("MEDIA NETWORKS");
        segments.add(segment);
        
        segment = new Segment();
        segment.setSegmentId(4);
        segment.setSegmentName("PARKS AND RESORTS");
        segments.add(segment);
        return segments;
    }
	
	@Test
    public void testGetRegionBySegment() throws Exception {
    	 List<Region> regions = createRegions();
        EasyMock.expect(baccMapDao.findRegionsBySegmentId(3)).andReturn(regions);
        EasyMock.replay(baccMapDao);
        
        creditSubmitImpl.getRegionBySegment(3);
        
        EasyMock.verify(baccMapDao);
        assertTrue(regions.size() == 5);
    }

    private List<Region> createRegions() {
        List<Region> regions = new ArrayList<Region>();
        Region region = new Region();
        region.setRegionId(1);
        region.setRegionName("North America");
        regions.add(region);
        
        region = new Region();
        region.setRegionId(2);
        region.setRegionName("Latin America");
        regions.add(region);
        
        region = new Region();
        region.setRegionId(3);
        region.setRegionName("Asia");
        regions.add(region);
        
        region = new Region();
        region.setRegionId(4);
        region.setRegionName("Europe");
        regions.add(region);
        
        region = new Region();
        region.setRegionId(5);
        region.setRegionName("Australia");
        regions.add(region);
        
        return regions;
    }
    
    @Test
    public void testGetCompanyBySegmentRegion() throws Exception {
    	List<Company> companys = createCompanys();
       EasyMock.expect(baccMapDao.findCompanyBySegmentRegion(3, 1)).andReturn(companys);
       EasyMock.replay(baccMapDao);
       
       creditSubmitImpl.getCompanyBySegmentRegion(3, 1);
       
       EasyMock.verify(baccMapDao);
       assertTrue(companys.size() == 5);
    }

    private List<Company> createCompanys() {
        List<Company> companys = new ArrayList<Company>();
        Company company = new Company();
        company.setCompanyId(66);
        company.setCompanyCode("1213");
        company.setCompanyName("ESPN Affiliate Sales, Inc");
        companys.add(company);
        
        company = new Company();
        company.setCompanyId(65);
        company.setCompanyCode("1212");
        company.setCompanyName("ESPN Adv Sales, Inc");
        companys.add(company);
        
        company = new Company();
        company.setCompanyId(61);
        company.setCompanyCode("1274");
        company.setCompanyName("KGO Television, Inc");
        companys.add(company);
        
        company = new Company();
        company.setCompanyId(1);
        company.setCompanyCode("1218");
        company.setCompanyName("ESPN Enterprises, Inc");
        companys.add(company);
        
        company = new Company();
        company.setCompanyId(40);
        company.setCompanyCode("1504");
        company.setCompanyName("Am Brd Companies, Inc");
        companys.add(company);
        
        return companys;
    }
    
    @Test
    public void testGetBABySegementRegionCompany() throws Exception {
    	List<BusinessArea> businessAreas = createBusinessAreas();
        EasyMock.expect(baccMapDao.findBABySegmentRegionCompany(3, 1, 1212)).andReturn(businessAreas);
        EasyMock.replay(baccMapDao);
        
        creditSubmitImpl.getBABySegementRegionCompany(3, 1, 1212);
        
        EasyMock.verify(baccMapDao);
        assertTrue(businessAreas.size() == 1);
    }

    private List<BusinessArea> createBusinessAreas() {
        List<BusinessArea> businessAreas = new ArrayList<BusinessArea>();
        BusinessArea businessArea = new BusinessArea();
        businessArea.setBusinessAreaId(1);
        businessArea.setBusinessAreaCode("113");
        businessArea.setBusinessAreaName("ESPN Inc.");
        businessAreas.add(businessArea);
        
        return businessAreas;
    }
    
    @Test
    public void testGetBusinessGroup() throws Exception {
    	BusinessGroup businessGroup = new BusinessGroup();
    	businessGroup.setBusinessGroupId(1);
    	businessGroup.setBusinessGroupName("AdSales (No SetUps)");
    	
        EasyMock.expect(businessGroupMapDao.getBusinessGroup(65, 1)).andReturn(businessGroup);
        EasyMock.replay(businessGroupMapDao);
        
        creditSubmitImpl.getBusinessGroup(65, 1);
        
        EasyMock.verify(businessGroupMapDao);
        assertTrue(businessGroup != null);
    }

    @Test
    public void testGetAllCountries() throws Exception {
    	List<Country> countrys = createCountrys();
        EasyMock.expect(segmentDao.findAllCountries()).andReturn(countrys);
        EasyMock.replay(segmentDao);
        
        creditSubmitImpl.getAllCountries();
        
        EasyMock.verify(segmentDao);
        assertTrue(countrys.size() == 7);
    }
    
    private List<Country> createCountrys() {
        List<Country> countrys = new ArrayList<Country>();
        Country country = new Country();
        country.setCountryId(9);
        country.setCountryName("Australia");
        countrys.add(country);
        
        country = new Country();
        country.setCountryId(62);
        country.setCountryName("Germany");
        countrys.add(country);
        
        country = new Country();
        country.setCountryId(74);
        country.setCountryName("India");
        countrys.add(country);
        
        country = new Country();
        country.setCountryId(107);
        country.setCountryName("Mexico");
        countrys.add(country);
        
        country = new Country();
        country.setCountryId(119);
        country.setCountryName("New Zealand");
        countrys.add(country);
        
        country = new Country();
        country.setCountryId(139);
        country.setCountryName("Russia");
        countrys.add(country);
        
        country = new Country();
        country.setCountryId(180);
        country.setCountryName("US");
        countrys.add(country);
        
        return countrys;
    }
}
