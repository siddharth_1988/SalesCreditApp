package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.disney.salesapp.application.constants.QueryConstants;
import com.disney.salesapp.model.entities.BusinessGroup;

@Repository("businessGroupMapDaoImpl")
public class BusinessGroupMapDaoImpl implements BusinessGroupMapDao {
    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public BusinessGroup getBusinessGroup(Integer companyId, Integer businessAreaId) {
        Query query = entityManager.createQuery(QueryConstants.GET_BUSINESS_GROUP_BY_COMP_ID_AND_BA_ID);
        query.setMaxResults(1);
        query.setParameter("companyId", companyId);
        query.setParameter("businessAreaId", businessAreaId);
        List<BusinessGroup> businessGroups = query.getResultList();
        return CollectionUtils.isEmpty(businessGroups ) ? null : businessGroups.get(0);
    }

}
