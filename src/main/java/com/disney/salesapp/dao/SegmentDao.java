package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.Country;
import com.disney.salesapp.model.entities.Segment;
import com.disney.salesapp.model.entities.State;

public interface SegmentDao {

	Segment findById(Integer segmentId);

	List<Segment> findAllSegments();

	Segment findBySegmentName(String segmentName);

	List<Country> findAllCountries();
	
	List<State> getAllStatesOfUsa();
	
	void deleteSegment(Segment segment);

	
	
}
