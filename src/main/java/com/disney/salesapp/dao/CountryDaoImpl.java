/**
 * 
 */
package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.disney.salesapp.application.constants.QueryConstants;
import com.disney.salesapp.model.entities.Country;

/**
 * @author GHV001
 *
 */
@Repository("countryDao")
public class CountryDaoImpl implements CountryDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public Country getCountryByName(String countryName) {
		Query query = entityManager.createQuery(QueryConstants.GET_COUNTRY_BY_NAME);
		query.setParameter("countryName", countryName);
		query.setMaxResults(1);
		List<Country> country =query.getResultList();
		return CollectionUtils.isEmpty(country) ? null : country.get(0);
	}

}
