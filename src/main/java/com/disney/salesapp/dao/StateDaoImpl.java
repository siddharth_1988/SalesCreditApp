/**
 * 
 */
package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.disney.salesapp.application.constants.QueryConstants;
import com.disney.salesapp.model.entities.State;

/**
 * @author GHV001
 *
 */
@Repository("stateDao")
public class StateDaoImpl implements StateDao{
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public State getStateByName(String stateName) {
		Query query = entityManager.createQuery(QueryConstants.GET_STATE_BY_NAME);
		query.setParameter("stateName", stateName);
		query.setMaxResults(1);
		List<State> state =query.getResultList();
		return CollectionUtils.isEmpty(state) ? null : state.get(0);
	}

}
