package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.Company;

@Repository("companyDao")
public class CompanyDaoImpl implements CompanyDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Company findById(Integer companyId) {
		return entityManager.find(Company.class, companyId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Company> findAllCompany() {
		Query query = entityManager.createQuery("From Company");
		return query.getResultList();
	}

	@Override
	public Company findByCompanyName(String companyName) {
		Query query = entityManager
				.createQuery("From Company c where upper(c.companyName)= :companyName");
		query.setParameter("companyName", companyName.toUpperCase());
		return query.getResultList().isEmpty() ? null
				:(Company) query.getSingleResult();

	}

	@Override
	public Company findByCompanyCode(String companyCode) {
		Query query = entityManager
				.createQuery("From Company c where c.companyCode= :companyCode");
		query.setParameter("companyCode", companyCode);
		return (Company) query.getSingleResult();

	}

	@Override
	public void saveCompany(Company company) {
		entityManager.merge(company);

	}
	
	@Override
	public Company findByCompany(String companyCode, String companyName) {
		Query query = entityManager
				.createQuery("Select c From Company c where c.companyCode= :companyCode and c.companyName= :companyName");
		query.setParameter("companyCode", companyCode);
		query.setParameter("companyName", companyName);
		return (Company) query.getSingleResult();
	}

}
