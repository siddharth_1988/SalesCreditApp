package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.BaccMap;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Region;
import com.disney.salesapp.model.entities.Segment;

@Repository("businesAreaDao")
public class BusinessAreaDaoImpl implements BusinessAreaDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public BusinessArea findById(Integer businessAreaId) {
		return entityManager.find(BusinessArea.class, businessAreaId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BusinessArea> findAllBusinessArea() {
		Query query = entityManager.createQuery("From BusinessArea");
		return query.getResultList();
	}

	@Override
	public BusinessArea findByBusinessAreaName(String businessAreaName) {
		Query query = entityManager
				.createQuery("From BusinessArea b where upper(b.businessAreaName)= :businessAreaName");
		query.setParameter("businessAreaName", businessAreaName.toUpperCase());
		return query.getResultList().isEmpty() ? null : (BusinessArea) query
				.getSingleResult();

	}

	@Override
	public BusinessArea findByBusinessAreaCode(String businessAreaCode) {
		Query query = entityManager
				.createQuery("From BusinessArea b where b.businessAreaCode= :businessAreaCode");
		query.setParameter("businessAreaCode", businessAreaCode);
		return (BusinessArea) query.getSingleResult();

	}

	@Override
	public void saveBusinessArea(BusinessArea businessArea) {
		entityManager.merge(businessArea);

	}

	@SuppressWarnings("unchecked")
	@Override
	public BaccMap getBaccMapId(Integer segmentId, Integer regionId,
			Integer companyId) {
		Query query = entityManager
				.createQuery("From BaccMap bm where bm.segment.segmentId = :segmentId and "
						+ "bm.region.regionId = :regionId and bm.company.companyId = :companyId and bm.businessArea.businessAreaId is null");
		query.setParameter("segmentId", segmentId);
		query.setParameter("regionId", regionId);
		query.setParameter("companyId", companyId);
		query.setMaxResults(1);
		List<BaccMap> baccMap = query.getResultList();
		return CollectionUtils.isEmpty(baccMap) ? null : baccMap.get(0);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BaccMap> getBaccMapData() {
		Query query = entityManager
				.createQuery("From BaccMap bm where bm.logicalDeleteInd is null or bm.logicalDeleteInd='N' ");
		return query.getResultList();
	}

	@Override
	public Integer saveSegment(Segment segment) {
		Segment segmentObj = entityManager.merge(segment);
		return segmentObj.getSegmentId();
	}

	@Override
	public Integer saveRegion(Region region) {
		Region regionObj = entityManager.merge(region);
		return regionObj.getRegionId();
	}

	@Override
	public Integer saveACompany(Company company) {
		Company companyObj = entityManager.merge(company);
		return companyObj.getCompanyId();
	}

	@Override
	public Integer saveABusinessArea(BusinessArea businessArea) {
		BusinessArea businessAreaObj = entityManager.merge(businessArea);
		return businessAreaObj.getBusinessAreaId();
	}

	@Override
	public void saveABaccMap(BaccMap baccMap) {
		entityManager.merge(baccMap);

	}

	@Override
	public void deleteBusinessArea(BusinessArea businessArea) {
		Query query = entityManager
				.createQuery("Delete From BusinessArea ba where ba.businessAreaId= :businessAreaId");
		query.setParameter("businessAreaId", businessArea.getBusinessAreaId());
		query.executeUpdate();
		// entityManager.remove(businessArea);

	}

}
