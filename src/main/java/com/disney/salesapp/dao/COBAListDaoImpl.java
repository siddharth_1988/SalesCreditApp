package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.COBAList;

@Repository("cobaListDao")
public class COBAListDaoImpl implements COBAListDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public COBAList findById(Integer cobaListId) {
		return entityManager.find(COBAList.class, cobaListId);
	}

	@Override
	public List<COBAList> findAllCOBAList() {
		Query query = entityManager.createQuery("From COBAList");
		return query.getResultList();
	}

	@Override
	public List<COBAList> findBySegmentId(Integer segmentId) {
		Query query = entityManager
				.createQuery("From COBAList where segment.segmentId=:segmentId");
		query.setParameter("segmentId", segmentId);
		return query.getResultList();
	}

}
