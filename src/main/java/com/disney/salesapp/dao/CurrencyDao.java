/**
 * 
 */
package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.Currency;

public interface CurrencyDao {
	
	Currency getCurrencyByCode(String currencyCode);

    List<Currency> getAllCurrencies();

}
