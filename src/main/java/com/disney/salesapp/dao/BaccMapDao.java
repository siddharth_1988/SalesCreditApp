package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.BaccMap;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Region;

public interface BaccMapDao {

    BaccMap findById(Integer baccMapId);

    List<BaccMap> findAllBaccMap();

    List<BaccMap> findByBusinessAreaId(Integer businessAreaId);

    List<BaccMap> findBySegmentId(Integer segmentId);

    List<Region> findRegionsBySegmentId(Integer segmentId);

    List<Company> findCompanyBySegmentRegion(Integer segmentId, Integer regionId);

    List<BusinessArea> findBABySegmentRegionCompany(Integer segmentId, Integer regionId, Integer companyId);
    
    void saveBaccMap(BaccMap baccMap);
    
    void deleteBaccMap(Integer baccMapId);
    
}
