package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.Region;

public interface RegionDao {

    Region findById(Integer regionId);

    List<Region> findAllRegions();

    Region findByRegionName(String regionName);

}
