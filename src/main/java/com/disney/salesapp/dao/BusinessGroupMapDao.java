package com.disney.salesapp.dao;

import com.disney.salesapp.model.entities.BusinessGroup;

public interface BusinessGroupMapDao {
    
    BusinessGroup getBusinessGroup(Integer companyId, Integer businessAreaId);
    
    

}
