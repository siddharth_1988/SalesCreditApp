/**
 * 
 */
package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.User;

/**
 * @author GHV001
 *
 */
@Repository("userDao")
public class UserDaoImpl implements UserDao{
	
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public User findById(Integer userID) {
		return entityManager.find(User.class, userID);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsers() {
		Query query = entityManager.createQuery("From User");
		return (List<User>)query.getResultList();
	}

	@Override
	public void saveUser(User user) {
		entityManager.persist(user);
		
	}

	@Override
	public void updatUser(User user) {
		entityManager.merge(user);
		
	}
	
	@Override
    public User findByPortalId(String portalId) {
        Query query = entityManager.createQuery("from User where portalId=:portalId");
        query.setParameter("portalId", portalId);
        List<User> users = query.getResultList();
        return CollectionUtils.isEmpty(users) ? null : users.get(0);
    }
    
    @Override
    public User findByUserName(String userName) {
        Query query = entityManager.createQuery("from User where userName=:userName");
        query.setParameter("userName", userName);
        List<User> users = query.getResultList();
        return CollectionUtils.isEmpty(users) ? null : users.get(0);
    }

}
