package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.Region;

@Repository("regionDao")
public class RegionDaoImpl implements RegionDao {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public Region findById(Integer regionId) {
        return entityManager.find(Region.class, regionId);
    }
    @Override
    public List<Region> findAllRegions() {
        Query query = entityManager.createQuery("From Region");
        return query.getResultList();
    }
    @Override
    public Region findByRegionName(String regionName) {
        Query query = entityManager.createQuery("From Region r where upper(r.regionName)= :regionName");
        query.setParameter("regionName", regionName.toUpperCase());
        return query.getResultList().isEmpty() ? null
				:(Region) query.getSingleResult();
        
    }

}
