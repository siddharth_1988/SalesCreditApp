package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.BaccMap;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Region;
import com.disney.salesapp.model.entities.Segment;

public interface BusinessAreaDao {

	BusinessArea findById(Integer businessAreaId);

	List<BusinessArea> findAllBusinessArea();

	BusinessArea findByBusinessAreaName(String businessAreaName);

	BusinessArea findByBusinessAreaCode(String businessAreaCode);

	void saveBusinessArea(BusinessArea businessArea);

	BaccMap getBaccMapId(Integer segmentId, Integer regionId, Integer companyId);

	List<BaccMap> getBaccMapData();

	Integer saveSegment(Segment segment);

	Integer saveRegion(Region region);

	Integer saveACompany(Company company);

	Integer saveABusinessArea(BusinessArea businessArea);

	void saveABaccMap(BaccMap baccMap);

	void deleteBusinessArea(BusinessArea businessArea);

}
