/**
 * 
 */
package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.User;

/**
 * @author GHV001
 *
 */
public interface UserDao {
	
	User findById(Integer userID);

    List<User> findAllUsers();
    
    void saveUser(User user);
    
    void updatUser(User user);

    User findByPortalId(String portalId);

    User findByUserName(String userName);

}
