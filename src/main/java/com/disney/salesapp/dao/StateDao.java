/**
 * 
 */
package com.disney.salesapp.dao;

import com.disney.salesapp.model.entities.State;

/**
 * @author GHV001
 *
 */
public interface StateDao {
	
	State getStateByName(String stateName);

}
