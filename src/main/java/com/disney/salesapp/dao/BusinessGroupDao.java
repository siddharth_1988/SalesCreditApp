package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.BusinessGroup;

public interface BusinessGroupDao {

    BusinessGroup findById(Integer businessGroupId);

    List<BusinessGroup> findAllBusinessGroup();

    BusinessGroup findByBusinessGroupName(String businessGroupName);

}
