package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.COBAList;

public interface COBAListDao {

    COBAList findById(Integer cobaListId);

    List<COBAList> findAllCOBAList();

    List<COBAList> findBySegmentId(Integer segmentId);
    
}
