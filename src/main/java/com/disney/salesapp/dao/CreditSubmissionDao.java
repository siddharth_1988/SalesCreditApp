package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.CreditRequest;
import com.disney.salesapp.model.entities.CreditSubmission;

public interface CreditSubmissionDao {
	
	List<BusinessArea> getAllBusinessArea();
	
	Integer saveCreditRequest(CreditRequest creditRequest);
	
	CreditSubmission saveCreditSubmission(CreditSubmission creditSubmission);
	
	CreditSubmission getCreditRequestDetails(String portalId);
	
	CreditSubmission getLastCreditRequestDetails(String portalId);

    /**
     * Gets the credit submission by portal id.
     *
     * @param userName the user name
     * @return the credit submission by portal id
     */
    List<CreditSubmission> getCreditSubmissionByPortalId(String userName);

	Integer saveUserCreditRequest(CreditRequest creditRequest);
}
