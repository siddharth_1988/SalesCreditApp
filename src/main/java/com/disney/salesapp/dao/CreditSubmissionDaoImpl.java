package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.disney.salesapp.application.constants.QueryConstants;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.CreditRequest;
import com.disney.salesapp.model.entities.CreditSubmission;

@Repository("creditSubmissionDao")
public class CreditSubmissionDaoImpl implements CreditSubmissionDao{
	
	@PersistenceContext
    private EntityManager entityManager;
	

	@SuppressWarnings("unchecked")
	@Override
	public List<BusinessArea> getAllBusinessArea() {
		
		return (List<BusinessArea>) entityManager.createQuery("from BusinessArea").getResultList();
	}


	@Override
	public Integer saveCreditRequest(CreditRequest creditRequest) {
		CreditRequest creditRequestObject = entityManager.merge(creditRequest);
		return creditRequestObject.getCreditRequestId();
	}
    
	
	@Override
	public Integer saveUserCreditRequest(CreditRequest creditRequest) {
		entityManager.merge(creditRequest);
		return 1;
	}
	

	@Override
	public CreditSubmission saveCreditSubmission(CreditSubmission creditSubmission) {
		return entityManager.merge(creditSubmission);
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public CreditSubmission getCreditRequestDetails(String portalId) {
		Query query = entityManager.createQuery(QueryConstants.GET_CREDIT_REQUEST_DETAILS_FOR_PARTIAL_SAVE);
		query.setParameter("portalId", portalId);
		query.setMaxResults(1);
        List<CreditSubmission> creditSubmission = query.getResultList();
        return CollectionUtils.isEmpty(creditSubmission ) ? null : creditSubmission.get(0);
	}
	
    @SuppressWarnings("unchecked")
	@Override
    public List<CreditSubmission> getCreditSubmissionByPortalId(String portalId) {
        Query query = entityManager.createQuery(QueryConstants.GET_CREDIT_REQUEST_DETAILS_FOR_LOGGED_IN_USER);
        query.setParameter("portalId", portalId);
        return query.getResultList();
    }


	@SuppressWarnings("unchecked")
	@Override
	public CreditSubmission getLastCreditRequestDetails(String portalId) {
		Query query = entityManager.createNativeQuery(QueryConstants.GET_LAST_CREDIT_REQUEST_DETAILS_FOR_LOGGED_IN_USER,CreditSubmission.class);
		query.setParameter("portalId", portalId);
		query.setMaxResults(1);
        List<CreditSubmission> creditSubmission = query.getResultList();
        return CollectionUtils.isEmpty(creditSubmission ) ? null : creditSubmission.get(0);
	}
}
