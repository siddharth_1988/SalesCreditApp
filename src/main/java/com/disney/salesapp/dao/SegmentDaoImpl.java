package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.Country;
import com.disney.salesapp.model.entities.Segment;
import com.disney.salesapp.model.entities.State;

@Repository("segmentDao")
public class SegmentDaoImpl implements SegmentDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Segment findById(Integer segmentId) {
		return entityManager.find(Segment.class, segmentId);
	}

	@Override
	public List<Segment> findAllSegments() {
		Query query = entityManager.createQuery("From Segment");
		return query.getResultList();
	}

	@Override
	public Segment findBySegmentName(String segmentName) {
		Query query = entityManager
				.createQuery("From Segment s where upper(s.segmentName)= :segmentName");
		query.setParameter("segmentName", segmentName.toUpperCase());
		return query.getResultList().isEmpty() ? null
				: (Segment) query.getSingleResult();

	}

	@Override
	public List<Country> findAllCountries() {
		Query query = entityManager.createQuery("From Country");
		return query.getResultList();
	}

	@Override
	public List<State> getAllStatesOfUsa() {
		Query query = entityManager.createQuery("From State");
		return query.getResultList();
	}

	@Override
	public void deleteSegment(Segment segment) {
		entityManager.detach(segment);

	}

}
