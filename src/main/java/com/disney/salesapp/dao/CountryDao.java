/**
 * 
 */
package com.disney.salesapp.dao;

import com.disney.salesapp.model.entities.Country;

/**
 * @author GHV001
 *
 */
public interface CountryDao {
	
	Country getCountryByName(String countryName);

}
