/**
 * 
 */
package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.disney.salesapp.application.constants.QueryConstants;
import com.disney.salesapp.model.entities.Currency;

@Repository("currencyDao")
public class CurrencyDaoImpl implements CurrencyDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public Currency getCurrencyByCode(String currencyCode) {
		Query query = entityManager.createQuery(QueryConstants.GET_CURRENCY_BY_CURRENCY_CODE);
		query.setParameter("currencyCode", currencyCode);
		query.setMaxResults(1);
		List<Currency> currencies =query.getResultList();
		return CollectionUtils.isEmpty(currencies) ? null : currencies.get(0);
	}
	
	@Override
    public List<Currency> getAllCurrencies() {
        Query query = entityManager.createQuery(QueryConstants.GET_ALL_CURRENCIES);
        return query.getResultList();
    }

}
