package com.disney.salesapp.dao;

import java.util.List;

import com.disney.salesapp.model.entities.Company;

public interface CompanyDao {

	Company findById(Integer companyId);

	List<Company> findAllCompany();

	Company findByCompanyName(String companyName);

	Company findByCompanyCode(String companyCode);
	
	Company findByCompany(String companyCode,String companyName);

	void saveCompany(Company company);

}
