package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.BusinessGroup;

@Repository("businesGroupDao")
public class BusinessGroupDaoImpl implements BusinessGroupDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public BusinessGroup findById(Integer businessGroupId) {
        return entityManager.find(BusinessGroup.class, businessGroupId);
    }

    @Override
    public List<BusinessGroup> findAllBusinessGroup() {
        Query query = entityManager.createQuery("From BusinessGroup");
        return query.getResultList();
    }

    @Override
    public BusinessGroup findByBusinessGroupName(String businessGroupName) {
        Query query = entityManager.createQuery("Select BusinessGroup From BusinessGroup where businessGroupName= :businessGroupName");
        query.setParameter("businessGroupName", businessGroupName);
        return (BusinessGroup) query.getSingleResult();

    }

}
