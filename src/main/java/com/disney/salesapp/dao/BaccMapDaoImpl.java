package com.disney.salesapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.disney.salesapp.model.entities.BaccMap;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Region;

@Repository("baccMapDao")
public class BaccMapDaoImpl implements BaccMapDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public BaccMap findById(Integer baccMapId) {
		return entityManager.find(BaccMap.class, baccMapId);
	}

	@Override
	public List<BaccMap> findAllBaccMap() {
		Query query = entityManager.createQuery("From BaccMap");
		return query.getResultList();
	}

	@Override
	public List<BaccMap> findByBusinessAreaId(Integer businessAreaId) {
		Query query = entityManager
				.createQuery("Select BaccMap From BaccMap where businessArea.businessAreaId= :businessAreaId");
		query.setParameter("businessAreaId", businessAreaId);
		return query.getResultList();

	}

	@Override
	public List<BaccMap> findBySegmentId(Integer segmentId) {
		Query query = entityManager
				.createQuery("Select BaccMap From BaccMap where segment.segmentId= :segmentId");
		query.setParameter("segmentId", segmentId);
		return query.getResultList();

	}

	@Override
	public List<Region> findRegionsBySegmentId(Integer segmentId) {
		Query query = entityManager
				.createQuery("Select region From BaccMap baccMap where baccMap.segment.segmentId= :segmentId");
		query.setParameter("segmentId", segmentId);
		return query.getResultList();

	}

	@Override
	public List<Company> findCompanyBySegmentRegion(Integer segmentId,
			Integer regionId) {
		Query query = entityManager
				.createQuery("Select company From BaccMap baccMap where baccMap.segment.segmentId= :segmentId and baccMap.region.regionId= :regionId");
		query.setParameter("segmentId", segmentId);
		query.setParameter("regionId", regionId);
		return query.getResultList();

	}

	@Override
	public List<BusinessArea> findBABySegmentRegionCompany(Integer segmentId,
			Integer regionId, Integer companyId) {
		Query query = entityManager
				.createQuery("Select businessArea From BaccMap baccMap where baccMap.segment.segmentId= :segmentId and baccMap.region.regionId= :regionId and baccMap.company.companyId= :companyId");
		query.setParameter("segmentId", segmentId);
		query.setParameter("regionId", regionId);
		query.setParameter("companyId", companyId);
		return query.getResultList();

	}

	@Override
	public void saveBaccMap(BaccMap baccMap) {
		entityManager.merge(baccMap);

	}

	@Override
	public void deleteBaccMap(Integer baccMapId) {
		Query query = entityManager
				.createNativeQuery("Update BACC_MAP bm set bm.LOGICAL_DEL_IN = 'Y' where bm.BACC_MAP_ID = :baccMapId");
		query.setParameter("baccMapId", baccMapId);
		query.executeUpdate();
		//entityManager.remove(baccMap);

	}

}
