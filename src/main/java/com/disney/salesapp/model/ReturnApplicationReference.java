/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="ReturnApplicationReference")
@XmlAccessorType(XmlAccessType.NONE)
public class ReturnApplicationReference {
	
	@XmlElement(name="ApplicationReference")
	private ApplicationReference applicationReference;

	/**
	 * @return the applicationReference
	 */
	public ApplicationReference getApplicationReference() {
		return applicationReference;
	}

	/**
	 * @param applicationReference the applicationReference to set
	 */
	public void setApplicationReference(ApplicationReference applicationReference) {
		this.applicationReference = applicationReference;
	}
	
	

}
