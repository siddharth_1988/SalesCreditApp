/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="BusinessInfo")
@XmlAccessorType(XmlAccessType.NONE)
public class BusinessInfo {
	
	@XmlElement(name="CustomerReference")
    private CustomerReference customerReference;
	
	@XmlElement(name="LegalName")
	private String legalName;
	
	@XmlElement(name="DbaName")
    private String dbaName;

	@XmlElement(name="TaxId")
    private String taxId;
	
	@XmlElement(name="DunsNo")
    private String dunsNo;

	@XmlElement(name="LegalEntityType")
    private String legalEntityType;
	
	@XmlElement(name="BusinessSince")
    private String businessSince;

	/**
	 * @return the legalName
	 */
	public String getLegalName() {
		return legalName;
	}

	/**
	 * @param legalName the legalName to set
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	/**
	 * @return the businessSince
	 */
	public String getBusinessSince() {
		return businessSince;
	}

	/**
	 * @param businessSince the businessSince to set
	 */
	public void setBusinessSince(String businessSince) {
		this.businessSince = businessSince;
	}

	/**
	 * @return the customerReference
	 */
	public CustomerReference getCustomerReference() {
		return customerReference;
	}

	/**
	 * @param customerReference the customerReference to set
	 */
	public void setCustomerReference(CustomerReference customerReference) {
		this.customerReference = customerReference;
	}

	/**
	 * @return the taxId
	 */
	public String getTaxId() {
		return taxId;
	}

	/**
	 * @param taxId the taxId to set
	 */
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	/**
	 * @return the dbaName
	 */
	public String getDbaName() {
		return dbaName;
	}

	/**
	 * @param dbaName the dbaName to set
	 */
	public void setDbaName(String dbaName) {
		this.dbaName = dbaName;
	}

	/**
	 * @return the legalEntityType
	 */
	public String getLegalEntityType() {
		return legalEntityType;
	}

	/**
	 * @param legalEntityType the legalEntityType to set
	 */
	public void setLegalEntityType(String legalEntityType) {
		this.legalEntityType = legalEntityType;
	}

	/**
	 * @return the dunsNo
	 */
	public String getDunsNo() {
		return dunsNo;
	}

	/**
	 * @param dunsNo the dunsNo to set
	 */
	public void setDunsNo(String dunsNo) {
		this.dunsNo = dunsNo;
	}

	
}
