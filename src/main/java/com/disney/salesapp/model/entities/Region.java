/**
 * 
 */
package com.disney.salesapp.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class Region.
 *
 * @author GHV001
 */
@Entity
@Table(name="REGION")
public class Region extends AbstractBaseEntity {
	
	/** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8255505087682480313L;

    /** The region id. */
    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "REGION_ID")
	private Integer regionId;
	
	/** The region name. */
	@Column(name = "REGION_NAME")
	private String regionName;
	
	/** The bacc maps. */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "region")
    private Set<BaccMap> baccMaps = new HashSet<BaccMap>();

    /**
     * Gets the region id.
     *
     * @return the region id
     */
    public Integer getRegionId() {
        return regionId;
    }

    /**
     * Sets the region id.
     *
     * @param regionId the new region id
     */
    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }


    /**
     * Gets the region name.
     *
     * @return the region name
     */
    public String getRegionName() {
        return regionName;
    }

    /**
     * Sets the region name.
     *
     * @param regionName the new region name
     */
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    /**
     * Gets the bacc maps.
     *
     * @return the bacc maps
     */
    public Set<BaccMap> getBaccMaps() {
        return baccMaps;
    }

    /**
     * Sets the bacc maps.
     *
     * @param baccMaps the new bacc maps
     */
    public void setBaccMaps(Set<BaccMap> baccMaps) {
        this.baccMaps = baccMaps;
    }
	
    
	
}
