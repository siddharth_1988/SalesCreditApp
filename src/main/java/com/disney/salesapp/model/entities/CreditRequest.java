package com.disney.salesapp.model.entities;

import java.io.File;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class CreditRequest.
 *
 * @author GHV001
 */
@Entity
@Table(name = "CREDIT_REQUEST")
public class CreditRequest extends AbstractBaseEntity {

	private static final long serialVersionUID = -7066514623209348165L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CREDIT_REQUEST_ID")
	private Integer creditRequestId;

	@Column(name = "PORTAL_ID")
	private String portalId;

	@Column(name = "SEGMENT")
	private Integer segment;

	@Column(name = "REGION")
	private Integer region;

	@Column(name = "COMPANY")
	private Integer company;

	@Column(name = "BUSINESS_AREA")
	private Integer businessArea;

	@Column(name = "REQUESTOR_NAME")
	private String requestorName;

	@Column(name = "REQUESTOR_EMAIL")
	private String requestorEmail;

	@Column(name = "REQUEST_NEEDED_BY_DATE")
	private Date requestNeededByDate;

	@Column(name = "REQUESTED_CREDIT_LIMIT")
	private Integer requestedCreditLimit;

	@Column(name = "TERM_START_DATE")
	private Date termStartDate;

	@Column(name = "TERM_END_DATE")
	private Date termEndDate;

	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = "BRAND_NAME")
	private String brandName;

	@Column(name = "COMPANY_WEB_ADDRESS")
	private String companyWebAddress;

	@Column(name = "COMPANY_STREET_ADDRESS")
	private String companyStreetAddress;

	@Column(name = "COMPANY_CITY")
	private String companyCity;

	@Column(name = "COMPANY_STATE")
	private String companyState;

	@Column(name = "COMPANY_ZIP")
	private String companyZip;

	@Column(name = "COMPANY_COUNTRY")
	private String companyCountry;

	@Column(name = "COMPANY_TELEPHONE")
	private Integer companyTelephone;

	@Column(name = "COMPANY_CONTACT_NAME")
	private String companyContactName;

	@Column(name = "CONTACT_TITLE")
	private String contactTitle;

	@Column(name = "COMPANY_CONTACT_EMAIL_ADDRESS")
	private String companyContactEmailAddress;

	@Column(name = "OKAY_TO_CONTACT")
	private boolean okayToContact;

	@Column(name = "AGENCY_NAME")
	private String agencyName;

	@Column(name = "AGENCY_WEB_ADDRESS")
	private String agencyWebAddress;

	@Column(name = "AGENCY_STREET_ADDRESS")
	private String agencyStreetAddress;

	@Column(name = "AGENCY_CITY")
	private String agencyCity;

	@Column(name = "AGENCY_STATE")
	private String agencyState;

	@Column(name = "AGENCY_ZIP")
	private String agencyZip;

	@Column(name = "AGENCY_COUNTRY")
	private String agencyCountry;

	@Column(name = "AGENCY_PHONE")
	private Integer agencyPhone;

	@Column(name = "AGENCY_CONTACT")
	private String agencyContact;

	@Column(name = "AGENCY_BILLING_EXPORT")
	private File aAgencyBillingExport;

	@Column(name = "ACCOUNT_EXECUTIVE")
	private String accountExecutive;

	@Column(name = "CONTACT_PHONE")
	private String cContactPhone;

	@Column(name = "CONTACT_EMAIL")
	private String cContactEmail;

	@Column(name = "CONTACT_FIRST_NAME")
	private String cContactFirstName;

	@Column(name = "CASH_ADVANCE")
	private boolean cashAdvance;

	@Column(name = "DIRECT")
	private boolean direct;

	@Column(name = "ATTACHMENT")
	private File attachment;

	@Column(name = "FOH_ID_ADV")
	private String fohIdAdv;

	@Column(name = "FOH_AGENCY")
	private String fohAgency;

	@Column(name = "AGENCY_COMMISSION_RATE")
	private String agencyCommissionRate;

	@Column(name = "COMMISSION_RATE_APPROVED")
	private boolean isCommissionRateApproved;

	@Column(name = "TRADE_BILL_PAY_INDICATOR")
	private boolean isTradeBillPayIndicator;

	@Column(name = "POLITICAL_BUY")
	private boolean isPoliticalBuy;

	@Column(name = "LOCAL_OR_NATIONAL")
	private boolean localOrNational;

	@Column(name = "CAP_NET")
	private boolean isCapNet;

	@Column(name = "WEB_BILLING")
	private boolean isWebBilling;

	@Column(name = "PRODUCT_CODE")
	private String productCode;

	@Column(name = "REVENUE_CODE1")
	private String revenueCode1;

	@Column(name = "REVENUE_CODE2")
	private String revenueCode2;

	@Column(name = "REVENUE_CODE3")
	private String revenueCode3;

	@Column(name = "PRIORITY_CODE")
	private String priorityCode;

	@Column(name = "DEMOGRAPHIC")
	private String demographic;

	@Column(name = "DEAL_ORDER_TYPE")
	private String dealOrderType;

	@Column(name = "NOTES")
	private String notes;

	@Column(name = "OUTSTANDING_AMOUNT")
	private String outstandingArAmount;
	
	@Column(name = "AE_LASTNAME")
	private String aeLastname;


	public String getPortalId() {
		return portalId;
	}

	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	/**
	 * @return the creditRequestId
	 */
	public Integer getCreditRequestId() {
		return creditRequestId;
	}

	/**
	 * @param creditRequestId
	 *            the creditRequestId to set
	 */
	public void setCreditRequestId(Integer creditRequestId) {
		this.creditRequestId = creditRequestId;
	}

	/**
	 * @return the requestorName
	 */
	public String getRequestorName() {
		return requestorName;
	}

	/**
	 * @param requestorName
	 *            the requestorName to set
	 */
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	/**
	 * @return the requestorEmail
	 */
	public String getRequestorEmail() {
		return requestorEmail;
	}

	/**
	 * @param requestorEmail
	 *            the requestorEmail to set
	 */
	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	/**
	 * @return the requestNeededByDate
	 */
	public Date getRequestNeededByDate() {
		return requestNeededByDate;
	}

	/**
	 * @param requestNeededByDate
	 *            the requestNeededByDate to set
	 */
	public void setRequestNeededByDate(Date requestNeededByDate) {
		this.requestNeededByDate = requestNeededByDate;
	}

	/**
	 * @return the requestedCreditLimit
	 */
	public Integer getRequestedCreditLimit() {
		return requestedCreditLimit;
	}

	/**
	 * @param requestedCreditLimit
	 *            the requestedCreditLimit to set
	 */
	public void setRequestedCreditLimit(Integer requestedCreditLimit) {
		this.requestedCreditLimit = requestedCreditLimit;
	}

	/**
	 * @return the termStartDate
	 */
	public Date getTermStartDate() {
		return termStartDate;
	}

	/**
	 * @param termStartDate
	 *            the termStartDate to set
	 */
	public void setTermStartDate(Date termStartDate) {
		this.termStartDate = termStartDate;
	}

	/**
	 * @return the termEndDate
	 */
	public Date getTermEndDate() {
		return termEndDate;
	}

	/**
	 * @param termEndDate
	 *            the termEndDate to set
	 */
	public void setTermEndDate(Date termEndDate) {
		this.termEndDate = termEndDate;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName
	 *            the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName
	 *            the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the companyWebAddress
	 */
	public String getCompanyWebAddress() {
		return companyWebAddress;
	}

	/**
	 * @param companyWebAddress
	 *            the companyWebAddress to set
	 */
	public void setCompanyWebAddress(String companyWebAddress) {
		this.companyWebAddress = companyWebAddress;
	}

	/**
	 * @return the companyStreetAddress
	 */
	public String getCompanyStreetAddress() {
		return companyStreetAddress;
	}

	/**
	 * @param companyStreetAddress
	 *            the companyStreetAddress to set
	 */
	public void setCompanyStreetAddress(String companyStreetAddress) {
		this.companyStreetAddress = companyStreetAddress;
	}

	/**
	 * @return the companyCity
	 */
	public String getCompanyCity() {
		return companyCity;
	}

	/**
	 * @param companyCity
	 *            the companyCity to set
	 */
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	/**
	 * @return the companyState
	 */
	public String getCompanyState() {
		return companyState;
	}

	/**
	 * @param companyState
	 *            the companyState to set
	 */
	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	/**
	 * @return the companyZip
	 */
	public String getCompanyZip() {
		return companyZip;
	}

	/**
	 * @param companyZip
	 *            the companyZip to set
	 */
	public void setCompanyZip(String companyZip) {
		this.companyZip = companyZip;
	}

	/**
	 * @return the companyCountry
	 */
	public String getCompanyCountry() {
		return companyCountry;
	}

	/**
	 * @param companyCountry
	 *            the companyCountry to set
	 */
	public void setCompanyCountry(String companyCountry) {
		this.companyCountry = companyCountry;
	}

	/**
	 * @return the companyTelephone
	 */
	public Integer getCompanyTelephone() {
		return companyTelephone;
	}

	/**
	 * @param companyTelephone
	 *            the companyTelephone to set
	 */
	public void setCompanyTelephone(Integer companyTelephone) {
		this.companyTelephone = companyTelephone;
	}

	/**
	 * @return the companyContactName
	 */
	public String getCompanyContactName() {
		return companyContactName;
	}

	/**
	 * @param companyContactName
	 *            the companyContactName to set
	 */
	public void setCompanyContactName(String companyContactName) {
		this.companyContactName = companyContactName;
	}

	/**
	 * @return the contactTitle
	 */
	public String getContactTitle() {
		return contactTitle;
	}

	/**
	 * @param contactTitle
	 *            the contactTitle to set
	 */
	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	/**
	 * @return the companyContactEmailAddress
	 */
	public String getCompanyContactEmailAddress() {
		return companyContactEmailAddress;
	}

	/**
	 * @param companyContactEmailAddress
	 *            the companyContactEmailAddress to set
	 */
	public void setCompanyContactEmailAddress(String companyContactEmailAddress) {
		this.companyContactEmailAddress = companyContactEmailAddress;
	}

	/**
	 * @return the okayToContact
	 */
	public boolean isOkayToContact() {
		return okayToContact;
	}

	/**
	 * @param okayToContact
	 *            the okayToContact to set
	 */
	public void setOkayToContact(boolean okayToContact) {
		this.okayToContact = okayToContact;
	}

	/**
	 * @return the agencyName
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * @param agencyName
	 *            the agencyName to set
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * @return the agencyWebAddress
	 */
	public String getAgencyWebAddress() {
		return agencyWebAddress;
	}

	/**
	 * @param agencyWebAddress
	 *            the agencyWebAddress to set
	 */
	public void setAgencyWebAddress(String agencyWebAddress) {
		this.agencyWebAddress = agencyWebAddress;
	}

	/**
	 * @return the agencyStreetAddress
	 */
	public String getAgencyStreetAddress() {
		return agencyStreetAddress;
	}

	/**
	 * @param agencyStreetAddress
	 *            the agencyStreetAddress to set
	 */
	public void setAgencyStreetAddress(String agencyStreetAddress) {
		this.agencyStreetAddress = agencyStreetAddress;
	}

	/**
	 * @return the agencyCity
	 */
	public String getAgencyCity() {
		return agencyCity;
	}

	/**
	 * @param agencyCity
	 *            the agencyCity to set
	 */
	public void setAgencyCity(String agencyCity) {
		this.agencyCity = agencyCity;
	}

	/**
	 * @return the agencyState
	 */
	public String getAgencyState() {
		return agencyState;
	}

	/**
	 * @param agencyState
	 *            the agencyState to set
	 */
	public void setAgencyState(String agencyState) {
		this.agencyState = agencyState;
	}

	/**
	 * @return the agencyZip
	 */
	public String getAgencyZip() {
		return agencyZip;
	}

	/**
	 * @param agencyZip
	 *            the agencyZip to set
	 */
	public void setAgencyZip(String agencyZip) {
		this.agencyZip = agencyZip;
	}

	/**
	 * @return the agencyCountry
	 */
	public String getAgencyCountry() {
		return agencyCountry;
	}

	/**
	 * @param agencyCountry
	 *            the agencyCountry to set
	 */
	public void setAgencyCountry(String agencyCountry) {
		this.agencyCountry = agencyCountry;
	}

	/**
	 * @return the agencyPhone
	 */
	public Integer getAgencyPhone() {
		return agencyPhone;
	}

	/**
	 * @param agencyPhone
	 *            the agencyPhone to set
	 */
	public void setAgencyPhone(Integer agencyPhone) {
		this.agencyPhone = agencyPhone;
	}

	/**
	 * @return the agencyContact
	 */
	public String getAgencyContact() {
		return agencyContact;
	}

	/**
	 * @param agencyContact
	 *            the agencyContact to set
	 */
	public void setAgencyContact(String agencyContact) {
		this.agencyContact = agencyContact;
	}

	/**
	 * @return the aAgencyBillingExport
	 */
	public File getaAgencyBillingExport() {
		return aAgencyBillingExport;
	}

	/**
	 * @param aAgencyBillingExport
	 *            the aAgencyBillingExport to set
	 */
	public void setaAgencyBillingExport(File aAgencyBillingExport) {
		this.aAgencyBillingExport = aAgencyBillingExport;
	}

	/**
	 * @return the accountExecutive
	 */
	public String getAccountExecutive() {
		return accountExecutive;
	}

	/**
	 * @param accountExecutive
	 *            the accountExecutive to set
	 */
	public void setAccountExecutive(String accountExecutive) {
		this.accountExecutive = accountExecutive;
	}

	/**
	 * @return the cContactPhone
	 */
	public String getcContactPhone() {
		return cContactPhone;
	}

	/**
	 * @param cContactPhone
	 *            the cContactPhone to set
	 */
	public void setcContactPhone(String cContactPhone) {
		this.cContactPhone = cContactPhone;
	}

	/**
	 * @return the cContactEmail
	 */
	public String getcContactEmail() {
		return cContactEmail;
	}

	/**
	 * @param cContactEmail
	 *            the cContactEmail to set
	 */
	public void setcContactEmail(String cContactEmail) {
		this.cContactEmail = cContactEmail;
	}

	/**
	 * @return the cContactFirstName
	 */
	public String getcContactFirstName() {
		return cContactFirstName;
	}

	/**
	 * @param cContactFirstName
	 *            the cContactFirstName to set
	 */
	public void setcContactFirstName(String cContactFirstName) {
		this.cContactFirstName = cContactFirstName;
	}

	/**
	 * @return the cashAdvance
	 */
	public boolean isCashAdvance() {
		return cashAdvance;
	}

	/**
	 * @param cashAdvance
	 *            the cashAdvance to set
	 */
	public void setCashAdvance(boolean cashAdvance) {
		this.cashAdvance = cashAdvance;
	}

	/**
	 * @return the direct
	 */
	public boolean isDirect() {
		return direct;
	}

	/**
	 * @param direct
	 *            the direct to set
	 */
	public void setDirect(boolean direct) {
		this.direct = direct;
	}

	/**
	 * @return the attachment
	 */
	public File getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment
	 *            the attachment to set
	 */
	public void setAttachment(File attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the fohIdAdv
	 */
	public String getFohIdAdv() {
		return fohIdAdv;
	}

	/**
	 * @param fohIdAdv
	 *            the fohIdAdv to set
	 */
	public void setFohIdAdv(String fohIdAdv) {
		this.fohIdAdv = fohIdAdv;
	}

	/**
	 * @return the fohAgency
	 */
	public String getFohAgency() {
		return fohAgency;
	}

	/**
	 * @param fohAgency
	 *            the fohAgency to set
	 */
	public void setFohAgency(String fohAgency) {
		this.fohAgency = fohAgency;
	}

	/**
	 * @return the agencyCommissionRate
	 */
	public String getAgencyCommissionRate() {
		return agencyCommissionRate;
	}

	/**
	 * @param agencyCommissionRate
	 *            the agencyCommissionRate to set
	 */
	public void setAgencyCommissionRate(String agencyCommissionRate) {
		this.agencyCommissionRate = agencyCommissionRate;
	}

	/**
	 * @return the isCommissionRateApproved
	 */
	public boolean isCommissionRateApproved() {
		return isCommissionRateApproved;
	}

	/**
	 * @param isCommissionRateApproved
	 *            the isCommissionRateApproved to set
	 */
	public void setCommissionRateApproved(boolean isCommissionRateApproved) {
		this.isCommissionRateApproved = isCommissionRateApproved;
	}

	/**
	 * @return the isTradeBillPayIndicator
	 */
	public boolean isTradeBillPayIndicator() {
		return isTradeBillPayIndicator;
	}

	/**
	 * @param isTradeBillPayIndicator
	 *            the isTradeBillPayIndicator to set
	 */
	public void setTradeBillPayIndicator(boolean isTradeBillPayIndicator) {
		this.isTradeBillPayIndicator = isTradeBillPayIndicator;
	}

	/**
	 * @return the isPoliticalBuy
	 */
	public boolean isPoliticalBuy() {
		return isPoliticalBuy;
	}

	/**
	 * @param isPoliticalBuy
	 *            the isPoliticalBuy to set
	 */
	public void setPoliticalBuy(boolean isPoliticalBuy) {
		this.isPoliticalBuy = isPoliticalBuy;
	}

	/**
	 * @return the localOrNational
	 */
	public boolean isLocalOrNational() {
		return localOrNational;
	}

	/**
	 * @param localOrNational
	 *            the localOrNational to set
	 */
	public void setLocalOrNational(boolean localOrNational) {
		this.localOrNational = localOrNational;
	}

	/**
	 * @return the isCapNet
	 */
	public boolean isCapNet() {
		return isCapNet;
	}

	/**
	 * @param isCapNet
	 *            the isCapNet to set
	 */
	public void setCapNet(boolean isCapNet) {
		this.isCapNet = isCapNet;
	}

	/**
	 * @return the isWebBilling
	 */
	public boolean isWebBilling() {
		return isWebBilling;
	}

	/**
	 * @param isWebBilling
	 *            the isWebBilling to set
	 */
	public void setWebBilling(boolean isWebBilling) {
		this.isWebBilling = isWebBilling;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode
	 *            the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the revenueCode1
	 */
	public String getRevenueCode1() {
		return revenueCode1;
	}

	/**
	 * @param revenueCode1
	 *            the revenueCode1 to set
	 */
	public void setRevenueCode1(String revenueCode1) {
		this.revenueCode1 = revenueCode1;
	}

	/**
	 * @return the revenueCode2
	 */
	public String getRevenueCode2() {
		return revenueCode2;
	}

	/**
	 * @param revenueCode2
	 *            the revenueCode2 to set
	 */
	public void setRevenueCode2(String revenueCode2) {
		this.revenueCode2 = revenueCode2;
	}

	/**
	 * @return the revenueCode3
	 */
	public String getRevenueCode3() {
		return revenueCode3;
	}

	/**
	 * @param revenueCode3
	 *            the revenueCode3 to set
	 */
	public void setRevenueCode3(String revenueCode3) {
		this.revenueCode3 = revenueCode3;
	}

	/**
	 * @return the priorityCode
	 */
	public String getPriorityCode() {
		return priorityCode;
	}

	/**
	 * @param priorityCode
	 *            the priorityCode to set
	 */
	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	/**
	 * @return the demographic
	 */
	public String getDemographic() {
		return demographic;
	}

	/**
	 * @param demographic
	 *            the demographic to set
	 */
	public void setDemographic(String demographic) {
		this.demographic = demographic;
	}

	/**
	 * @return the dealOrderType
	 */
	public String getDealOrderType() {
		return dealOrderType;
	}

	/**
	 * @param dealOrderType
	 *            the dealOrderType to set
	 */
	public void setDealOrderType(String dealOrderType) {
		this.dealOrderType = dealOrderType;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the outstandingArAmount
	 */
	public String getOutstandingArAmount() {
		return outstandingArAmount;
	}

	/**
	 * @param outstandingArAmount
	 *            the outstandingArAmount to set
	 */
	public void setOutstandingArAmount(String outstandingArAmount) {
		this.outstandingArAmount = outstandingArAmount;
	}

	/**
	 * @return the segment
	 */
	public Integer getSegment() {
		return segment;
	}

	/**
	 * @param segment
	 *            the segment to set
	 */
	public void setSegment(Integer segment) {
		this.segment = segment;
	}

	/**
	 * @return the region
	 */
	public Integer getRegion() {
		return region;
	}

	/**
	 * @param region
	 *            the region to set
	 */
	public void setRegion(Integer region) {
		this.region = region;
	}

	/**
	 * @return the company
	 */
	public Integer getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(Integer company) {
		this.company = company;
	}

	/**
	 * @return the businessArea
	 */
	public Integer getBusinessArea() {
		return businessArea;
	}

	/**
	 * @param businessArea
	 *            the businessArea to set
	 */
	public void setBusinessArea(Integer businessArea) {
		this.businessArea = businessArea;
	}
	
	public String getAeLastname() {
		return aeLastname;
	}

	public void setAeLastname(String aeLastname) {
		this.aeLastname = aeLastname;
	}

}
