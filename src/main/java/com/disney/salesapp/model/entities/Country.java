package com.disney.salesapp.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Country.
 *
 * @author BARAA034
 */
@Entity
@Table(name = "COUNTRY")
public class Country extends AbstractBaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2332269773524867082L;

	/** The country id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COUNTRY_ID")
	private Integer countryId;

	/** The country name. */
	@Column(name = "COUNTRY_NAME")
	private String countryName;

	public Integer getCountryId() {
		return countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

}
