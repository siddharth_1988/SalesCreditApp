package com.disney.salesapp.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class State.
 *
 * @author BARAA034
 */
@Entity
@Table(name = "STATE")
public class State extends AbstractBaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4667962079415658700L;

	/** The state id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STATE_ID")
	private Integer stateId;

	/** The country id. */
	@Column(name = "COUNTRY_ID")
	private Integer countryId;

	/** The state name. */
	@Column(name = "STATE_NAME")
	private String stateName;

	public Integer getStateId() {
		return stateId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}
