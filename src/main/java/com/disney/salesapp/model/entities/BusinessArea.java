/**
 * 
 */
package com.disney.salesapp.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class BusinessArea.
 *
 * @author GHV001
 */
@Entity
@Table(name="BUSINESS_AREA")
public class BusinessArea extends AbstractBaseEntity {
	
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5099943249119313927L;

    /** The business area id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "BUSINESS_AREA_ID")
	private Integer businessAreaId;
	
	/** The business area code. */
	@Column(name = "BUSINESS_AREA_CODE")
	private String businessAreaCode;
	
	/** The business area name. */
	@Column(name = "BUSINESS_AREA_NAME")
	private String businessAreaName;
	
	/** The bacc maps. */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "businessArea")
    private Set<BaccMap> baccMaps = new HashSet<BaccMap>();
	
	/** The business group maps. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "businessArea")
    private Set<BusinessGroupMap> businessGroupMaps = new HashSet<BusinessGroupMap>();

    /**
     * Gets the business area id.
     *
     * @return the business area id
     */
    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    /**
     * Sets the business area id.
     *
     * @param businessAreaId the new business area id
     */
    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    /**
     * Gets the business area code.
     *
     * @return the business area code
     */
    public String getBusinessAreaCode() {
        return businessAreaCode;
    }

    /**
     * Sets the business area code.
     *
     * @param businessAreaCode the new business area code
     */
    public void setBusinessAreaCode(String businessAreaCode) {
        this.businessAreaCode = businessAreaCode;
    }

    /**
     * Gets the business area name.
     *
     * @return the business area name
     */
    public String getBusinessAreaName() {
        return businessAreaName;
    }

    /**
     * Sets the business area name.
     *
     * @param businessAreaName the new business area name
     */
    public void setBusinessAreaName(String businessAreaName) {
        this.businessAreaName = businessAreaName;
    }

    /**
     * Gets the bacc maps.
     *
     * @return the bacc maps
     */
    public Set<BaccMap> getBaccMaps() {
        return baccMaps;
    }

    /**
     * Sets the bacc maps.
     *
     * @param baccMaps the new bacc maps
     */
    public void setBaccMaps(Set<BaccMap> baccMaps) {
        this.baccMaps = baccMaps;
    }

    /**
     * Gets the business group maps.
     *
     * @return the business group maps
     */
    public Set<BusinessGroupMap> getBusinessGroupMaps() {
        return businessGroupMaps;
    }

    /**
     * Sets the business group maps.
     *
     * @param businessGroupMaps the new business group maps
     */
    public void setBusinessGroupMaps(Set<BusinessGroupMap> businessGroupMaps) {
        this.businessGroupMaps = businessGroupMaps;
    }
	
	
		
}
