/**
 * 
 */
package com.disney.salesapp.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author GHV001
 *
 */
@Entity
@Table(name="CREDIT_SUBMISSION")
public class CreditSubmission  extends AbstractBaseEntity{

	private static final long serialVersionUID = -9201701741249798675L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CREDIT_SUBMISSION_ID")
	private Integer creditSubmissionId;

	@Column(name = "PORTAL_ID")
	private String portalId;

	@OneToOne(targetEntity = CreditRequest.class)
	@JoinColumn(name = "CREDIT_REQUEST_ID")
	private CreditRequest creditRequest;
	
	@Column(name = "APPLICATION_REFERENCE_NUMBER")
	private String applicationReferenceNumber;
	
	@Column(name = "STATUS")
	private String status;

	@Column(name = "PARTIAL_SAVE")
	private String partialSave;
	
	@Column(name = "REQUEST_RAISED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestRaisedDate;
	
	/**
	 * @return the creditSubmissionId
	 */
	public Integer getCreditSubmissionId() {
		return creditSubmissionId;
	}

	/**
	 * @param creditSubmissionId the creditSubmissionId to set
	 */
	public void setCreditSubmissionId(Integer creditSubmissionId) {
		this.creditSubmissionId = creditSubmissionId;
	}

	/**
	 * @return the portalId
	 */
	public String getPortalId() {
		return portalId;
	}

	/**
	 * @param portalId the portalId to set
	 */
	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	/**
	 * @return the creditRequest
	 */
	public CreditRequest getCreditRequest() {
		return creditRequest;
	}

	/**
	 * @param creditRequest the creditRequest to set
	 */
	public void setCreditRequest(CreditRequest creditRequest) {
		this.creditRequest = creditRequest;
	}

	/**
	 * @return the applicationReferenceNumber
	 */
	public String getApplicationReferenceNumber() {
		return applicationReferenceNumber;
	}

	/**
	 * @param applicationReferenceNumber the applicationReferenceNumber to set
	 */
	public void setApplicationReferenceNumber(String applicationReferenceNumber) {
		this.applicationReferenceNumber = applicationReferenceNumber;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the partialSave
	 */
	public String getPartialSave() {
		return partialSave;
	}

	/**
	 * @param partialSave the partialSave to set
	 */
	public void setPartialSave(String partialSave) {
		this.partialSave = partialSave;
	}

    /**
     * Gets the request raised date.
     *
     * @return the request raised date
     */
    public Date getRequestRaisedDate() {
        return requestRaisedDate;
    }

    /**
     * Sets the request raised date.
     *
     * @param requestRaisedDate the new request raised date
     */
    public void setRequestRaisedDate(Date requestRaisedDate) {
        this.requestRaisedDate = requestRaisedDate;
    }
	
	

}
