package com.disney.salesapp.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class BusinessGroup.
 */
@Entity
@Table(name="BUSINESS_GROUP")
public class BusinessGroup extends AbstractBaseEntity implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8210376976131307562L;

    /** The business group id. */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "BUSINESS_GROUP_ID")
    private Integer businessGroupId;
    
    /** The business group name. */
    @Column(name = "BUSINESS_GROUP_NAME")
    private String businessGroupName;
    
    /** The business group maps. */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "businessGroup")
    private Set<BusinessGroupMap> businessGroupMaps = new HashSet<BusinessGroupMap>();

    /**
     * Gets the business group id.
     *
     * @return the business group id
     */
    public Integer getBusinessGroupId() {
        return businessGroupId;
    }

    /**
     * Sets the business group id.
     *
     * @param businessGroupId the new business group id
     */
    public void setBusinessGroupId(Integer businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    /**
     * Gets the business group name.
     *
     * @return the business group name
     */
    public String getBusinessGroupName() {
        return businessGroupName;
    }

    /**
     * Sets the business group name.
     *
     * @param businessGroupName the new business group name
     */
    public void setBusinessGroupName(String businessGroupName) {
        this.businessGroupName = businessGroupName;
    }

    /**
     * Gets the business group maps.
     *
     * @return the business group maps
     */
    public Set<BusinessGroupMap> getBusinessGroupMaps() {
        return businessGroupMaps;
    }

    /**
     * Sets the business group maps.
     *
     * @param businessGroupMaps the new business group maps
     */
    public void setBusinessGroupMaps(Set<BusinessGroupMap> businessGroupMaps) {
        this.businessGroupMaps = businessGroupMaps;
    }
    
    
    
}
