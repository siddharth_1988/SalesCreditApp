/**
 * 
 */
package com.disney.salesapp.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class Company.
 *
 * @author GHV001
 */
@Entity
@Table(name = "COMPANY")
public class Company extends AbstractBaseEntity {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4958923244733549377L;

	/** The company id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COMPANY_ID")
	private Integer companyId;

	/** The company code. */
	@Column(name = "COMPANY_CODE")
	private String companyCode;

	/** The company name. */
	@Column(name = "COMPANY_NAME")
	private String companyName;

	/** The bacc maps. */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "company")
	private Set<BaccMap> baccMaps = new HashSet<BaccMap>();

	/** The business group maps. */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "company")
	private Set<BusinessGroupMap> businessGroupMaps = new HashSet<BusinessGroupMap>();

	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the company id.
	 *
	 * @param companyId
	 *            the new company id
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/**
	 * Gets the company code.
	 *
	 * @return the company code
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * Sets the company code.
	 *
	 * @param companyCode
	 *            the new company code
	 */
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the company name.
	 *
	 * @param companyName
	 *            the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * Gets the bacc maps.
	 *
	 * @return the bacc maps
	 */
	public Set<BaccMap> getBaccMaps() {
		return baccMaps;
	}

	/**
	 * Sets the bacc maps.
	 *
	 * @param baccMaps
	 *            the new bacc maps
	 */
	public void setBaccMaps(Set<BaccMap> baccMaps) {
		this.baccMaps = baccMaps;
	}

	/**
	 * Gets the business group maps.
	 *
	 * @return the business group maps
	 */
	public Set<BusinessGroupMap> getBusinessGroupMaps() {
		return businessGroupMaps;
	}

	/**
	 * Sets the business group maps.
	 *
	 * @param businessGroupMaps
	 *            the new business group maps
	 */
	public void setBusinessGroupMaps(Set<BusinessGroupMap> businessGroupMaps) {
		this.businessGroupMaps = businessGroupMaps;
	}

}
