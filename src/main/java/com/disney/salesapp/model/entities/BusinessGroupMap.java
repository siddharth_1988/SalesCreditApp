package com.disney.salesapp.model.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class BusinessGroupMap.
 */
@Entity
@Table(name="BUSINESS_GROUP_MAP")
public class BusinessGroupMap extends AbstractBaseEntity implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6421333962197571301L;

    /** The business group map pk. */
    @EmbeddedId
    BusinessGroupMapPK businessGroupMapPK;
    
    
    /** The business area. */
    @MapsId("businessAreaId")
    @ManyToOne(targetEntity = BusinessArea.class)
    @JoinColumn(name = "BUSINESS_AREA_ID")
    private BusinessArea businessArea;
    
    /** The company. */
    @MapsId("companyId")
    @ManyToOne(targetEntity = Company.class)
    @JoinColumn(name = "COMPANY_ID")
    private Company company;
    
    /** The business group. */
    @MapsId("businessGroupId")
    @ManyToOne(targetEntity = BusinessGroup.class)
    @JoinColumn(name = "BUSINESS_GROUP_ID")
    private BusinessGroup businessGroup;

    /**
     * Gets the business group map pk.
     *
     * @return the business group map pk
     */
    public BusinessGroupMapPK getBusinessGroupMapPK() {
        return businessGroupMapPK;
    }

    /**
     * Sets the business group map pk.
     *
     * @param businessGroupMapPK the new business group map pk
     */
    public void setBusinessGroupMapPK(BusinessGroupMapPK businessGroupMapPK) {
        this.businessGroupMapPK = businessGroupMapPK;
    }

    /**
     * Gets the business area.
     *
     * @return the business area
     */
    public BusinessArea getBusinessArea() {
        return businessArea;
    }

    /**
     * Sets the business area.
     *
     * @param businessArea the new business area
     */
    public void setBusinessArea(BusinessArea businessArea) {
        this.businessArea = businessArea;
    }

    /**
     * Gets the company.
     *
     * @return the company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * Sets the company.
     *
     * @param company the new company
     */
    public void setCompany(Company company) {
        this.company = company;
    }

    /**
     * Gets the business group.
     *
     * @return the business group
     */
    public BusinessGroup getBusinessGroup() {
        return businessGroup;
    }

    /**
     * Sets the business group.
     *
     * @param businessGroup the new business group
     */
    public void setBusinessGroup(BusinessGroup businessGroup) {
        this.businessGroup = businessGroup;
    }
    

}
