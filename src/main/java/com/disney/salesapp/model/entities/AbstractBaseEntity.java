package com.disney.salesapp.model.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.disney.salesapp.application.constants.QueryConstants;

/**
 * The Class AbstractBaseEntity.
 */
@MappedSuperclass
public abstract class AbstractBaseEntity implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2616572009033799257L;

    /** The created. */
    @Column(name = "CREATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;
    
    /** The created by. */
    @Column(name = "CREATED_BY")
    private String createdBy;
    
    /** The updated. */
    @Column(name = "UPDATED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;
    
    /** The updated by. */
    @Column(name = "UPDATED_BY")
    private String updatedBy;
    
    /** The logical delete ind. */
    @Column(name = "LOGICAL_DEL_IN")
    private String logicalDeleteInd;

    /**
     * Gets the created.
     *
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * Sets the created.
     *
     * @param created the new created
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * Gets the created by.
     *
     * @return the created by
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets the created by.
     *
     * @param createdBy the new created by
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Gets the updated.
     *
     * @return the updated
     */
    public Date getUpdated() {
        return updated;
    }

    /**
     * Sets the updated.
     *
     * @param updated the new updated
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    /**
     * Gets the updated by.
     *
     * @return the updated by
     */
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the updated by.
     *
     * @param updatedBy the new updated by
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Gets the logical delete ind.
     *
     * @return the logical delete ind
     */
    public String getLogicalDeleteInd() {
        return logicalDeleteInd;
    }

    /**
     * Sets the logical delete ind.
     *
     * @param logicalDeleteInd the new logical delete ind
     */
    public void setLogicalDeleteInd(String logicalDeleteInd) {
        this.logicalDeleteInd = logicalDeleteInd;
    }

    /**
     * Executed before making an update to the entity
     */
    @PreUpdate
    void beforeUpdate() {
        final String userAuthId = determineUserAuthId();
        populateCommonFields(userAuthId);
    }
    
    /**
     * Executed before persisting the entity
     */
    @PrePersist
    void beforeCreate() {       
        final String userAuthId = determineUserAuthId();

        String createdId = this.getCreatedBy();
        if (createdId == null || createdId.equals(QueryConstants.EMPTY_STRING)) { 
            createdId = userAuthId;
        }

        this.setCreatedBy(createdId);
        this.setCreated(new Date());
        populateCommonFields(userAuthId);
    }

    /**
     * Method to populate common fields set for both updates and creates.
     * 
     * @param modifiedId - the update user ID
     */
    private void populateCommonFields(final String modifiedId) {
        this.setUpdatedBy(modifiedId);
//        this.setVersion(new Date());
        this.setUpdated(new Date());

        String logicalDelete = QueryConstants.NO_INDICATOR;
        if (this.getLogicalDeleteInd() != null
                && this.getLogicalDeleteInd().equalsIgnoreCase(
                        QueryConstants.YES_INDICATOR)) {
            logicalDelete = QueryConstants.YES_INDICATOR;
        }
        this.setLogicalDeleteInd(logicalDelete);
    }

    /**
     * Determines the authorized user in context
     * 
     * @return USER_AUTH_ID session variable, or if that does not exist, 'app'
     */
    private String determineUserAuthId() {
//        String userAuthId = "app";
//        final String sytemOriginId = MetadataContext
//                .getMetadata(MetadataType.ORIGIN_SYSTEM_ID);
//        if(!NullUtil.isNullOrBlank(sytemOriginId)) {
//            userAuthId=sytemOriginId;
//        } else {        
//            if(Context.getCurrentInstance() != null){       
//            final HttpServletRequest request = Context.getCurrentInstance().getRequest();
//            final HttpSession session = request.getSession(false);
//            if(session != null && session.getAttribute(GXPConstants.USER_AUTH_ID) != null){
//                userAuthId = (String)session.getAttribute(GXPConstants.USER_AUTH_ID);
//            }
//          }
//        }
        return "app";
    }

}
