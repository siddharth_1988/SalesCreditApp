/**
 * 
 */
package com.disney.salesapp.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COBA_LIST")
public class COBAList extends AbstractBaseEntity {


    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1987216359108172546L;

    /** The coba list id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COBA_LIST_ID")
	private Integer cobaListId;

	/** The coba. */
	@Column(name = "COBA")
	private String coba;

	/** The comp code. */
	@Column(name = "COMP_CODE")
    private Integer compCode;
	
	/** The business area. */
	@Column(name = "BUSINESS_AREA_ID")
    private Integer businessAreaId;
	
	/** The cu description. */
	@Column(name = "CU_DESCRIPTION")
    private String cuDescription;

	/** The segment. */
	@ManyToOne(targetEntity = Segment.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "SEGMENT_ID")
    private Segment segment;

    /** The scoring region. */
    @Column(name = "SCORING_REGION")
    private String scoringRegion;

    /**
     * Gets the coba list id.
     *
     * @return the coba list id
     */
    public Integer getCobaListId() {
        return cobaListId;
    }

    /**
     * Sets the coba list id.
     *
     * @param cobaListId the new coba list id
     */
    public void setCobaListId(Integer cobaListId) {
        this.cobaListId = cobaListId;
    }

    /**
     * Gets the coba.
     *
     * @return the coba
     */
    public String getCoba() {
        return coba;
    }

    /**
     * Sets the coba.
     *
     * @param coba the new coba
     */
    public void setCoba(String coba) {
        this.coba = coba;
    }

    /**
     * Gets the comp code.
     *
     * @return the comp code
     */
    public Integer getCompCode() {
        return compCode;
    }

    /**
     * Sets the comp code.
     *
     * @param compCode the new comp code
     */
    public void setCompCode(Integer compCode) {
        this.compCode = compCode;
    }

    /**
     * Gets the business area id.
     *
     * @return the business area id
     */
    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    /**
     * Sets the business area id.
     *
     * @param businessAreaId the new business area id
     */
    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    /**
     * Gets the cu description.
     *
     * @return the cu description
     */
    public String getCuDescription() {
        return cuDescription;
    }

    /**
     * Sets the cu description.
     *
     * @param cuDescription the new cu description
     */
    public void setCuDescription(String cuDescription) {
        this.cuDescription = cuDescription;
    }

    
    /**
     * Gets the segment.
     *
     * @return the segment
     */
    public Segment getSegment() {
        return segment;
    }

    /**
     * Sets the segment.
     *
     * @param segment the new segment
     */
    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    /**
     * Gets the scoring region.
     *
     * @return the scoring region
     */
    public String getScoringRegion() {
        return scoringRegion;
    }

    /**
     * Sets the scoring region.
     *
     * @param scoringRegion the new scoring region
     */
    public void setScoringRegion(String scoringRegion) {
        this.scoringRegion = scoringRegion;
    }
    
    
    
}
