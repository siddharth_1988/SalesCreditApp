package com.disney.salesapp.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The Class BusinessGroupMapPK.
 */
@Embeddable
public class BusinessGroupMapPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -748757035990835620L;

    /** The business area id. */
    @Column(name = "BUSINESS_AREA_ID")
    private Integer businessAreaId;
    
    /** The company id. */
    @Column(name = "COMPANY_ID")
    private Integer companyId;
    
    /** The business group id. */
    @Column(name = "BUSINESS_GROUP_ID")
    private Integer businessGroupId;

    /**
     * Gets the business area id.
     *
     * @return the business area id
     */
    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    /**
     * Sets the business area id.
     *
     * @param businessAreaId the new business area id
     */
    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    /**
     * Gets the company id.
     *
     * @return the company id
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * Sets the company id.
     *
     * @param companyId the new company id
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     * Gets the business group id.
     *
     * @return the business group id
     */
    public Integer getBusinessGroupId() {
        return businessGroupId;
    }

    /**
     * Sets the business group id.
     *
     * @param businessGroupId the new business group id
     */
    public void setBusinessGroupId(Integer businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((businessAreaId == null) ? 0 : businessAreaId.hashCode());
        result = prime * result + ((businessGroupId == null) ? 0 : businessGroupId.hashCode());
        result = prime * result + ((companyId == null) ? 0 : companyId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BusinessGroupMapPK other = (BusinessGroupMapPK) obj;
        if (businessAreaId == null) {
            if (other.businessAreaId != null)
                return false;
        } else if (!businessAreaId.equals(other.businessAreaId))
            return false;
        if (businessGroupId == null) {
            if (other.businessGroupId != null)
                return false;
        } else if (!businessGroupId.equals(other.businessGroupId))
            return false;
        if (companyId == null) {
            if (other.companyId != null)
                return false;
        } else if (!companyId.equals(other.companyId))
            return false;
        return true;
    }
    
    
}
