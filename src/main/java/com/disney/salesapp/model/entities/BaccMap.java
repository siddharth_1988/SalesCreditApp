/**
 * 
 */
package com.disney.salesapp.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Class BaccMap.
 *
 * @author GHV001
 */
@Entity
@Table(name = "BACC_MAP")
public class BaccMap extends AbstractBaseEntity {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6940194756031242902L;

	/** The bacc map id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BACC_MAP_ID")
	private Integer baccMapId;

	/** The region. */
	@ManyToOne(targetEntity = Region.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "REGION_ID")
	private Region region;

	/** The segment. */
	@ManyToOne(targetEntity = Segment.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "SEGMENT_ID")
	private Segment segment;

	/** The business area. */
	@ManyToOne(targetEntity = BusinessArea.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "BUSINESS_AREA_ID")
	private BusinessArea businessArea;

	/** The company. */
	@ManyToOne(targetEntity = Company.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;

	/**
	 * Gets the bacc map id.
	 *
	 * @return the bacc map id
	 */
	public Integer getBaccMapId() {
		return baccMapId;
	}

	/**
	 * Sets the bacc map id.
	 *
	 * @param baccMapId
	 *            the new bacc map id
	 */
	public void setBaccMapId(Integer baccMapId) {
		this.baccMapId = baccMapId;
	}

	/**
	 * Gets the region.
	 *
	 * @return the region
	 */
	public Region getRegion() {
		return region;
	}

	/**
	 * Sets the region.
	 *
	 * @param region
	 *            the new region
	 */
	public void setRegion(Region region) {
		this.region = region;
	}

	/**
	 * Gets the segment.
	 *
	 * @return the segment
	 */
	public Segment getSegment() {
		return segment;
	}

	/**
	 * Sets the segment.
	 *
	 * @param segment
	 *            the new segment
	 */
	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	/**
	 * Gets the business area.
	 *
	 * @return the business area
	 */
	public BusinessArea getBusinessArea() {
		return businessArea;
	}

	/**
	 * Sets the business area.
	 *
	 * @param businessArea
	 *            the new business area
	 */
	public void setBusinessArea(BusinessArea businessArea) {
		this.businessArea = businessArea;
	}

	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * Sets the company.
	 *
	 * @param company
	 *            the new company
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

}
