package com.disney.salesapp.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Currency.
 *
 */
@Entity
@Table(name = "CURRENCY")
public class Currency extends AbstractBaseEntity {


	/** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2290638481876195632L;

    /** The currency id. */
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CURRENCY_ID")
	private Integer currencyId;

	/** The currency name. */
	@Column(name = "CURRENCY_NAME")
	private String currencyName;
	
	/** The currency code. */
	@Column(name = "CURRENCY_CODE")
    private String currencyCode;

    /**
     * Gets the currency id.
     *
     * @return the currency id
     */
    public Integer getCurrencyId() {
        return currencyId;
    }

    /**
     * Sets the currency id.
     *
     * @param currencyId the new currency id
     */
    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * Gets the currency name.
     *
     * @return the currency name
     */
    public String getCurrencyName() {
        return currencyName;
    }

    /**
     * Sets the currency name.
     *
     * @param currencyName the new currency name
     */
    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    /**
     * Gets the currency code.
     *
     * @return the currency code
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the currency code.
     *
     * @param currencyCode the new currency code
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

	

}
