package com.disney.salesapp.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The Class Segment.
 *
 * @author GHV001
 */
@Entity
@Table(name="SEGMENT")
public class Segment extends AbstractBaseEntity {
	
	/** The Constant serialVersionUID. */
    private static final long serialVersionUID = 991327458468308304L;

    /** The segment id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "SEGMENT_ID")
	private Integer segmentId;
	
	/** The segment name. */
	@Column(name = "SEGMENT_NAME")
	private String segmentName;
	
	/** The bacc maps. */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "segment")
    private Set<BaccMap> baccMaps = new HashSet<BaccMap>();

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "segment")
    private Set<COBAList> cobaLists = new HashSet<COBAList>();
	
    /**
     * Gets the segment id.
     *
     * @return the segment id
     */
    public Integer getSegmentId() {
        return segmentId;
    }

    /**
     * Sets the segment id.
     *
     * @param segmentId the new segment id
     */
    public void setSegmentId(Integer segmentId) {
        this.segmentId = segmentId;
    }


    /**
     * Gets the segment name.
     *
     * @return the segment name
     */
    public String getSegmentName() {
        return segmentName;
    }

    /**
     * Sets the segment name.
     *
     * @param segmentName the new segment name
     */
    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    /**
     * Gets the bacc maps.
     *
     * @return the bacc maps
     */
    public Set<BaccMap> getBaccMaps() {
        return baccMaps;
    }

    /**
     * Sets the bacc maps.
     *
     * @param baccMaps the new bacc maps
     */
    public void setBaccMaps(Set<BaccMap> baccMaps) {
        this.baccMaps = baccMaps;
    }

    /**
     * Gets the coba lists.
     *
     * @return the coba lists
     */
    public Set<COBAList> getCobaLists() {
        return cobaLists;
    }

    /**
     * Sets the coba lists.
     *
     * @param cobaLists the new coba lists
     */
    public void setCobaLists(Set<COBAList> cobaLists) {
        this.cobaLists = cobaLists;
    }

    
}
