/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="OriginationInfo")
@XmlAccessorType(XmlAccessType.NONE)
public class OriginationInfo {
	
	@XmlElement(name="SalesRep")
	private SalesRep salesRep;

	/**
	 * @return the salesRep
	 */
	public SalesRep getSalesRep() {
		return salesRep;
	}

	/**
	 * @param salesRep the salesRep to set
	 */
	public void setSalesRep(SalesRep salesRep) {
		this.salesRep = salesRep;
	}

}
