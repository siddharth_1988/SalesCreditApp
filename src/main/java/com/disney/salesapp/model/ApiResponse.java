/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="ApiResponse")
@XmlAccessorType(XmlAccessType.NONE)
public class ApiResponse {
	
	@XmlElement(name="Acknowledgement")
    private Acknowledgement acknowledgement;
	
	@XmlElement(name="ResponseOperation")
	private ResponseOperation responseOperation;

	/**
	 * @return the responseOperation
	 */
	public ResponseOperation getResponseOperation() {
		return responseOperation;
	}

	/**
	 * @param responseOperation the responseOperation to set
	 */
	public void setResponseOperation(ResponseOperation responseOperation) {
		this.responseOperation = responseOperation;
	}

	/**
	 * @return the acknowledgement
	 */
	public Acknowledgement getAcknowledgement() {
		return acknowledgement;
	}

	/**
	 * @param acknowledgement the acknowledgement to set
	 */
	public void setAcknowledgement(Acknowledgement acknowledgement) {
		this.acknowledgement = acknowledgement;
	}

	
}
