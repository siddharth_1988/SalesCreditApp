/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="ApplicationForm")
@XmlAccessorType(XmlAccessType.NONE)
public class ApplicationForm {
	
	@XmlElement(name="OriginationInfo")
    private OriginationInfo originationInfo;
	
	@XmlElement(name="CreditRequest")
	private CreditRequest creditRequest;

	@XmlElement(name="BusinessInfo")
    private BusinessInfo businessInfo;

	@XmlElement(name="Address")
    private Address address;

	/**
	 * @return the creditRequest
	 */
	public CreditRequest getCreditRequest() {
		return creditRequest;
	}

	/**
	 * @param creditRequest the creditRequest to set
	 */
	public void setCreditRequest(CreditRequest creditRequest) {
		this.creditRequest = creditRequest;
	}

	/**
	 * @return the originationInfo
	 */
	public OriginationInfo getOriginationInfo() {
		return originationInfo;
	}

	/**
	 * @param originationInfo the originationInfo to set
	 */
	public void setOriginationInfo(OriginationInfo originationInfo) {
		this.originationInfo = originationInfo;
	}

	/**
	 * @return the businessInfo
	 */
	public BusinessInfo getBusinessInfo() {
		return businessInfo;
	}

	/**
	 * @param businessInfo the businessInfo to set
	 */
	public void setBusinessInfo(BusinessInfo businessInfo) {
		this.businessInfo = businessInfo;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}
	
	

}
