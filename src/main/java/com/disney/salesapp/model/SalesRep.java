/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="SalesRep")
@XmlAccessorType(XmlAccessType.NONE)
public class SalesRep {

	@XmlAttribute(name = "id")
	private String id;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }
}
