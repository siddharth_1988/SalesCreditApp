/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="Acknowledgement")
@XmlAccessorType(XmlAccessType.NONE)
public class Acknowledgement {

	@XmlElement(name="CompletionCode")
	private String completionCode;

	/**
	 * @return the completionCode
	 */
	public String getCompletionCode() {
		return completionCode;
	}

	/**
	 * @param completionCode the completionCode to set
	 */
	public void setCompletionCode(String completionCode) {
		this.completionCode = completionCode;
	}
	
	
}
