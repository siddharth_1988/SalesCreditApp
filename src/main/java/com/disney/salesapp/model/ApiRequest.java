/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="ApiRequest")
@XmlAccessorType(XmlAccessType.NONE)
public class ApiRequest {

	@XmlElement(name="AuthenticationInfo")
	private AuthenticationInfo authenticationInfo;
	
	@XmlElement(name="RequestOperation")
	private RequestOperation requestOperation;

	/**
	 * @return the authenticationInfo
	 */
	public AuthenticationInfo getAuthenticationInfo() {
		return authenticationInfo;
	}

	/**
	 * @param authenticationInfo the authenticationInfo to set
	 */
	public void setAuthenticationInfo(AuthenticationInfo authenticationInfo) {
		this.authenticationInfo = authenticationInfo;
	}

	/**
	 * @return the requestOperation
	 */
	public RequestOperation getRequestOperation() {
		return requestOperation;
	}

	/**
	 * @param requestOperation the requestOperation to set
	 */
	public void setRequestOperation(RequestOperation requestOperation) {
		this.requestOperation = requestOperation;
	}
	
}
