/**
 * 
 */
package com.disney.salesapp.model;

/**
 * @author GHV001
 *
 */
public class UserInfoLite {
	public static final String PACKAGE_NAME = "com.disney.salescredit.bo";
	public static final String CLASS_NAME = "UserInfoLite";
	
	
	private Integer userId;
	private String portalId;
	private int permissionGroupId;
	private String userName;
	private boolean isActive;
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the portalId
	 */
	public String getPortalId() {
		return portalId;
	}
	/**
	 * @param portalId the portalId to set
	 */
	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}
	/**
	 * @return the permissionGroupId
	 */
	public int getPermissionGroupId() {
		return permissionGroupId;
	}
	/**
	 * @param permissionGroupId the permissionGroupId to set
	 */
	public void setPermissionGroupId(int permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	

}
