package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CreditRequest")
@XmlAccessorType(XmlAccessType.NONE)
public class CreditRequest {
	
	@XmlElement(name="ProductName")
    private String productName;
	
	@XmlElement(name="CreditRequestType")
    private String creditRequestType;

	@XmlElement(name="Amount")
	private String amount;

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the creditRequestType
	 */
	public String getCreditRequestType() {
		return creditRequestType;
	}

	/**
	 * @param creditRequestType the creditRequestType to set
	 */
	public void setCreditRequestType(String creditRequestType) {
		this.creditRequestType = creditRequestType;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	
}	
