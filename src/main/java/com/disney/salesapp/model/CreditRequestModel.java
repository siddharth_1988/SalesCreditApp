package com.disney.salesapp.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class CreditRequestModel {

	private List<CreditRequestModel> personDataList;
	private Integer segmentId;
	private String segmentName;

	private String regionId;
	private String regionName;
	
	private String companyCodeId;
	private String companyCodeName;
	
	private String businessAreaId;
	private String businessAreaName;
	
	String name;
	String email;
	String customerName;
	String streetAddress;
	String city;
	String state;
	String zipCode;
	String country;

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegionId() {
		return regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public String getCompanyCodeId() {
		return companyCodeId;
	}

	public String getCompanyCodeName() {
		return companyCodeName;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public void setCompanyCodeId(String companyCodeId) {
		this.companyCodeId = companyCodeId;
	}

	public void setCompanyCodeName(String companyCodeName) {
		this.companyCodeName = companyCodeName;
	}

	public Integer getSegmentId() {
		return segmentId;
	}

	public String getSegmentName() {
		return segmentName;
	}

	public String getBusinessAreaId() {
		return businessAreaId;
	}

	public String getBusinessAreaName() {
		return businessAreaName;
	}

	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public void setBusinessAreaId(String businessAreaId) {
		this.businessAreaId = businessAreaId;
	}

	public void setBusinessAreaName(String businessAreaName) {
		this.businessAreaName = businessAreaName;
	}

	public List<CreditRequestModel> getPersonDataList() {
		return personDataList;
	}

	public void setPersonDataList(List<CreditRequestModel> personDataList) {
		this.personDataList = personDataList;
	}

}