/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="ResponseOperation")
@XmlAccessorType(XmlAccessType.NONE)
public class ResponseOperation {
	
	@XmlElement(name="ReturnApplicationReference")
	private ReturnApplicationReference returnApplicationReference;

	/**
	 * @return the returnApplicationReference
	 */
	public ReturnApplicationReference getReturnApplicationReference() {
		return returnApplicationReference;
	}

	/**
	 * @param returnApplicationReference the returnApplicationReference to set
	 */
	public void setReturnApplicationReference(
			ReturnApplicationReference returnApplicationReference) {
		this.returnApplicationReference = returnApplicationReference;
	}
	

}
