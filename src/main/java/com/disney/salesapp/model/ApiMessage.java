/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="ApiMessage")
@XmlAccessorType(XmlAccessType.NONE)
public class ApiMessage {
	
	@XmlElement(name="ApiRequest")
	private ApiRequest apiRequest;

	@XmlElement(name="ApiResponse")
	private ApiResponse apiResponse;
	/**
	 * @return the apiRequest
	 */
	public ApiRequest getApiRequest() {
		return apiRequest;
	}

	/**
	 * @param apiRequest the apiRequest to set
	 */
	public void setApiRequest(ApiRequest apiRequest) {
		this.apiRequest = apiRequest;
	}

	/**
	 * @return the apiResponse
	 */
	public ApiResponse getApiResponse() {
		return apiResponse;
	}

	/**
	 * @param apiResponse the apiResponse to set
	 */
	public void setApiResponse(ApiResponse apiResponse) {
		this.apiResponse = apiResponse;
	}
	
	

}
