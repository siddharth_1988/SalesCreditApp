/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="RequestOperation")
@XmlAccessorType(XmlAccessType.NONE)
public class RequestOperation {
	
	@XmlElement(name="SubmitApplication")
	private SubmitApplication submitApplication;

	/**
	 * @return the submitApplication
	 */
	public SubmitApplication getSubmitApplication() {
		return submitApplication;
	}

	/**
	 * @param submitApplication the submitApplication to set
	 */
	public void setSubmitApplication(SubmitApplication submitApplication) {
		this.submitApplication = submitApplication;
	}

}
