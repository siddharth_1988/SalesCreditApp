/**
 * 
 */
package com.disney.salesapp.model;


/**
 * @author MADHS008
 *
 */

public class User {
    
  
    private Integer id;
    private String email;
    private String portalId;
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * Gets the portal id.
     *
     * @return the portal id
     */
    public String getPortalId() {
        return portalId;
    }
    
    /**
     * Sets the portal id.
     *
     * @param portalId the new portal id
     */
    public void setPortalId(String portalId) {
        this.portalId = portalId;
    }
    

}
