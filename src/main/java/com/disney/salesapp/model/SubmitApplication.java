/**
 * 
 */
package com.disney.salesapp.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author GHV001
 *
 */
@XmlRootElement(name="SubmitApplication")
@XmlAccessorType(XmlAccessType.NONE)
public class SubmitApplication {
	
	@XmlElement(name="ApplicationForm")
	private ApplicationForm applicationForm;

	/**
	 * @return the applicationForm
	 */
	public ApplicationForm getApplicationForm() {
		return applicationForm;
	}

	/**
	 * @param applicationForm the applicationForm to set
	 */
	public void setApplicationForm(ApplicationForm applicationForm) {
		this.applicationForm = applicationForm;
	}
	

}
