package com.disney.salesapp.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.disney.salesapp.bo.DashboardBO;
import com.disney.salesapp.services.DashboardService;

@Controller
public class DashboardController {
	
	@Autowired
	private DashboardService dashboardService;

	@RequestMapping(value = "/getDashboardDetails", method = RequestMethod.GET)
	public @ResponseBody List<DashboardBO> getDashboardDetails(
			HttpSession session) {
		String portalId = session.getAttribute("portalId").toString();

		List<DashboardBO> dashboardBOs = dashboardService.getDashboardDetails(portalId);
		return dashboardBOs;
	}

    public DashboardService getDashboardService() {
        return dashboardService;
    }

    public void setDashboardService(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }
	
	

}
