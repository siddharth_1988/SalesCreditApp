/**
 * 
 */
package com.disney.salesapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author MADHS008
 * 
 *         The class below will be invoked when the login action is fired.
 *
 */
@Controller
public class LoginController {

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/userCheck", method = RequestMethod.POST)
	public String userCheck(ModelMap model, HttpServletRequest request) {
		String portalId = request.getParameter("portalId");
		String pwd = request.getParameter("pwd");

		if ("test001".equalsIgnoreCase(portalId) && "test".equalsIgnoreCase(pwd) 
				|| "admin".equalsIgnoreCase(portalId) && "admin".equalsIgnoreCase(pwd)) {
			HttpSession session = request.getSession();
			session.setAttribute("portalId", portalId);
			return "dashboard";
		} else {
			return "redirect:failure.jsp";
		}
	}

	/*
	 * @RequestMapping(value="userCheck", method = RequestMethod.POST) public
	 * String userCheck(ModelMap model ,@ModelAttribute("login") Login login,
	 * BindingResult result) { String name=login.getUsername(); String
	 * pwd=login.getPassword();
	 * if("vinay".equalsIgnoreCase(name)&&"vinay".equalsIgnoreCase(pwd)){
	 * model.addAttribute("message", "Successfully logged in.");
	 * 
	 * }else{ model.addAttribute("message", "Username or password is wrong."); }
	 * return "success"; }
	 */

}
