package com.disney.salesapp.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.disney.salesapp.model.CreditXmlRequest;

@Path("/xml")
public class ResponseController {

	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response createTrackInXML(CreditXmlRequest creditXmlRequest) {
		//String result = "CreditXmlRequest saved : "+creditXmlRequest;
		String result = "Media";
		return Response.status(201).entity(result).build();
		
	}
}
