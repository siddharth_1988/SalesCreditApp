package com.disney.salesapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.disney.salesapp.bo.UserBO;
import com.disney.salesapp.services.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping("/addUser")
	public String addUser(@ModelAttribute("userBO") UserBO userBO,
			BindingResult result, ModelMap model, HttpSession session) {
		String returnPage = "redirect:login.jsp";
		if (session.getAttribute("portalId") != null) {
			returnPage = "addUser";
		}
		return returnPage;
	}

	@RequestMapping("/dashboard")
	public String dashboard(ModelMap model, HttpSession session) {
		String returnPage = "redirect:login.jsp";
		if (session.getAttribute("portalId") != null) {
			returnPage = "dashboard";
		}
		return returnPage;
	}
	
	@RequestMapping("/admin")
	public String admin(ModelMap model, HttpSession session) {
		String returnPage = "redirect:login.jsp";
		if (session.getAttribute("portalId") != null) {
			returnPage = "admin";
		}
		return returnPage;
	}

	@RequestMapping("/help")
	public String help(ModelMap model, HttpSession session) {
		String returnPage = "redirect:login.jsp";
		if (session.getAttribute("userName") != null) {
			returnPage = "help";
		}
		return returnPage;
	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("userBO") UserBO userBO,
			BindingResult result, ModelMap model) {
		userService.saveUser(userBO);
		return "userDataList";

	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("userBO") UserBO userBO,
			BindingResult result, ModelMap model) {
		userService.updatUser(userBO);
		return "userDataList";

	}

	@RequestMapping("/userDataList")
	public String userData(ModelMap model, HttpSession session) {
		String returnPage = "redirect:login.jsp";
		if (session.getAttribute("userName") != null) {
			returnPage = "userDataList";
		}
		return returnPage;
	}

	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
	public @ResponseBody List<UserBO> getAllUsers() {
		List<UserBO> userList = userService.findAllUsers();
		return userList;
	}

	@RequestMapping(value = "/editUser", method = RequestMethod.GET)
	public String getContact(ModelMap model, HttpServletRequest request,
			HttpSession session) {
		String returnPage = "redirect:login.jsp";
		if (session.getAttribute("userName") != null) {
			String userId = request.getParameter("userId");
			UserBO userBO = userService.findById(Integer.parseInt(userId));
			model.addAttribute("userBO", userBO);
			returnPage = "editUser";
		}
		return returnPage;
	}
}
