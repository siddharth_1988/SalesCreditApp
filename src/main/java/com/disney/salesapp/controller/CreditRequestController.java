package com.disney.salesapp.controller;

import java.io.StringReader;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.disney.salesapp.bo.BusinessAreaBO;
import com.disney.salesapp.bo.BusinessGroupBO;
import com.disney.salesapp.bo.COBAListBO;
import com.disney.salesapp.bo.CompanyBO;
import com.disney.salesapp.bo.CountryBO;
import com.disney.salesapp.bo.CreditRequestBO;
import com.disney.salesapp.bo.CreditSubmissionBO;
import com.disney.salesapp.bo.CreditUserRequest;
import com.disney.salesapp.bo.CurrencyBO;
import com.disney.salesapp.bo.RegionBO;
import com.disney.salesapp.bo.SegmentBO;
import com.disney.salesapp.bo.StateBO;
import com.disney.salesapp.model.Address;
import com.disney.salesapp.model.ApiMessage;
import com.disney.salesapp.model.ApiRequest;
import com.disney.salesapp.model.ApplicationForm;
import com.disney.salesapp.model.AuthenticationInfo;
import com.disney.salesapp.model.BusinessInfo;
import com.disney.salesapp.model.CustomerReference;
import com.disney.salesapp.model.OriginationInfo;
import com.disney.salesapp.model.RequestOperation;
import com.disney.salesapp.model.SalesRep;
import com.disney.salesapp.model.SubmitApplication;
import com.disney.salesapp.services.CreditSubmit;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Controller
public class CreditRequestController {

	@Autowired
	private CreditSubmit creditSubmit;

	private Integer selectedSegment;

	private Integer selectedRegion;

	private Integer selectedCompany;

	private Integer selectedBusinessArea;

	private CreditSubmissionBO creditSubmissionBO;

	private CreditRequestBO creditRequestBO;

	private Integer creditRequestId;
	
	CountryBO countryBO = null;
	
	StateBO stateBO = null;
	
	CountryBO agencyCountryBO = null;
	
	StateBO agencyStateBO = null;
	
	private String portalId;

	@RequestMapping(value = "/creditRequest", method = RequestMethod.GET)
	public String printWelcome(ModelMap model, HttpSession session) {
		String returnPage = "redirect:login.jsp";
		if (session.getAttribute("portalId") != null) {
			model.addAttribute("creditRequest", new CreditRequestBO());
			returnPage = "creditRequest";
		}
		return returnPage;
	}

	/*
	 * 
	 * @RequestMapping(value = "/getNames", method = RequestMethod.GET, produces
	 * = { MediaType.APPLICATION_JSON_VALUE }) public @ResponseBody
	 * List<CreditRequestModel> populateSegmentGroups(ModelMap model) {
	 * Map<String, String> segmentGroup = getSegmentGroup(); //
	 * model.addAttribute("segmentGroupList", segmentGroup);
	 * 
	 * List<CreditRequestModel> personDatas = new
	 * ArrayList<CreditRequestModel>();
	 * 
	 * for (String key : segmentGroup.keySet()) { CreditRequestModel personData
	 * = new CreditRequestModel();
	 * personData.setSegmentId(Integer.parseInt(key));
	 * personData.setSegmentName(segmentGroup.get(key));
	 * personDatas.add(personData); } return personDatas; }
	 */

	@RequestMapping(value = "/getAllSegments", method = RequestMethod.GET)
	public @ResponseBody List<SegmentBO> getAllSegments() {
		return creditSubmit.getAllSegments();
	}

	@RequestMapping(value = "/getCOBAListBySegment/{segmentId}", method = RequestMethod.GET)
    public @ResponseBody List<COBAListBO> getCOBAListBySegment(
            @PathVariable Integer segmentId) {
        return creditSubmit.getCOBAListBySegment(segmentId);
    }
	
	@RequestMapping(value = "/getRegionBySegment/{segmentId}", method = RequestMethod.GET)
	public @ResponseBody List<RegionBO> getRegionBySegment(
			@PathVariable Integer segmentId) {
		return creditSubmit.getRegionBySegment(segmentId);
	}

	@RequestMapping(value = "/getCompanyByRegion/{segmentId}/{regionId}", method = RequestMethod.GET)
	public @ResponseBody List<CompanyBO> getCompanyCodeByRegion(
			@PathVariable Integer segmentId, @PathVariable Integer regionId) {
		selectedSegment = segmentId;
		selectedRegion = regionId;
		return creditSubmit.getCompanyBySegmentRegion(segmentId, regionId);
	}

	@RequestMapping(value = "/getBusinessAreaByCompany/{segmentId}/{regionId}/{companyId}", method = RequestMethod.GET)
	public @ResponseBody List<BusinessAreaBO> getBusinessAreaByCompany(
			@PathVariable Integer segmentId, @PathVariable Integer regionId,
			@PathVariable Integer companyId) {
		return creditSubmit.getBABySegementRegionCompany(segmentId, regionId,
				companyId);
	}

	@RequestMapping(value = "/getBusinessGroup/{companyId}/{businessAreaId}", method = RequestMethod.GET)
	public @ResponseBody BusinessGroupBO getBusinessGroup(
			@PathVariable Integer companyId,
			@PathVariable Integer businessAreaId) {
		selectedCompany = companyId;
		selectedBusinessArea = businessAreaId;
		return creditSubmit.getBusinessGroup(companyId, businessAreaId);

	}

	@RequestMapping(value = "/getCountryList", method = RequestMethod.GET)
	public @ResponseBody List<CountryBO> getCountryList() {
		return creditSubmit.getAllCountries();
	}
	
	@RequestMapping(value = "/getCurrencyList", method = RequestMethod.GET)
    public @ResponseBody List<CurrencyBO> getCurrencyList() {
        return creditSubmit.getAllCurrencies();
    }

	@RequestMapping(value = "/getUsaStateList", method = RequestMethod.GET)
	public @ResponseBody List<StateBO> getUsaStateList() {
		return creditSubmit.getAllStatesOfUsa();
	}

	@RequestMapping(value = "/getCreditRequestDetails", method = RequestMethod.GET)
	public @ResponseBody CreditRequestBO getCreditRequestDetails(HttpSession session) {
		portalId = session.getAttribute("portalId").toString();
		creditSubmissionBO = creditSubmit.getCreditRequestDetails(portalId);
		creditRequestBO = creditSubmissionBO.getCreditRequestBO();
		creditRequestBO.setSegmentBOs(getAllSegments());
		creditRequestBO.setCountryBOs(getCountryList());
		creditRequestBO.setStateBOs(getUsaStateList());
		creditRequestBO.setCurrencyBOs(getCurrencyList());
		
		if (creditRequestBO.getSegmentBO() != null) {
		    creditRequestBO.setRegionBOs(getRegionBySegment(creditRequestBO.getSegmentBO().getSegmentId()));
		    
		    if (creditRequestBO.getRegionBO() != null) {
		        creditRequestBO.setCompanyBOs(getCompanyCodeByRegion(creditRequestBO.getSegmentBO().getSegmentId(), creditRequestBO.getRegionBO().getRegionId()));
		        
                if (creditRequestBO.getCompanyBO() != null) {
                    creditRequestBO.setBusinessAreaBOs(getBusinessAreaByCompany(
                            creditRequestBO.getSegmentBO().getSegmentId(), 
                            creditRequestBO.getRegionBO().getRegionId(), 
                            creditRequestBO.getCompanyBO().getCompanyId()));
                    
                    if (creditRequestBO.getBusinessAreaBO().getBusinessAreaId() != null) {
                        creditRequestBO.setBusinessGroupBO(getBusinessGroup(creditRequestBO.getCompanyBO().getCompanyId(), 
                                creditRequestBO.getBusinessAreaBO().getBusinessAreaId()));
                    }
                    
                }
		    }
		}
		return creditRequestBO;
	}

	@RequestMapping(value = "/creditRequestSubmission", method = RequestMethod.POST)
	public String getCreditRequestSubmission(
			@ModelAttribute("creditRequest") CreditRequestBO creditRequestBO,
			BindingResult result, ModelMap model) {
		/*
		 * if (selectedSegment != null) { SegmentBO segmentBO =
		 * creditSubmit.getSegmentById(selectedSegment);
		 * model.addAttribute("segment", segmentBO.getSegmentName()); } if
		 * (selectedRegion != null) { RegionBO regionBO =
		 * creditSubmit.getRegionById(selectedRegion);
		 * model.addAttribute("region", regionBO.getRegionName()); } if
		 * (selectedCompany != null) { CompanyBO companyBO =
		 * creditSubmit.getCompanyById(selectedCompany);
		 * model.addAttribute("company", companyBO.getCompanyName()); } if
		 * (selectedBusinessArea != null) { BusinessAreaBO businessAreaBO =
		 * creditSubmit .getBusinessAreaById(selectedBusinessArea);
		 * model.addAttribute("businessArea",
		 * businessAreaBO.getBusinessAreaName()); }
		 */

		model.addAttribute("name", creditRequestBO.getName());
		model.addAttribute("email", creditRequestBO.getEmail());
		model.addAttribute("customerName", creditRequestBO.getCustomerName());
		model.addAttribute("customerAdress", creditRequestBO.getStreetAddress());
		model.addAttribute("city", creditRequestBO.getCity());
		model.addAttribute("state", creditRequestBO.getState());
		model.addAttribute("zipCode", creditRequestBO.getZipCode());
		model.addAttribute("country", creditRequestBO.getCountry());
		return "creditRequestConfirmation";
	}
     
	@RequestMapping(value = "/saveUserRequesterDetails", method = RequestMethod.POST)
	@Consumes(MediaType.APPLICATION_JSON)
	public @ResponseBody void saveUserRequesterDetails(@RequestBody CreditUserRequest creditUserRequest, HttpSession session) {
       System.out.println("RequestorName..."+creditUserRequest.getRequestorName());
       System.out.println("RequesterDate..."+creditUserRequest.getRequesterDate());
       System.out.println("AeLastName..."+creditUserRequest.getAeLastName());
		portalId = session.getAttribute("portalId").toString();
		
		if (portalId == null) {
			portalId = "Test001";
		}
		
		Integer id =  creditSubmit.saveUserRequesterDetails(creditUserRequest);
		System.out.println("Idd..."+id);
	}
	
	
	
	@RequestMapping(value = "/saveRequesterDetails/{requesterName}/{requesterEmail}/{requesterDate}/{aeLastName}", method = RequestMethod.POST)
	public @ResponseBody void saveRequesterDetails(
			@PathVariable String requesterName,
			@PathVariable String requesterEmail,
			@PathVariable String requesterDate,
			@PathVariable String aeLastName,
			@RequestBody  
			HttpSession session) {

		portalId = session.getAttribute("portalId").toString();
		
		if (portalId == null) {
			portalId = "Test001";
		}
		setGlobalParametersToCreditSubmit(creditRequestBO);
		creditRequestBO.setPortalId(portalId);
		creditRequestBO.setRequestorName(requesterName);
		creditRequestBO.setRequestorEmail(requesterEmail);
		creditRequestBO.setRequestNeededByDate(requesterDate);
		creditRequestBO.setAeLastName(aeLastName);
		
		
		
		

		int creditRequestId = creditSubmit
				.saveRequesterDetails(creditRequestBO);

		creditRequestBO.setCreditRequestId(creditRequestId);
		creditSubmissionBO.setPortalId(portalId);
		creditSubmissionBO.setPartialSave("Y");
		creditSubmissionBO.setCreditRequestBO(creditRequestBO);
		creditSubmissionBO = creditSubmit
				.saveCreditSubmission(creditSubmissionBO);

		this.setCreditRequestId(creditRequestId);
		this.setCreditRequestBO(creditRequestBO);
	}

	@RequestMapping(value = "/saveBusinessDetails/{requestedCreditLimit}/{startDate}/{endDate}", method = RequestMethod.POST)
	public @ResponseBody void saveBusinessDetails(
			@PathVariable Integer requestedCreditLimit,
			@PathVariable String startDate, @PathVariable String endDate,
			HttpSession session) {

		CreditRequestBO creditRequestBO = this.getCreditRequestBO();
		creditRequestBO.setRequestedCreditLimit(requestedCreditLimit);
		creditRequestBO.setTermStartDate(startDate);
		creditRequestBO.setTermEndDate(endDate);

		if (getCreditRequestId() > 0) {
			creditSubmit.saveBusinessDetails(creditRequestBO,
					getCreditRequestId());
		}
		this.setCreditRequestBO(creditRequestBO);

	}

	@RequestMapping(value = "/saveCompanyDetailsForUS/{companyName}/{address}/{city}/{countryId}/{countryName}/{stateId}/{stateName}/{state1}/{zip}", method = RequestMethod.POST)
	public @ResponseBody void saveCompanyDetailsForUS(
			@PathVariable String companyName, @PathVariable String address,
			@PathVariable String city, @PathVariable Integer countryId,
			@PathVariable String countryName, @PathVariable Integer stateId,
			@PathVariable String stateName, @PathVariable String state1,
			@PathVariable String zip, HttpSession session) {
		try {
			CreditRequestBO creditRequestBO = this.getCreditRequestBO();
			creditRequestBO.setCompanyName(companyName);
			creditRequestBO.setCompanyStreetAddress(address);
			
			countryBO = new CountryBO();
			countryBO.setCountryId(countryId);
			countryBO.setCountryName(countryName);
			
			creditRequestBO.setCompanyCountry(countryBO);
			
			stateBO = new StateBO();
			stateBO.setStateId(stateId);
			stateBO.setStateName(stateName);

			if (countryName.equalsIgnoreCase("USA")) {
				creditRequestBO.setCompanyState(stateBO);
			} else {
				creditRequestBO.setCompanyState1(state1);
			}
			creditRequestBO.setCompanyCity(city);
			creditRequestBO.setCompanyZip(zip);

			if (getCreditRequestId() > 0) {
				creditSubmit.saveCompanyDetails(creditRequestBO,
						getCreditRequestId());
			}
			this.setCreditRequestBO(creditRequestBO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "/saveCompanyDetails/{companyName}/{address}/{city}/{countryId}/{countryName}/{state1}/{zip}", method = RequestMethod.POST)
	public @ResponseBody void saveCompanyDetails(
			@PathVariable String companyName, @PathVariable String address,
			@PathVariable String city, @PathVariable Integer countryId,
			@PathVariable String countryName, @PathVariable String state1,
			@PathVariable String zip, HttpSession session) {
		try {
			CreditRequestBO creditRequestBO = this.getCreditRequestBO();
			creditRequestBO.setCompanyName(companyName);
			creditRequestBO.setCompanyStreetAddress(address);
			
			countryBO = new CountryBO();
			countryBO.setCountryId(countryId);
			countryBO.setCountryName(countryName);
			
			creditRequestBO.setCompanyCountry(countryBO);
			creditRequestBO.setCompanyState1(state1);
			
			creditRequestBO.setCompanyCity(city);
			creditRequestBO.setCompanyZip(zip);

			if (getCreditRequestId() > 0) {
				creditSubmit.saveCompanyDetails(creditRequestBO,
						getCreditRequestId());
			}
			this.setCreditRequestBO(creditRequestBO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/saveAgencyDetailsForUS/{agencyName}/{address}/{city}/{countryId}/{countryName}/{stateId}/{stateName}/{state1}/{zip}/{executive}", method = RequestMethod.POST)
	public @ResponseBody void saveAgencyDetailsForUS(
			@PathVariable String agencyName, @PathVariable String address,
			@PathVariable String city, @PathVariable Integer countryId,
			@PathVariable String countryName, @PathVariable Integer stateId,
			@PathVariable String stateName, @PathVariable String state1,
			@PathVariable String zip, @PathVariable String executive,
			HttpSession session) {

		CreditRequestBO creditRequestBO = this.getCreditRequestBO();
		creditRequestBO.setAgencyName(agencyName);
		creditRequestBO.setAgencyStreetAddress(address);
		creditRequestBO.setAgencyCity(city);
		
		agencyCountryBO = new CountryBO();
		agencyCountryBO.setCountryId(countryId);
		agencyCountryBO.setCountryName(countryName);
		
		creditRequestBO.setAgencyCountry(agencyCountryBO);
		
		agencyStateBO = new StateBO();
		agencyStateBO.setStateId(stateId);
		agencyStateBO.setStateName(stateName);

		if (countryName.equalsIgnoreCase("USA")) {
			creditRequestBO.setAgencyState(agencyStateBO);
		} else {
			creditRequestBO.setAgencyState1(state1);
		}
		creditRequestBO.setAgencyZip(zip);
		creditRequestBO.setAccountExecutive(executive);

		if (getCreditRequestId() > 0) {
			creditSubmit.saveAgencyDetails(creditRequestBO,
					getCreditRequestId());
		}
		this.setCreditRequestBO(creditRequestBO);
	}
	
	@RequestMapping(value = "/saveAgencyDetails/{agencyName}/{address}/{city}/{countryId}/{countryName}/{state1}/{zip}/{executive}", method = RequestMethod.POST)
	public @ResponseBody void saveAgencyDetails(
			@PathVariable String agencyName, @PathVariable String address,
			@PathVariable String city, @PathVariable Integer countryId,
			@PathVariable String countryName, @PathVariable String state1,
			@PathVariable String zip, @PathVariable String executive,
			HttpSession session) {

		CreditRequestBO creditRequestBO = this.getCreditRequestBO();
		creditRequestBO.setAgencyName(agencyName);
		creditRequestBO.setAgencyStreetAddress(address);
		creditRequestBO.setAgencyCity(city);
		
		agencyCountryBO = new CountryBO();
		agencyCountryBO.setCountryId(countryId);
		agencyCountryBO.setCountryName(countryName);
		
		creditRequestBO.setAgencyCountry(agencyCountryBO);
		
		creditRequestBO.setAgencyState1(state1);
		
		creditRequestBO.setAgencyZip(zip);
		creditRequestBO.setAccountExecutive(executive);

		if (getCreditRequestId() > 0) {
			creditSubmit.saveAgencyDetails(creditRequestBO,
					getCreditRequestId());
		}
		this.setCreditRequestBO(creditRequestBO);
	}

	@RequestMapping(value = "/saveCollectionDetails/{phone}/{email}/{name}", method = RequestMethod.POST)
	public @ResponseBody void saveCollectionDetails(@PathVariable String phone,
			@PathVariable String email, @PathVariable String name,
			HttpSession session) {
		CreditRequestBO creditRequestBO = this.getCreditRequestBO();
		creditRequestBO.setcContactPhone(phone);
		creditRequestBO.setcContactEmail(email);
		creditRequestBO.setcContactFirstName(name);

		if (getCreditRequestId() > 0) {
			creditSubmit.saveCollectionDetails(creditRequestBO,
					getCreditRequestId());
		}
		this.setCreditRequestBO(creditRequestBO);

	}

	@RequestMapping(value = "/saveAdvanceCashDetails/{cashAdvance}/{direct}", method = RequestMethod.POST)
	public @ResponseBody void saveAdvanceCashDetails(
			@PathVariable Boolean cashAdvance, @PathVariable Boolean direct,
			HttpSession session) {

		CreditRequestBO creditRequestBO = this.getCreditRequestBO();
		creditRequestBO.setCashAdvance(cashAdvance);
		creditRequestBO.setDirect(direct);
		if (getCreditRequestId() > 0) {
			creditSubmit.saveAdvanceCashDetails(creditRequestBO,
					getCreditRequestId());
		}
		this.setCreditRequestBO(creditRequestBO);

	}

	private void setGlobalParametersToCreditSubmit(
			CreditRequestBO creditRequestBO) {
		if (selectedSegment != null) {
			SegmentBO segmentBO = creditSubmit.getSegmentById(selectedSegment);
			creditRequestBO.setSegmentBO(segmentBO);
		}
		if (selectedRegion != null) {
			RegionBO regionBO = creditSubmit.getRegionById(selectedRegion);
			creditRequestBO.setRegionBO(regionBO);
		}
		if (selectedCompany != null) {
			CompanyBO companyBO = creditSubmit.getCompanyById(selectedCompany);
			creditRequestBO.setCompanyBO(companyBO);
		}
		if (selectedBusinessArea != null) {
			BusinessAreaBO businessAreaBO = creditSubmit.getBusinessAreaById(selectedBusinessArea);
			creditRequestBO.setBusinessAreaBO(businessAreaBO);
		}
	}

	@RequestMapping(value = "/xmlbasedcreditRequestSubmission", method = RequestMethod.POST)
	public String getXMLCreditRequestSubmission(
			@ModelAttribute("creditRequest") CreditRequestBO creditRequestBO,
			BindingResult result, ModelMap model, HttpSession session) {
		try {
			System.out.println("Inside Controller....");
			setGlobalParametersToCreditSubmit(creditRequestBO);
			creditRequestBO.setCreditRequestId(getCreditRequestId());
			creditRequestBO.setPortalId(portalId);
			creditRequestBO.setCompanyCountry(countryBO);
			creditRequestBO.setCompanyState(stateBO);
			creditRequestBO.setAgencyCountry(agencyCountryBO);
			creditRequestBO.setAgencyState(agencyStateBO);
			creditSubmit.saveCreditRequest(creditRequestBO);

			com.disney.salesapp.model.CreditRequest creditRequest = new com.disney.salesapp.model.CreditRequest();
			creditRequest.setAmount("10");
			creditRequest.setCreditRequestType("Line of Credit");
			creditRequest.setProductName("Line of credit");

			SalesRep salesRep = new SalesRep();
			salesRep.setId("disney8");

			OriginationInfo originationInfo = new OriginationInfo();
			originationInfo.setSalesRep(salesRep);

			CustomerReference customerReference = new CustomerReference();
			customerReference.setId("32SS");

			BusinessInfo businessInfo = new BusinessInfo();
			businessInfo.setBusinessSince("2000-12-31");
			businessInfo.setDbaName("SERVICES UNLIMITED");
			businessInfo.setDunsNo("678967899");
			businessInfo.setCustomerReference(customerReference);
			businessInfo.setLegalEntityType("Public Corporation");
			businessInfo.setLegalName("SERVICES UNLIMITED EXPRESS");
			businessInfo.setTaxId("987654321");

			Address address = new Address();
			address.setAddress1("P.O. Box 13287");
			address.setAddressType("Legal");
			address.setCity("Florence");
			address.setCountry("US");
			address.setPhone("8006621520");
			address.setPostalCode("29504");
			address.setState("South Carolina");

			ApplicationForm applicationForm = new ApplicationForm();
			applicationForm.setAddress(address);
			applicationForm.setBusinessInfo(businessInfo);
			applicationForm.setCreditRequest(creditRequest);
			applicationForm.setOriginationInfo(originationInfo);

			SubmitApplication submitApplication = new SubmitApplication();
			submitApplication.setApplicationForm(applicationForm);

			RequestOperation requestOperation = new RequestOperation();
			requestOperation.setSubmitApplication(submitApplication);

			AuthenticationInfo authenticationInfo = new AuthenticationInfo();
			authenticationInfo.setUserId("disney8");
			authenticationInfo.setPassword("test123!");

			ApiRequest apiRequest = new ApiRequest();
			apiRequest.setAuthenticationInfo(authenticationInfo);
			apiRequest.setRequestOperation(requestOperation);

			ApiMessage apiMessage = new ApiMessage();
			apiMessage.setApiRequest(apiRequest);

			Client client = Client.create();
			WebResource webResource = client
					.resource("https://demo-nfusion.ecredit.com/nfusion/HTTPInboundAdapter");

			ClientResponse response = webResource.type("text/xml").post(
					ClientResponse.class, apiMessage);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);

			System.out.println(output);
			JAXBContext context = JAXBContext.newInstance(ApiMessage.class);

			ApiMessage response1 = xml2Pojo(output, context);
			System.out.println(response1.getApiResponse()
					.getResponseOperation().getReturnApplicationReference()
					.getApplicationReference().getId());
			String applicationReferenceId = response1.getApiResponse()
					.getResponseOperation().getReturnApplicationReference()
					.getApplicationReference().getId();
			String status = response1.getApiResponse().getAcknowledgement()
					.getCompletionCode();
			model.addAttribute("applicationReferenceId", applicationReferenceId);
			model.addAttribute("status", status);

			creditSubmissionBO
					.setApplicationReferenceNumber(applicationReferenceId);
			creditSubmissionBO.setStatus("In progress");
			creditSubmissionBO.setPartialSave("N");
			creditSubmissionBO.setRequestRaisedDate(new Date());
			creditSubmit.saveCreditSubmission(creditSubmissionBO);
			/*
			 * model.addAttribute("businessGroup", output); SegmentBO segmentBO
			 * = creditSubmit.getSegmentById(selectedSegment); RegionBO regionBO
			 * = creditSubmit.getRegionById(selectedRegion); CompanyBO companyBO
			 * = creditSubmit.getCompanyById(selectedCompany); BusinessAreaBO
			 * businessAreaBO =
			 * creditSubmit.getBusinessAreaById(selectedBusinessArea);
			 * model.addAttribute("segment", segmentBO.getSegmentName());
			 * model.addAttribute("region", regionBO.getRegionName());
			 * model.addAttribute("company", companyBO.getCompanyName());
			 * model.addAttribute("businessArea",
			 * businessAreaBO.getBusinessAreaName());
			 * model.addAttribute("creditRequest", new CreditRequest());
			 */

		} catch (Exception e) {

			e.printStackTrace();

		}

		return "creditTracking";

	}

	private ApiMessage xml2Pojo(String xmlStringData, JAXBContext context)

	throws JAXBException {

		StringReader reader = new StringReader(xmlStringData);

		Unmarshaller unmarshaller = context.createUnmarshaller();

		ApiMessage response = (ApiMessage) unmarshaller.unmarshal(reader);

		return response;

	}

	/**
	 * Gets the credit submit.
	 *
	 * @return the credit submit
	 */
	public CreditSubmit getCreditSubmit() {
		return creditSubmit;
	}

	/**
	 * Sets the credit submit.
	 *
	 * @param creditSubmit
	 *            the new credit submit
	 */
	public void setCreditSubmit(CreditSubmit creditSubmit) {
		this.creditSubmit = creditSubmit;
	}

	/**
	 * @return the creditSubmissionBO
	 */
	public CreditSubmissionBO getCreditSubmissionBO() {

		return creditSubmissionBO;
	}

	/**
	 * @param creditSubmissionBO
	 *            the creditSubmissionBO to set
	 */
	public void setCreditSubmissionBO(CreditSubmissionBO creditSubmissionBO) {
		this.creditSubmissionBO = creditSubmissionBO;
	}

	public CreditRequestBO getCreditRequestBO() {
		return creditRequestBO;
	}

	public Integer getCreditRequestId() {
		return creditRequestId;
	}

	public void setCreditRequestBO(CreditRequestBO creditRequestBO) {
		this.creditRequestBO = creditRequestBO;
	}

	public void setCreditRequestId(Integer creditRequestId) {
		this.creditRequestId = creditRequestId;
	}

}