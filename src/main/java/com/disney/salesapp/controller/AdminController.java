package com.disney.salesapp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.disney.salesapp.bo.BaccMapBO;
import com.disney.salesapp.bo.BusinessAreaBO;
import com.disney.salesapp.bo.CompanyBO;
import com.disney.salesapp.bo.CreditRequestBO;
import com.disney.salesapp.bo.RegionBO;
import com.disney.salesapp.bo.SegmentBO;
import com.disney.salesapp.services.AdminService;
import com.disney.salesapp.services.CreditSubmit;

@Controller
public class AdminController {

	@Autowired
	private CreditSubmit creditSubmit;

	@Autowired
	private AdminService adminService;

	private Integer CompanyId;
	private Integer businessAreaId;

	public Integer getCompanyId() {
		return CompanyId;
	}

	public Integer getBusinessAreaId() {
		return businessAreaId;
	}

	public void setCompanyId(Integer companyId) {
		CompanyId = companyId;
	}

	public void setBusinessAreaId(Integer businessAreaId) {
		this.businessAreaId = businessAreaId;
	}

	@RequestMapping(value = "/getAllAdminSegments", method = RequestMethod.GET)
	public @ResponseBody List<SegmentBO> getAllSegments() {
		return creditSubmit.getAllSegments();
	}

	@RequestMapping(value = "/getAllRegionBySegment/{segmentId}", method = RequestMethod.GET)
	public @ResponseBody List<RegionBO> getRegionBySegment(
			@PathVariable Integer segmentId) {
		return creditSubmit.getRegionBySegment(segmentId);
	}

	@RequestMapping(value = "/getCompanyBySegmentAndRegion/{segmentId}/{regionId}", method = RequestMethod.GET)
	public @ResponseBody List<CompanyBO> getCompanyBySegmentAndRegion(
			@PathVariable Integer segmentId, @PathVariable Integer regionId) {
		return creditSubmit.getCompanyBySegmentRegion(segmentId, regionId);
	}

	@RequestMapping(value = "/getBusinessAreaBySegmentRegionAndCompany/{segmentId}/{regionId}/{companyId}", method = RequestMethod.GET)
	public @ResponseBody List<BusinessAreaBO> getBusinessAreaBySegmentRegionAndCompany(
			@PathVariable Integer segmentId, @PathVariable Integer regionId,
			@PathVariable Integer companyId) {
		return creditSubmit.getBABySegementRegionCompany(segmentId, regionId,
				companyId);
	}

	@RequestMapping(value = "/getBaccMapData", method = RequestMethod.GET)
	public @ResponseBody List<BaccMapBO> getBaccMapData() {
		return adminService.getBaccMapData();
	}

	@RequestMapping(value = "/isSegmentExists/{segmentName}", method = RequestMethod.POST)
	public @ResponseBody boolean isSegmentExists(
			@PathVariable String segmentName) {
		return adminService.isSegmentExists(segmentName);
	}

	@RequestMapping(value = "/isRegionExists/{regionName}", method = RequestMethod.POST)
	public @ResponseBody boolean isRegionExists(@PathVariable String regionName) {
		return adminService.isRegionExists(regionName);
	}

	@RequestMapping(value = "/isCompanyExists/{companyName}", method = RequestMethod.POST)
	public @ResponseBody boolean isCompanyExists(
			@PathVariable String companyName) {
		return adminService.isCompanyExists(companyName);
	}

	@RequestMapping(value = "/isBusinessAreaExists/{businessAreaName}", method = RequestMethod.POST)
	public @ResponseBody boolean isBusinessAreaExists(
			@PathVariable String businessAreaName) {
		return adminService.isBusinessAreaExists(businessAreaName);
	}

	@RequestMapping(value = "/updateBaccMapGroupData/{segmentName}/{regionName}/{companyCode}/{companyName}/{businessAreaCode}/{businessAreaName}", method = RequestMethod.POST)
	public @ResponseBody List<BaccMapBO> updateBaccMapGroupData(
			@RequestBody BaccMapBO baccMapBO,@PathVariable String segmentName,
			@PathVariable String regionName, @PathVariable String companyCode,
			@PathVariable String companyName,
			@PathVariable String businessAreaCode,
			@PathVariable String businessAreaName) {
		
		if (baccMapBO.getSegmentBO().getSegmentId() == 0) {
			SegmentBO segmentBO = new SegmentBO();
			segmentBO.setSegmentName(segmentName);
			baccMapBO.setSegmentBO(segmentBO);
		}

		if (baccMapBO.getRegionBO().getRegionId() == 0) {
			RegionBO regionBO = new RegionBO();
			regionBO.setRegionName(regionName);
			baccMapBO.setRegionBO(regionBO);
		}
		if (baccMapBO.getCompanyBO().getCompanyId() == 0) {
			CompanyBO companyBO = new CompanyBO();
			companyBO.setCompanyCode(companyCode);
			companyBO.setCompanyName(companyName);
			baccMapBO.setCompanyBO(companyBO);

		}
		if (baccMapBO.getBusinessAreaBO().getBusinessAreaId() == 0) {
			BusinessAreaBO businessAreaBO = new BusinessAreaBO();
			businessAreaBO.setBusinessAreaCode(businessAreaCode);
			businessAreaBO.setBusinessAreaName(businessAreaName);
			baccMapBO.setBusinessAreaBO(businessAreaBO);
			
		}
		adminService.updateBaccMap(baccMapBO);

		return adminService.getBaccMapData();

	}

	@RequestMapping(value = "/addBaccMapBaccMapGroupData/{segmentName}/{regionName}/{companyCode}/{companyName}/{businessAreaCode}/{businessAreaName}", method = RequestMethod.POST)
	public @ResponseBody List<BaccMapBO> addBaccMapGroupData(
			@RequestBody BaccMapBO baccMapBO, @PathVariable String segmentName,
			@PathVariable String regionName, @PathVariable String companyCode,
			@PathVariable String companyName,
			@PathVariable String businessAreaCode,
			@PathVariable String businessAreaName) {
		if (baccMapBO.getSegmentBO().getSegmentId() == 0) {
			SegmentBO segmentBO = new SegmentBO();
			segmentBO.setSegmentName(segmentName);
			baccMapBO.setSegmentBO(segmentBO);
		}

		if (baccMapBO.getRegionBO().getRegionId() == 0) {
			RegionBO regionBO = new RegionBO();
			regionBO.setRegionName(regionName);
			baccMapBO.setRegionBO(regionBO);
		}
		if (baccMapBO.getCompanyBO().getCompanyId() == 0) {
			CompanyBO companyBO = new CompanyBO();
			companyBO.setCompanyCode(companyCode);
			companyBO.setCompanyName(companyName);
			baccMapBO.setCompanyBO(companyBO);

		}
		if (baccMapBO.getBusinessAreaBO().getBusinessAreaId() == 0) {
			BusinessAreaBO businessAreaBO = new BusinessAreaBO();
			businessAreaBO.setBusinessAreaCode(businessAreaCode);
			businessAreaBO.setBusinessAreaName(businessAreaName);
			baccMapBO.setBusinessAreaBO(businessAreaBO);
		}

		adminService.addBaccMap(baccMapBO);
		return adminService.getBaccMapData();
	}

	@RequestMapping(value = "/deleteBaccMapGroupData", method = RequestMethod.POST)
	public @ResponseBody List<BaccMapBO> deleteBaccMapGroupData(
			@RequestBody BaccMapBO baccMapBO) {
		adminService.deleteBaccMap(baccMapBO);
		// adminService.deleteBusinessArea(businessAreaBO);
		return adminService.getBaccMapData();

	}

	@RequestMapping(value = "/getBaccMapDetails", method = RequestMethod.GET)
	public @ResponseBody CreditRequestBO getBaccMapDetails(HttpSession session) {
		CreditRequestBO creditRequestBO = new CreditRequestBO();

		creditRequestBO.setSegmentBOs(getAllSegmentList());
		creditRequestBO.setRegionBOs(getAllRegions());
		creditRequestBO.setCompanyBOs(getAllCompanys());
		creditRequestBO.setBusinessAreaBOs(getAllBusinessAreas());

		return creditRequestBO;
	}

	public List<SegmentBO> getAllSegmentList() {
		List<SegmentBO> segmentBOs = new ArrayList<SegmentBO>();
		SegmentBO segmentBO = new SegmentBO();
		segmentBO.setSegmentId(0);
		segmentBO.setSegmentName("OTHERS");
		segmentBOs = creditSubmit.getAllSegments();
		segmentBOs.add(segmentBO);
		return segmentBOs;
	}

	public List<RegionBO> getAllRegions() {
		List<RegionBO> regionBOs = new ArrayList<RegionBO>();
		RegionBO regionBO = new RegionBO();
		regionBO.setRegionId(0);
		regionBO.setRegionName("OTHERS");

		regionBOs = creditSubmit.getAllRegions();
		regionBOs.add(regionBO);

		return regionBOs;
	}

	public List<CompanyBO> getAllCompanys() {
		List<CompanyBO> companyBOs = new ArrayList<CompanyBO>();
		CompanyBO companyBO = new CompanyBO();
		companyBO.setCompanyId(0);
		companyBO.setCompanyCode("OTHERS");
		companyBO.setCompanyName("");

		companyBOs = creditSubmit.getAllCompanys();
		companyBOs.add(companyBO);

		return companyBOs;
	}

	public List<BusinessAreaBO> getAllBusinessAreas() {
		List<BusinessAreaBO> businessAreaBOs = new ArrayList<BusinessAreaBO>();
		BusinessAreaBO businessAreaBO = new BusinessAreaBO();
		businessAreaBO.setBusinessAreaId(0);
		businessAreaBO.setBusinessAreaCode("OTHERS");
		businessAreaBO.setBusinessAreaName("");

		businessAreaBOs = creditSubmit.getAllBusinessArea();
		businessAreaBOs.add(businessAreaBO);

		return businessAreaBOs;
	}

}
