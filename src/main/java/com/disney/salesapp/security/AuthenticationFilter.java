package com.disney.salesapp.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.disney.salesapp.model.UserInfoLite;
import com.disney.salesapp.services.UserAuthorizationService;
import com.disney.salesapp.utils.WorkflowUtils;


@WebFilter(urlPatterns = { "/*" })
public class AuthenticationFilter implements Filter {

	private HttpSession session;

	Logger log = Logger.getLogger(AuthenticationFilter.class);

	UserAuthorizationService userService = null;

	String siteminderurl = null;

	public void init(FilterConfig config) {
		final WebApplicationContext springContext = WebApplicationContextUtils
				.getWebApplicationContext(config.getServletContext());
		userService = (UserAuthorizationService) springContext
				.getBean("UserAuthorizationService");

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		siteminderurl = userService.getSiteminderurl();

		HttpServletRequest request = (HttpServletRequest) req;

		HttpServletResponse response = (HttpServletResponse) res;
		request.setCharacterEncoding("UTF-8");
		session = request.getSession();

		String userIdFromSiteMinder = (String) request.getHeader("SMUSER");
		UserInfoLite userInfo = null;

		if (!userService.isSecured()) {
			chain.doFilter(req, res);
			return;
		}
		/*session.invalidate();
		session = request.getSession(true);*/
		setTimeZoneInSession(request);

		userInfo = (UserInfoLite) session.getAttribute("loggedInUser");
		if (userIdFromSiteMinder != null && userInfo != null
				&& userInfo.getPortalId() != null) {
			log.trace("existing session " + request.getRequestURI()
					+ " Userfrom SM:" + userIdFromSiteMinder
					+ ", user from session " + userInfo.getPortalId());
			if (!userInfo.getPortalId().equalsIgnoreCase(userIdFromSiteMinder)) {

				session.invalidate();
				session = request.getSession(true);

			}

		}

		if (session.isNew()) {

			log.debug("new session " + request.getRequestURI() + " User:"
					+ userIdFromSiteMinder);

			if (!isTimeZoneAvailable(request)) {

				String protocol = WorkflowUtils.getProtocol(request);

				String baseURL = protocol + req.getServerName()
						+ request.getContextPath();
				log.debug("baseURL=" + baseURL);
				response.sendRedirect(baseURL + "/start.jsp?refrerer="
						+ request.getRequestURI());

			}
			if (userIdFromSiteMinder != null) {
				// user token available in request header meaning user has
				// signed on thru login page
				// log.debug("new session user available "+user);
				
				/*userInfo = getUserDetails(userIdFromSiteMinder);

				if (isUserAuthorizedForPage(userInfo, request)) {
					// log.debug("new session, user available, isUserAuthorizedForPage=true "+user);
					// user is in ITSDB, set user Object in session
					session.setAttribute(
							ITSCommonConstants.SESSION_ATTR_USERID, userInfo);
					chain.doFilter(req, res);
					return;

				} else {*/
					// site minder authenticated but user is not authorized for
					// ITS
					// log.debug("new session, user available, isUserAuthorizedForPage=false "+user);
					log.debug(" Not Authorized for ITS " + userIdFromSiteMinder);
					accessDenied(response, request);
					return;
				//}
			} else {
				log.debug(" Not through siteminder" + userIdFromSiteMinder);
				// log.debug("new session, user null, isUserAuthorizedForPage=false "+user);
//				doLogin(req, response, request);
				doLogin(response);
				return;
				// user token not available in request header meaning user has
				// not logged in via siteminder

			}

		} else {
			// log.debug("existing session "+request .getRequestURI());
			log.trace("existing session " + request.getRequestURI()
					+ " Userfrom SM:" + userIdFromSiteMinder);
			userInfo = (UserInfoLite) session.getAttribute("loggedInUser");
			/*if (isUserAuthorizedForPage(userInfo, request)) {
				// log.debug("user found"+userInfo.getPortalId()+" url: "+request.getRequestURI());
				// user is in session
				chain.doFilter(req, res);
				return;
			} else {*/
				// log.debug("existing session userinfo null isUserAuthorizedForPage=false");
				log.debug(userIdFromSiteMinder + " not Authorized for ITS "
						+ request.getRequestURI());
				accessDenied(response, request);

			//}

		}

	}

	private boolean isTimeZoneAvailable(HttpServletRequest request) {
		if (request.getRequestURI() != null
				&& request.getRequestURI().endsWith(".jsp") != true)
			return true;
		if (request.getSession().getAttribute("usertimezone") != null)
			return true;
		else
			return false;
	}

	private void setTimeZoneInSession(HttpServletRequest request) {

		if (session.getAttribute("usertimezone") == null) {
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    if (cookies[i].getName() != null && cookies[i].getName().equals("timezoneCookie")
                            && cookies[i].getValue() != null) {
                        session.setAttribute("usertimezone", cookies[i].getValue());
                        break;
                    }
                }
			}
		}
	}

	@Override
	public void destroy() {

	}

	/*private void doLogin(ServletRequest request, HttpServletResponse response,
			HttpServletRequest req) throws ServletException, IOException {

		response.sendRedirect(siteminderurl);
	}*/
	
	private void doLogin(HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect(siteminderurl);
    }

	private void accessDenied(HttpServletResponse response, HttpServletRequest req)
			throws ServletException, IOException {
		String protocol = null;
		protocol = WorkflowUtils.getProtocol(req);
		String baseURL = protocol + req.getServerName() + req.getContextPath();

		response.sendRedirect(baseURL + "/accessdenied.html");

	}

}
