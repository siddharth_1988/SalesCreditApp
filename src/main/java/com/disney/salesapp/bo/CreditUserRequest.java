package com.disney.salesapp.bo;

import java.util.List;

public class CreditUserRequest {
	private Integer creditRequestId;
	private String portalId;
	private String requestorName;
	private String requestorEmail;
	private String requesterDate;
	private String aeFirstName;
	private String aeLastName;
	private String aeEmail;
	private List<SegmentBO> segmentBOs;
	private List<RegionBO> regionBOs;
	private List<CompanyBO> companyBOs;
	private List<BusinessAreaBO> businessAreaBOs;
	private List<COBAListBO> cobaListBOs;
	private List<CountryBO> countryBOs;
	private List<StateBO> stateBOs;
	private List<CurrencyBO> currencyBOs;

	public List<SegmentBO> getSegmentBOs() {
		return segmentBOs;
	}

	public void setSegmentBOs(List<SegmentBO> segmentBOs) {
		this.segmentBOs = segmentBOs;
	}

	public List<RegionBO> getRegionBOs() {
		return regionBOs;
	}

	public void setRegionBOs(List<RegionBO> regionBOs) {
		this.regionBOs = regionBOs;
	}

	public List<CompanyBO> getCompanyBOs() {
		return companyBOs;
	}

	public void setCompanyBOs(List<CompanyBO> companyBOs) {
		this.companyBOs = companyBOs;
	}

	public List<BusinessAreaBO> getBusinessAreaBOs() {
		return businessAreaBOs;
	}

	public void setBusinessAreaBOs(List<BusinessAreaBO> businessAreaBOs) {
		this.businessAreaBOs = businessAreaBOs;
	}

	public List<COBAListBO> getCobaListBOs() {
		return cobaListBOs;
	}

	public void setCobaListBOs(List<COBAListBO> cobaListBOs) {
		this.cobaListBOs = cobaListBOs;
	}

	public List<CountryBO> getCountryBOs() {
		return countryBOs;
	}

	public void setCountryBOs(List<CountryBO> countryBOs) {
		this.countryBOs = countryBOs;
	}

	public List<StateBO> getStateBOs() {
		return stateBOs;
	}

	public void setStateBOs(List<StateBO> stateBOs) {
		this.stateBOs = stateBOs;
	}

	public List<CurrencyBO> getCurrencyBOs() {
		return currencyBOs;
	}

	public void setCurrencyBOs(List<CurrencyBO> currencyBOs) {
		this.currencyBOs = currencyBOs;
	}

	public Integer getCreditRequestId() {
		return creditRequestId;
	}

	public void setCreditRequestId(Integer creditRequestId) {
		this.creditRequestId = creditRequestId;
	}

	public String getPortalId() {
		return portalId;
	}

	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequesterName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getRequesterDate() {
		return requesterDate;
	}

	public void setRequesterDate(String requesterDate) {
		this.requesterDate = requesterDate;
	}

	public String getAeLastName() {
		return aeLastName;
	}

	public void setAeLastName(String aeLastName) {
		this.aeLastName = aeLastName;
	}

	public String getAeFirstName() {
		return aeFirstName;
	}

	public void setAeFirstName(String aeFirstName) {
		this.aeFirstName = aeFirstName;
	}

	public String getAeEmail() {
		return aeEmail;
	}

	public void setAeEmail(String aeEmail) {
		this.aeEmail = aeEmail;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

}
