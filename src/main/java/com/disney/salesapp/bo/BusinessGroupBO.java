package com.disney.salesapp.bo;

import java.io.Serializable;

/**
 * The Class BusinessGroupBO.
 */
public class BusinessGroupBO implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8370416432706313553L;

    /** The business group id. */
    private Integer businessGroupId;
    
    /** The business group name. */
    private String businessGroupName;


    /**
     * Gets the business group id.
     *
     * @return the business group id
     */
    public Integer getBusinessGroupId() {
        return businessGroupId;
    }

    /**
     * Sets the business group id.
     *
     * @param businessGroupId the new business group id
     */
    public void setBusinessGroupId(Integer businessGroupId) {
        this.businessGroupId = businessGroupId;
    }

    /**
     * Gets the business group name.
     *
     * @return the business group name
     */
    public String getBusinessGroupName() {
        return businessGroupName;
    }

    /**
     * Sets the business group name.
     *
     * @param businessGroupName the new business group name
     */
    public void setBusinessGroupName(String businessGroupName) {
        this.businessGroupName = businessGroupName;
    }
    
    
}
