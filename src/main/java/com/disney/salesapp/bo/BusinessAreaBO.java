package com.disney.salesapp.bo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The Class BusinessAreaBO.
 */
public class BusinessAreaBO implements Serializable {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2611126224586302935L;

    /** The business area id. */
    private Integer businessAreaId;
    
    /** The business area code. */
    private String businessAreaCode;
    
    /** The business area name. */
    private String businessAreaName;
    
    private Set<BaccMapBO> baccMapBOList = new HashSet<BaccMapBO>();

    /**
     * Gets the business area id.
     *
     * @return the business area id
     */
    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    /**
     * Sets the business area id.
     *
     * @param businessAreaId the new business area id
     */
    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    /**
     * Gets the business area code.
     *
     * @return the business area code
     */
    public String getBusinessAreaCode() {
        return businessAreaCode;
    }

    /**
     * Sets the business area code.
     *
     * @param businessAreaCode the new business area code
     */
    public void setBusinessAreaCode(String businessAreaCode) {
        this.businessAreaCode = businessAreaCode;
    }

    /**
     * Gets the business area name.
     *
     * @return the business area name
     */
    public String getBusinessAreaName() {
        return businessAreaName;
    }

    /**
     * Sets the business area name.
     *
     * @param businessAreaName the new business area name
     */
    public void setBusinessAreaName(String businessAreaName) {
        this.businessAreaName = businessAreaName;
    }

    /**
	 * @return the baccMapBOList
	 */
	public Set<BaccMapBO> getBaccMapBOList() {
		return baccMapBOList;
	}

	/**
	 * @param baccMapBOList the baccMapBOList to set
	 */
	public void setBaccMapBOList(Set<BaccMapBO> baccMapBOList) {
		this.baccMapBOList = baccMapBOList;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((businessAreaId == null) ? 0 : businessAreaId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BusinessAreaBO other = (BusinessAreaBO) obj;
        if (businessAreaId == null) {
            if (other.businessAreaId != null)
                return false;
        } else if (!businessAreaId.equals(other.businessAreaId))
            return false;
        return true;
    }
    

}
