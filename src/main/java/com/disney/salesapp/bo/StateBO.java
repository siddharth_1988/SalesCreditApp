package com.disney.salesapp.bo;

import java.io.Serializable;

public class StateBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2353813457449566234L;
	private Integer countryId;
	private String stateName;
	private Integer stateId;

	public Integer getCountryId() {
		return countryId;
	}

	public String getStateName() {
		return stateName;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

}
