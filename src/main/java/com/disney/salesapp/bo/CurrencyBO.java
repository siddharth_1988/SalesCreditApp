package com.disney.salesapp.bo;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class CurrencyBO.
 */
public class CurrencyBO implements Serializable {
	
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6386619805921434385L;
    
    /** The currency id. */
    private Integer currencyId;
	
	/** The currency name. */
	private String currencyName;
	
	/** The currency code. */
	private String currencyCode;
    
    /**
     * Gets the currency id.
     *
     * @return the currency id
     */
    public Integer getCurrencyId() {
        return currencyId;
    }

    /**
     * Sets the currency id.
     *
     * @param currencyId the new currency id
     */
    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * Gets the currency name.
     *
     * @return the currency name
     */
    public String getCurrencyName() {
        return currencyName;
    }
    
    /**
     * Sets the currency name.
     *
     * @param currencyName the new currency name
     */
    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
    
    /**
     * Gets the currency code.
     *
     * @return the currency code
     */
    public String getCurrencyCode() {
        return currencyCode;
    }
    
    /**
     * Sets the currency code.
     *
     * @param currencyCode the new currency code
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

	
}
