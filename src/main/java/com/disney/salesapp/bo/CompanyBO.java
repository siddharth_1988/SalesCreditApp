package com.disney.salesapp.bo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The Class CompanyBO.
 */
public class CompanyBO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8920791098964591968L;

	/** The comapany id. */
	private Integer companyId;

	/** The company code. */
	private String companyCode;

	/** The company name. */
	private String companyName;

	private Set<BaccMapBO> baccMapBOList = new HashSet<BaccMapBO>();

	// private Set<BusinessGroupMapBO> businessGroupMaps = new
	// HashSet<BusinessGroupMapBO>();

	/**
	 * Gets the comapany id.
	 *
	 * @return the comapany id
	 */
	public Integer getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the comapany id.
	 *
	 * @param comapanyId
	 *            the new comapany id
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	/**
	 * Gets the company code.
	 *
	 * @return the company code
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * Sets the company code.
	 *
	 * @param companyCode
	 *            the new company code
	 */
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the company name.
	 *
	 * @param companyName
	 *            the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the baccMapBOList
	 */
	public Set<BaccMapBO> getBaccMapBOList() {
		return baccMapBOList;
	}

	/**
	 * @param baccMapBOList the baccMapBOList to set
	 */
	public void setBaccMapBOList(Set<BaccMapBO> baccMapBOList) {
		this.baccMapBOList = baccMapBOList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((companyId == null) ? 0 : companyId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompanyBO other = (CompanyBO) obj;
		if (companyId == null) {
			if (other.companyId != null)
				return false;
		} else if (!companyId.equals(other.companyId))
			return false;
		return true;
	}

}
