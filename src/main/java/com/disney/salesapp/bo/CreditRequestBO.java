/**
 * 
 */
package com.disney.salesapp.bo;

import java.io.File;
import java.util.List;

/**
 * @author GHV001
 *
 */
public class CreditRequestBO {
	
	private Integer creditRequestId;
	private String portalId;
	private String name;
	private String email;
	private String customerName;
	private String streetAddress;
	private String city;
	private String state;
	private String zipCode;
	private String country;

	private List<SegmentBO> segmentBOs;
	private List<RegionBO> regionBOs;
	private List<CompanyBO> companyBOs;
	private List<BusinessAreaBO> businessAreaBOs;
	private List<COBAListBO> cobaListBOs;
	private List<CountryBO> countryBOs;
	private List<StateBO> stateBOs;
	private List<CurrencyBO> currencyBOs;
	
	private SegmentBO segmentBO;
	private RegionBO regionBO;
	private CompanyBO companyBO;
	private BusinessAreaBO businessAreaBO;
	private BusinessGroupBO businessGroupBO;

	/* Parameters for AdSales (No Setup) page */
	private CurrencyBO currencyBO;
	private String requestorName;
	private String requestorEmail;
	private String requestNeededByDate;
	private Integer requestedCreditLimit;
	private String termStartDate;
	private String termEndDate;
	private String companyName;
	private String brandName;
	private String companyWebAddress;
	private String companyStreetAddress;
	private String companyCity;
	private StateBO companyState;
	private String companyState1;
	private String companyZip;
	private CountryBO companyCountry;
	private Integer companyTelephone;
	private String companyContactName;
	private String contactTitle;
	private String companyContactEmailAddress;
	private boolean okayToContact;
	private String agencyName;
	private String agencyWebAddress;
	private String agencyStreetAddress;
	private String agencyCity;
	private StateBO agencyState;
	private String agencyState1;
	private String agencyZip;
	private CountryBO agencyCountry;
	private Integer agencyPhone;
	private String agencyContact;
	private File aAgencyBillingExport;

	private String accountExecutive;
	private String cContactPhone;
	private String cContactEmail;
	private String cContactFirstName;
	private boolean cashAdvance;
	private boolean direct;
	private File attachment;
	private String fohIdAdv;
	private String fohAgency;

	/* AD SALES Setup */
	private String agencyCommissionRate;
	private boolean isCommissionRateApproved;
	private boolean isTradeBillPayIndicator;
	private boolean isPoliticalBuy;
	private boolean localOrNational;
	private boolean isCapNet;
	private boolean isWebBilling;
	private String productCode;
	private String revenueCode1;
	private String revenueCode2;
	private String revenueCode3;
	private String priorityCode;
	private String demographic;
	private String dealOrderType;
	private String notes;
	private String creditReqCurrency;
	
	private String aeLastName;
	

	/* EMEA AD SALES */
	private String outstandingArAmount;

	
	/**
	 * @return the creditRequestId
	 */
	public Integer getCreditRequestId() {
		return creditRequestId;
	}

	/**
	 * @param creditRequestId the creditRequestId to set
	 */
	public void setCreditRequestId(Integer creditRequestId) {
		this.creditRequestId = creditRequestId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName
	 *            the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress
	 *            the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the requestorName
	 */
	public String getRequestorName() {
		return requestorName;
	}

	/**
	 * @param requestorName
	 *            the requestorName to set
	 */
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	/**
	 * @return the requestorEmail
	 */
	public String getRequestorEmail() {
		return requestorEmail;
	}

	/**
	 * @param requestorEmail
	 *            the requestorEmail to set
	 */
	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	/**
	 * @return the requestNeededByDate
	 */
	public String getRequestNeededByDate() {
		return requestNeededByDate;
	}

	/**
	 * @param requestNeededByDate
	 *            the requestNeededByDate to set
	 */
	public void setRequestNeededByDate(String requestNeededByDate) {
		this.requestNeededByDate = requestNeededByDate;
	}

	/**
	 * @return the requestedCreditLimit
	 */
	public Integer getRequestedCreditLimit() {
		return requestedCreditLimit;
	}

	/**
	 * @param requestedCreditLimit
	 *            the requestedCreditLimit to set
	 */
	public void setRequestedCreditLimit(Integer requestedCreditLimit) {
		this.requestedCreditLimit = requestedCreditLimit;
	}

	/**
	 * @return the termStartDate
	 */
	public String getTermStartDate() {
		return termStartDate;
	}

	/**
	 * @param termStartDate
	 *            the termStartDate to set
	 */
	public void setTermStartDate(String termStartDate) {
		this.termStartDate = termStartDate;
	}

	/**
	 * @return the termEndDate
	 */
	public String getTermEndDate() {
		return termEndDate;
	}

	/**
	 * @param termEndDate
	 *            the termEndDate to set
	 */
	public void setTermEndDate(String termEndDate) {
		this.termEndDate = termEndDate;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param companyName
	 *            the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName
	 *            the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the companyWebAddress
	 */
	public String getCompanyWebAddress() {
		return companyWebAddress;
	}

	/**
	 * @param companyWebAddress
	 *            the companyWebAddress to set
	 */
	public void setCompanyWebAddress(String companyWebAddress) {
		this.companyWebAddress = companyWebAddress;
	}

	/**
	 * @return the companyStreetAddress
	 */
	public String getCompanyStreetAddress() {
		return companyStreetAddress;
	}

	/**
	 * @param companyStreetAddress
	 *            the companyStreetAddress to set
	 */
	public void setCompanyStreetAddress(String companyStreetAddress) {
		this.companyStreetAddress = companyStreetAddress;
	}

	/**
	 * @return the companyCity
	 */
	public String getCompanyCity() {
		return companyCity;
	}

	/**
	 * @param companyCity
	 *            the companyCity to set
	 */
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	/**
	 * @return the companyState
	 */
	public StateBO getCompanyState() {
		return companyState;
	}

	/**
	 * @param companyState the companyState to set
	 */
	public void setCompanyState(StateBO companyState) {
		this.companyState = companyState;
	}

	/**
	 * @return the companyCountry
	 */
	public CountryBO getCompanyCountry() {
		return companyCountry;
	}

	/**
	 * @param companyCountry the companyCountry to set
	 */
	public void setCompanyCountry(CountryBO companyCountry) {
		this.companyCountry = companyCountry;
	}

	/**
	 * @return the companyState1
	 */
	public String getCompanyState1() {
		return companyState1;
	}

	/**
	 * @param companyState1
	 *            the companyState1 to set
	 */
	public void setCompanyState1(String companyState1) {
		this.companyState1 = companyState1;
	}

	/**
	 * @return the companyZip
	 */
	public String getCompanyZip() {
		return companyZip;
	}

	/**
	 * @param companyZip
	 *            the companyZip to set
	 */
	public void setCompanyZip(String companyZip) {
		this.companyZip = companyZip;
	}

	/**
	 * @return the companyTelephone
	 */
	public Integer getCompanyTelephone() {
		return companyTelephone;
	}

	/**
	 * @param companyTelephone
	 *            the companyTelephone to set
	 */
	public void setCompanyTelephone(Integer companyTelephone) {
		this.companyTelephone = companyTelephone;
	}

	/**
	 * @return the companyContactName
	 */
	public String getCompanyContactName() {
		return companyContactName;
	}

	/**
	 * @param companyContactName
	 *            the companyContactName to set
	 */
	public void setCompanyContactName(String companyContactName) {
		this.companyContactName = companyContactName;
	}

	/**
	 * @return the contactTitle
	 */
	public String getContactTitle() {
		return contactTitle;
	}

	/**
	 * @param contactTitle
	 *            the contactTitle to set
	 */
	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	/**
	 * @return the companyContactEmailAddress
	 */
	public String getCompanyContactEmailAddress() {
		return companyContactEmailAddress;
	}

	/**
	 * @param companyContactEmailAddress
	 *            the companyContactEmailAddress to set
	 */
	public void setCompanyContactEmailAddress(String companyContactEmailAddress) {
		this.companyContactEmailAddress = companyContactEmailAddress;
	}

	/**
	 * @return the okayToContact
	 */
	public boolean isOkayToContact() {
		return okayToContact;
	}

	/**
	 * @param okayToContact
	 *            the okayToContact to set
	 */
	public void setOkayToContact(boolean okayToContact) {
		this.okayToContact = okayToContact;
	}

	/**
	 * @return the agencyName
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * @param agencyName
	 *            the agencyName to set
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	/**
	 * @return the agencyWebAddress
	 */
	public String getAgencyWebAddress() {
		return agencyWebAddress;
	}

	/**
	 * @param agencyWebAddress
	 *            the agencyWebAddress to set
	 */
	public void setAgencyWebAddress(String agencyWebAddress) {
		this.agencyWebAddress = agencyWebAddress;
	}

	/**
	 * @return the agencyStreetAddress
	 */
	public String getAgencyStreetAddress() {
		return agencyStreetAddress;
	}

	/**
	 * @param agencyStreetAddress
	 *            the agencyStreetAddress to set
	 */
	public void setAgencyStreetAddress(String agencyStreetAddress) {
		this.agencyStreetAddress = agencyStreetAddress;
	}

	/**
	 * @return the agencyCity
	 */
	public String getAgencyCity() {
		return agencyCity;
	}

	/**
	 * @param agencyCity
	 *            the agencyCity to set
	 */
	public void setAgencyCity(String agencyCity) {
		this.agencyCity = agencyCity;
	}

	/**
	 * @return the agencyState
	 */
	public StateBO getAgencyState() {
		return agencyState;
	}

	/**
	 * @param agencyState the agencyState to set
	 */
	public void setAgencyState(StateBO agencyState) {
		this.agencyState = agencyState;
	}

	/**
	 * @return the agencyCountry
	 */
	public CountryBO getAgencyCountry() {
		return agencyCountry;
	}

	/**
	 * @param agencyCountry the agencyCountry to set
	 */
	public void setAgencyCountry(CountryBO agencyCountry) {
		this.agencyCountry = agencyCountry;
	}

	/**
	 * @return the agencyState1
	 */
	public String getAgencyState1() {
		return agencyState1;
	}

	/**
	 * @param agencyState1
	 *            the agencyState1 to set
	 */
	public void setAgencyState1(String agencyState1) {
		this.agencyState1 = agencyState1;
	}

	/**
	 * @return the agencyZip
	 */
	public String getAgencyZip() {
		return agencyZip;
	}

	/**
	 * @param agencyZip
	 *            the agencyZip to set
	 */
	public void setAgencyZip(String agencyZip) {
		this.agencyZip = agencyZip;
	}

	/**
	 * @return the agencyPhone
	 */
	public Integer getAgencyPhone() {
		return agencyPhone;
	}

	/**
	 * @param agencyPhone
	 *            the agencyPhone to set
	 */
	public void setAgencyPhone(Integer agencyPhone) {
		this.agencyPhone = agencyPhone;
	}

	/**
	 * @return the agencyContact
	 */
	public String getAgencyContact() {
		return agencyContact;
	}

	/**
	 * @param agencyContact
	 *            the agencyContact to set
	 */
	public void setAgencyContact(String agencyContact) {
		this.agencyContact = agencyContact;
	}

	/**
	 * @return the aAgencyBillingExport
	 */
	public File getaAgencyBillingExport() {
		return aAgencyBillingExport;
	}

	/**
	 * @param aAgencyBillingExport
	 *            the aAgencyBillingExport to set
	 */
	public void setaAgencyBillingExport(File aAgencyBillingExport) {
		this.aAgencyBillingExport = aAgencyBillingExport;
	}

	/**
	 * @return the accountExecutive
	 */
	public String getAccountExecutive() {
		return accountExecutive;
	}

	/**
	 * @param accountExecutive
	 *            the accountExecutive to set
	 */
	public void setAccountExecutive(String accountExecutive) {
		this.accountExecutive = accountExecutive;
	}

	/**
	 * @return the cContactPhone
	 */
	public String getcContactPhone() {
		return cContactPhone;
	}

	/**
	 * @param cContactPhone
	 *            the cContactPhone to set
	 */
	public void setcContactPhone(String cContactPhone) {
		this.cContactPhone = cContactPhone;
	}

	/**
	 * @return the cContactEmail
	 */
	public String getcContactEmail() {
		return cContactEmail;
	}

	/**
	 * @param cContactEmail
	 *            the cContactEmail to set
	 */
	public void setcContactEmail(String cContactEmail) {
		this.cContactEmail = cContactEmail;
	}

	/**
	 * @return the cContactFirstName
	 */
	public String getcContactFirstName() {
		return cContactFirstName;
	}

	/**
	 * @param cContactFirstName
	 *            the cContactFirstName to set
	 */
	public void setcContactFirstName(String cContactFirstName) {
		this.cContactFirstName = cContactFirstName;
	}

	/**
	 * @return the cashAdvance
	 */
	public boolean isCashAdvance() {
		return cashAdvance;
	}

	/**
	 * @param cashAdvance
	 *            the cashAdvance to set
	 */
	public void setCashAdvance(boolean cashAdvance) {
		this.cashAdvance = cashAdvance;
	}

	/**
	 * @return the direct
	 */
	public boolean isDirect() {
		return direct;
	}

	/**
	 * @param direct
	 *            the direct to set
	 */
	public void setDirect(boolean direct) {
		this.direct = direct;
	}

	/**
	 * @return the attachment
	 */
	public File getAttachment() {
		return attachment;
	}

	/**
	 * @param attachment
	 *            the attachment to set
	 */
	public void setAttachment(File attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the fohIdAdv
	 */
	public String getFohIdAdv() {
		return fohIdAdv;
	}

	/**
	 * @param fohIdAdv
	 *            the fohIdAdv to set
	 */
	public void setFohIdAdv(String fohIdAdv) {
		this.fohIdAdv = fohIdAdv;
	}

	/**
	 * @return the fohAgency
	 */
	public String getFohAgency() {
		return fohAgency;
	}

	/**
	 * @param fohAgency
	 *            the fohAgency to set
	 */
	public void setFohAgency(String fohAgency) {
		this.fohAgency = fohAgency;
	}

	/**
	 * @return the agencyCommissionRate
	 */
	public String getAgencyCommissionRate() {
		return agencyCommissionRate;
	}

	public String getPortalId() {
		return portalId;
	}

	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	/**
	 * @param agencyCommissionRate
	 *            the agencyCommissionRate to set
	 */
	public void setAgencyCommissionRate(String agencyCommissionRate) {
		this.agencyCommissionRate = agencyCommissionRate;
	}

	/**
	 * @return the isCommissionRateApproved
	 */
	public boolean isCommissionRateApproved() {
		return isCommissionRateApproved;
	}

	/**
	 * @param isCommissionRateApproved
	 *            the isCommissionRateApproved to set
	 */
	public void setCommissionRateApproved(boolean isCommissionRateApproved) {
		this.isCommissionRateApproved = isCommissionRateApproved;
	}

	/**
	 * @return the isTradeBillPayIndicator
	 */
	public boolean isTradeBillPayIndicator() {
		return isTradeBillPayIndicator;
	}

	/**
	 * @param isTradeBillPayIndicator
	 *            the isTradeBillPayIndicator to set
	 */
	public void setTradeBillPayIndicator(boolean isTradeBillPayIndicator) {
		this.isTradeBillPayIndicator = isTradeBillPayIndicator;
	}

	/**
	 * @return the isPoliticalBuy
	 */
	public boolean isPoliticalBuy() {
		return isPoliticalBuy;
	}

	/**
	 * @param isPoliticalBuy
	 *            the isPoliticalBuy to set
	 */
	public void setPoliticalBuy(boolean isPoliticalBuy) {
		this.isPoliticalBuy = isPoliticalBuy;
	}

	/**
	 * @return the isCapNet
	 */
	public boolean isCapNet() {
		return isCapNet;
	}

	/**
	 * @param isCapNet
	 *            the isCapNet to set
	 */
	public void setCapNet(boolean isCapNet) {
		this.isCapNet = isCapNet;
	}

	/**
	 * @return the isWebBilling
	 */
	public boolean isWebBilling() {
		return isWebBilling;
	}

	/**
	 * @param isWebBilling
	 *            the isWebBilling to set
	 */
	public void setWebBilling(boolean isWebBilling) {
		this.isWebBilling = isWebBilling;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode
	 *            the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the revenueCode1
	 */
	public String getRevenueCode1() {
		return revenueCode1;
	}

	/**
	 * @param revenueCode1
	 *            the revenueCode1 to set
	 */
	public void setRevenueCode1(String revenueCode1) {
		this.revenueCode1 = revenueCode1;
	}

	/**
	 * @return the revenueCode2
	 */
	public String getRevenueCode2() {
		return revenueCode2;
	}

	/**
	 * @param revenueCode2
	 *            the revenueCode2 to set
	 */
	public void setRevenueCode2(String revenueCode2) {
		this.revenueCode2 = revenueCode2;
	}

	/**
	 * @return the revenueCode3
	 */
	public String getRevenueCode3() {
		return revenueCode3;
	}

	/**
	 * @param revenueCode3
	 *            the revenueCode3 to set
	 */
	public void setRevenueCode3(String revenueCode3) {
		this.revenueCode3 = revenueCode3;
	}

	/**
	 * @return the priorityCode
	 */
	public String getPriorityCode() {
		return priorityCode;
	}

	/**
	 * @param priorityCode
	 *            the priorityCode to set
	 */
	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}

	/**
	 * @return the demographic
	 */
	public String getDemographic() {
		return demographic;
	}

	/**
	 * @param demographic
	 *            the demographic to set
	 */
	public void setDemographic(String demographic) {
		this.demographic = demographic;
	}

	/**
	 * @return the dealOrderType
	 */
	public String getDealOrderType() {
		return dealOrderType;
	}

	/**
	 * @param dealOrderType
	 *            the dealOrderType to set
	 */
	public void setDealOrderType(String dealOrderType) {
		this.dealOrderType = dealOrderType;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes
	 *            the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the outstandingArAmount
	 */
	public String getOutstandingArAmount() {
		return outstandingArAmount;
	}

	/**
	 * @param outstandingArAmount
	 *            the outstandingArAmount to set
	 */
	public void setOutstandingArAmount(String outstandingArAmount) {
		this.outstandingArAmount = outstandingArAmount;
	}

	/**
	 * @return the localOrNational
	 */
	public boolean isLocalOrNational() {
		return localOrNational;
	}

	/**
	 * @param localOrNational
	 *            the localOrNational to set
	 */
	public void setLocalOrNational(boolean localOrNational) {
		this.localOrNational = localOrNational;
	}

	/**
	 * @return the segmentBO
	 */
	public SegmentBO getSegmentBO() {
		return segmentBO;
	}

	/**
	 * @param segmentBO the segmentBO to set
	 */
	public void setSegmentBO(SegmentBO segmentBO) {
		this.segmentBO = segmentBO;
	}

	/**
	 * @return the regionBO
	 */
	public RegionBO getRegionBO() {
		return regionBO;
	}

	/**
	 * @param regionBO the regionBO to set
	 */
	public void setRegionBO(RegionBO regionBO) {
		this.regionBO = regionBO;
	}

	/**
	 * @return the companyBO
	 */
	public CompanyBO getCompanyBO() {
		return companyBO;
	}

	/**
	 * @param companyBO the companyBO to set
	 */
	public void setCompanyBO(CompanyBO companyBO) {
		this.companyBO = companyBO;
	}

	/**
	 * @return the businessAreaBO
	 */
	public BusinessAreaBO getBusinessAreaBO() {
		return businessAreaBO;
	}

	/**
	 * @param businessAreaBO the businessAreaBO to set
	 */
	public void setBusinessAreaBO(BusinessAreaBO businessAreaBO) {
		this.businessAreaBO = businessAreaBO;
	}

    /**
     * Gets the segment bos.
     *
     * @return the segment bos
     */
    public List<SegmentBO> getSegmentBOs() {
        return segmentBOs;
    }

    /**
     * Sets the segment bos.
     *
     * @param segmentBOs the new segment b os
     */
    public void setSegmentBOs(List<SegmentBO> segmentBOs) {
        this.segmentBOs = segmentBOs;
    }

    /**
     * Gets the region bos.
     *
     * @return the region bos
     */
    public List<RegionBO> getRegionBOs() {
        return regionBOs;
    }

    /**
     * Sets the region bos.
     *
     * @param regionBOs the new region bos
     */
    public void setRegionBOs(List<RegionBO> regionBOs) {
        this.regionBOs = regionBOs;
    }

    /**
     * Gets the company bos.
     *
     * @return the company bos
     */
    public List<CompanyBO> getCompanyBOs() {
        return companyBOs;
    }

    /**
     * Sets the company bos.
     *
     * @param companyBOs the new company bos
     */
    public void setCompanyBOs(List<CompanyBO> companyBOs) {
        this.companyBOs = companyBOs;
    }

    /**
     * Gets the business area bos.
     *
     * @return the business area bos
     */
    public List<BusinessAreaBO> getBusinessAreaBOs() {
        return businessAreaBOs;
    }

    /**
     * Sets the business area bos.
     *
     * @param businessAreaBOs the new business area bos
     */
    public void setBusinessAreaBOs(List<BusinessAreaBO> businessAreaBOs) {
        this.businessAreaBOs = businessAreaBOs;
    }

    /**
     * Gets the business group bo.
     *
     * @return the business group bo
     */
    public BusinessGroupBO getBusinessGroupBO() {
        return businessGroupBO;
    }

    /**
     * Sets the business group bo.
     *
     * @param businessGroupBO the new business group bo
     */
    public void setBusinessGroupBO(BusinessGroupBO businessGroupBO) {
        this.businessGroupBO = businessGroupBO;
    }

	/**
	 * @return the countryBOs
	 */
	public List<CountryBO> getCountryBOs() {
		return countryBOs;
	}

	/**
	 * @param countryBOs the countryBOs to set
	 */
	public void setCountryBOs(List<CountryBO> countryBOs) {
		this.countryBOs = countryBOs;
	}

	/**
	 * @return the stateBOs
	 */
	public List<StateBO> getStateBOs() {
		return stateBOs;
	}

	/**
	 * @param stateBOs the stateBOs to set
	 */
	public void setStateBOs(List<StateBO> stateBOs) {
		this.stateBOs = stateBOs;
	}

    /**
     * Gets the coba list b os.
     *
     * @return the coba list b os
     */
    public List<COBAListBO> getCobaListBOs() {
        return cobaListBOs;
    }

    /**
     * Sets the coba list b os.
     *
     * @param cobaListBOs the new coba list b os
     */
    public void setCobaListBOs(List<COBAListBO> cobaListBOs) {
        this.cobaListBOs = cobaListBOs;
    }

    /**
     * Gets the credit req currency.
     *
     * @return the credit req currency
     */
    public String getCreditReqCurrency() {
        return creditReqCurrency;
    }

    /**
     * Sets the credit req currency.
     *
     * @param creditReqCurrency the new credit req currency
     */
    public void setCreditReqCurrency(String creditReqCurrency) {
        this.creditReqCurrency = creditReqCurrency;
    }

    /**
     * Gets the currency bos.
     *
     * @return the currency bos
     */
    public List<CurrencyBO> getCurrencyBOs() {
        return currencyBOs;
    }

    /**
     * Sets the currency bos.
     *
     * @param currencyBOs the new currency bos
     */
    public void setCurrencyBOs(List<CurrencyBO> currencyBOs) {
        this.currencyBOs = currencyBOs;
    }

    /**
     * Gets the currency bo.
     *
     * @return the currency bo
     */
    public CurrencyBO getCurrencyBO() {
        return currencyBO;
    }

    /**
     * Sets the currency bo.
     *
     * @param currencyBO the new currency bo
     */
    public void setCurrencyBO(CurrencyBO currencyBO) {
        this.currencyBO = currencyBO;
    }

	public String getAeLastName() {
		return aeLastName;
	}

	public void setAeLastName(String aeLastName) {
		this.aeLastName = aeLastName;
	}


    
	
}
