package com.disney.salesapp.bo;


public class COBAListBO {

	private Integer cobaListId;
	private String cuDescription;
	private String coba;
	private SegmentBO segmentBO;
	
	/**
	 * Gets the cu description.
	 *
	 * @return the cu description
	 */
	public String getCuDescription() {
        return cuDescription;
    }

    /**
     * Sets the cu description.
     *
     * @param cuDescription the new cu description
     */
    public void setCuDescription(String cuDescription) {
        this.cuDescription = cuDescription;
    }

    /**
	 * Gets the coba list id.
	 *
	 * @return the coba list id
	 */
	public Integer getCobaListId() {
        return cobaListId;
    }
    
    /**
     * Sets the coba list id.
     *
     * @param cobaListId the new coba list id
     */
    public void setCobaListId(Integer cobaListId) {
        this.cobaListId = cobaListId;
    }
    

    /**
	 * @return the segmentBO
	 */
	public SegmentBO getSegmentBO() {
		return segmentBO;
	}
	/**
	 * @param segmentBO the segmentBO to set
	 */
	public void setSegmentBO(SegmentBO segmentBO) {
		this.segmentBO = segmentBO;
	}

    /**
     * Gets the coba.
     *
     * @return the coba
     */
    public String getCoba() {
        return coba;
    }

    /**
     * Sets the coba.
     *
     * @param coba the new coba
     */
    public void setCoba(String coba) {
        this.coba = coba;
    }

	
}
