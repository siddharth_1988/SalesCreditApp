package com.disney.salesapp.bo;

import java.io.Serializable;

/**
 * The Class SegmentBO.
 */
public class SegmentBO implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5089907893375984772L;
    
    /** The segment id. */
    private Integer segmentId;
    
    /** The Segment name. */
    private String segmentName;
    
    /**
     * Gets the segment id.
     *
     * @return the segment id
     */
    public Integer getSegmentId() {
        return segmentId;
    }
    
    /**
     * Sets the segment id.
     *
     * @param segmentId the new segment id
     */
    public void setSegmentId(Integer segmentId) {
        this.segmentId = segmentId;
    }

	/**
	 * Gets the segment name.
	 *
	 * @return the segmentName
	 */
	public String getSegmentName() {
		return segmentName;
	}

	/**
	 * Sets the segment name.
	 *
	 * @param segmentName the segmentName to set
	 */
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
    
    
}
