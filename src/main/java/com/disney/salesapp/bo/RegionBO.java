package com.disney.salesapp.bo;

import java.io.Serializable;

/**
 * The Class RegionBO.
 */
public class RegionBO implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7900116129316078950L;

    /** The region id. */
    private Integer regionId;

    /** The region name. */
    private String regionName;

    /**
     * Gets the region id.
     *
     * @return the region id
     */
    public Integer getRegionId() {
        return regionId;
    }

    /**
     * Sets the region id.
     *
     * @param regionId the new region id
     */
    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    /**
     * Gets the region name.
     *
     * @return the region name
     */
    public String getRegionName() {
        return regionName;
    }

    /**
     * Sets the region name.
     *
     * @param regionName the new region name
     */
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((regionId == null) ? 0 : regionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RegionBO other = (RegionBO) obj;
        if (regionId == null) {
            if (other.regionId != null) {
                return false;
            }
        } else if (!regionId.equals(other.regionId)) {
            return false;
        }
        return true;
    }
    
    
}
