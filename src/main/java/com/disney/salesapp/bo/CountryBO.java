package com.disney.salesapp.bo;

import java.io.Serializable;

public class CountryBO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8966271635341080456L;
	private Integer countryId;
	private String countryName;

	public Integer getCountryId() {
		return countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

}
