package com.disney.salesapp.bo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author baraa034
 *
 */
public class SalesBO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String requestorName;
	private String requestorEmail;
	private Date requestNeededByDate;
	private Integer requestedCreditLimit;
	private Date termStartDate;
	private String companyName;
	private String brandName;
	private String companyWebAddress;
	private String companyStreetAddress;
	private String companyCity;
	private String companyState;
	private String companyZip;
	private String companyCountry;
	private Integer companyTelephone;
	private String companyContactName;
	private String contactTitle;
	private String companyContactEmailAddress;
	private boolean okayToContact;
	private String agencyName;
	private String agencyWebAddress;
	private String agencyStreetAddress;
	private Integer agencyCity;
	private String agencyState;
	private String agencyZip;
	private String agencyCountry;
	private Integer agencyPhone;
	private String agencyContact;
	private String aAgencyBillingExport;

	public String getRequestorName() {
		return requestorName;
	}

	public String getRequestorEmail() {
		return requestorEmail;
	}

	public Date getRequestNeededByDate() {
		return requestNeededByDate;
	}

	public Integer getRequestedCreditLimit() {
		return requestedCreditLimit;
	}

	public Date getTermStartDate() {
		return termStartDate;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getBrandName() {
		return brandName;
	}

	public String getCompanyWebAddress() {
		return companyWebAddress;
	}

	public String getCompanyStreetAddress() {
		return companyStreetAddress;
	}

	public String getCompanyCity() {
		return companyCity;
	}

	public String getCompanyState() {
		return companyState;
	}

	public String getCompanyZip() {
		return companyZip;
	}

	public String getCompanyCountry() {
		return companyCountry;
	}

	public Integer getCompanyTelephone() {
		return companyTelephone;
	}

	public String getCompanyContactName() {
		return companyContactName;
	}

	public String getContactTitle() {
		return contactTitle;
	}

	public String getCompanyContactEmailAddress() {
		return companyContactEmailAddress;
	}

	public boolean isOkayToContact() {
		return okayToContact;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public String getAgencyWebAddress() {
		return agencyWebAddress;
	}

	public String getAgencyStreetAddress() {
		return agencyStreetAddress;
	}

	public Integer getAgencyCity() {
		return agencyCity;
	}

	public String getAgencyState() {
		return agencyState;
	}

	public String getAgencyZip() {
		return agencyZip;
	}

	public String getAgencyCountry() {
		return agencyCountry;
	}

	public Integer getAgencyPhone() {
		return agencyPhone;
	}

	public String getAgencyContact() {
		return agencyContact;
	}

	public String getaAgencyBillingExport() {
		return aAgencyBillingExport;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public void setRequestNeededByDate(Date requestNeededByDate) {
		this.requestNeededByDate = requestNeededByDate;
	}

	public void setRequestedCreditLimit(Integer requestedCreditLimit) {
		this.requestedCreditLimit = requestedCreditLimit;
	}

	public void setTermStartDate(Date termStartDate) {
		this.termStartDate = termStartDate;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public void setCompanyWebAddress(String companyWebAddress) {
		this.companyWebAddress = companyWebAddress;
	}

	public void setCompanyStreetAddress(String companyStreetAddress) {
		this.companyStreetAddress = companyStreetAddress;
	}

	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	public void setCompanyState(String companyState) {
		this.companyState = companyState;
	}

	public void setCompanyZip(String companyZip) {
		this.companyZip = companyZip;
	}

	public void setCompanyCountry(String companyCountry) {
		this.companyCountry = companyCountry;
	}

	public void setCompanyTelephone(Integer companyTelephone) {
		this.companyTelephone = companyTelephone;
	}

	public void setCompanyContactName(String companyContactName) {
		this.companyContactName = companyContactName;
	}

	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	public void setCompanyContactEmailAddress(String companyContactEmailAddress) {
		this.companyContactEmailAddress = companyContactEmailAddress;
	}

	public void setOkayToContact(boolean okayToContact) {
		this.okayToContact = okayToContact;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public void setAgencyWebAddress(String agencyWebAddress) {
		this.agencyWebAddress = agencyWebAddress;
	}

	public void setAgencyStreetAddress(String agencyStreetAddress) {
		this.agencyStreetAddress = agencyStreetAddress;
	}

	public void setAgencyCity(Integer agencyCity) {
		this.agencyCity = agencyCity;
	}

	public void setAgencyState(String agencyState) {
		this.agencyState = agencyState;
	}

	public void setAgencyZip(String agencyZip) {
		this.agencyZip = agencyZip;
	}

	public void setAgencyCountry(String agencyCountry) {
		this.agencyCountry = agencyCountry;
	}

	public void setAgencyPhone(Integer agencyPhone) {
		this.agencyPhone = agencyPhone;
	}

	public void setAgencyContact(String agencyContact) {
		this.agencyContact = agencyContact;
	}

	public void setaAgencyBillingExport(String aAgencyBillingExport) {
		this.aAgencyBillingExport = aAgencyBillingExport;
	}
}
