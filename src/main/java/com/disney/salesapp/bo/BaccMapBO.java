package com.disney.salesapp.bo;


public class BaccMapBO {

	private Integer baccMapId;
	private RegionBO regionBO;
	private SegmentBO segmentBO;
	private BusinessAreaBO businessAreaBO;
	private CompanyBO companyBO;
	/**
	 * @return the baccMapId
	 */
	public Integer getBaccMapId() {
		return baccMapId;
	}
	/**
	 * @return the regionBO
	 */
	public RegionBO getRegionBO() {
		return regionBO;
	}
	/**
	 * @return the segmentBO
	 */
	public SegmentBO getSegmentBO() {
		return segmentBO;
	}
	/**
	 * @return the businessAreaBO
	 */
	public BusinessAreaBO getBusinessAreaBO() {
		return businessAreaBO;
	}
	/**
	 * @return the companyBO
	 */
	public CompanyBO getCompanyBO() {
		return companyBO;
	}
	/**
	 * @param baccMapId the baccMapId to set
	 */
	public void setBaccMapId(Integer baccMapId) {
		this.baccMapId = baccMapId;
	}
	/**
	 * @param regionBO the regionBO to set
	 */
	public void setRegionBO(RegionBO regionBO) {
		this.regionBO = regionBO;
	}
	/**
	 * @param segmentBO the segmentBO to set
	 */
	public void setSegmentBO(SegmentBO segmentBO) {
		this.segmentBO = segmentBO;
	}
	/**
	 * @param businessAreaBO the businessAreaBO to set
	 */
	public void setBusinessAreaBO(BusinessAreaBO businessAreaBO) {
		this.businessAreaBO = businessAreaBO;
	}
	/**
	 * @param companyBO the companyBO to set
	 */
	public void setCompanyBO(CompanyBO companyBO) {
		this.companyBO = companyBO;
	}

	
}
