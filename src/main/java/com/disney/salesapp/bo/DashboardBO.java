package com.disney.salesapp.bo;

import java.io.Serializable;
import java.util.Date;

public class DashboardBO implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7831325847621366014L;
    private Integer creditRequestId;
    private String creditRequestStatus;
    private String creditRequestNumber;
    private Date requestRaisedDate;
    private Date requestNeededByDate;
    private Date slaDate;
    
    public Integer getCreditRequestId() {
        return creditRequestId;
    }
    public void setCreditRequestId(Integer creditRequestId) {
        this.creditRequestId = creditRequestId;
    }
    public String getCreditRequestStatus() {
        return creditRequestStatus;
    }
    public void setCreditRequestStatus(String creditRequestStatus) {
        this.creditRequestStatus = creditRequestStatus;
    }
    public String getCreditRequestNumber() {
        return creditRequestNumber;
    }
    public void setCreditRequestNumber(String creditRequestNumber) {
        this.creditRequestNumber = creditRequestNumber;
    }
    public Date getRequestRaisedDate() {
        return requestRaisedDate;
    }
    public void setRequestRaisedDate(Date requestRaisedDate) {
        this.requestRaisedDate = requestRaisedDate;
    }
    public Date getSlaDate() {
        return slaDate;
    }
    public void setSlaDate(Date slaDate) {
        this.slaDate = slaDate;
    }
    public Date getRequestNeededByDate() {
        return requestNeededByDate;
    }
    public void setRequestNeededByDate(Date requestNeededByDate) {
        this.requestNeededByDate = requestNeededByDate;
    }
    
    
    
}
