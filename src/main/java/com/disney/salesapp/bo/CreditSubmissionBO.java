/**
 * 
 */
package com.disney.salesapp.bo;

import java.util.Date;


/**
 * @author GHV001
 *
 */
public class CreditSubmissionBO {

	private Integer creditSubmissionId;

	private String portalId;

	private CreditRequestBO creditRequestBO;
	
	private String applicationReferenceNumber;
	
	private String status;

	private String partialSave;
	
	private Date requestRaisedDate;

	/**
	 * @return the creditSubmissionId
	 */
	public Integer getCreditSubmissionId() {
		return creditSubmissionId;
	}

	/**
	 * @param creditSubmissionId the creditSubmissionId to set
	 */
	public void setCreditSubmissionId(Integer creditSubmissionId) {
		this.creditSubmissionId = creditSubmissionId;
	}

	/**
	 * @return the portalId
	 */
	public String getPortalId() {
		return portalId;
	}

	/**
	 * @param portalId the portalId to set
	 */
	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	/**
	 * @return the applicationReferenceNumber
	 */
	public String getApplicationReferenceNumber() {
		return applicationReferenceNumber;
	}

	/**
	 * @param applicationReferenceNumber the applicationReferenceNumber to set
	 */
	public void setApplicationReferenceNumber(String applicationReferenceNumber) {
		this.applicationReferenceNumber = applicationReferenceNumber;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the partialSave
	 */
	public String getPartialSave() {
		return partialSave;
	}

	/**
	 * @param partialSave the partialSave to set
	 */
	public void setPartialSave(String partialSave) {
		this.partialSave = partialSave;
	}

	/**
	 * @return the creditRequestBO
	 */
	public CreditRequestBO getCreditRequestBO() {
		return creditRequestBO;
	}

	/**
	 * @param creditRequestBO the creditRequestBO to set
	 */
	public void setCreditRequestBO(CreditRequestBO creditRequestBO) {
		this.creditRequestBO = creditRequestBO;
	}

    /**
     * Gets the request raised date.
     *
     * @return the request raised date
     */
    public Date getRequestRaisedDate() {
        return requestRaisedDate;
    }

    /**
     * Sets the request raised date.
     *
     * @param requestRaisedDate the new request raised date
     */
    public void setRequestRaisedDate(Date requestRaisedDate) {
        this.requestRaisedDate = requestRaisedDate;
    }

	
	
	
}
