/**
 * 
 */
package com.disney.salesapp.application;


/**
 * @author MADHS008
 * 
 *         Spring Boot Application Hook file.
 *
 

@SpringBootApplication
public class SalesAppApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	return application.sources(SalesAppApplication.class);
    }

    public static void main(String[] args) throws Exception {
	SpringApplication.run(SalesAppApplication.class, args);
    }
}
*/