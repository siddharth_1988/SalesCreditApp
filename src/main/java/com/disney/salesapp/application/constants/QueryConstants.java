package com.disney.salesapp.application.constants;

public interface QueryConstants {

    String GET_BUSINESS_GROUP_BY_COMP_ID_AND_BA_ID = 
            "Select BGM.businessGroup From BusinessGroupMap BGM where BGM.company.companyId=:companyId "
            + "and BGM.businessArea.businessAreaId=:businessAreaId";
    
    String GET_CREDIT_REQUEST_DETAILS_FOR_PARTIAL_SAVE = "Select CB From CreditSubmission CB where CB.portalId=:portalId and CB.partialSave = 'Y'";
    
    String GET_CREDIT_REQUEST_DETAILS_FOR_LOGGED_IN_USER = "from CreditSubmission where portalId=:portalId and partialSave='N'";
    
    String GET_COUNTRY_BY_NAME = "From Country where countryName = :countryName";
    
    String GET_STATE_BY_NAME = "From State where stateName= :stateName";
    
    String GET_LAST_CREDIT_REQUEST_DETAILS_FOR_LOGGED_IN_USER = "SELECT cb.* FROM CREDIT_SUBMISSION  cb WHERE cb.Created = (SELECT MAX(Created ) FROM CREDIT_SUBMISSION where portal_id = :portalId and partial_save = 'N' )";
    
    String EMPTY_STRING = "";
    
    String NO_INDICATOR = "N";
    
    String YES_INDICATOR = "Y";
    
    String GET_CURRENCY_BY_CURRENCY_CODE = "From Currency where currencyCode = :currencyCode";
    
    String GET_ALL_CURRENCIES = "From Currency";
}
