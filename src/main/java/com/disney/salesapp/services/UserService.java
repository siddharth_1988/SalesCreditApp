/**
 * 
 */
package com.disney.salesapp.services;

import java.util.List;

import com.disney.salesapp.bo.UserBO;


/**
 * The Interface UserService.
 *
 * @author MADHS008
 * This defines the contract for all user related
 * activities for Sales Application - that includes
 * mapping the logged in user to the respective
 * business unit and area, mapping the page view
 * according to the type of user (admin user, regular
 * user and so on and so forth).
 */
public interface UserService {
    
   // public User findByEmail(String email);
	/**
    * Find by id.
    *
    * @param userID the user id
    * @return the user bo
    */
   UserBO findById(Integer userID);

    /**
     * Find all users.
     *
     * @return the list
     */
    List<UserBO> findAllUsers();
    
    /**
     * Save user.
     *
     * @param userBO the user bo
     */
    void saveUser(UserBO userBO);
    
    /**
     * Updat user.
     *
     * @param userBO the user bo
     */
    void updatUser(UserBO userBO);

    /**
     * Find by user name.
     *
     * @param userName the user name
     * @return the user bo
     */
    UserBO findByUserName(String userName);

}
