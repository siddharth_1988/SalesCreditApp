package com.disney.salesapp.services;

import java.util.List;

import com.disney.salesapp.bo.DashboardBO;

public interface DashboardService {

    List<DashboardBO> getDashboardDetails(String portalId);

}
