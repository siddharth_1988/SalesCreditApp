package com.disney.salesapp.services;

/**
 * Description TODO Specify class description.
 *
 * @author GHV001
 */
public interface UserAuthorizationService {
    String PACKAGE_NAME = "com.disney.salescredit.services";
    String CLASS_NAME = "UserAuthorizationService";

    boolean isSecured();

    String getSiteminderurl();

    void setSiteminderurl(String siteminderurl);

    String getDefaultuserid();

    void setDefaultuserid(String defaultuserid);

    String getDefaultDateformat();

    void setDefaultDateformat(String defaultDateformat);

}
