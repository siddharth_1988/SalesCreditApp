/**
 * 
 */
package com.disney.salesapp.services;

import java.util.List;

import com.disney.salesapp.bo.BusinessAreaBO;
import com.disney.salesapp.bo.BusinessGroupBO;
import com.disney.salesapp.bo.COBAListBO;
import com.disney.salesapp.bo.CompanyBO;
import com.disney.salesapp.bo.CountryBO;
import com.disney.salesapp.bo.CreditRequestBO;
import com.disney.salesapp.bo.CreditSubmissionBO;
import com.disney.salesapp.bo.CreditUserRequest;
import com.disney.salesapp.bo.CurrencyBO;
import com.disney.salesapp.bo.RegionBO;
import com.disney.salesapp.bo.SegmentBO;
import com.disney.salesapp.bo.StateBO;

/**
 * @author MADHS008
 * 
 *         This service contract is used to do the pre & post processing activities that are required for Credit
 *         submission on the Sales Application. Some of the things this does are provide the business unit that the user
 *         belongs to and accordingly build the required data. take the request and post it to the Cortera API for
 *         submission.
 *
 */
public interface CreditSubmit {

    List<BusinessAreaBO> getAllBusinessArea();

    List<SegmentBO> getAllSegments();

    List<RegionBO> getRegionBySegment(Integer segmentId);

    List<CompanyBO> getCompanyBySegmentRegion(Integer segmentId, Integer regionId);

    List<BusinessAreaBO> getBABySegementRegionCompany(Integer segmentId, Integer regionId, Integer companyId);

    SegmentBO getSegmentById(Integer segmentId);

    RegionBO getRegionById(Integer regionId);

    CompanyBO getCompanyById(Integer companyId);

    BusinessAreaBO getBusinessAreaById(Integer businessAreaId);

    BusinessGroupBO getBusinessGroup(Integer companyId, Integer businessAreaId);

    BusinessGroupBO getBusinessGroup(Integer businessGroupId);
    
    List<CountryBO> getAllCountries();
    
    List<StateBO> getAllStatesOfUsa();
    
    Integer saveCreditRequest(CreditRequestBO creditRequestBO);
    
    Integer saveRequesterDetails(CreditRequestBO creditRequestBO);
    
    Integer saveUserRequesterDetails(CreditUserRequest creditUserRequest);
    
    void saveBusinessDetails(CreditRequestBO creditRequestBO, Integer creditRequestId);
    void saveCompanyDetails(CreditRequestBO creditRequestBO, Integer creditRequestId);
    
    void saveAgencyDetails(CreditRequestBO creditRequestBO, Integer creditRequestId);
    
    void saveCollectionDetails(CreditRequestBO creditRequestBO, Integer creditRequestId);
    void saveAdvanceCashDetails(CreditRequestBO creditRequestBO, Integer creditRequestId);
    void saveAttachmentDetails(CreditRequestBO creditRequestBO, Integer creditRequestId);
    void saveFohDetails(CreditRequestBO creditRequestBO, Integer creditRequestId);
    
    CreditSubmissionBO saveCreditSubmission(CreditSubmissionBO creditSubmissionBO);
    
    CreditSubmissionBO getCreditRequestDetails(String portalId);
    
    List<RegionBO> getAllRegions();
    
    List<CompanyBO> getAllCompanys();

    List<COBAListBO> getCOBAListBySegment(Integer segmentId);

    List<CurrencyBO> getAllCurrencies();
}
