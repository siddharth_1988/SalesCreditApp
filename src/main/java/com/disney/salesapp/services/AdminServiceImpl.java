package com.disney.salesapp.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.disney.salesapp.bo.BaccMapBO;
import com.disney.salesapp.bo.BusinessAreaBO;
import com.disney.salesapp.bo.CompanyBO;
import com.disney.salesapp.bo.RegionBO;
import com.disney.salesapp.bo.SegmentBO;
import com.disney.salesapp.dao.BaccMapDao;
import com.disney.salesapp.dao.BusinessAreaDao;
import com.disney.salesapp.dao.CompanyDao;
import com.disney.salesapp.dao.RegionDao;
import com.disney.salesapp.dao.SegmentDao;
import com.disney.salesapp.model.entities.BaccMap;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Region;
import com.disney.salesapp.model.entities.Segment;

@Service("AdminService")
@Transactional
public class AdminServiceImpl implements AdminService {

	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private BusinessAreaDao businessAreaDao;
	@Autowired
	private BaccMapDao baccMapDao;
	@Autowired
	private SegmentDao segmentDao;
	@Autowired
	private BusinessAreaDao businesAreaDao;
	@Autowired
	private RegionDao regionDao;

	@Override
	public CompanyBO findById(Integer companyID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CompanyBO> findAllCompanies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveCompany(CompanyBO companyBO) {
		Company company = new Company();
		company.setCompanyCode(companyBO.getCompanyCode());
		company.setCompanyName(companyBO.getCompanyName());

		Set<BaccMapBO> baccMapBOList = companyBO.getBaccMapBOList();
		Set<BaccMap> baccMapList = new HashSet<BaccMap>();

		for (BaccMapBO baccMapBO : baccMapBOList) {
			RegionBO regionBO = baccMapBO.getRegionBO();
			SegmentBO segmentBO = baccMapBO.getSegmentBO();

			Region region = new Region();
			region.setRegionId(regionBO.getRegionId());

			Segment segment = new Segment();
			segment.setSegmentId(segmentBO.getSegmentId());

			BaccMap baccMap = new BaccMap();
			baccMap.setBaccMapId(baccMapBO.getBaccMapId());
			baccMap.setCompany(company);
			baccMap.setSegment(segment);
			baccMap.setRegion(region);

			baccMapList.add(baccMap);

		}

		company.setBaccMaps(baccMapList);

		companyDao.saveCompany(company);

	}

	@Override
	public void updatCompany(CompanyBO companyBO) {
		Company company = new Company();
		company.setCompanyId(companyBO.getCompanyId());
		company.setCompanyCode(companyBO.getCompanyCode());
		company.setCompanyName(companyBO.getCompanyName());
		companyDao.saveCompany(company);

	}

	@Override
	public void deleteCompany(CompanyBO companyBO) {
		// TODO Auto-generated method stub

	}

	@Override
	public CompanyBO findByCode(String companyCode) {
		CompanyBO companyBO = new CompanyBO();
		Company company = companyDao.findByCompanyCode(companyCode);
		companyBO.setCompanyId(company.getCompanyId());
		companyBO.setCompanyCode(company.getCompanyCode());
		companyBO.setCompanyName(company.getCompanyName());
		return companyBO;
	}

	@Override
	public void saveBusinessArea(BusinessAreaBO businessAreaBO) {
		BusinessArea businessArea = new BusinessArea();
		businessArea.setBusinessAreaId(businessAreaBO.getBusinessAreaId());
		businessArea.setBusinessAreaCode(businessAreaBO.getBusinessAreaCode());
		businessArea.setBusinessAreaName(businessAreaBO.getBusinessAreaName());

		Set<BaccMapBO> baccMapBOList = businessAreaBO.getBaccMapBOList();
		Set<BaccMap> baccMapList = new HashSet<BaccMap>();

		for (BaccMapBO baccMapBO : baccMapBOList) {
			CompanyBO companyBO = baccMapBO.getCompanyBO();
			RegionBO regionBO = baccMapBO.getRegionBO();
			SegmentBO segmentBO = baccMapBO.getSegmentBO();

			Region region = new Region();
			region.setRegionId(regionBO.getRegionId());

			Segment segment = new Segment();
			segment.setSegmentId(segmentBO.getSegmentId());

			Company company = new Company();
			company.setCompanyId(companyBO.getCompanyId());

			BaccMap baccMap = new BaccMap();
			baccMap.setBaccMapId(baccMapBO.getBaccMapId());
			baccMap.setCompany(company);
			baccMap.setSegment(segment);
			baccMap.setRegion(region);
			baccMap.setBusinessArea(businessArea);

			baccMapList.add(baccMap);

		}

		businessArea.setBaccMaps(baccMapList);

		businessAreaDao.saveBusinessArea(businessArea);

	}

	@Override
	public BaccMapBO getBaccMapId(Integer segmentId, Integer regionId,
			Integer companyId) {
		BaccMapBO baccMapBO = null;

		BaccMap baccMap = businessAreaDao.getBaccMapId(segmentId, regionId,
				companyId);

		if (baccMap != null) {
			Company company = baccMap.getCompany();
			Region region = baccMap.getRegion();
			Segment segment = baccMap.getSegment();

			RegionBO regionBO = new RegionBO();
			regionBO.setRegionId(region.getRegionId());

			SegmentBO segmentBO = new SegmentBO();
			segmentBO.setSegmentId(segment.getSegmentId());

			CompanyBO companyBO = new CompanyBO();
			companyBO.setCompanyId(company.getCompanyId());

			// BusinessAreaBO businessAreaBO = new BusinessAreaBO();

			baccMapBO = new BaccMapBO();

			baccMapBO.setBaccMapId(baccMap.getBaccMapId());
			baccMapBO.setCompanyBO(companyBO);
			baccMapBO.setSegmentBO(segmentBO);
			baccMapBO.setRegionBO(regionBO);
			// baccMapBO.setBusinessAreaBO(businessAreaBO);
		}

		return baccMapBO;
	}

	@Override
	public List<BaccMapBO> getBaccMapData() {
		List<BaccMap> baccMapList = businessAreaDao.getBaccMapData();
		List<BaccMapBO> baccMapBoList = new ArrayList<BaccMapBO>();

		if (baccMapList.size() > 0) {
			for (BaccMap baccMap : baccMapList) {

				BaccMapBO baccMapBO = new BaccMapBO();
				Company company = baccMap.getCompany();
				Region region = baccMap.getRegion();
				Segment segment = baccMap.getSegment();
				BusinessArea businessArea = baccMap.getBusinessArea();

				RegionBO regionBO = new RegionBO();
				regionBO.setRegionId(region.getRegionId());
				regionBO.setRegionName(region.getRegionName());

				SegmentBO segmentBO = new SegmentBO();
				segmentBO.setSegmentId(segment.getSegmentId());
				segmentBO.setSegmentName(segment.getSegmentName());

				CompanyBO companyBO = new CompanyBO();
				companyBO.setCompanyId(company.getCompanyId());
				companyBO.setCompanyCode(company.getCompanyCode());
				companyBO.setCompanyName(company.getCompanyName());

				BusinessAreaBO businessAreaBO = new BusinessAreaBO();
				businessAreaBO.setBusinessAreaId(businessArea
						.getBusinessAreaId());
				businessAreaBO.setBusinessAreaCode(businessArea
						.getBusinessAreaCode());
				businessAreaBO.setBusinessAreaName(businessArea
						.getBusinessAreaName());

				baccMapBO.setBaccMapId(baccMap.getBaccMapId());
				baccMapBO.setCompanyBO(companyBO);
				baccMapBO.setSegmentBO(segmentBO);
				baccMapBO.setRegionBO(regionBO);
				baccMapBO.setBusinessAreaBO(businessAreaBO);

				baccMapBoList.add(baccMapBO);
			}
		}
		return baccMapBoList;
	}

	@Override
	public Integer saveSegment(SegmentBO segmentBO) {
		Segment segment = new Segment();
		segment.setSegmentName(segmentBO.getSegmentName());
		segment.setSegmentId(segmentBO.getSegmentId());
		return businessAreaDao.saveSegment(segment);
	}

	@Override
	public Integer saveRegion(RegionBO regionBO) {
		Region region = new Region();
		region.setRegionName(regionBO.getRegionName());
		region.setRegionId(regionBO.getRegionId());
		return businessAreaDao.saveRegion(region);
	}

	@Override
	public Integer saveACompany(CompanyBO companyBO) {
		Company company = new Company();
		company.setCompanyCode(companyBO.getCompanyCode());
		company.setCompanyName(companyBO.getCompanyName());
		company.setCompanyId(companyBO.getCompanyId());
		return businessAreaDao.saveACompany(company);
	}

	@Override
	public Integer saveABusinessArea(BusinessAreaBO businessAreaBO) {
		BusinessArea businessArea = new BusinessArea();
		businessArea.setBusinessAreaCode(businessAreaBO.getBusinessAreaCode());
		businessArea.setBusinessAreaName(businessAreaBO.getBusinessAreaName());
		businessArea.setBusinessAreaId(businessAreaBO.getBusinessAreaId());
		return businessAreaDao.saveABusinessArea(businessArea);
	}

	@Override
	public void saveBaccMap(BaccMapBO baccMapBO) {
		BaccMap baccMap = new BaccMap();

		Segment segment = new Segment();
		if (baccMapBO.getSegmentBO().getSegmentId() != null) {
			segment.setSegmentId(baccMapBO.getSegmentBO().getSegmentId());
			segment.setLogicalDeleteInd("N");
		}
		if (baccMapBO.getSegmentBO().getSegmentName() != null) {
			segment.setSegmentName(baccMapBO.getSegmentBO().getSegmentName());
			segment.setLogicalDeleteInd("N");
		}
		Region region = new Region();
		if (baccMapBO.getRegionBO().getRegionId() != null) {
			region.setRegionId(baccMapBO.getRegionBO().getRegionId());
			region.setLogicalDeleteInd("N");
		}
		if (baccMapBO.getRegionBO().getRegionName() != null) {
			region.setRegionName(baccMapBO.getRegionBO().getRegionName());
			region.setLogicalDeleteInd("N");
		}

		Company company = new Company();
		if (baccMapBO.getCompanyBO().getCompanyId() != null) {
			company.setCompanyId(baccMapBO.getCompanyBO().getCompanyId());
			company.setLogicalDeleteInd("N");
		}
		if (baccMapBO.getCompanyBO().getCompanyCode() != null) {
			company.setCompanyCode(baccMapBO.getCompanyBO().getCompanyCode());
			company.setLogicalDeleteInd("N");
		}
		if (baccMapBO.getCompanyBO().getCompanyName() != null) {
			company.setCompanyName(baccMapBO.getCompanyBO().getCompanyName());
			company.setLogicalDeleteInd("N");
		}

		BusinessArea businessArea = new BusinessArea();
		if (baccMapBO.getBusinessAreaBO().getBusinessAreaCode() != null) {
			businessArea.setBusinessAreaCode(baccMapBO.getBusinessAreaBO()
					.getBusinessAreaCode());
			businessArea.setLogicalDeleteInd("N");
		}
		if (baccMapBO.getBusinessAreaBO().getBusinessAreaName() != null) {
			businessArea.setBusinessAreaName(baccMapBO.getBusinessAreaBO()
					.getBusinessAreaName());
			businessArea.setLogicalDeleteInd("N");
		}
		if (baccMapBO.getBusinessAreaBO().getBusinessAreaId() != null) {
			businessArea.setBusinessAreaId(baccMapBO.getBusinessAreaBO()
					.getBusinessAreaId());
			businessArea.setLogicalDeleteInd("N");
		}
		baccMap.setBusinessArea(businessArea);
		baccMap.setSegment(segment);
		baccMap.setCompany(company);
		baccMap.setRegion(region);
		baccMap.setLogicalDeleteInd("N");
		baccMapDao.saveBaccMap(baccMap);
	}

	@Override
	public void deleteSegment(SegmentBO segmentBO) {
		Segment segment = new Segment();
		segment.setSegmentName(segmentBO.getSegmentName());
		segment.setSegmentId(segmentBO.getSegmentId());
		segmentDao.deleteSegment(segment);

	}

	@Override
	public void deleteBusinessArea(BusinessAreaBO areaBO) {
		BusinessArea businessArea = new BusinessArea();
		businessArea.setBusinessAreaId(areaBO.getBusinessAreaId());
		businessArea.setBusinessAreaCode(areaBO.getBusinessAreaCode());
		businessArea.setBusinessAreaName(areaBO.getBusinessAreaName());
		businesAreaDao.deleteBusinessArea(businessArea);

	}

	@Override
	public void deleteBaccMap(BaccMapBO baccMapBO) {
		/*BaccMap baccMap = new BaccMap();
		baccMap.setBaccMapId(baccMapBO.getBaccMapId());

		Segment segment = new Segment();
		segment.setSegmentId(baccMapBO.getSegmentBO().getSegmentId());
		segment.setSegmentName(baccMapBO.getSegmentBO().getSegmentName());

		Region region = new Region();
		region.setRegionId(baccMapBO.getRegionBO().getRegionId());
		region.setRegionName(baccMapBO.getRegionBO().getRegionName());

		Company company = new Company();
		company.setCompanyId(baccMapBO.getCompanyBO().getCompanyId());
		company.setCompanyCode(baccMapBO.getCompanyBO().getCompanyCode());
		company.setCompanyName(baccMapBO.getCompanyBO().getCompanyName());

		BusinessArea businessArea = new BusinessArea();
		businessArea.setBusinessAreaCode(baccMapBO.getBusinessAreaBO()
				.getBusinessAreaCode());
		businessArea.setBusinessAreaName(baccMapBO.getBusinessAreaBO()
				.getBusinessAreaName());
		businessArea.setBusinessAreaId(baccMapBO.getBusinessAreaBO()
				.getBusinessAreaId());

		baccMap.setBusinessArea(businessArea);
		baccMap.setSegment(segment);
		baccMap.setCompany(company);
		baccMap.setRegion(region);*/
		baccMapDao.deleteBaccMap(baccMapBO.getBaccMapId());

	}

	@Override
	public Region getRegionById(Integer regionId) {
		return regionDao.findById(regionId);
	}

	@Override
	public Company getCompanyById(Integer companyId) {
		return companyDao.findById(companyId);
	}

	@Override
	public BusinessArea getBusinessAreaById(Integer businessAreaId) {
		return businesAreaDao.findById(businessAreaId);
	}

	@Override
	public Segment getSegmentById(Integer segmentId) {
		return segmentDao.findById(segmentId);
	}

	@Override
	public boolean isSegmentExists(String segmentName) {
		Segment segment = segmentDao.findBySegmentName(segmentName);
		return segment != null ? true : false;
	}

	@Override
	public boolean isRegionExists(String regionName) {
		Region region = regionDao.findByRegionName(regionName);
		return region != null ? true : false;
	}

	@Override
	public boolean isCompanyExists(String companyName) {
		Company company = companyDao.findByCompanyName(companyName);
		return company != null ? true : false;
	}

	@Override
	public boolean isBusinessAreaExists(String businessAreaName) {
		BusinessArea businessArea = businessAreaDao
				.findByBusinessAreaName(businessAreaName);
		return businessArea != null ? true : false;
	}
	
	@Override
	public void updateBaccMap(BaccMapBO baccMapBO) {
		baccMapDao.deleteBaccMap(baccMapBO.getBaccMapId());
		addBaccMap(baccMapBO);
	}
	
	@Override
    public void addBaccMap(BaccMapBO baccMapBO) {
        BaccMap baccMap = new BaccMap();

        Segment segment = new Segment();
        segment.setSegmentId(baccMapBO.getSegmentBO().getSegmentId());
        segment.setSegmentName(baccMapBO.getSegmentBO().getSegmentName());

        Region region = new Region();
        region.setRegionId(baccMapBO.getRegionBO().getRegionId());
        region.setRegionName(baccMapBO.getRegionBO().getRegionName());

        Company company = new Company();
        company.setCompanyId(baccMapBO.getCompanyBO().getCompanyId());
        company.setCompanyCode(baccMapBO.getCompanyBO().getCompanyCode());
        company.setCompanyName(baccMapBO.getCompanyBO().getCompanyName());

        BusinessArea businessArea = new BusinessArea();
        businessArea.setBusinessAreaCode(baccMapBO.getBusinessAreaBO()
                .getBusinessAreaCode());
        businessArea.setBusinessAreaName(baccMapBO.getBusinessAreaBO()
                .getBusinessAreaName());
        businessArea.setBusinessAreaId(baccMapBO.getBusinessAreaBO()
                .getBusinessAreaId());

        baccMap.setBusinessArea(businessArea);
        baccMap.setSegment(segment);
        baccMap.setCompany(company);
        baccMap.setRegion(region);
        
        baccMapDao.saveBaccMap(baccMap);
        
    }

}
