package com.disney.salesapp.services;

import java.util.List;

import com.disney.salesapp.bo.BaccMapBO;
import com.disney.salesapp.bo.BusinessAreaBO;
import com.disney.salesapp.bo.CompanyBO;
import com.disney.salesapp.bo.RegionBO;
import com.disney.salesapp.bo.SegmentBO;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Region;
import com.disney.salesapp.model.entities.Segment;

/**
 * The Interface UserService.
 *
 * @author Baral This defines the contract for all company related activities
 *         for Sales Application.
 */
public interface AdminService {
	/**
	 * Find by id.
	 *
	 * @param companyID
	 *            the company id
	 * @return the company bo
	 */
	CompanyBO findById(Integer companyID);

	/**
	 * Find by companyCode.
	 *
	 * @param companyCode
	 *            the companyCode
	 * @return the company bo
	 */
	CompanyBO findByCode(String companyCode);

	/**
	 * Find all companies.
	 *
	 * @return the list
	 */
	List<CompanyBO> findAllCompanies();

	/**
	 * Save company.
	 *
	 * @param companyBO
	 *            the company bo
	 */
	void saveCompany(CompanyBO companyBO);

	/**
	 * Update company.
	 *
	 * @param companyBO
	 *            the company bo
	 */
	void updatCompany(CompanyBO companyBO);

	/**
	 * Update company.
	 *
	 * @param companyBO
	 *            the company bo
	 */
	void deleteCompany(CompanyBO companyBO);

	void saveBusinessArea(BusinessAreaBO businessAreaBO);

	BaccMapBO getBaccMapId(Integer segmentId, Integer regionId,
			Integer companyId);

	List<BaccMapBO> getBaccMapData();

	Integer saveSegment(SegmentBO segmentBO);

	Integer saveRegion(RegionBO regionBO);

	Integer saveACompany(CompanyBO companyBO);

	Integer saveABusinessArea(BusinessAreaBO businessAreaBO);

	void saveBaccMap(BaccMapBO baccMapBO);

	void deleteSegment(SegmentBO segmentBO);

	void deleteBusinessArea(BusinessAreaBO areaBO);

	void deleteBaccMap(BaccMapBO baccMapBO);

	Region getRegionById(Integer regionId);

	Company getCompanyById(Integer companyId);

	BusinessArea getBusinessAreaById(Integer businessAreaId);

	Segment getSegmentById(Integer segmentId);

	boolean isSegmentExists(String segmentName);

	boolean isRegionExists(String regionName);

	boolean isCompanyExists(String companyName);

	boolean isBusinessAreaExists(String businessAreaName);
	
	void updateBaccMap(BaccMapBO baccMapBO);

    void addBaccMap(BaccMapBO baccMapBO);
}
