
package com.disney.salesapp.services;

/**
 * Description TODO Specify class description.
 *
 * @author GHV001
 */
public class UserAuthorizationServiceImpl implements UserAuthorizationService {
	public static final String PACKAGE_NAME = "com.disney.salescredit.services";
	public static final String CLASS_NAME = "UserAuthorizationServiceImpl";

	private String security;
	private String siteminderurl;
	private String defaultuserid;
	private String defaultDateformat;

	@Override
	public String getDefaultDateformat() {
		return defaultDateformat;
	}
	@Override
	public void setDefaultDateformat(String defaultDateformat) {
		this.defaultDateformat = defaultDateformat;
	}

	@Override
	public String getDefaultuserid() {
		return defaultuserid;
	}

	@Override
	public void setDefaultuserid(String defaultuserid) {
		this.defaultuserid = defaultuserid;
	}

	@Override
	public String getSiteminderurl() {
		return siteminderurl;
	}

	@Override
	public void setSiteminderurl(String siteminderurl) {
		this.siteminderurl = siteminderurl;
	}


	@Override
	public boolean  isSecured(){
		if(security!=null && security.equalsIgnoreCase("off")){
			
			return false;
		}else{
			return true;
		}
	}
	
	
	public String getSecurity() {
		return security;
	}

	
	public void setSecurity(String security) {
		this.security = security;
	}

	
}
