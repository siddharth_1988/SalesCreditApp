/**
 * 
 */
package com.disney.salesapp.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.disney.salesapp.application.constants.SalesAppConstants;
import com.disney.salesapp.bo.BusinessAreaBO;
import com.disney.salesapp.bo.BusinessGroupBO;
import com.disney.salesapp.bo.COBAListBO;
import com.disney.salesapp.bo.CompanyBO;
import com.disney.salesapp.bo.CountryBO;
import com.disney.salesapp.bo.CreditRequestBO;
import com.disney.salesapp.bo.CreditSubmissionBO;
import com.disney.salesapp.bo.CreditUserRequest;
import com.disney.salesapp.bo.CurrencyBO;
import com.disney.salesapp.bo.RegionBO;
import com.disney.salesapp.bo.SegmentBO;
import com.disney.salesapp.bo.StateBO;
import com.disney.salesapp.dao.BaccMapDao;
import com.disney.salesapp.dao.BusinessAreaDao;
import com.disney.salesapp.dao.BusinessGroupDao;
import com.disney.salesapp.dao.BusinessGroupMapDao;
import com.disney.salesapp.dao.COBAListDao;
import com.disney.salesapp.dao.CompanyDao;
import com.disney.salesapp.dao.CountryDao;
import com.disney.salesapp.dao.CreditSubmissionDao;
import com.disney.salesapp.dao.CurrencyDao;
import com.disney.salesapp.dao.RegionDao;
import com.disney.salesapp.dao.SegmentDao;
import com.disney.salesapp.dao.StateDao;
import com.disney.salesapp.dao.UserDao;
import com.disney.salesapp.model.entities.BusinessArea;
import com.disney.salesapp.model.entities.BusinessGroup;
import com.disney.salesapp.model.entities.COBAList;
import com.disney.salesapp.model.entities.Company;
import com.disney.salesapp.model.entities.Country;
import com.disney.salesapp.model.entities.CreditRequest;
import com.disney.salesapp.model.entities.CreditSubmission;
import com.disney.salesapp.model.entities.Currency;
import com.disney.salesapp.model.entities.Region;
import com.disney.salesapp.model.entities.Segment;
import com.disney.salesapp.model.entities.State;
import com.disney.salesapp.model.entities.User;

@Service("CreditSubmit")
@Transactional
public class CreditSubmitImpl implements CreditSubmit {
	@Autowired
	private CreditSubmissionDao creditSubmissionDao;

	@Autowired
	private SegmentDao segmentDao;

	@Autowired
	private RegionDao regionDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private BusinessAreaDao businessAreaDao;

	@Autowired
	private BusinessGroupDao businessGroupDao;

	@Autowired
	private BaccMapDao baccMapDao;

	@Autowired
	private BusinessGroupMapDao businessGroupMapDao;
	
	@Autowired
    private UserDao userDao;
	
	@Autowired
	private CountryDao countryDao;
	
	@Autowired
    private CurrencyDao currencyDao;
	
	@Autowired
	private StateDao stateDao;

	@Autowired
    private COBAListDao cobaListDao;

	/**
	 * @return the creditSubmissionDao
	 */
	public CreditSubmissionDao getCreditSubmissionDao() {
		return creditSubmissionDao;
	}

	/**
	 * @param creditSubmissionDao
	 *            the creditSubmissionDao to set
	 */
	public void setCreditSubmissionDao(CreditSubmissionDao creditSubmissionDao) {
		this.creditSubmissionDao = creditSubmissionDao;
	}

	/**
	 * @return the segmentDao
	 */
	public SegmentDao getSegmentDao() {
		return segmentDao;
	}

	/**
	 * @param segmentDao
	 *            the segmentDao to set
	 */
	public void setSegmentDao(SegmentDao segmentDao) {
		this.segmentDao = segmentDao;
	}

	/**
	 * @return the regionDao
	 */
	public RegionDao getRegionDao() {
		return regionDao;
	}

	/**
	 * @param regionDao
	 *            the regionDao to set
	 */
	public void setRegionDao(RegionDao regionDao) {
		this.regionDao = regionDao;
	}

	/**
	 * @return the companyDao
	 */
	public CompanyDao getCompanyDao() {
		return companyDao;
	}

	/**
	 * @param companyDao
	 *            the companyDao to set
	 */
	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	/**
	 * @return the businessAreaDao
	 */
	public BusinessAreaDao getBusinessAreaDao() {
		return businessAreaDao;
	}

	/**
	 * @param businessAreaDao
	 *            the businessAreaDao to set
	 */
	public void setBusinessAreaDao(BusinessAreaDao businessAreaDao) {
		this.businessAreaDao = businessAreaDao;
	}

	/**
	 * @return the businessGroupDao
	 */
	public BusinessGroupDao getBusinessGroupDao() {
		return businessGroupDao;
	}

	/**
	 * @param businessGroupDao
	 *            the businessGroupDao to set
	 */
	public void setBusinessGroupDao(BusinessGroupDao businessGroupDao) {
		this.businessGroupDao = businessGroupDao;
	}

	/**
	 * @return the baccMapDao
	 */
	public BaccMapDao getBaccMapDao() {
		return baccMapDao;
	}

	/**
	 * @param baccMapDao
	 *            the baccMapDao to set
	 */
	public void setBaccMapDao(BaccMapDao baccMapDao) {
		this.baccMapDao = baccMapDao;
	}

	/**
	 * @return the businessGroupMapDao
	 */
	public BusinessGroupMapDao getBusinessGroupMapDao() {
		return businessGroupMapDao;
	}

	/**
	 * @param businessGroupMapDao
	 *            the businessGroupMapDao to set
	 */
	public void setBusinessGroupMapDao(BusinessGroupMapDao businessGroupMapDao) {
		this.businessGroupMapDao = businessGroupMapDao;
	}

	/**
	 * @return the countryDao
	 */
	public CountryDao getCountryDao() {
		return countryDao;
	}

	/**
	 * @param countryDao the countryDao to set
	 */
	public void setCountryDao(CountryDao countryDao) {
		this.countryDao = countryDao;
	}

	/**
	 * @return the stateDao
	 */
	public StateDao getStateDao() {
		return stateDao;
	}

	/**
	 * @param stateDao the stateDao to set
	 */
	public void setStateDao(StateDao stateDao) {
		this.stateDao = stateDao;
	}

	@Override
	public List<BusinessAreaBO> getAllBusinessArea() {
		List<BusinessArea> businessAreas = businessAreaDao.findAllBusinessArea();
		List<BusinessAreaBO> businessAreaBOs = new ArrayList<BusinessAreaBO>();
		for(BusinessArea businessArea :businessAreas){
			BusinessAreaBO businessAreaBO = new BusinessAreaBO();
			businessAreaBO.setBusinessAreaId(businessArea.getBusinessAreaId());
			businessAreaBO.setBusinessAreaCode(businessArea.getBusinessAreaCode());
			businessAreaBO.setBusinessAreaName(businessArea.getBusinessAreaName());
			businessAreaBOs.add(businessAreaBO);
		}
		return businessAreaBOs;
	}

	@Override
	public List<SegmentBO> getAllSegments() {
		List<Segment> segments = segmentDao.findAllSegments();
		List<SegmentBO> segmentBOs = new ArrayList<SegmentBO>();
		for (Segment segment : segments) {
			SegmentBO segmentBO = new SegmentBO();
			segmentBO.setSegmentId(segment.getSegmentId());
			segmentBO.setSegmentName(segment.getSegmentName());
			segmentBOs.add(segmentBO);
		}
		return segmentBOs;
	}

	@Override
	public List<RegionBO> getRegionBySegment(Integer segmentId) {
		List<Region> regions = baccMapDao.findRegionsBySegmentId(segmentId);
		Set<RegionBO> regionBOs = new HashSet<RegionBO>();
		for (Region region : regions) {
			RegionBO regionBO = new RegionBO();
			regionBO.setRegionId(region.getRegionId());
			regionBO.setRegionName(region.getRegionName());
			regionBOs.add(regionBO);
		}
		List<RegionBO> regionBOList = new ArrayList<RegionBO>();
		regionBOList.addAll(regionBOs);
		return regionBOList;
	}
	
	@Override
    public List<COBAListBO> getCOBAListBySegment(Integer segmentId) {
        List<COBAList> cobaLists = cobaListDao.findBySegmentId(segmentId);
        Set<COBAListBO> cobaListBOs = new HashSet<COBAListBO>();
        for (COBAList cobaList : cobaLists) {
            COBAListBO cobaListBO = new COBAListBO();
            cobaListBO.setCobaListId(cobaList.getCobaListId());
            cobaListBO.setCuDescription(cobaList.getCuDescription());
            cobaListBO.setCoba(cobaList.getCoba());
            SegmentBO segmentBO = new SegmentBO();
            segmentBO.setSegmentId(cobaList.getSegment().getSegmentId());
            segmentBO.setSegmentName(cobaList.getSegment().getSegmentName());
            cobaListBO.setSegmentBO(segmentBO);
            cobaListBOs.add(cobaListBO);
        }
        List<COBAListBO> cobaListBOList = new ArrayList<COBAListBO>();
        cobaListBOList.addAll(cobaListBOs);
        return cobaListBOList;
    }

	@Override
	public List<CompanyBO> getCompanyBySegmentRegion(Integer segmentId,
			Integer regionId) {
		List<Company> companies = baccMapDao.findCompanyBySegmentRegion(
				segmentId, regionId);
		Set<CompanyBO> companyBOs = new HashSet<CompanyBO>();
		for (Company company : companies) {
			CompanyBO companyBO = new CompanyBO();
			companyBO.setCompanyId(company.getCompanyId());
			companyBO.setCompanyCode(company.getCompanyCode());
			companyBO.setCompanyName(company.getCompanyName());
			companyBOs.add(companyBO);
		}
		List<CompanyBO> companyBOList = new ArrayList<CompanyBO>();
		companyBOList.addAll(companyBOs);
		return companyBOList;
	}

	@Override
	public List<BusinessAreaBO> getBABySegementRegionCompany(Integer segmentId,
			Integer regionId, Integer companyId) {
		List<BusinessArea> businessAreas = baccMapDao
				.findBABySegmentRegionCompany(segmentId, regionId, companyId);
		Set<BusinessAreaBO> businessAreaBOs = new HashSet<BusinessAreaBO>();
		for (BusinessArea businessArea : businessAreas) {
			BusinessAreaBO businessAreaBO = new BusinessAreaBO();
			businessAreaBO.setBusinessAreaId(businessArea.getBusinessAreaId());
			businessAreaBO.setBusinessAreaCode(businessArea
					.getBusinessAreaCode());
			businessAreaBO.setBusinessAreaName(businessArea
					.getBusinessAreaName());
			businessAreaBOs.add(businessAreaBO);
		}
		List<BusinessAreaBO> businessAreaBOList = new ArrayList<BusinessAreaBO>();
		businessAreaBOList.addAll(businessAreaBOs);
		return businessAreaBOList;
	}

	@Override
	public SegmentBO getSegmentById(Integer segmentId) {
		Segment segment = segmentDao.findById(segmentId);
		SegmentBO segmentBO = new SegmentBO();
		segmentBO.setSegmentId(segment.getSegmentId());
		segmentBO.setSegmentName(segment.getSegmentName());
		return segmentBO;
	}

	@Override
	public RegionBO getRegionById(Integer regionId) {
		Region region = regionDao.findById(regionId);
		RegionBO regionBO = new RegionBO();
		regionBO.setRegionId(region.getRegionId());
		regionBO.setRegionName(region.getRegionName());
		return regionBO;
	}

	@Override
	public CompanyBO getCompanyById(Integer companyId) {
		Company company = companyDao.findById(companyId);
		CompanyBO companyBO = new CompanyBO();
		companyBO.setCompanyId(company.getCompanyId());
		companyBO.setCompanyCode(company.getCompanyCode());
		companyBO.setCompanyName(company.getCompanyName());
		return companyBO;
	}

	@Override
	public BusinessAreaBO getBusinessAreaById(Integer businessAreaId) {
		BusinessArea businessArea = businessAreaDao.findById(businessAreaId);
		BusinessAreaBO businessAreaBO = new BusinessAreaBO();
		businessAreaBO.setBusinessAreaId(businessArea.getBusinessAreaId());
		businessAreaBO.setBusinessAreaCode(businessArea.getBusinessAreaCode());
		businessAreaBO.setBusinessAreaName(businessArea.getBusinessAreaName());
		return businessAreaBO;
	}

	@Override
	public BusinessGroupBO getBusinessGroup(Integer businessGroupId) {
		BusinessGroup businessGroup = businessGroupDao
				.findById(businessGroupId);
		BusinessGroupBO businessGroupBO = new BusinessGroupBO();
		businessGroupBO.setBusinessGroupId(businessGroup.getBusinessGroupId());
		businessGroupBO.setBusinessGroupName(businessGroup
				.getBusinessGroupName());
		return businessGroupBO;
	}

	@Override
	public BusinessGroupBO getBusinessGroup(Integer companyId,
			Integer businessAreaId) {
		BusinessGroup businessGroup = businessGroupMapDao.getBusinessGroup(
				companyId, businessAreaId);
		BusinessGroupBO businessGroupBO = new BusinessGroupBO();
		if (businessGroup != null) {
			businessGroupBO.setBusinessGroupId(businessGroup
					.getBusinessGroupId());
			businessGroupBO.setBusinessGroupName(businessGroup
					.getBusinessGroupName());
		} else {
			businessGroupBO
					.setBusinessGroupId(SalesAppConstants.NO_BUSINESS_GROUP_FOUND);
		}
		return businessGroupBO;
	}

	@Override
	public List<CountryBO> getAllCountries() {
		List<Country> countries = segmentDao.findAllCountries();
		List<CountryBO> countryBOs = new ArrayList<CountryBO>();
		for (Country country : countries) {
			CountryBO countryBO = new CountryBO();
			countryBO.setCountryId(country.getCountryId());
			countryBO.setCountryName(country.getCountryName());
			countryBOs.add(countryBO);
		}
		return countryBOs;
	}
	
	@Override
    public List<CurrencyBO> getAllCurrencies() {
        List<Currency> currencies = currencyDao.getAllCurrencies();
        List<CurrencyBO> currencyBOs = new ArrayList<CurrencyBO>();
        for (Currency currency : currencies) {
            CurrencyBO currencyBO = new CurrencyBO();
            currencyBO.setCurrencyId(currency.getCurrencyId());
            currencyBO.setCurrencyName(currency.getCurrencyName());
            currencyBO.setCurrencyCode(currency.getCurrencyCode());
            currencyBOs.add(currencyBO);
        }
        return currencyBOs;
    }

	@Override
	public List<StateBO> getAllStatesOfUsa() {
		List<State> states = segmentDao.getAllStatesOfUsa();
		List<StateBO> stateBOs = new ArrayList<StateBO>();
		for (State state : states) {
			StateBO stateBO = new StateBO();
			stateBO.setStateId(state.getStateId());
			stateBO.setCountryId(state.getCountryId());
			stateBO.setStateName(state.getStateName());
			stateBOs.add(stateBO);
		}
		return stateBOs;
	}

	@Override
	public Integer saveCreditRequest(CreditRequestBO creditRequestBO) {
		
		CreditRequest creditRequest = convertBOToEntity(creditRequestBO);

		return creditSubmissionDao.saveCreditRequest(creditRequest);

	}

	@Override
	public Integer saveRequesterDetails(CreditRequestBO creditRequestBO) {
		
		CreditRequest creditRequest = convertBOToEntity(creditRequestBO);

		return creditSubmissionDao.saveCreditRequest(creditRequest);

	}
	
	@Override
	public Integer saveUserRequesterDetails(CreditUserRequest creditUserRequest) {
		
		CreditRequest creditRequest = convertUserBOToEntity(creditUserRequest);

		return creditSubmissionDao.saveUserCreditRequest(creditRequest);

	}
	
	@Override
	public void saveBusinessDetails(CreditRequestBO creditRequestBO,
			Integer creditRequestId) {
		
		CreditRequest creditRequest = convertBOToEntity(creditRequestBO);
		creditRequest.setCreditRequestId(creditRequestId);
		
		creditSubmissionDao.saveCreditRequest(creditRequest);

	}

	@Override
	public void saveCompanyDetails(CreditRequestBO creditRequestBO,
			Integer creditRequestId) {
		
		CreditRequest creditRequest = convertBOToEntity(creditRequestBO);
		creditRequest.setCreditRequestId(creditRequestId);
		
		creditSubmissionDao.saveCreditRequest(creditRequest);

	}

	@Override
	public void saveAgencyDetails(CreditRequestBO creditRequestBO,
			Integer creditRequestId) {
		
		CreditRequest creditRequest = convertBOToEntity(creditRequestBO);
		creditRequest.setCreditRequestId(creditRequestId);

		creditSubmissionDao.saveCreditRequest(creditRequest);
	}

	@Override
	public void saveCollectionDetails(CreditRequestBO creditRequestBO,
			Integer creditRequestId) {
		
		CreditRequest creditRequest = convertBOToEntity(creditRequestBO);
		creditRequest.setCreditRequestId(creditRequestId);

		creditSubmissionDao.saveCreditRequest(creditRequest);

	}

	@Override
	public void saveAdvanceCashDetails(CreditRequestBO creditRequestBO,
			Integer creditRequestId) {
		
		CreditRequest creditRequest = convertBOToEntity(creditRequestBO);
		creditRequest.setCreditRequestId(creditRequestId);

		creditSubmissionDao.saveCreditRequest(creditRequest);

	}

	@Override
	public void saveAttachmentDetails(CreditRequestBO creditRequestBO,
			Integer creditRequestId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void saveFohDetails(CreditRequestBO creditRequestBO,
			Integer creditRequestId) {
		// TODO Auto-generated method stub

	}

	@Override
	public CreditSubmissionBO saveCreditSubmission(
			CreditSubmissionBO creditSubmissionBO) {
		Country country = null;
		State state = null;
		Country agencyCountry = null;
		State agencyState = null;
		CreditSubmission creditSubmission = new CreditSubmission();
		CreditSubmissionBO creditSubmissionBOResult = new CreditSubmissionBO();
		CreditRequestBO creditRequestBO = new CreditRequestBO();

		CreditRequest creditRequest = new CreditRequest();
		creditRequest.setCreditRequestId(creditSubmissionBO.getCreditRequestBO().getCreditRequestId());
				
		creditSubmission.setCreditSubmissionId(creditSubmissionBO
				.getCreditSubmissionId());
		creditSubmission.setPortalId(creditSubmissionBO.getPortalId());
		creditSubmission.setApplicationReferenceNumber(creditSubmissionBO
				.getApplicationReferenceNumber());
		creditSubmission.setStatus(creditSubmissionBO.getStatus());
		creditSubmission.setPartialSave(creditSubmissionBO.getPartialSave());
		creditSubmission.setCreated(new Date());
		creditSubmission.setCreatedBy(creditSubmissionBO.getPortalId());
		creditSubmission.setCreditRequest(creditRequest);
		if (creditSubmissionBO.getRequestRaisedDate() != null) {
		    creditSubmission.setRequestRaisedDate(creditSubmissionBO.getRequestRaisedDate());
		}
		CreditSubmission creditSubmissionResult = creditSubmissionDao
				.saveCreditSubmission(creditSubmission);

		CreditRequest creditRequestResult = creditSubmissionResult.getCreditRequest();
		creditSubmissionBOResult.setCreditSubmissionId(creditSubmissionResult
				.getCreditSubmissionId());
		creditSubmissionBOResult.setPortalId(creditSubmissionResult
				.getPortalId());
		// creditSubmissionBOResult.setApplicationReferenceNumber(creditSubmissionResult.getApplicationReferenceNumber());
		// creditSubmissionBOResult.setStatus(creditSubmissionResult.getStatus());
		creditSubmissionBOResult.setPartialSave(creditSubmissionResult
				.getPartialSave());

		creditRequestBO.setCreditRequestId(creditRequestResult.getCreditRequestId());
		if(creditRequestResult.getSegment() != null){
		Segment segment = segmentDao.findById(creditRequestResult.getSegment());
		SegmentBO segmentBO = new SegmentBO();
		segmentBO.setSegmentId(segment.getSegmentId());
		segmentBO.setSegmentName(segment.getSegmentName());
		creditRequestBO.setSegmentBO(segmentBO);
		}
		
		
		if(creditRequestResult.getRegion() != null){

		Region region = regionDao.findById(creditRequestResult.getRegion());
		RegionBO regionBO = new RegionBO();
		regionBO.setRegionId(region.getRegionId());
		regionBO.setRegionName(region.getRegionName());
		
		creditRequestBO.setRegionBO(regionBO);
		
		}
		
		if(creditRequestResult.getRegion() != null){
			Company company = companyDao.findById(creditRequestResult.getCompany());
			CompanyBO companyBO = new CompanyBO();
			companyBO.setCompanyId(company.getCompanyId());
			companyBO.setCompanyCode(company.getCompanyCode());
			companyBO.setCompanyName(company.getCompanyName());
			
			creditRequestBO.setCompanyBO(companyBO);
		}
		
		if(creditRequestResult.getBusinessArea() != null){
			BusinessArea businessArea = businessAreaDao.findById(creditRequestResult.getBusinessArea());
			BusinessAreaBO businessAreaBO = new BusinessAreaBO();
			businessAreaBO.setBusinessAreaId(businessArea.getBusinessAreaId());
			businessAreaBO.setBusinessAreaCode(businessArea.getBusinessAreaCode());
			businessAreaBO.setBusinessAreaName(businessArea.getBusinessAreaName());
			
			creditRequestBO.setBusinessAreaBO(businessAreaBO);
		}
		
		creditRequestBO.setRequestorName(creditRequestResult.getRequestorName());
		creditRequestBO.setRequestorEmail(creditRequestResult.getRequestorEmail());
		creditRequestBO.setRequestNeededByDate(convertDateToString(creditRequestResult
				.getRequestNeededByDate()));

		creditRequestBO.setRequestedCreditLimit(creditRequestResult
				.getRequestedCreditLimit());
		if(creditRequestResult.getTermStartDate() != null && creditRequestResult.getTermEndDate() != null){
			creditRequestBO.setTermStartDate(convertDateToString(creditRequestResult.getTermStartDate()));
			creditRequestBO.setTermEndDate(convertDateToString(creditRequestResult.getTermEndDate()));
		}

		creditRequestBO.setCompanyName(creditRequestResult.getCompanyName());
		creditRequestBO.setCompanyStreetAddress(creditRequestResult
				.getCompanyStreetAddress());
		if(creditRequestResult.getCompanyCountry() != null){
			country = countryDao.getCountryByName(creditRequestResult.getCompanyCountry());
			if(country != null){
				CountryBO countryBO = new CountryBO();
				countryBO.setCountryId(country.getCountryId());
				countryBO.setCountryName(country.getCountryName());
				creditRequestBO.setCompanyCountry(countryBO);
			}
			
		}
		
		StateBO stateBO = new StateBO();
		if(creditRequestResult.getCompanyState() != null){
			state = stateDao.getStateByName(creditRequestResult.getCompanyState());
			if(state != null){
				stateBO.setStateId(state.getStateId());
				stateBO.setStateName(state.getStateName());
			}
			
		}
		
		
		if(country != null){
			if(country.getCountryName().equalsIgnoreCase("USA") && state != null){
				creditRequestBO.setCompanyState(stateBO);
			}else{
				creditRequestBO.setCompanyState1(creditRequestResult.getCompanyState());
			}
		}
		
		creditRequestBO.setCompanyCity(creditRequestResult.getCompanyCity());
		creditRequestBO.setCompanyZip(creditRequestResult.getCompanyZip());

		creditRequestBO.setCompanyContactName(creditRequestResult
				.getCompanyContactName());
		creditRequestBO.setCompanyContactEmailAddress(creditRequestResult
				.getCompanyContactEmailAddress());
		creditRequestBO
				.setCompanyTelephone(creditRequestResult.getCompanyTelephone());
		creditRequestBO.setCompanyWebAddress(creditRequestResult
				.getCompanyWebAddress());
		creditRequestBO.setcContactFirstName(creditRequestResult
				.getcContactFirstName());
		creditRequestBO.setContactTitle(creditRequestResult.getContactTitle());
		creditRequestBO.setcContactEmail(creditRequestResult.getcContactEmail());
		creditRequestBO.setcContactPhone(creditRequestResult.getcContactPhone());
		creditRequestBO.setCapNet(creditRequestResult.isCapNet());
		creditRequestBO.setCashAdvance(creditRequestResult.isCashAdvance());
		creditRequestBO.setCommissionRateApproved(creditRequestResult
				.isCommissionRateApproved());
		creditRequestBO.setAgencyName(creditRequestResult.getAgencyName());
		creditRequestBO.setAgencyStreetAddress(creditRequestResult
				.getAgencyStreetAddress());
		creditRequestBO.setAgencyCity(creditRequestResult.getAgencyCity());
		
		if(creditRequestResult.getAgencyCountry() != null){
			agencyCountry = countryDao.getCountryByName(creditRequestResult.getAgencyCountry());
			if(agencyCountry != null){
				CountryBO agencyCountryBO = new CountryBO();
				agencyCountryBO.setCountryId(agencyCountry.getCountryId());
				agencyCountryBO.setCountryName(agencyCountry.getCountryName());
				
				creditRequestBO.setAgencyCountry(agencyCountryBO);
			}
			
		}
		
		StateBO agencyStateBO = new StateBO();
		if(creditRequestResult.getAgencyState() != null){
			agencyState = stateDao.getStateByName(creditRequestResult.getAgencyState());
			if(agencyState != null){
				agencyStateBO.setStateId(agencyState.getStateId());
				agencyStateBO.setStateName(agencyState.getStateName());
			}
			
		}
		
		if(agencyCountry != null){
			if(agencyCountry.getCountryName().equalsIgnoreCase("USA") && agencyState != null){
				creditRequestBO.setAgencyState(agencyStateBO);
			}else{
				creditRequestBO.setAgencyState1(creditRequestResult.getAgencyState());
			}
		}
		
		creditRequestBO.setAgencyPhone(creditRequestResult.getAgencyPhone());
		creditRequestBO.setAgencyZip(creditRequestResult.getAgencyZip());
		creditRequestBO
				.setAgencyWebAddress(creditRequestResult.getAgencyWebAddress());
		creditRequestBO.setAgencyContact(creditRequestResult.getAgencyContact());
		creditRequestBO
				.setAccountExecutive(creditRequestResult.getAccountExecutive());
		// creditRequestBO.setaAgencyBillingExport(creditRequestResult.getaAgencyBillingExport());
		creditRequestBO.setAgencyCommissionRate(creditRequestResult
				.getAgencyCommissionRate());
		// creditRequestBO.setAttachment(creditRequestResult.getAttachment());
		creditRequestBO.setBrandName(creditRequestResult.getBrandName());
		creditRequestBO.setDealOrderType(creditRequestResult.getDealOrderType());
		creditRequestBO.setDemographic(creditRequestResult.getDemographic());
		creditRequestBO.setDirect(creditRequestResult.isDirect());
		creditRequestBO.setFohAgency(creditRequestResult.getFohAgency());
		creditRequestBO.setFohIdAdv(creditRequestResult.getFohIdAdv());
		creditRequestBO.setLocalOrNational(creditRequestResult.isLocalOrNational());
		creditRequestBO.setNotes(creditRequestResult.getNotes());
		creditRequestBO.setOkayToContact(creditRequestResult.isOkayToContact());
		creditRequestBO.setOutstandingArAmount(creditRequestResult
				.getOutstandingArAmount());
		creditRequestBO.setPoliticalBuy(creditRequestResult.isPoliticalBuy());
		creditRequestBO.setPriorityCode(creditRequestResult.getPriorityCode());
		creditRequestBO.setProductCode(creditRequestResult.getProductCode());
		creditRequestBO.setRevenueCode1(creditRequestResult.getRevenueCode1());
		creditRequestBO.setRevenueCode2(creditRequestResult.getRevenueCode2());
		creditRequestBO.setRevenueCode3(creditRequestResult.getRevenueCode3());
		creditRequestBO.setTradeBillPayIndicator(creditRequestResult
				.isTradeBillPayIndicator());
		creditRequestBO.setWebBilling(creditRequestResult.isWebBilling());
		
		creditSubmissionBOResult.setCreditRequestBO(creditRequestBO);

		return creditSubmissionBOResult;

	}

	@Override
	public CreditSubmissionBO getCreditRequestDetails(String portalId) {
		Country country = null;
		State state = null;
		Country agencyCountry = null;
		State agencyState = null;
		CreditSubmissionBO creditSubmissionBO = new CreditSubmissionBO();
		CreditRequestBO creditRequestBO = new CreditRequestBO();
		CreditSubmission creditSubmission = creditSubmissionDao.getCreditRequestDetails(portalId);
		if(creditSubmission != null){
			
			CreditRequest creditRequest = creditSubmission.getCreditRequest();
			
			creditSubmissionBO.setCreditSubmissionId(creditSubmission.getCreditSubmissionId());
			creditSubmissionBO.setPortalId(creditSubmission.getPortalId());
			creditSubmissionBO.setApplicationReferenceNumber(creditSubmission.getApplicationReferenceNumber());
			creditSubmissionBO.setStatus(creditSubmission.getStatus());
			creditSubmissionBO.setPartialSave(creditSubmission.getPartialSave());
			
			creditRequestBO.setCreditRequestId(creditRequest.getCreditRequestId());
			
			Segment segment = segmentDao.findById(creditRequest.getSegment());
			SegmentBO segmentBO = new SegmentBO();
			segmentBO.setSegmentId(segment.getSegmentId());
			segmentBO.setSegmentName(segment.getSegmentName());
			
			creditRequestBO.setSegmentBO(segmentBO);
			
			Region region = regionDao.findById(creditRequest.getRegion());
			RegionBO regionBO = new RegionBO();
			regionBO.setRegionId(region.getRegionId());
			regionBO.setRegionName(region.getRegionName());
			
			creditRequestBO.setRegionBO(regionBO);
			
			Company company = companyDao.findById(creditRequest.getCompany());
			CompanyBO companyBO = new CompanyBO();
			companyBO.setCompanyId(company.getCompanyId());
			companyBO.setCompanyCode(company.getCompanyCode());
			companyBO.setCompanyName(company.getCompanyName());
			
			creditRequestBO.setCompanyBO(companyBO);
			
			BusinessArea businessArea = businessAreaDao.findById(creditRequest.getBusinessArea());
			BusinessAreaBO businessAreaBO = new BusinessAreaBO();
			businessAreaBO.setBusinessAreaId(businessArea.getBusinessAreaId());
			businessAreaBO.setBusinessAreaCode(businessArea.getBusinessAreaCode());
			businessAreaBO.setBusinessAreaName(businessArea.getBusinessAreaName());
			
			creditRequestBO.setBusinessAreaBO(businessAreaBO);

			creditRequestBO.setRequestorName(creditRequest.getRequestorName());
			creditRequestBO.setRequestorEmail(creditRequest.getRequestorEmail());
			creditRequestBO.setRequestNeededByDate(convertDateToString(creditRequest
					.getRequestNeededByDate()));

			creditRequestBO.setRequestedCreditLimit(creditRequest
					.getRequestedCreditLimit());
			if(creditRequest.getTermStartDate() != null && creditRequest.getTermEndDate() != null){
				creditRequestBO.setTermStartDate(convertDateToString(creditRequest.getTermStartDate()));
				creditRequestBO.setTermEndDate(convertDateToString(creditRequest.getTermEndDate()));
			}

			creditRequestBO.setCompanyName(creditRequest.getCompanyName());
			creditRequestBO.setCompanyStreetAddress(creditRequest
					.getCompanyStreetAddress());
			
			
			if(creditRequest.getCompanyCountry() != null){
				country = countryDao.getCountryByName(creditRequest.getCompanyCountry());
				if(country != null){
					CountryBO countryBO = new CountryBO();
					countryBO.setCountryId(country.getCountryId());
					countryBO.setCountryName(country.getCountryName());
					creditRequestBO.setCompanyCountry(countryBO);
				}
			}
			
			StateBO stateBO = new StateBO();
			if(creditRequest.getCompanyState() != null){
				state = stateDao.getStateByName(creditRequest.getCompanyState());
				if(state != null){
					stateBO.setStateId(state.getStateId());
					stateBO.setStateName(state.getStateName());
				}
				
			}
			
			if(country != null){
				if(country.getCountryName().equalsIgnoreCase("USA") && state != null){
					creditRequestBO.setCompanyState(stateBO);
				}else{
					creditRequestBO.setCompanyState1(creditRequest.getCompanyState());
				}
			}
			
			creditRequestBO.setCompanyCity(creditRequest.getCompanyCity());
			creditRequestBO.setCompanyZip(creditRequest.getCompanyZip());

			creditRequestBO.setCompanyContactName(creditRequest
					.getCompanyContactName());
			creditRequestBO.setCompanyContactEmailAddress(creditRequest
					.getCompanyContactEmailAddress());
			creditRequestBO
					.setCompanyTelephone(creditRequest.getCompanyTelephone());
			creditRequestBO.setCompanyWebAddress(creditRequest
					.getCompanyWebAddress());
			creditRequestBO.setcContactFirstName(creditRequest
					.getcContactFirstName());
			creditRequestBO.setContactTitle(creditRequest.getContactTitle());
			creditRequestBO.setcContactEmail(creditRequest.getcContactEmail());
			creditRequestBO.setcContactPhone(creditRequest.getcContactPhone());
			creditRequestBO.setCapNet(creditRequest.isCapNet());
			creditRequestBO.setCashAdvance(creditRequest.isCashAdvance());
			creditRequestBO.setCommissionRateApproved(creditRequest
					.isCommissionRateApproved());
			creditRequestBO.setAgencyName(creditRequest.getAgencyName());
			creditRequestBO.setAgencyStreetAddress(creditRequest
					.getAgencyStreetAddress());
			creditRequestBO.setAgencyCity(creditRequest.getAgencyCity());
			
			if(creditRequest.getAgencyCountry() != null){
				agencyCountry = countryDao.getCountryByName(creditRequest.getAgencyCountry());
				if(agencyCountry != null){
					CountryBO agencyCountryBO = new CountryBO();
					agencyCountryBO.setCountryId(agencyCountry.getCountryId());
					agencyCountryBO.setCountryName(agencyCountry.getCountryName());
					
					creditRequestBO.setAgencyCountry(agencyCountryBO);
				}
				
			}
			
			StateBO agencyStateBO = new StateBO();
			if(creditRequest.getAgencyState() != null){
				agencyState = stateDao.getStateByName(creditRequest.getAgencyState());
				if(agencyState != null){
					agencyStateBO.setStateId(agencyState.getStateId());
					agencyStateBO.setStateName(agencyState.getStateName());
				}
				
			}
			
			
			if(agencyCountry != null){
				if(agencyCountry.getCountryName().equalsIgnoreCase("USA") && agencyState != null){
					creditRequestBO.setAgencyState(agencyStateBO);
				}else{
					creditRequestBO.setAgencyState1(creditRequest.getAgencyState());
				}
			}
			
			creditRequestBO.setAgencyPhone(creditRequest.getAgencyPhone());
			creditRequestBO.setAgencyZip(creditRequest.getAgencyZip());
			creditRequestBO
					.setAgencyWebAddress(creditRequest.getAgencyWebAddress());
			creditRequestBO.setAgencyContact(creditRequest.getAgencyContact());
			creditRequestBO
					.setAccountExecutive(creditRequest.getAccountExecutive());
			// creditRequestBO.setaAgencyBillingExport(creditRequest.getaAgencyBillingExport());
			creditRequestBO.setAgencyCommissionRate(creditRequest
					.getAgencyCommissionRate());
			// creditRequestBO.setAttachment(creditRequest.getAttachment());
			creditRequestBO.setBrandName(creditRequest.getBrandName());
			creditRequestBO.setDealOrderType(creditRequest.getDealOrderType());
			creditRequestBO.setDemographic(creditRequest.getDemographic());
			creditRequestBO.setDirect(creditRequest.isDirect());
			creditRequestBO.setFohAgency(creditRequest.getFohAgency());
			creditRequestBO.setFohIdAdv(creditRequest.getFohIdAdv());
			creditRequestBO.setLocalOrNational(creditRequest.isLocalOrNational());
			creditRequestBO.setNotes(creditRequest.getNotes());
			creditRequestBO.setOkayToContact(creditRequest.isOkayToContact());
			creditRequestBO.setOutstandingArAmount(creditRequest
					.getOutstandingArAmount());
			creditRequestBO.setPoliticalBuy(creditRequest.isPoliticalBuy());
			creditRequestBO.setPriorityCode(creditRequest.getPriorityCode());
			creditRequestBO.setProductCode(creditRequest.getProductCode());
			creditRequestBO.setRevenueCode1(creditRequest.getRevenueCode1());
			creditRequestBO.setRevenueCode2(creditRequest.getRevenueCode2());
			creditRequestBO.setRevenueCode3(creditRequest.getRevenueCode3());
			creditRequestBO.setTradeBillPayIndicator(creditRequest
					.isTradeBillPayIndicator());
			creditRequestBO.setWebBilling(creditRequest.isWebBilling());
			
			
		} else {
		    User user = userDao.findByPortalId(portalId);
		    CreditSubmission creditSubmissionResult = creditSubmissionDao.getLastCreditRequestDetails(portalId);
		    if(creditSubmissionResult != null){
		    	CreditRequest creditRequest = creditSubmissionResult.getCreditRequest();
		    	
		    	Segment segment = segmentDao.findById(creditRequest.getSegment());
				SegmentBO segmentBO = new SegmentBO();
				segmentBO.setSegmentId(segment.getSegmentId());
				segmentBO.setSegmentName(segment.getSegmentName());
				
				creditRequestBO.setSegmentBO(segmentBO);
				
				Region region = regionDao.findById(creditRequest.getRegion());
				RegionBO regionBO = new RegionBO();
				regionBO.setRegionId(region.getRegionId());
				regionBO.setRegionName(region.getRegionName());
				
				creditRequestBO.setRegionBO(regionBO);
				
				Company company = companyDao.findById(creditRequest.getCompany());
				CompanyBO companyBO = new CompanyBO();
				companyBO.setCompanyId(company.getCompanyId());
				companyBO.setCompanyCode(company.getCompanyCode());
				companyBO.setCompanyName(company.getCompanyName());
				
				creditRequestBO.setCompanyBO(companyBO);
				
				BusinessArea businessArea = businessAreaDao.findById(creditRequest.getBusinessArea());
				BusinessAreaBO businessAreaBO = new BusinessAreaBO();
				businessAreaBO.setBusinessAreaId(businessArea.getBusinessAreaId());
				businessAreaBO.setBusinessAreaCode(businessArea.getBusinessAreaCode());
				businessAreaBO.setBusinessAreaName(businessArea.getBusinessAreaName());
				
				creditRequestBO.setBusinessAreaBO(businessAreaBO);
		    	
		    }
		    creditRequestBO.setRequestorEmail(user.getEmail());
		    creditRequestBO.setRequestorName(user.getUserName());
		    
		    // Default value for currency, need to put in constant file or properties file.
		    CurrencyBO currencyBO = new CurrencyBO();
		    currencyBO.setCurrencyCode("USD");
		    currencyBO.setCurrencyName("US Dollar");
		    creditRequestBO.setCurrencyBO(currencyBO);
		    
//		    SegmentBO segmentBO = new SegmentBO();
//		    segmentBO.setSegmentId(0);
//		    segmentBO.setSegmentName("Select Segment");
//		    creditRequestBO.setSegmentBO(segmentBO);
//		    
//		    RegionBO regionBO = new RegionBO();
//		    regionBO.setRegionId(0);
//		    regionBO.setRegionName("Select Region");
//		    creditRequestBO.setRegionBO(regionBO);
//			
//			CompanyBO companyBO = new CompanyBO();
//			companyBO.setCompanyId(0);
//			companyBO.setCompanyName("Select Company");
//			companyBO.setCompanyCode("");
//			creditRequestBO.setCompanyBO(companyBO);
//			
//			BusinessAreaBO businessAreaBO = new BusinessAreaBO();
//			businessAreaBO.setBusinessAreaId(0);
//			businessAreaBO.setBusinessAreaName("Select Business Area");
//			businessAreaBO.setBusinessAreaCode("");
//			creditRequestBO.setBusinessAreaBO(businessAreaBO);
			
			/*CountryBO countryBO = new CountryBO();
			countryBO.setCountryId(0);
			countryBO.setCountryName("Select Country");
			creditRequestBO.setCompanyCountry(countryBO);
			
			StateBO stateBO = new StateBO();
			stateBO.setStateId(0);
			stateBO.setStateName("Select State");
			creditRequestBO.setCompanyState(stateBO);
			
			creditRequestBO.setAgencyCountry(countryBO);
			creditRequestBO.setAgencyState(stateBO);*/
			
		    Date today = new Date();
//		    Date defaultDate = DateUtils.addDays(today, 5);
		    creditRequestBO.setRequestNeededByDate(convertDateToString(today));
		    
		}
		creditSubmissionBO.setCreditRequestBO(creditRequestBO);
		return creditSubmissionBO;
	}

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
	
    private Date convertStringToDate(String inputDate){
		Date date = null;
	    SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
	    try
	    {
	      date = formatter.parse(inputDate);
	    }
	    catch (ParseException e)
	    {
	      e.printStackTrace();
	    }
		return date;
	}
	
	private String convertDateToString(Date inputDate){
		
	    SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
	    String output = formatter.format(inputDate);
		return output;
	}
	
	private CreditRequest convertBOToEntity(CreditRequestBO creditRequestBO){
		
		CreditRequest creditRequest = new CreditRequest();
		creditRequest.setCreditRequestId(creditRequestBO.getCreditRequestId());
		
		SegmentBO segmentBO = creditRequestBO.getSegmentBO();
		if(segmentBO !=null){
			creditRequest.setSegment(segmentBO.getSegmentId());
		}
		
		
		RegionBO regionBO = creditRequestBO.getRegionBO();
		if(regionBO !=null){
		creditRequest.setRegion(regionBO.getRegionId());
		}
		
		CompanyBO companyBO = creditRequestBO.getCompanyBO();
		if(companyBO !=null){
		creditRequest.setCompany(companyBO.getCompanyId());
		}
		
		BusinessAreaBO businessAreaBO = creditRequestBO.getBusinessAreaBO();
		if(businessAreaBO !=null){
		creditRequest.setBusinessArea(businessAreaBO.getBusinessAreaId());
		}
		
		creditRequest.setRequestorName(creditRequestBO.getRequestorName());
		creditRequest.setRequestorEmail(creditRequestBO.getRequestorEmail());
		creditRequest.setRequestNeededByDate(convertStringToDate(creditRequestBO.getRequestNeededByDate()));
		creditRequest.setAeLastname(creditRequestBO.getAeLastName());
		
		creditRequest.setRequestedCreditLimit(creditRequestBO
				.getRequestedCreditLimit());
		if(creditRequestBO.getTermStartDate() != null && creditRequestBO.getTermEndDate() != null){
			creditRequest.setTermStartDate(convertStringToDate(creditRequestBO.getTermStartDate()));
			creditRequest.setTermEndDate(convertStringToDate(creditRequestBO.getTermEndDate()));
		}

		creditRequest.setCompanyName(creditRequestBO.getCompanyName());
		creditRequest.setCompanyStreetAddress(creditRequestBO
				.getCompanyStreetAddress());
		
		CountryBO countryBO = creditRequestBO.getCompanyCountry();
		if(countryBO != null){
			creditRequest.setCompanyCountry(countryBO.getCountryName());
		}
		
		StateBO stateBO = creditRequestBO.getCompanyState();
				
		if(stateBO != null){
				creditRequest.setCompanyState(stateBO.getStateName());
		} else {
				creditRequest.setCompanyState(creditRequestBO.getCompanyState1());
		}
		
		creditRequest.setCompanyCity(creditRequestBO.getCompanyCity());
		creditRequest.setCompanyZip(creditRequestBO.getCompanyZip());

		creditRequest.setCompanyContactName(creditRequestBO
				.getCompanyContactName());
		creditRequest.setCompanyContactEmailAddress(creditRequestBO
				.getCompanyContactEmailAddress());
		creditRequest
				.setCompanyTelephone(creditRequestBO.getCompanyTelephone());
		creditRequest.setCompanyWebAddress(creditRequestBO
				.getCompanyWebAddress());
		creditRequest.setcContactFirstName(creditRequestBO
				.getcContactFirstName());
		creditRequest.setContactTitle(creditRequestBO.getContactTitle());
		creditRequest.setcContactEmail(creditRequestBO.getcContactEmail());
		creditRequest.setcContactPhone(creditRequestBO.getcContactPhone());
		creditRequest.setCapNet(creditRequestBO.isCapNet());
		creditRequest.setCashAdvance(creditRequestBO.isCashAdvance());
		creditRequest.setCommissionRateApproved(creditRequestBO
				.isCommissionRateApproved());
		creditRequest.setAgencyName(creditRequestBO.getAgencyName());
		creditRequest.setAgencyStreetAddress(creditRequestBO
				.getAgencyStreetAddress());
		creditRequest.setAgencyCity(creditRequestBO.getAgencyCity());
		
		CountryBO agencyCountryBO = creditRequestBO.getAgencyCountry();
		if(agencyCountryBO != null){
			creditRequest.setAgencyCountry(agencyCountryBO.getCountryName());
		}
		
		StateBO agencyStateBO = creditRequestBO.getAgencyState();
		
		if (agencyStateBO != null) {
			creditRequest.setAgencyState(agencyStateBO.getStateName());
		} else {
			creditRequest.setAgencyState(creditRequestBO.getAgencyState1());
		}
		

		creditRequest.setAgencyPhone(creditRequestBO.getAgencyPhone());
		creditRequest.setAgencyZip(creditRequestBO.getAgencyZip());
		creditRequest
				.setAgencyWebAddress(creditRequestBO.getAgencyWebAddress());
		creditRequest.setAgencyContact(creditRequestBO.getAgencyContact());
		creditRequest
				.setAccountExecutive(creditRequestBO.getAccountExecutive());
		// creditRequest.setaAgencyBillingExport(creditRequestBO.getaAgencyBillingExport());
		creditRequest.setAgencyCommissionRate(creditRequestBO
				.getAgencyCommissionRate());
		// creditRequest.setAttachment(creditRequestBO.getAttachment());
		creditRequest.setBrandName(creditRequestBO.getBrandName());
		creditRequest.setDealOrderType(creditRequestBO.getDealOrderType());
		creditRequest.setDemographic(creditRequestBO.getDemographic());
		creditRequest.setDirect(creditRequestBO.isDirect());
		creditRequest.setFohAgency(creditRequestBO.getFohAgency());
		creditRequest.setFohIdAdv(creditRequestBO.getFohIdAdv());
		creditRequest.setLocalOrNational(creditRequestBO.isLocalOrNational());
		creditRequest.setNotes(creditRequestBO.getNotes());
		creditRequest.setOkayToContact(creditRequestBO.isOkayToContact());
		creditRequest.setOutstandingArAmount(creditRequestBO
				.getOutstandingArAmount());
		creditRequest.setPoliticalBuy(creditRequestBO.isPoliticalBuy());
		creditRequest.setPriorityCode(creditRequestBO.getPriorityCode());
		creditRequest.setProductCode(creditRequestBO.getProductCode());
		creditRequest.setRevenueCode1(creditRequestBO.getRevenueCode1());
		creditRequest.setRevenueCode2(creditRequestBO.getRevenueCode2());
		creditRequest.setRevenueCode3(creditRequestBO.getRevenueCode3());
		creditRequest.setTradeBillPayIndicator(creditRequestBO
				.isTradeBillPayIndicator());
		creditRequest.setWebBilling(creditRequestBO.isWebBilling());
		creditRequest.setCreditRequestId(creditRequestBO.getCreditRequestId());
		creditRequest.setCreated(new Date());
		creditRequest.setCreatedBy(creditRequestBO.getPortalId());
		
		return creditRequest;
		
	}
	
	
	
      private CreditRequest convertUserBOToEntity(CreditUserRequest creditUserRequest){
		
		CreditRequest creditRequest = new CreditRequest();
		creditRequest.setRequestorName(creditUserRequest.getRequestorName());
		creditRequest.setRequestorEmail(creditUserRequest.getRequestorEmail());
		creditRequest.setAeLastname(creditUserRequest.getAeLastName());
		creditRequest.setCreditRequestId(1);
	     
		return creditRequest;
		
	}


	@Override
	public List<RegionBO> getAllRegions() {
		List<Region> regions = regionDao.findAllRegions();
		List<RegionBO> regionBOs = new ArrayList<RegionBO>();
		for (Region region : regions) {
			RegionBO regionBO = new RegionBO();
			regionBO.setRegionId(region.getRegionId());
			regionBO.setRegionName(region.getRegionName());
			regionBOs.add(regionBO);
		}
		return regionBOs;
	}

	@Override
	public List<CompanyBO> getAllCompanys() {
		List<Company> companys = companyDao.findAllCompany();
		List<CompanyBO> companyBOs= new ArrayList<CompanyBO>();
		for (Company company : companys) {
			CompanyBO companyBO = new CompanyBO();
			companyBO.setCompanyId(company.getCompanyId());
			companyBO.setCompanyCode(company.getCompanyCode());
			companyBO.setCompanyName(company.getCompanyName());
			companyBOs.add(companyBO);
		}
		return companyBOs;
	}
}
