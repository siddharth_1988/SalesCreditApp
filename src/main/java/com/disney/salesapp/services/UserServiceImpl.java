/**
 * 
 */
package com.disney.salesapp.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.disney.salesapp.bo.UserBO;
import com.disney.salesapp.dao.UserDao;
import com.disney.salesapp.model.entities.User;

/**
 * @author MADHS008
 *
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public UserBO findById(Integer userID) {
		UserBO userBO = new UserBO();
		User user = userDao.findById(userID);
		userBO.setUserId(user.getUserId());
		userBO.setUserName(user.getUserName());
		userBO.setPortalId(user.getPortalId());
		userBO.setStatus(user.getStatus());
		userBO.setEmail(user.getEmail());
		userBO.setFirstName(user.getFirstName());
		userBO.setLastName(user.getLastName());
		userBO.setInitials(user.getInitials());
		userBO.setActive(user.isActive());
		userBO.setPhone(user.getPhone());
		userBO.setFax(user.getFax());
		userBO.setCompany(user.getCompany());
		userBO.setAddress1(user.getAddress1());
		userBO.setAddress2(user.getAddress2());
		userBO.setCity(user.getCity());
		userBO.setZip(user.getZip());
		userBO.setState(user.getState());
		userBO.setCountry(user.getCountry());
		return userBO;
	}

	@Override
	public List<UserBO> findAllUsers() {
		List<User> userList = userDao.findAllUsers();
		List<UserBO> userBOList = new ArrayList<UserBO>();
		for(User user:userList){
			UserBO userBO = new UserBO();
			userBO.setUserId(user.getUserId());
			userBO.setUserName(user.getUserName());
			userBO.setPortalId(user.getPortalId());
			userBO.setEmail(user.getEmail());
			userBO.setFirstName(user.getFirstName());
			userBO.setLastName(user.getLastName());
			userBO.setInitials(user.getInitials());
			if(user.isActive() != null){
				userBO.setActive(user.isActive());
			}
			userBO.setPhone(user.getPhone());
			userBO.setFax(user.getFax());
			userBO.setCompany(user.getCompany());
			userBO.setAddress1(user.getAddress1());
			userBO.setAddress2(user.getAddress2());
			userBO.setCity(user.getCity());
			userBO.setZip(user.getZip());
			userBO.setState(user.getState());
			userBO.setCountry(user.getCountry());
			userBOList.add(userBO);
		}
		
		return userBOList;
	}

	@Override
	public void saveUser(UserBO userBO) {
		User user = new User();
		user.setUserName(userBO.getUserName());
		user.setPortalId("test");
		user.setEmail(userBO.getEmail());
		user.setStatus(userBO.getStatus());
		user.setFirstName(userBO.getFirstName());
		user.setLastName(userBO.getLastName());
		user.setInitials(userBO.getInitials());
		user.setActive(userBO.isActive());
		user.setPhone(userBO.getPhone());
		user.setFax(userBO.getFax());
		user.setCompany(userBO.getCompany());
		user.setAddress1(userBO.getAddress1());
		user.setAddress2(userBO.getAddress2());
		user.setCity(userBO.getCity());
		user.setZip(userBO.getZip());
		user.setState(userBO.getState());
		user.setCountry(userBO.getCountry());
		user.setCreated(new Date());
		user.setCreatedBy("test");
		
		userDao.saveUser(user);
	}

	@Override
	public void updatUser(UserBO userBO) {
		User user = new User();
		user.setUserId(userBO.getUserId());
		user.setUserName(userBO.getUserName());
		user.setPortalId(userBO.getPortalId());
		user.setEmail(userBO.getEmail());
		user.setStatus(userBO.getStatus());
		user.setFirstName(userBO.getFirstName());
		user.setLastName(userBO.getLastName());
		user.setInitials(userBO.getInitials());
		user.setActive(userBO.isActive());
		user.setPhone(userBO.getPhone());
		user.setFax(userBO.getFax());
		user.setCompany(userBO.getCompany());
		user.setAddress1(userBO.getAddress1());
		user.setAddress2(userBO.getAddress2());
		user.setCity(userBO.getCity());
		user.setZip(userBO.getZip());
		user.setState(userBO.getState());
		user.setCountry(userBO.getCountry());
		user.setUpdated(new Date());
		user.setUpdatedBy("test");
		userDao.updatUser(user);
	}
	
	@Override
    public UserBO findByUserName(String userName) {
        User user = userDao.findByUserName(userName);
        UserBO userBO = new UserBO();
        userBO.setUserName(user.getUserName());
        userBO.setEmail(user.getEmail());
        return userBO;
    }

}