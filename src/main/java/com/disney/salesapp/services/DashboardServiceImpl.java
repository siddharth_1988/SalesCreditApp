package com.disney.salesapp.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.disney.salesapp.bo.DashboardBO;
import com.disney.salesapp.dao.CreditSubmissionDao;
import com.disney.salesapp.model.entities.CreditSubmission;

@Service("DashboardService")
@Transactional
public class DashboardServiceImpl implements DashboardService {
    
    @Autowired
    private CreditSubmissionDao creditSubmissionDao;
    
    public List<DashboardBO> getDashboardDetails(String portalId) {
        List<CreditSubmission> creditSubmissions = creditSubmissionDao
                .getCreditSubmissionByPortalId(portalId);

        List<DashboardBO> dashboardBOs = new ArrayList<DashboardBO>();
        for (CreditSubmission creditSubmission : creditSubmissions) {
            DashboardBO dashboardBO = new DashboardBO();
            dashboardBO.setCreditRequestId(creditSubmission
                    .getCreditSubmissionId());
            dashboardBO.setCreditRequestNumber(creditSubmission
                    .getApplicationReferenceNumber());
            dashboardBO.setCreditRequestStatus(creditSubmission.getStatus());
            dashboardBO.setRequestRaisedDate(creditSubmission
                    .getRequestRaisedDate());

            if (creditSubmission.getRequestRaisedDate() != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(creditSubmission.getRequestRaisedDate());
                cal.add(Calendar.HOUR_OF_DAY, 48);
                cal.getTime();
                dashboardBO.setSlaDate(cal.getTime());
            }
            if (creditSubmission.getCreditRequest() != null 
                    && creditSubmission.getCreditRequest().getRequestNeededByDate() != null) {
                dashboardBO.setRequestNeededByDate(creditSubmission.getCreditRequest().getRequestNeededByDate());
            }
            dashboardBOs.add(dashboardBO);
        }
        return dashboardBOs;
    }


}
