/**
 * 
 */
package com.disney.salesapp.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author GHV001
 *
 */
public class WorkflowUtils {

    // private static Logger log = Logger.getLogger(WorkflowUtils.class);

    public static String getProtocol(HttpServletRequest req) {
        String protocol;
        if (req.isSecure()) {
            protocol = "https://";
        } else {
            protocol = "http://";
        }
        return protocol;
    }

}
