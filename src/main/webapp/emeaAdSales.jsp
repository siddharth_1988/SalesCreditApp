<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="utf-8">
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript" src="./resources/js/salesApp.js"></script>	
</head>

<body>
	<form name="emeaAdSalesForm" class="form-horizontal row-border"
		id="emeaAdSalesForm" method="post"
		action="xmlbasedcreditRequestSubmission">
		<div id="custDetails" class="panel form-horizontal">
			<div id="adsales" class="panel-heading">Request Submission</div>
			<div class="panel-body">

				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-target="#requestorDetails"
									href="#requestorDetails"> Requester Details </a>
							</h4>
						</div>
						<div id="requestorDetails" class="panel-collapse collapse in">
							<div class="panel-body">

								<div class="row form-group"
									data-ng-class="{ 'has-error' : emeaAdSalesForm.requestorName.$invalid && !emeaAdSalesForm.requestorName.$pristine }">
									<div class="form-group no-margin-hr required">
										<label class="col-md-2 control-label">Requester Name:</label>
										<div class="col-md-7">
											<input type="text" name="requestorName"
												data-ng-model="creditRequest.requestorName"
												class='form-control textboxmargin' required
												data-ng-pattern='/^[a-z\s]+$/i' />
											<p
												data-ng-show="emeaAdSalesForm.requestorName.$error.pattern"
												class="help-block">Please enter alphabets only.</p>
										</div>
									</div>
								</div>

								<div class="row form-group"
									data-ng-class="{ 'has-error' : emeaAdSalesForm.requestorEmail.$invalid && !emeaAdSalesForm.requestorEmail.$pristine }">
									<div class="form-group no-margin-hr required">
										<label class="col-md-2 control-label">Requester Email:</label>
										<div class="col-md-7">
											<input type="email" name="requestorEmail"
												data-ng-model="creditRequest.requestorEmail"
												class='form-control textboxmargin' required />
											<p
												data-ng-show="emeaAdSalesForm.requestorEmail.$invalid && !emeaAdSalesForm.requestorEmail.$pristine"
												class="help-block">Please enter valid email.</p>
										</div>
									</div>
								</div>

								<div class="row form-group"
									data-ng-class="{ 'has-error' : emeaAdSalesForm.outstandingArAmount.$invalid && !emeaAdSalesForm.outstandingArAmount.$pristine }">
									<div class="form-group no-margin-hr required">
										<label class="col-md-2 control-label">Outstanding A/R
											Amount:</label>
										<div class="col-md-7">
											<input type="text" name="outstandingArAmount"
												data-ng-model="creditRequest.outstandingArAmount"
												class='form-control textboxmargin'
												data-ng-pattern='/^[0-9.]*$/' required="required" />
											<p
												data-ng-show="emeaAdSalesForm.outstandingArAmount.$error.pattern"
												class="help-block">Please enter positive number.</p>
										</div>
									</div>
								</div>
								<a data-toggle="collapse" data-target="#companyDetails"
									data-parent="#requestorDetails" href="#companyDetails"
									class="btn btn-primary pull-right continue"
									onclick="$('#requestorDetails').collapse('hide');"
									data-ng-disabled="emeaAdSalesForm.requestorName.$invalid || emeaAdSalesForm.requestorEmail.$invalid
									 || emeaAdSalesForm.outstandingArAmount.$invalid">Next
								</a>
							</div>
						</div>

					</div>
				</div>

				<div class="panel panel-default" id="companyDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#companyDetails"
								href="#companyDetails" class="collapsed"
								data-ng-disabled="emeaAdSalesForm.requestorName.$invalid || emeaAdSalesForm.requestorEmail.$invalid
									 || emeaAdSalesForm.outstandingArAmount.$invalid">
								Company Details </a>
						</h4>
					</div>
					<div id="companyDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.companyName.$invalid && !emeaAdSalesForm.companyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company/Licensee
										Name:</label>
									<div class="col-md-7">
										<input type="text" name="companyName"
											data-ng-model="creditRequest.companyName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="emeaAdSalesForm.companyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Street
										Address:</label>
									<div class="col-md-7">
										<input type="text" name="companyStreetAddress"
											data-ng-model="creditRequest.companyStreetAddress"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.companyCity.$invalid && !emeaAdSalesForm.companyCity.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company City:</label>
									<div class="col-md-7">
										<input type="text" name="companyCity"
											data-ng-model="creditRequest.companyCity"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="emeaAdSalesForm.companyCity.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div data-ng-init="getCountryList()"
									class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Country:</label>
									<div class="col-md-7">
										<select id="companyCountry" name="companyCountry"
											data-ng-model="creditRequest.companyCountry"
											data-ng-change="isUSASelected(creditRequest.companyCountry)"
											class='form-control' required="required">
											<option value="">-- Select Country --</option>
											<option data-ng-repeat="countryBO in countryList"
												value="{{countryBO.countryName}}">{{countryBO.countryName}}</option>
										</select>
									</div>
								</div>
							</div>
							<div id="usaStateId" class="row form-group" data-ng-show="isUSA" class="ng-hide">
								<div data-ng-init="getUsaStateList()"
									class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company State:</label>
									<div class="col-md-7">
										<select id="companyState" name="companyState"
											data-ng-model="creditRequest.companyState"
											class='form-control' required="required">
											<option value="">-- Select State --</option>
											<option data-ng-repeat="stateBO in stateList"
												value="{{stateBO.stateName}}">{{stateBO.stateName}}</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row form-group" data-ng-show="showdefaultState" class="ng-hide"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.companyState.$invalid && !emeaAdSalesForm.companyState.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company State:</label>
									<div class="col-md-7">
										<input type="text" name="companyState"
											data-ng-model="creditRequest.companyState"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="emeaAdSalesForm.companyState.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.companyZip.$invalid && !emeaAdSalesForm.companyZip.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Zip:</label>
									<div class="col-md-7">
										<input type="text" name="companyZip"
											data-ng-model="creditRequest.companyZip"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="emeaAdSalesForm.companyZip.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.companyContactName.$invalid && !emeaAdSalesForm.companyContactName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Contact
										Name:</label>
									<div class="col-md-7">
										<input type="text" name="companyContactName"
											data-ng-model="creditRequest.companyContactName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p
											data-ng-show="emeaAdSalesForm.companyContactName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>



							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.companyContactEmailAddress.$invalid && !emeaAdSalesForm.companyContactEmailAddress.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Contact
										Email Address:</label>
									<div class="col-md-7">
										<input type="email" name="companyContactEmailAddress"
											data-ng-model="creditRequest.companyContactEmailAddress"
											class='form-control textboxmargin' required />
										<p
											data-ng-show="emeaAdSalesForm.companyContactEmailAddress.$invalid && !emeaAdSalesForm.companyContactEmailAddress.$pristine"
											class="help-block">Please enter valid email.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Ok to Contact?:</label>
									<div class="col-md-7">
										<input
											style="margin-left: -316px; margin-top: 4px; height: 25px;"
											type="checkbox" name="okayToContact"
											data-ng-model="creditRequest.okayToContact"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>

							<a data-toggle="collapse" data-target="#requestorDetails"
								data-parent="#companyDetails" href="#requestorDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#companyDetails').collapse('hide');">Previous </a> <a
								data-toggle="collapse" data-target="#agencyDetails"
								data-parent="#companyDetails" href="#agencyDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#companyDetails').collapse('hide');"
								data-ng-disabled="emeaAdSalesForm.companyName.$invalid || emeaAdSalesForm.companyStreetAddress.$invalid
									 || emeaAdSalesForm.companyCity.$invalid || emeaAdSalesForm.companyState.$invalid || 
									 emeaAdSalesForm.companyZip.$invalid || emeaAdSalesForm.companyCountry.$invalid
									 || emeaAdSalesForm.companyContactName.$invalid || 
									 emeaAdSalesForm.companyContactEmailAddress.$invalid || 
									 emeaAdSalesForm.okayToContact.$invalid">Next
							</a>
						</div>
					</div>
				</div>


				<div class="panel panel-default" id="agencyDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#agencyDetails"
								href="#agencyDetails" class="collapsed"
								data-ng-disabled="emeaAdSalesForm.companyName.$invalid || emeaAdSalesForm.companyStreetAddress.$invalid
									 || emeaAdSalesForm.companyCity.$invalid || emeaAdSalesForm.companyState.$invalid || 
									 emeaAdSalesForm.companyZip.$invalid
									 || emeaAdSalesForm.companyCountry.$invalid || emeaAdSalesForm.companyContactName.$invalid || 
									 emeaAdSalesForm.companyContactEmailAddress.$invalid || 
									 emeaAdSalesForm.okayToContact.$invalid">Agency
								Details </a>
						</h4>
					</div>
					<div id="agencyDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.agencyName.$invalid && !emeaAdSalesForm.agencyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Name:</label>
									<div class="col-md-6">
										<input type="text" name="agencyName"
											data-ng-model="creditRequest.agencyName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="emeaAdSalesForm.agencyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Street
										Address:</label>
									<div class="col-md-6">
										<input type="text" name="agencyStreetAddress"
											data-ng-model="creditRequest.agencyStreetAddress"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.agencyCity.$invalid && !emeaAdSalesForm.agencyCity.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency City:</label>
									<div class="col-md-6">
										<input type="text" name="agencyCity"
											data-ng-model="creditRequest.agencyCity"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="emeaAdSalesForm.agencyCity.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div data-ng-init="getCountryList()"
									class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Country:</label>
									<div class="col-md-6">
										<select id="agencyCountry" name="agencyCountry"
											data-ng-model="creditRequest.agencyCountry"
											data-ng-change="isUSASelectedForAgency(creditRequest.agencyCountry)"
											class='form-control' required="required">
											<option value="">-- Select Country --</option>
											<option data-ng-repeat="countryBO in countryList"
												value="{{countryBO.countryName}}">{{countryBO.countryName}}</option>
										</select>
									</div>
								</div>
							</div>

							<div id="usaStateAgencyId" class="row form-group"
								data-ng-show="isUSAForAgency" class="ng-hide">
								<div data-ng-init="getUsaStateListForAgency()"
									class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency State:</label>
									<div class="col-md-6">
										<select id="agencyState" name="agencyState"
											data-ng-model="creditRequest.agencyState"
											class='form-control' required="required">
											<option value="">-- Select State --</option>
											<option data-ng-repeat="stateBO in agencyStateList"
												value="{{stateBO.stateName}}">{{stateBO.stateName}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row form-group" data-ng-show="defaultAgencyState"
								class="ng-hide"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.agencyState.$invalid && !emeaAdSalesForm.agencyState.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency State:</label>
									<div class="col-md-6">
										<input type="text" name="agencyState"
											data-ng-model="creditRequest.agencyState"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="emeaAdSalesForm.agencyState.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : emeaAdSalesForm.agencyZip.$invalid && !emeaAdSalesForm.agencyZip.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Zip:</label>
									<div class="col-md-6">
										<input type="text" name="agencyZip"
											data-ng-model="creditRequest.agencyZip"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="emeaAdSalesForm.agencyZip.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>



							<a data-toggle="collapse" data-target="#companyDetails"
								data-parent="#agencyDetails" href="#companyDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#agencyDetails').collapse('hide');">Previous </a>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer text-right">
			<button class="btn btn-primary"
				data-ng-disabled="emeaAdSalesForm.$invalid">Submit</button>
		</div>
	</form>
</body>

</html>
