
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Credit Request Confirmation</title>
<style>
.borderDiv {
	border: 1px solid #dfafaf;
	border-radius: 8px;
	height: 134px;
	width: 88%
}

.borderDiv1 {
	border: 1px solid #dfafaf;
	border-radius: 8px;
	height: 127px;
	width: 88%;
}

.headText {
	background-color: seashell;
	font-family: Lucida Console;
	margin-left: 21px;
	margin-top: -11px;
	width: 162px;
}

.referenceMsg {
	background-color: seashell;
	border: 1px solid antiquewhite;
	font-family: Lucida Console;
	margin-left: 21px;
	margin-top: -11px;
	width: 467px;
}
</style>
</head>
<body>
	<div class="referenceMsg">
		Your Credit Request submission is Successfull.<br /> Application
		Reference Number: EC9487&nbsp;
	</div>
	<br />
	<form>
		<div
			style="overflow: initial; height: 411px; width: 150%; margin-top: 10px">
			<div class="borderDiv1">
				<div class="headText">&nbsp;Know your Customer&nbsp;</div>
				<table>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 20px;">Segment</label>
								${segment}
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 78px;">Scoring
									Region</label> ${region}
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 60px;">Company</label>
								${company}
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 26px;">Business
									Area</label> ${businessArea}
							</div>
						</td>
					</tr>
				</table>
			</div>
			<br />
			<div class="borderDiv">
				<div class="headText">&nbsp;Request Submission&nbsp;</div>
				<table>
					<tr>
						<td><table>
								<tr>
									<td>
										<div>
											<label style="margin-left: 15px; margin-right: 77px;">Requestor
												Name</label> ${name}
										</div>
									</td>

								</tr>
								<tr>
									<td>
										<div>
											<label style="margin-left: 15px; margin-right: 79px;">Requestor
												Email</label> ${email}
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div>
											<label style="margin-left: 15px; margin-right: 82px;">Customer
												Name</label> ${customerName}
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div>
											<label style="margin-left: 15px; margin-right: 20px;">Customer
												Street Address</label> ${customerAdress}
										</div>
									</td>
								</tr>
							</table></td>
						<td><table>
								<tr>
									<td>
										<div>
											<label style="margin-left: 20px; margin-right: 47px;">Customer
												City</label> ${city}
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div>
											<label style="margin-left: 20px; margin-right: 40px;">Customer
												State</label> ${state}
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div>
											<label style="margin-left: 20px; margin-right: 11px;">Customer
												Zip Code</label> ${zipCode}
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div>
											<label style="margin-left: 20px; margin-right: 20px;">Customer
												Country</label> ${country}
										</div>
									</td>
								</tr>
							</table></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</body>
</html>