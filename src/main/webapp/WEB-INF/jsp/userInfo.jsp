<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Get User Info</title>
<!-- Hex CSS -->

<link href="<c:url value="/resources/css/themes/southpaw.min.css" />"
	rel="stylesheet" type="text/css">
	<link href="<c:url value="/resources/css/salesapp.css" />"
	rel="stylesheet" type="text/css">
	
	<script type="text/javascript">
	function isNumber(){
		var phone = document.getElementById("idx").value;
		if(phone.match(/^\d+$/)) {
			document.getElementById("error").innerHTML = "";
			return true;
		}
		else{
			//alert("Please Enter only Integer Value !");
			document.getElementById("error").innerHTML = "Please Enter only Integer Value !";
			return false;
		}
		
	}

	
	</script>
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
</head>
<body>

	<br />
	<br />

	<form id="myForm" action="getContact" method="post" class="form-menu" style="width: 866px;">
		<div style="background-color: #cdcdcd;
    border: 1px solid black;
    height: 324px;
    margin-left: -98px;
    width: 658px;">
    
    		<h2 class="header-content">Get User Info By UserId</h2>

			<table>
				<tr>
					<td style=" padding-left: 194px;"><h4>Please Enter your UserId:</h4> </td>
				</tr>
				<tr>
					<td><input onchange="isNumber();" style="margin-left: 120px; margin-top: 27px;" type="text" name="id" id="idx" class="form-control"/></td>
					
				</tr>
				
				<tr>
					<td><input style="margin-left: 248px;  margin-top: 60px;" type="submit" value="Get Details"
						class="btn btn-primary pull-center" onclick=" return isNumber()" /></td>
				</tr>
			</table>
			<div style="color: red;  margin-left: 122px;    margin-top: -85px;" id="error" ></div>
		</div>
	</form>
</body>
</html>