<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Credit Tracking</title>
</head>
<body>
	<form>
		<div class="text-info" style="margin-left: 30px;">
			<p class="text-info">Your Credit Request submission is
				successful.</p>
		</div>
		<div style="margin-top: 30px; margin-left: 60px; width: 1000px;">

			<table>

				<tr>
					<td><label>Application Reference Number:</label></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td style="border: 2px solid black; text-align: center">${applicationReferenceId}</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td><label>Request submission status:</label></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<!-- <td
						style="border: 2px solid black; text-align: center; width: 110px">${status}</td> -->
					<td
						style="border: 2px solid black; text-align: center; width: 110px">Pending</td>
				</tr>

			</table>
		</div>
		<div style="margin-top: 60px;">
			<div>
				<i class="fa fa-clock-o fa-5x"></i> <span class="text-info">Please
					allow 48 hours to process your request.</span>
			</div>

		</div>


	</form>
</body>
</html>