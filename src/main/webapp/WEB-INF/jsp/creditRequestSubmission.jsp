<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Credit Request Submission</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<style>
.borderDiv {
	border: 1px solid #dfafaf;
	height: 214px;
	width: 100%;
}

.borderDiv1 {
	border: 1px solid #dfafaf;
	height: 127px;
}

.headText {
	background-color: seashell;
	font-family: Lucida Console;
	margin-left: 21px;
	margin-top: -11px;
	width: 162px;
}

.referenceMsg {
	background-color: seashell;
	border: 1px solid antiquewhite;
	font-family: Lucida Console;
	margin-left: 21px;
	margin-top: -11px;
	width: 467px;
}
</style>
</head>
<body>
	<div class="referenceMsg">
		Your Credit Request submission is Successfull.<br /> Application
		Reference Number: EC9487&nbsp;
	</div>
	<br />
	<form:form method="post" action="creditRequestSubmission"
		modelAttribute="creditRequest">
		<div
			style="overflow: initial; height: 411px; width: 120%; margin-top: 10px">
			<div class="borderDiv1">
				<div class="headText">&nbsp;Know your Customer&nbsp;</div>
				<table>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 20px;">Segment</label>
								${segment}
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 78px;">Scoring
									Region</label> ${region}
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 60px;">Company</label>
								${company}
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div>
								<label style="margin-left: 15px; margin-right: 26px;">Business
									Area</label> ${businessArea}
							</div>
						</td>
					</tr>
				</table>
			</div>
			<br />
			<div id="custDetails" class="borderDiv">
				<div id="req" class="headText">&nbsp;Request Submission&nbsp;</div>
				<table>
					<tr>
						<td>
							<table>
								<c:if test="${businessGroup eq 'Media'}">
									<tr>
										<td><form:label path="name">Name&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
										<td><form:input path="name"
												class='form-control textboxmargin' /></td>

									</tr>
								</c:if>

								<tr>
									<td><form:label path="email">Email&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
									<td><form:input path="email"
											class='form-control textboxmargin ' /></td>
								</tr>
								<tr>
									<td><form:label path="customerName">Customer Name&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
									<td><form:input path="customerName"
											class='form-control textboxmargin ' /></td>
								</tr>
								<tr>
									<td><form:label path="streetAddress">Street Address&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
									<td><form:input path="streetAddress"
											class='form-control textboxmargin' /></td>
								</tr>


							</table>
						</td>
						<td><table>
								<c:if test="${businessGroup eq 'Entertainment'}">
									<tr>
										<td><form:label path="city">&nbsp;&nbsp;&nbsp;&nbsp;City&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
										<td><form:input path="city"
												class='form-control textboxmargin' /></td>
									</tr>
								</c:if>
								<tr>
									<td><form:label path="state">&nbsp;&nbsp;&nbsp;&nbsp;State&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
									<td><form:input path="state"
											class='form-control textboxmargin ' /></td>
								</tr>
								<tr>
									<td><form:label path="zipCode">&nbsp;&nbsp;&nbsp;&nbsp;ZipCode&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
									<td><form:input path="zipCode"
											class='form-control textboxmargin ' /></td>
								</tr>
								<tr>
									<td><form:label path="country">&nbsp;&nbsp;&nbsp;&nbsp;Country&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
									<td><form:input path="country"
											class='form-control textboxmargin' /></td>
								</tr>


							</table></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				<div id="submitdiv" class="submitButtonStyle">
					<input type="submit" class='btn pull-center' value="Submit" />
				</div>
			</div>

		</div>
	</form:form>
</body>
</html>