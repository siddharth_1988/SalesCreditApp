<div style="text-align:center"><h5>DASHBOARD</h5></div>
<div class="stat-panel" data-ng-app="salesCreditApp">
					<div class="stat-row" data-ng-controller="DashboardController" data-ng-init="getDashboardDetails()">
						<!-- Small horizontal padding, bordered, without right border, top aligned text -->
						<div class="stat-cell col-sm-4 padding-sm-hr bordered no-border-r valign-top">
							<!-- Small padding, without top padding, extra small horizontal padding -->
							<h5 class="padding-sm no-padding-t padding-xs-hr"><i class="fa fa-cloud-upload text-primary"></i>&nbsp;&nbsp;Credit Requests</h5>
							<!-- Without margin -->
							<!-- <h5 class="text-slim">Credit Request</h5> -->
	<div>		
    <table ng-show="(dashboardBOs | filter:criteria).length" class="table table-bordered table-primary">
    	<thead>
          <tr>
            <th>#</th>
            <th>Application Ref#</th>
            <th>Request Submission Status</th>
            <th>Raised Date</th>
            <th>Request Needed By Date</th>
            <th>SLA</th>
          </tr>
          </thead>
		  <tr ng-repeat="dashboard in dashboardBOs">
		    <td>{{ $index + 1 }}</td>
		    <td>{{ dashboard.creditRequestNumber }}</td>
		    <td>{{ dashboard.creditRequestStatus }}</td>
		    <td>{{ dashboard.requestRaisedDate | date:'MM/dd/yyyy'}}</td>
		    <td>{{ dashboard.requestNeededByDate | date:'MM/dd/yyyy'}}</td>
		    <td>{{ dashboard.slaDate | date:'MM/dd/yyyy'}}</td>
		  </tr>
	</table>
	</div>
    <div ng-show="(!dashboardBOs.length)">
 		You have not yet submitted any credit request. <br> You can submit a credit request using Credit Submission option. 
    </div>					
    </div>
    
    <!-- <ul class="list-group no-margin">
	    <li class="list-group-item no-border-hr padding-xs-hr no-bg no-border-radius" ng-repeat-start="dashboard in dashboardBOs">{{dashboard.creditRequestId}}</li>
	    <li class="list-group-item no-border-hr padding-xs-hr no-bg">{{dashboard.creditRequestStatus}}</li>
	    <li class="list-group-item no-border-hr no-border-b padding-xs-hr no-bg" ng-repeat-end>{{dashboard.creditRequestNumber}}</li>
	</ul>
     -->
							
						</div> <!-- /.stat-cell -->
					</div>
