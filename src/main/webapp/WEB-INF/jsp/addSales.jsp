<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>jQuery UI Accordion - Default functionality</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script>
	$(function() {
		jQuery.noConflict();
		$("#accordion").accordion();
	});
</script>
<style>
.addsalebox {
	border: 1px solid #dfafaf;
	border-radius: 8px;
	height: 472px;
	width: 1010px;
}
</style>
</head>
<body>
<body data-ng-controller="AddSalesController">
	<form:form method="post" action="saveSales" modelAttribute="salesBO">
		<div id="userDiv" style="overflow: initial; height: 411px">
			<div id="addSalesDetails" class="addsalebox">
				<div id="req" class="userHeadText">&nbsp;Add Sales Page &nbsp;</div>
				<div id="accordion">
					<h3>Requester Details</h3>
					<div>
					<div id="one" style="width:950px"><table><tr><td>
						<table>

							<tr>
								<td><form:label path="requestorName">Requestor Name&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="requestorName"
										class='form-control textboxmargin' /></td>
							</tr>


							<tr>
								<td><form:label path="requestorEmail">Requestor Email&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="requestorEmail"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="requestNeededByDate">Request Needed By Date&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="requestNeededByDate"
										class='form-control textboxmargin ' /></td>
							</tr>
							
						</table></td>
						<td><table><tr>
								<td><form:label path="requestedCreditLimit">&nbsp;&nbsp;&nbsp;Amount of Business&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="requestedCreditLimit"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="termStartDate">&nbsp;&nbsp;&nbsp;Request Needed By Date&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="termStartDate"
										class='form-control textboxmargin ' /></td>
							</tr></table></td>
						</tr></table> 
						</div>
					</div>
					
					<h3>Requester Company Details Part1</h3>
					<div>
					<div id="two" style="width:950px"><table><tr><td>
						<table>

							<tr>
								<td><form:label path="companyName">Company Name&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyName"
										class='form-control textboxmargin' /></td>
							</tr>


							<tr>
								<td><form:label path="brandName">Company Brand Name&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="brandName"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="companyWebAddress">Company Web Address&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyWebAddress"
										class='form-control textboxmargin ' /></td>
							</tr>
							
						</table></td>
						<td><table>
						<tr>
								<td><form:label path="companyStreetAddress">&nbsp;&nbsp;&nbsp;Company Street Address&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyStreetAddress"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="companyCity">&nbsp;&nbsp;&nbsp;Company City&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyCity"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="companyState">&nbsp;&nbsp;&nbsp;Company State&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyState"
										class='form-control textboxmargin ' /></td>
							</tr>
						</table></td>
						</tr></table></div>
					</div>
					
						<h3>Requester Company Details Part2</h3>
					<div>
					<div id="one" style="width:950px"><table><tr><td>
						<table>
						<tr>
								<td><form:label path="companyZip">Company Zip&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyZip"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="companyCountry">Company Country&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyCountry"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="companyTelephone">Company Telephone&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyTelephone"
										class='form-control textboxmargin ' /></td>
							</tr>
						</table></td>
						<td><table>
						<tr>
								<td><form:label path="companyContactName">&nbsp;&nbsp;&nbsp;Company Contact Name&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyContactName"
										class='form-control textboxmargin ' /></td>
							</tr>

							<tr>
								<td><form:label path="contactTitle">&nbsp;&nbsp;&nbsp;Company Contact Title&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="contactTitle"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="companyContactEmailAddress">&nbsp;&nbsp;&nbsp;Company Contact Email Address&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="companyContactEmailAddress"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="okayToContact">&nbsp;&nbsp;&nbsp;okay To Contact&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:checkbox path="okayToContact"
										class='form-control textboxmargin ' /></td>
							</tr>
						</table></td>
						</tr></table></div>
						</div>

						
					<h3>Agency Details part1</h3>
					<div>
					<div id="one1" style="width:950px">	<table><tr><td>
						<table>

							<tr>
								<td><form:label path="agencyName">Agency Name&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyName"
										class='form-control textboxmargin' /></td>
							</tr>


							<tr>
								<td><form:label path="agencyWebAddress">Agency Web Address&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyWebAddress"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="agencyStreetAddress">Agency Street Address&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyStreetAddress"
										class='form-control textboxmargin ' /></td>
							</tr>

						</table></td>
						<td><table>
						<tr>
								<td><form:label path="agencyCity">&nbsp;&nbsp;&nbsp;Agency City&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyCity"
										class='form-control textboxmargin' /></td>
							</tr>


							<tr>
								<td><form:label path="agencyState">&nbsp;&nbsp;&nbsp;Agency State&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyState"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="agencyZip">&nbsp;&nbsp;&nbsp;Agency Zip&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyZip"
										class='form-control textboxmargin ' /></td>
							</tr>
						</table></td></tr></table></div>
					</div>
					
						<h3>Agency Details part2</h3>
					<div>
						<div id="one2" style="width:950px"><table>
						<tr>
								<td><form:label path="agencyCountry">Agency Country&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyCountry"
										class='form-control textboxmargin' /></td>
							</tr>


							<tr>
								<td><form:label path="agencyPhone">Agency Phone&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyPhone"
										class='form-control textboxmargin ' /></td>
							</tr>
							<tr>
								<td><form:label path="agencyContact">Agency Contact&nbsp;&nbsp;&nbsp;&nbsp;</form:label></td>
								<td><form:input path="agencyContact"
										class='form-control textboxmargin ' /></td>
							</tr>
						</table></div></div>
					<h3>Other Information</h3>
					<div><div id="one2" style="width:950px">Yet to apply.</div></div>
				</div>

			</div>
		</div>
		<div id="saleSubmitdiv" style="margin-left: 434px;margin-top: 22px;">
				<input type="submit" class='btn btn-primary pull-center' value="Submit" />
			</div>
	</form:form>
</body>
</html>