<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html data-ng-app="adminApp">
<head lang="en">
<meta charset="utf-8">
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript" src="./resources/js/adminApp.js"></script>
</head>

<body data-ng-controller="AdminController">
	<form:form method="post" action="" id="adminForm"
		class="panel form-horizontal" modelAttribute="">

		<div id="know" class="panel-heading">
			<h4>Administrator Management Page</h4>
		</div>

		<div id="baccMapDetails" data-ng-init="getBaccMapGroupData()">
			<div id="addDetailsId" style="margin-bottom: 1px; margin-left: 1041px;">
				<button type="button" class="btn btn-primary btn-sm"
					data-ng-click="openAddBaccMapModal();">
					<span class="glyphicon glyphicon-plus"></span> Add
				</button>
			</div>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<td><a href="#"
							data-ng-click="sortType = 'segmentBO.segmentName'; sortReverse = !sortReverse">
								SEGMENT <span
								data-ng-show="sortType == 'segmentBO.segmentName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'segmentBO.segmentName' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"
							data-ng-click="sortType = 'regionBO.regionName'; sortReverse = !sortReverse">
								REGION <span
								data-ng-show="sortType == 'regionBO.regionName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'regionBO.regionName' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"
							data-ng-click="sortType = 'companyBO.companyName'; sortReverse = !sortReverse">
								COMPANY <span
								data-ng-show="sortType == 'companyBO.companyName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'companyBO.companyName' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"
							data-ng-click="sortType = 'businessAreaBO.businessAreaName'; sortReverse = !sortReverse">
								BUSINESS AREA <span
								data-ng-show="sortType == 'businessAreaBO.businessAreaName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'businessAreaBO.businessAreaName' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td style="width: 15%"><a href="#"> Action </a></td>
					</tr>
				</thead>

				<tbody>
					<tr
						data-ng-repeat="data in baccMapBOData | orderBy:sortType:sortReverse"
						data-ng-model="itemCompanyCode">

						<td>{{ data.segmentBO.segmentName }}</td>
						<td>{{ data.regionBO.regionName }}</td>
						<td>{{ data.companyBO.companyCode }} - {{
							data.companyBO.companyName }}</td>
						<td>{{ data.businessAreaBO.businessAreaCode }} - {{
							data.businessAreaBO.businessAreaName }}</td>

						<td><button type="button" class="btn btn-primary btn-sm"
								data-ng-click="editBaccMapGroup(data);">
								<span class="glyphicon glyphicon-edit"></span> Edit
							</button> &nbsp;
							<button type="button" class="btn btn-primary btn-sm"
								data-ng-click="deleteBaccMapGroup(data);">
								<span class="glyphicon glyphicon-remove"></span> Delete
							</button></td>
					</tr>

				</tbody>
			</table>

			<dir-pagination-controls max-size="5" direction-links="true"
				boundary-links="true"> </dir-pagination-controls>

		</div>
	</form:form>


	<div id="addBaccMapModal" class="modal fade">
		<div class="modal-dialog" style="width: 90%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Details:</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="border col-md-3"
							style="margin-left: -9px; margin-right: -20px;">
							<label class="control-label"
								style="background-color: #cecece; height: 22px; width: 410%;">SEGMENT</label>
							<ui-select ng-model="baccMap.selected.segmentBO"
								reset-search-input="'false'" class='form-control'
								style="height:47px;"
								data-ng-change="onChangeSegment(baccMap.selected.segmentBO);">
							<ui-select-match placeholder="Select or search segment">{{$select.selected.segmentName}}</ui-select-match>
							<ui-select-choices
								repeat="segmentBO in creditRequest.segmentBOs | filter: $select.search">
							<div
								data-ng-bind-html="segmentBO.segmentName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>
						<div class="border col-md-3" style="margin-right: -9px;">
							<label class="control-label">REGION</label>
							<ui-select ng-model="baccMap.selected.regionBO"
								class='form-control' style="height:47px"
								data-ng-change="onChangeRegion(baccMap.selected.regionBO);">
							<ui-select-match placeholder="Select or search region">{{$select.selected.regionName}}</ui-select-match>
							<ui-select-choices
								repeat="regionBO in creditRequest.regionBOs | filter: $select.search">
							<div
								data-ng-bind-html="regionBO.regionName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>
						<div class="border col-md-3"
							style="margin-left: -13px; margin-right: -20px;">
							<label class="control-label">COMPANY</label>
							<ui-select ng-model="baccMap.selected.companyBO"
								class='form-control' style="height:47px"
								data-ng-change="onChangeCompany(baccMap.selected.companyBO);">
							<ui-select-match placeholder="Select or search company">{{$select.selected.companyCode}}
							{{$select.selected.companyName}}</ui-select-match> <ui-select-choices
								repeat="companyBO in creditRequest.companyBOs | filter: $select.search">
							<div
								data-ng-bind-html="companyBO.companyCode + '-' + companyBO.companyName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>
						<div class="border col-md-3">
							<label class="control-label">BUSINESS AREA</label>
							<ui-select ng-model="baccMap.selected.businessAreaBO"
								class='form-control' style="height:47px"
								data-ng-change="onChangeBusinessArea(baccMap.selected.businessAreaBO);">
							<ui-select-match placeholder="Select or search businessArea">{{$select.selected.businessAreaCode}}
							{{$select.selected.businessAreaName}}</ui-select-match> <ui-select-choices
								repeat="businessAreaBO in creditRequest.businessAreaBOs | filter: $select.search">
							<div
								data-ng-bind-html="businessAreaBO.businessAreaCode + '-' + businessAreaBO.businessAreaName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>

						<div style="margin-top: 33px;">
							<button type="button" class="btn btn-primary btn-sm"
								data-ng-click="addBaccMapGroupData(baccMap.selected);">
								<span class="glyphicon glyphicon-plus"></span> Add
							</button>
						</div>
					</div>
					<div class="row">

						<div class="border col-md-3"
							style="margin-left: -9px; margin-right: -20px;">
							<input type="text" name="newSegment" id="newSegment"
								class='form-control textboxmargin' placeholder="Enter Segment" />
						</div>
						<div class="border col-md-3" style="margin-right: -9px;">
							<input type="text" name="newRegion" id="newRegion"
								placeholder="Enter Region" class='form-control textboxmargin' />
						</div>
						<div class="border col-md-3"
							style="margin-left: -13px; margin-right: -20px;">
							<input type="text" name="newCompany" id="newCompany"
								placeholder="CompanyCode - CompanyName"
								class='form-control textboxmargin' />
						</div>
						<div class="border col-md-3">
							<input type="text" name="newBusinessArea"
								placeholder="BusinessAreaCode - BusinessAreaName"
								id="newBusinessArea" class='form-control textboxmargin' />
						</div>
					</div>

					<div class="row">
						<div id="segmentStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{segmentExists}}</div>
						<div id="regionStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{regionExists}}</div>
						<div id="companyStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{companyExists}}</div>
						<div id="businessAreaStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{businessAreaExists}}</div>
					</div>

					<div>
						<div id="addStatus" data-ng-show="showSuccessAlert"
							style="color: red; font-family: Georgia; margin-left: 463px; margin-top: 18px;">{{successTextAlert}}</div>

						<div id="successStatus"
							style="color: blue; font-family: Georgia; margin-left: 463px; margin-top: 18px;">{{successText}}</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>



	<div id="editBaccMapGroupModal" class="modal fade">
		<div class="modal-dialog" style="width: 90%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Details:</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="border col-md-3"
							style="margin-left: -9px; margin-right: -20px;">
							<label class="control-label"
								style="background-color: #cecece; height: 22px; width: 410%;">SEGMENT</label>
							<ui-select ng-model="editBaccMap.selected.segmentBO"
								reset-search-input="'false'" class='form-control'
								style="height:47px;"
								data-ng-change="onChangeSegmentEdit(editBaccMap.selected.segmentBO);">
							<ui-select-match placeholder="Select or search segment">{{$select.selected.segmentName}}</ui-select-match>
							<ui-select-choices
								repeat="segmentBO in creditRequest.segmentBOs | filter: $select.search">
							<div
								data-ng-bind-html="segmentBO.segmentName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>
						<div class="border col-md-3" style="margin-right: -9px;">
							<label class="control-label">REGION</label>
							<ui-select ng-model="editBaccMap.selected.regionBO"
								class='form-control' style="height:47px"
								data-ng-change="onChangeRegionEdit(editBaccMap.selected.regionBO);">
							<ui-select-match placeholder="Select or search region">{{$select.selected.regionName}}</ui-select-match>
							<ui-select-choices
								repeat="regionBO in creditRequest.regionBOs | filter: $select.search">
							<div
								data-ng-bind-html="regionBO.regionName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>
						<div class="border col-md-3"
							style="margin-left: -13px; margin-right: -20px;">
							<label class="control-label">COMPANY</label>
							<ui-select ng-model="editBaccMap.selected.companyBO"
								class='form-control' style="height:47px"
								data-ng-change="onChangeCompanyEdit(editBaccMap.selected.companyBO);">
							<ui-select-match placeholder="Select or search company">{{$select.selected.companyCode}}
							{{$select.selected.companyName}}</ui-select-match> <ui-select-choices
								repeat="companyBO in creditRequest.companyBOs | filter: $select.search">
							<div
								data-ng-bind-html="companyBO.companyCode + '-' + companyBO.companyName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>
						<div class="border col-md-3">
							<label class="control-label">BUSINESS AREA</label>
							<ui-select ng-model="editBaccMap.selected.businessAreaBO"
								class='form-control' style="height:47px"
								data-ng-change="onChangeBusinessAreaEdit(editBaccMap.selected.businessAreaBO);">
							<ui-select-match placeholder="Select or search businessArea">{{$select.selected.businessAreaCode}}
							{{$select.selected.businessAreaName}}</ui-select-match> <ui-select-choices
								repeat="businessAreaBO in creditRequest.businessAreaBOs | filter: $select.search">
							<div
								data-ng-bind-html="businessAreaBO.businessAreaCode + '-' + businessAreaBO.businessAreaName | highlight: $select.search"></div>
							</ui-select-choices> </ui-select>
						</div>

						<div style="margin-top: 33px;">
							<button type="button" class="btn btn-primary btn-sm"
								data-ng-click="updateBaccMapGroup(editBaccMap.selected);">
								<span class="glyphicon glyphicon-edit"></span> Edit
							</button>
						</div>
					</div>
					<div class="row">

						<div class="border col-md-3"
							style="margin-left: -9px; margin-right: -20px;">
							<input type="text" name="editNewSegment" id="editNewSegment"
								class='form-control textboxmargin' placeholder="Enter Segment" />
						</div>
						<div class="border col-md-3" style="margin-right: -9px;">
							<input type="text" name="editNewRegion" id="editNewRegion"
								placeholder="Enter Region" class='form-control textboxmargin' />
						</div>
						<div class="border col-md-3"
							style="margin-left: -13px; margin-right: -20px;">
							<input type="text" name="editNewCompany" id="editNewCompany"
								placeholder="CompanyCode - CompanyName"
								class='form-control textboxmargin' />
						</div>
						<div class="border col-md-3">
							<input type="text" name="editNewBusinessArea"
								placeholder="BusinessAreaCode - BusinessAreaName"
								id="editNewBusinessArea" class='form-control textboxmargin' />
						</div>
					</div>

					<div class="row">
						<div id="editSegmentStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{segmentExists}}</div>
						<div id="editRegionStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{regionExists}}</div>
						<div id="editCompanyStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{companyExists}}</div>
						<div id="editBusinessAreaStatusId" class="border col-md-3"
							style="color: red; font-family: Georgia; width: 273px;">{{businessAreaExists}}</div>
					</div>

					<div>
						<div id="editAddStatus" data-ng-show="showSuccessAlert"
							style="color: red; font-family: Georgia; margin-left: 463px; margin-top: 18px;">{{successTextAlert}}</div>

						<div id="editSuccessStatus"
							style="color: blue; font-family: Georgia; margin-left: 463px; margin-top: 18px;">{{successText}}</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


</body>

</html>