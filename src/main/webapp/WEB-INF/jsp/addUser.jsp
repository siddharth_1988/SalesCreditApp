<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html data-ng-app="userManagementApp">
<head lang="en">
<meta charset="utf-8">
<title>Add User</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>

<script type="text/javascript" src="./resources/js/angular.min.js"></script>
<script type="text/javascript" src="./resources/js/user.js"></script>

<script>
	function cancel() {
		window.location.href = "userDataList";
	}
</script>
</head>

<body data-ng-controller="userManagementController">
	<form:form method="post" action="saveUser" modelAttribute="userBO"
		id="addUserForm" name="addUserForm">
		<div id="custDetails" class="panel form-horizontal">
			<div id="adsales" class="panel-heading">Add User</div>
			<div class="panel-body">

				<div class="row"
					data-ng-class="{ 'has-error' : addUserForm.userName.$invalid && !addUserForm.userName.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">User Full Name:</label>
						<div class="col-md-6">
							<input type="text" name="userName"
								data-ng-model="userBO.userName"
								class='form-control textboxmargin' required
								data-ng-pattern='/^[a-z\s]+$/i' />
							<p data-ng-show="addUserForm.userName.$error.pattern"
								class="help-block">Please enter alphabets only.</p>
						</div>
					</div>
				</div>

				<div class="row"
					data-ng-class="{ 'has-error' : addUserForm.portalId.$invalid && !addUserForm.portalId.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Portal Id:</label>
						<div class="col-md-6">
							<input type="text" name="portalId"
								data-ng-model="userBO.portalId"
								class='form-control textboxmargin' required
								data-ng-pattern='/^[a-zA-Z0-9]*$/' />
							<p data-ng-show="addUserForm.portalId.$error.pattern"
								class="help-block">Please enter alphanumeric only.</p>
						</div>
					</div>
				</div>

				<div class="row"
					data-ng-class="{ 'has-error' : addUserForm.status.$invalid && !addUserForm.status.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Status:</label>
						<div class="col-md-6">
							<input type="text" name="status" data-ng-model="userBO.status"
								class='form-control textboxmargin' required
								data-ng-pattern='/^[a-zA-Z]*$/' />
							<p data-ng-show="addUserForm.status.$error.pattern"
								class="help-block">Please enter alphabets only.</p>
						</div>
					</div>
				</div>
				<div class="row"
					data-ng-class="{ 'has-error' : addUserForm.email.$invalid && !addUserForm.email.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Email:</label>
						<div class="col-md-6">
							<input type="email" name="email" data-ng-model="userBO.email"
								class='form-control textboxmargin' required />
							<p
								data-ng-show="addUserForm.email.$invalid && !addUserForm.email.$pristine"
								class="help-block">Please enter valid email.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-md-2 control-label">First Name:</label>
						<div class="col-md-6">
							<input type="text" name="firstName"
								data-ng-model="userBO.firstName"
								class='form-control textboxmargin' />

						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-md-2 control-label">Last Name:</label>
						<div class="col-md-6">
							<input type="text" name="lastName"
								data-ng-model="userBO.lastName"
								class='form-control textboxmargin' />

						</div>
					</div>
				</div>

				<div class="row"
					data-ng-class="{ 'has-error' : addUserForm.initials.$invalid && !addUserForm.initials.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Initials:</label>
						<div class="col-md-6">
							<input type="text" name="initials"
								data-ng-model="userBO.initials"
								class='form-control textboxmargin' required
								data-ng-pattern='/^[a-z\s]+$/i' />
							<p data-ng-show="addUserForm.initials.$error.pattern"
								class="help-block">Please enter alphabets only.</p>
						</div>
					</div>
				</div>

				<div class="row"
					data-ng-class="{ 'has-error' : addUserForm.address1.$invalid && !addUserForm.address1.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Address 1:</label>
						<div class="col-md-6">
							<input type="text" name="address1"
								data-ng-model="userBO.address1"
								class='form-control textboxmargin' required
								data-ng-pattern='/^[a-z\s]+$/i' />
							<p data-ng-show="addUserForm.address1.$error.pattern"
								class="help-block">Please enter alphabets only.</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-md-2 control-label">Address 2:</label>
						<div class="col-md-6">
							<input type="text" name="address2"
								data-ng-model="userBO.address2"
								class='form-control textboxmargin'/>
						</div>
					</div>
				</div>

				<table>
					<tr>
						<td><label class="col-md-2 control-label"
							style="margin-left: 87px;">Phone: </label><input type="text"
							name="phone" data-ng-model="userBO.phone"
							style="width: 211px; margin-left: 182px;"
							class='form-control textboxmargin' /></td>

						<td><label class="col-md-2 control-label"
							style="margin-left: 37px;">Fax: </label><input type="text"
							name="fax" data-ng-model="userBO.fax" style="width: 211px;"
							class='form-control textboxmargin' /></td>
					</tr>
				</table>
				<br />

				<!--		<div class="row" style="height: 27px;">

					<div class="form-group no-margin-hr">
						<label class="col-md-2 control-label"
							style="margin-left: -4px; margin-right: 19px;">Phone:</label>
						<div>
							<input type="text" name="phone" data-ng-model="userBO.phone"
								style="width: 210px;" class='form-control textboxmargin' />

						</div>

					</div>

					<div class="form-group no-margin-hr">
						<label class="col-md-2 control-label"
							style="margin-left: 335px; margin-right: 50px; margin-top: -46px;">Fax:</label>
						<div>
							<input type="text" name="fax" data-ng-model="userBO.fax"
								style="margin-left: 525px; margin-top: -48px; width: 210px;"
								class='form-control textboxmargin' />


						</div>
					</div>
				</div> -->

				<div class="row"
					data-ng-class="{ 'has-error' : addUserForm.company.$invalid && !addUserForm.company.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Company:</label>
						<div class="col-md-6">
							<input type="text" name="company" data-ng-model="userBO.company"
								class='form-control textboxmargin' required
								data-ng-pattern='/^[a-z\s]+$/i' />
							<p data-ng-show="addUserForm.company.$error.pattern"
								class="help-block">Please enter alphabets only.</p>
						</div>
					</div>
				</div>


				<!--	<div class="row" style="height: 47px;"
					data-ng-class="{ 'has-error' :addUserForm.city.$invalid && !addUserForm.city.$pristine }">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label"
							style="margin-left: -6px; margin-right: 21px;">City:</label>
						<div>
							<input type="text" name="city" data-ng-model="userBO.city"
								style="width: 210px;" class='form-control textboxmargin'
								required data-ng-pattern='/^[a-z\s]+$/i' />
							<p data-ng-show="addUserForm.city.$error.pattern"
								class="help-block">Please enter alphabets only.</p>

						</div>

					</div>
					<div
						data-ng-class="{ 'has-error' : addUserForm.state.$invalid && !addUserForm.state.$pristine }">
						<div class="form-group no-margin-hr required">
							<label class="col-md-2 control-label"
								style="margin-left: 335px; margin-right: 50px; margin-top: -46px;">State:</label>
							<div>
								<input type="text" name="state" data-ng-model="userBO.state"
									style="margin-left: 525px; margin-top: -48px; width: 210px;"
									class='form-control textboxmargin' required
									data-ng-pattern='/^[a-z\s]+$/i' />
								<p data-ng-show="addUserForm.state.$error.pattern"
									class="help-block">Please enter alphabets only.</p>

							</div>
						</div>
					</div>
				</div> -->

				<table>
					<tr>
						<td><label class="col-md-2 control-label"
							style="margin-left: 100px;">City: </label><input type="text"
							name="city" data-ng-model="userBO.city"
							style="width: 211px; margin-left: 182px;"
							class='form-control textboxmargin' /></td>

						<td><label class="col-md-2 control-label"
							style="margin-left: 23px; margin-right: 12px;">State: </label><input
							type="text" name="state" data-ng-model="userBO.state"
							style="width: 211px;" class='form-control textboxmargin' /></td>
					</tr>
				</table>
				<br />

				<div class="row">
					<div class="col-sm-8">
						<div data-ng-init="getCountryList()"
							class="form-group no-margin-hr required">
							<label class="col-md-2 control-label" style="margin-left: 52px;">Country:</label>
							<div class="col-md-4">
								<select id="country" name="country"
									data-ng-model="userBO.country" class='form-control'
									required="required">
									<option value="">-- Select Country --</option>
									<option data-ng-repeat="countryBO in countryList"
										value="{{countryBO.countryName}}">{{countryBO.countryName}}</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-sm-2"
						data-ng-class="{ 'has-error' : addUserForm.zip.$invalid && !addUserForm.zip.$pristine }">
						<div class="form-group no-margin-hr required">
							<label class="col-md-2 control-label"
								style="margin-left: -299px;">Zip:</label>
							<div>
								<input type="text" name="zip" data-ng-model="userBO.zip"
									style="margin-left: -238px; width: 207px;"
									class='form-control textboxmargin' required
									data-ng-pattern='/^[0-9.]*$/' />
								<p data-ng-show="addUserForm.zip.$error.pattern"
									class="help-block">Please enter positive number.</p>

							</div>
						</div>
					</div>
				</div>
				<br />
				<div id="submitdiv" style="margin-left: 325px;">
					<input type="button" class='btn btn-primary pull-center'
						value="Cancel" onclick="cancel();" /> <input type="submit"
						class='btn btn-primary pull-center'
						data-ng-disabled="addUserForm.$invalid" value="Submit" />
				</div>
			</div>
		</div>


		<!-- <div class="panel-footer text-right">
			<button class="btn btn-primary"
				data-ng-disabled="addUserForm.$invalid">Submit</button>
		</div> -->
	</form:form>
</body>

</html>