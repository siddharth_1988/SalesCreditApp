<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html data-ng-app="userManagementApp">
<head lang="en">
<meta charset="utf-8">
<title>Add User</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript" src="./resources/js/angular.min.js"></script>
<script type="text/javascript" src="./resources/js/user.js"></script>
<script>
	function cancel() {
		window.location.href = "userDataList";
	}
</script>
</head>

<body data-ng-controller="userManagementController">
	<form:form method="post" action="updateUser" modelAttribute="userBO">
		<form:hidden path="userId" />

		<div id="custDetails" class="panel form-horizontal">
			<div id="adsales" class="panel-heading">Edit User</div>
			<div class="panel-body">

				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">User Full Name:</label>
						<div class="col-md-6">
							<form:input path="userName" class='form-control textboxmargin' />


						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Portal Id:</label>
						<div class="col-md-6">
							<form:input path="portalId" class='form-control textboxmargin' />


						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Email:</label>
						<div class="col-md-6">
							<form:input path="email" class='form-control textboxmargin' />


						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Status:</label>
						<div class="col-md-6">
							<form:input path="status" class='form-control textboxmargin' />


						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-md-2 control-label">First Name:</label>
						<div class="col-md-6">
							<form:input path="firstName" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr">
						<label class="col-md-2 control-label">Last Name:</label>
						<div class="col-md-6">
							<form:input path="lastName" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Initials:</label>
						<div class="col-md-6">
							<form:input path="initials" class='form-control textboxmargin' />


						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Address 1:</label>
						<div class="col-md-6">
							<form:input path="address1" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr ">
						<label class="col-md-2 control-label">Address 2:</label>
						<div class="col-md-6">
							<form:input path="address2" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr ">
						<label class="col-md-2 control-label">Phone:</label>
						<div class="col-md-6">
							<form:input path="phone" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr ">
						<label class="col-md-2 control-label">Fax:</label>
						<div class="col-md-6">
							<form:input path="fax" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">company:</label>
						<div class="col-md-6">
							<form:input path="company" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row form-group">
					<div data-ng-init="getCountryList()"
						class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Country:</label>
						<div class="col-md-6">
							<select id="companyCountry" name="companyCountry"
								data-ng-model="userBO.country"
								class='form-control' required="required">
								<option value="">-- Select Country --</option>
								<option data-ng-repeat="countryBO in countryList"
									value="{{countryBO.countryName}}">{{countryBO.countryName}}</option>
							</select>
						</div>
					</div>
				</div>
						
				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">State:</label>
						<div class="col-md-6">
							<form:input path="state" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">City:</label>
						<div class="col-md-6">
							<form:input path="city" class='form-control textboxmargin' />


						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group no-margin-hr required">
						<label class="col-md-2 control-label">Zip:</label>
						<div class="col-md-6">
							<form:input path="zip" class='form-control textboxmargin' />


						</div>
					</div>
				</div>

			</div>
			<br />

			<div id="submitdiv" style="margin-left: 325px;">
				<input type="button" class='btn btn-primary pull-center'
					value="Cancel" onclick="cancel();" /> <input type="submit"
					class='btn btn-primary pull-center' value="Update" />
			</div>
		</div>
	</form:form>

</body>

</html>