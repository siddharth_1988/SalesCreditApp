<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html data-ng-app="salesCreditApp">
<head lang="en">
<meta charset="utf-8">
<title>Credit Request</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript" src="./resources/js/salesApp.js"></script>
</head>

<body data-ng-controller="CreditRequestController">
	<form:form method="post" action="creditRequestSubmission"
		id="creditRequestForm" class="panel form-horizontal"
		modelAttribute="creditUserRequest">

		<div id="know" class="panel-heading">Know your Customer</div>
		<div class="panel-body"
			data-ng-init="getCreditRequestDetailsFromServer()">
			<div class="row">
				<div class="border col-md-4">
					<div 
						class="form-group no-margin-hr required">
						<div class="numberCircle">1</div>
						<label class="control-label" id="segLabel"
							title=" TWDC is currently divided into 6 segments - STUDIO ENTERTAINMENT, MEDIA NETWORKS, CONSUMER PRODUCTS, PARKS AND RESORTS, INTERACTIVE, CORPORATE"
							data-toogle="tooltip" data-placement="right">Segment</label>
						<ui-select ng-model="segment.selected" 
							data-ng-change="selectedSegment(segment.selected)"
							class='form-control' style="height:47px"> 
							<ui-select-match placeholder="Select or search segment">{{$select.selected.segmentName}}</ui-select-match>
							<ui-select-choices repeat="segmentBO in creditUserRequest.segmentBOs | filter: $select.search">
								<div data-ng-bind-html="segmentBO.segmentName | highlight: $select.search"></div>
							</ui-select-choices> 
						</ui-select>
					</div>
				</div>

				<div class="border col-md-4">
					<div class="form-group no-margin-hr required">
						<div class="numberCircle">2</div>
						<label class="control-label" id="regLabel"
							title="For which region will this customer follow the credit rules and policies?"
							data-toogle="tooltip" data-placement="right">COBA</label> 
						<ui-select ng-model="cobaList.selected"
							data-ng-change="selectedCoba()"
							class='form-control' style="height:47px"> 
							<ui-select-match placeholder="Select or search COBA">{{$select.selected.coba}} {{$select.selected.cuDescription}}</ui-select-match>
							<ui-select-choices repeat="cobaListBO in creditUserRequest.cobaListBOs | filter: $select.search">
								<div data-ng-bind-html="cobaListBO.coba + ' ' + cobaListBO.cuDescription  | highlight: $select.search"></div>
							</ui-select-choices> 
						</ui-select>
					</div>
				</div>

			</div>
		</div>
	</form:form>
	<!-- <div data-ng-include="'adSalesNoSetup.jsp'"></div>  -->
	 
	<div data-ng-if="businessGroupIdData== 1 "
		data-ng-include="'adSalesNoSetup.jsp'"></div>
	<!--<div data-ng-if="businessGroupIdData== 2 "
		data-ng-include="'adSalesSetup.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 3 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 4 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 5 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 6 "
		data-ng-include="'emeaAdSales.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 7 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 0 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>  -->
</body>

</html>