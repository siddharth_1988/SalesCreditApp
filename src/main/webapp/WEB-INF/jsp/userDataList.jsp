<!DOCTYPE html>
<html data-ng-app="userApp">
<head lang="en">
<meta charset="UTF-8">
<title>User Management</title>

<!-- JS -->
<script type="text/javascript" src="./resources/js/angular.min.js"></script>
<script type="text/javascript" src="./resources/js/dirPagination.js"></script>
<script type="text/javascript" src="./resources/js/user.js"></script>

</head>
<body>
	<div data-ng-controller="userController">

		<form>

			<div class="form-group">
				<div class="input-group" style="width: 83%;">
					<div class="input-group-addon">
						<i class="fa fa-search"></i>
					</div>
					<input type="text" class="form-control" style="width: 50%;"
						placeholder="Search da User" data-ng-model="searchUser">
				</div>
				<div id="addTab" class="addUserButton">
					<a href="addUser">Add User</a>
				</div>
			</div>

		</form>
		<div data-ng-init="getUsersDataFromServer()">

			<table class="table table-bordered table-striped">

				<thead>
					<tr>
						<td><a href="#"
							data-ng-click="sortType = 'userName'; sortReverse = !sortReverse">
								UserName <span
								data-ng-show="sortType == 'userName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'name' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"
							data-ng-click="sortType = 'email'; sortReverse = !sortReverse">
								Email <span data-ng-show="sortType == 'email' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'email' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"
							data-ng-click="sortType = 'firstName'; sortReverse = !sortReverse">
								First Name <span
								data-ng-show="sortType == 'firstName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'firstName' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"
							data-ng-click="sortType = 'lastName'; sortReverse = !sortReverse">
								Last Name <span
								data-ng-show="sortType == 'lastName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'lastName' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"
							data-ng-click="sortType = 'lastName'; sortReverse = !sortReverse">
								Country <span
								data-ng-show="sortType == 'lastName' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								data-ng-show="sortType == 'lastName' && sortReverse"
								class="fa fa-caret-up"></span>
						</a></td>
						<td><a href="#"> Action </a></td>
					</tr>
				</thead>

				<tbody>
					<tr
						data-dir-paginate="userBO in userBODatas | orderBy:sortType:sortReverse | filter:searchUser | itemsPerPage:5">
						<td>{{ userBO.userName }}</td>
						<td>{{ userBO.email }}</td>
						<td>{{ userBO.firstName }}</td>
						<td>{{ userBO.lastName }}</td>
						<td>{{ userBO.country }}</td>
						<td><a href="editUser?userId={{ userBO.userId }}">Edit</a></td>
					</tr>
				</tbody>
			</table>
			<dir-pagination-controls max-size="5" direction-links="true"
				boundary-links="true"> </dir-pagination-controls>
		</div>
	</div>

</body>
</html>