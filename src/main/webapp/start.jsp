<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head><title>Welcome to Sales Credit</title>

 <script src="<c:url value="/resources/js/jquery-1.9.1.min.js" />"></script>
  <script src="<c:url value="/resources/js/jstz.js" />"></script>
<script type="text/javascript">
$(document).ready(function() {  
	 //alert("In ready!!");
	 setTimezoneCookie();
	 callImportTips();
 });

function setTimezoneCookie() {
     var timezone = jstz.determine().name();
		//alert("timezone:"+timezone);
     if (null == getCookie("timezoneCookie")) {
     document.cookie = "timezoneCookie=" + timezone;
     }
 }

 function getCookie(cookieName) {
     var cookieValue = document.cookie;
     var cookieStart = cookieValue.indexOf(" " + cookieName + "=");
     if (cookieStart == -1) {
         cookieStart = cookieValue.indexOf(cookieName + "=");
     }
     if (cookieStart == -1) {
         cookieValue = null;
     } else {
         cookieStart = cookieValue.indexOf("=", cookieStart) + 1;
         var cookieEnd = cookieValue.indexOf(";", cookieStart);
         if (cookieEnd == -1) {
             cookieEnd = cookieValue.length;
         }
         cookieValue = unescape(cookieValue.substring(cookieStart, cookieEnd));
     }
     return cookieValue;
 }

 function callImportTips(){
	//alert('three');
	 var referer = '<%= request.getParameter("refrerer") %>';
	//alert('document referer is '+referer);
	
	 if(referer=='null'){
		 	window.location="login.jsp";
	 }
	 else
		 window.location=referer;
     }
</script>   
</head>


<body>

</body>
</html>