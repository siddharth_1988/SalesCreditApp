<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html data-ng-app="salesCreditApp">
<head lang="en">
<meta charset="utf-8">
<title>Credit Request</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript"
	src="./resources/js/angular-1.2.26.min.js"></script>
<script type="text/javascript" src="./resources/js/salesApp.js"></script>
</head>

<body data-ng-controller="CreditRequestController">
	<form name="defaultForm" id="defaultForm">
		<div id="custDetails" class="panel form-horizontal"
			style="width: 800px; border-bottom: 1px none;">
			<div id="req" class="panel-heading">&nbsp;Request
				Submission&nbsp;</div>
			<div>No Business Group found for the selected fields.
				Displaying default mandatory fields for Credit Request Submission.</div>


			<div class="panel-body">

				<div class="row">

					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Requestor Name:</label> <input
								type="text" name="name" ng-model="creditRequest.name"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>

					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Requestor Email:</label> <input
								type="text" name="email" ng-model="creditRequest.email"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>

					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Customer Name:</label> <input
								type="text" name="customerName"
								ng-model="creditRequest.customerName"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>

					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Customer Street Address:</label> <input
								type="text" name="streetAddress"
								ng-model="creditRequest.streetAddress"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>


					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Customer City:</label> <input
								type="text" name="city" ng-model="creditRequest.city"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>

					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Customer State:</label> <input
								type="text" name="state" ng-model="creditRequest.state"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>

					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Customer ZipCode:</label> <input
								type="text" name="customerName" ng-model="creditRequest.zipCode"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>

					<div class="col-sm-5">
						<div class="form-group no-margin-hr">
							<label class="control-label">Customer Country:</label> <input
								type="text" name="country" ng-model="creditRequest.country"
								class='form-control textboxmargin' required="required" />

						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="panel-footer text-right">
			<button class="btn btn-primary">Submit</button>
		</div>
	</form>
</body>

</html>