<html data-ng-app="salesCreditApp">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Sales Portal</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<!-- Hex's stylesheets -->
	<link href="./resources/css/widgets.min.css" rel="stylesheet" type="text/css">
	<link href="http://hex.wds.io/0.7.0/css/rtl.min.css" rel="stylesheet" type="text/css">
  <link href="http://hex.wds.io/0.7.0/css/themes/iron-patriot.min.css" id="theme-css" rel="stylesheet" type="text/css">

<body class="theme-iron-patriot main-menu-animated">

<div id="main-wrapper">


<!-- 2. $MAIN_NAVIGATION ===========================================================================

	Main navigation
-->
	<div id="main-navbar" class="navbar navbar-inverse" role="navigation">
		<!-- Main menu toggle 
		<button type="button" id="main-menu-toggle"><i class="navbar-icon fa fa-bars icon"></i><span class="hide-menu-text">HIDE MENU</span></button>-->

		<div class="navbar-inner">
			<!-- Main navbar header -->
			<div class="navbar-header">

				<!-- Main navbar toggle -->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse" role="tab" aria-selected="false" aria-expanded="false" id="ui-collapse-335"><i class="navbar-icon fa fa-bars"></i></button>
				<div>
						<img src="./resources/images/eCredit_logo.jpg" alt="" height="7%" width="42%" class="applogo">
					</div>
			</div> <!-- / .navbar-header -->
			
			<div id="main-navbar-collapse" class="collapse navbar-collapse main-navbar-collapse">
				<div><h4 style="text-align: center;">Sales Portal</h4></div>
				<div>	
					
					<div class="right clearfix">
						
						<ul class="nav navbar-nav pull-right right-navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="./resources/images/user.png" alt="">
									<span><%=session.getAttribute("portalId")%></span>
								</a>
							</li>
						</ul> <!-- / .navbar-nav -->
					</div> <!-- / .right -->
				</div>
			</div> <!-- / #main-navbar-collapse -->
		</div> <!-- / .navbar-inner -->
	</div> <!-- / #main-navbar -->
<!-- /2. $END_MAIN_NAVIGATION -->


    <!-- MAIN SIDE NAV -->
<div id="main-menu" role="navigation">
<div id="main-menu-inner">
<ul class="navigation">
    <li id="nav-home">
        <a href="#/dashboard"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a>
    </li>
    <li id="nav-creditRequest">
        <a href="#/creditRequest"><i class="menu-icon fa fa-tasks"></i><span class="mm-text">Credit Submission</span></a>
    </li>
    <li id="nav-creditRequest">
    	<a href="#/admin"><i class="menu-icon glyphicon glyphicon-user"></i><span class="mm-text">Admin Module</span></a></li>
    <li id="nav-help">
        <a href="#/help"><i class="menu-icon fa fa-question"></i><span class="mm-text">Help</span></a>
    </li>
    
</ul> <!-- / .navigation -->
</div> <!-- / #main-menu-inner -->
</div>
    <!-- MAIN SIDE NAV -->

	<div ng-view></div>
</div> <!-- / #main-wrapper -->

<!-- Hex's javascripts -->

<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular-route.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular-sanitize.js"></script>
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">

  <!-- ui-select files -->
  <script src="https://cdn.rawgit.com/angular-ui/ui-select/master/dist/select.min.js"></script>
  <link rel="stylesheet" href="https://cdn.rawgit.com/angular-ui/ui-select/master/dist/select.min.css">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
  
  
<script type="text/javascript" src="./resources/js/jquery-2.1.4.min.js"></script>



<script type="text/javascript" src="./resources/js/salesApp.js"></script>


<div id="footer" class="container">
    <nav class="navbar navbar-default navbar-fixed-bottom">
        <div class="navbar-inner navbar-content-center" style="text-align: center;">
            <p class="text-muted credit">&copy;Copyright 2015 Sales Portal</p>
        </div>
    </nav>
</div>
</body>

</html>

 