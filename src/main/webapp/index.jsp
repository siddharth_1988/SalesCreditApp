<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
 <head>
  <title>Welcome</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <!-- Hex CSS -->
    
    <link href="<c:url value="/resources/css/themes/southpaw.min.css" />" rel="stylesheet" type="text/css">
    <script src="<c:url value="/resources/js/hex.min.js" />"></script>
 </head>
 <body>
 <h2>Sales Credit App</h2>
  <div id='easierExample' data-qa='easier'></div>
  <!-- Hex JS -->
    <script>
        // This is inline for demo purposes only. Please put your js in a js file... for the children.
        $('#easierExample').switcher();
    </script>
 </body>
</html>