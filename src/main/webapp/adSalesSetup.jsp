<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="utf-8">
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript" src="./resources/js/salesApp.js"></script>	
</head>

<body>
	<form:form name="adSalesSetupForm" class="form-horizontal row-border"
		id="adSalesSetupForm" method="post"
		action="xmlbasedcreditRequestSubmission">
		<div id="custDetails" class="panel form-horizontal">
			<div id="adsales" class="panel-heading">Request Submission</div>
			<div class="panel-body">

				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-target="#requestorDetails"
									href="#requestorDetails"> Requester Details </a>
							</h4>
						</div>
						<div id="requestorDetails" class="panel-collapse collapse in">
							<div class="panel-body">

								<div class="row form-group"
									data-ng-class="{ 'has-error' : adSalesSetupForm.requestorName.$invalid && !adSalesSetupForm.requestorName.$pristine }">
									<div class="form-group no-margin-hr required">
										<label class="col-md-2 control-label">Requester Name:</label>
										<div class="col-md-7">
											<input type="text" name="requestorName"
												data-ng-model="creditRequest.requestorName"
												class='form-control textboxmargin' required
												data-ng-pattern='/^[a-z\s]+$/i' />
											<p
												data-ng-show="adSalesSetupForm.requestorName.$error.pattern"
												class="help-block">Please enter alphabets only.</p>
										</div>
									</div>
								</div>

								<div class="row form-group"
									data-ng-class="{ 'has-error' : adSalesSetupForm.requestorEmail.$invalid && !adSalesSetupForm.requestorEmail.$pristine }">
									<div class="form-group no-margin-hr required">
										<label class="col-md-2 control-label">Requester Email:</label>
										<div class="col-md-7">
											<input type="email" name="requestorEmail"
												data-ng-model="creditRequest.requestorEmail"
												class='form-control textboxmargin' required />
											<p
												data-ng-show="adSalesSetupForm.requestorEmail.$invalid && !adSalesSetupForm.requestorEmail.$pristine"
												class="help-block">Please enter valid email.</p>
										</div>
									</div>
								</div>

								<div class="row form-group"
									data-ng-class="{ 'has-error' : adSalesSetupForm.requestNeededByDate.$invalid && !adSalesSetupForm.requestNeededByDate.$pristine }">
									<div class="form-group no-margin-hr required">
										<label class="col-md-2 control-label">Request Needed
											By Date:</label>
										<div class="col-md-7">
											<input type="date" name="requestNeededByDate"
												max="9999-12-31" min="{{currentDate | date: 'yyyy-MM-dd'}}"
												data-ng-model="creditRequest.requestNeededByDate"
												class='form-control textboxmargin' required="required" />
											<p
												data-ng-show="adSalesSetupForm.requestNeededByDate.$error.min"
												class="help-block">Should be equal/greater than Current
												Date.</p>
										</div>
									</div>
								</div>
								<a data-toggle="collapse" data-target="#businessDetails"
									data-parent="#requestorDetails" href="#businessDetails"
									class="btn btn-primary pull-right continue"
									onclick="$('#requestorDetails').collapse('hide');"
									data-ng-disabled="adSalesSetupForm.requestorName.$invalid || adSalesSetupForm.requestorEmail.$invalid
									 || adSalesSetupForm.requestNeededByDate.$invalid">Next
								</a>
							</div>
						</div>

					</div>
				</div>
				<div class="panel panel-default" id="businessDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#businessDetails"
								href="#businessDetails" class="collapsed"
								data-ng-disabled="adSalesSetupForm.requestorName.$invalid || adSalesSetupForm.requestorEmail.$invalid
									 || adSalesSetupForm.requestNeededByDate.$invalid">
								Business Details </a>
						</h4>
					</div>
					<div id="businessDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.requestedCreditLimit.$invalid && !adSalesSetupForm.requestedCreditLimit.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Amount of
										Business:</label>
									<div class="col-md-7">
										<input type="text" name="requestedCreditLimit"
											data-ng-model="creditRequest.requestedCreditLimit"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9.]*$/' />
										<p
											data-ng-show="adSalesSetupForm.requestedCreditLimit.$error.pattern"
											class="help-block">Please enter positive number.</p>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Term Start Date:</label>
									<div class="col-md-7">
										<input type="date" name="termStartDate" max="9999-12-31"
											data-ng-model="creditRequest.termStartDate"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : creditRequest.termEndDate < creditRequest.termStartDate  }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Term End Date:</label>
									<div class="col-md-7">
										<input type="date" name="termEndDate"
											data-ng-model="creditRequest.termEndDate" max="9999-12-31"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="creditRequest.termEndDate < creditRequest.termStartDate"
											class="help-block">Term End Date Should be equal/greater
											than Term Start Date</p>
									</div>
								</div>
							</div>
							<a data-toggle="collapse" data-target="#requestorDetails"
								data-parent="#businessDetails" href="#requestorDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#businessDetails').collapse('hide');">Previous </a>

							<a data-toggle="collapse" data-target="#companyDetails"
								data-parent="#businessDetails" href="#companyDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#businessDetails').collapse('hide');"
								data-ng-disabled="adSalesSetupForm.requestedCreditLimit.$invalid || adSalesSetupForm.termStartDate.$invalid
									 || adSalesSetupForm.termEndDate.$invalid || 
									 creditRequest.termEndDate < creditRequest.termStartDate">Next
							</a>
						</div>
					</div>
				</div>
				<div class="panel panel-default" id="companyDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#companyDetails"
								href="#companyDetails" class="collapsed"
								data-ng-disabled="adSalesSetupForm.requestedCreditLimit.$invalid || adSalesSetupForm.termStartDate.$invalid
									 || adSalesSetupForm.termEndDate.$invalid || creditRequest.termEndDate < creditRequest.termStartDate">
								Company Details </a>
						</h4>
					</div>
					<div id="companyDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.companyName.$invalid && !adSalesSetupForm.companyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company/Licensee
										Name:</label>
									<div class="col-md-7">
										<input type="text" name="companyName"
											data-ng-model="creditRequest.companyName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="adSalesSetupForm.companyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Street
										Address:</label>
									<div class="col-md-7">
										<input type="text" name="companyStreetAddress"
											data-ng-model="creditRequest.companyStreetAddress"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.companyCity.$invalid && !adSalesSetupForm.companyCity.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company City:</label>
									<div class="col-md-7">
										<input type="text" name="companyCity"
											data-ng-model="creditRequest.companyCity"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.companyCity.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div data-ng-init="getCountryList()"
									class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Country:</label>
									<div class="col-md-7">
										<select id="companyCountry" name="companyCountry"
											data-ng-model="creditRequest.companyCountry"
											data-ng-change="isUSASelected(creditRequest.companyCountry)"
											class='form-control' required="required">
											<option value="">-- Select Country --</option>
											<option data-ng-repeat="countryBO in countryList"
												value="{{countryBO.countryName}}">{{countryBO.countryName}}</option>
										</select>
									</div>
								</div>
							</div>

							<div id="usaStateId" class="row form-group" data-ng-show="isUSA">
								<div data-ng-init="getUsaStateList()"
									class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company State:</label>
									<div class="col-md-7">
										<select id="companyState" name="companyState"
											data-ng-model="creditRequest.companyState"
											class='form-control' required="required">
											<option value="">-- Select State --</option>
											<option data-ng-repeat="stateBO in stateList"
												value="{{stateBO.stateName}}">{{stateBO.stateName}}</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row form-group" data-ng-show="showdefaultState"
								class="ng-hide"
								data-ng-class="{ 'has-error' : adSalesSetupForm.companyState.$invalid && !adSalesSetupForm.companyState.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company State:</label>
									<div class="col-md-7">
										<input type="text" name="companyState"
											data-ng-model="creditRequest.companyState"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="adSalesSetupForm.companyState.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.companyZip.$invalid && !adSalesSetupForm.companyZip.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Company Zip:</label>
									<div class="col-md-7">
										<input type="text" name="companyZip"
											data-ng-model="creditRequest.companyZip"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.companyZip.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>


							<a data-toggle="collapse" data-target="#businessDetails"
								data-parent="#companyDetails" href="#businessDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#companyDetails').collapse('hide');">Previous </a> <a
								data-toggle="collapse" data-target="#agencyDetails"
								data-parent="#companyDetails" href="#agencyDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#companyDetails').collapse('hide');"
								data-ng-disabled="adSalesSetupForm.companyName.$invalid || adSalesSetupForm.companyStreetAddress.$invalid
									 || adSalesSetupForm.companyCity.$invalid || adSalesSetupForm.companyState.$invalid || 
									 adSalesSetupForm.companyZip.$invalid
									 || adSalesSetupForm.companyCountry.$invalid">Next
							</a>
						</div>
					</div>
				</div>
				<div class="panel panel-default" id="agencyDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#agencyDetails"
								href="#agencyDetails" class="collapsed"
								data-ng-disabled="adSalesSetupForm.companyName.$invalid || adSalesSetupForm.companyStreetAddress.$invalid
									 || adSalesSetupForm.companyCity.$invalid || adSalesSetupForm.companyState.$invalid || 
									 adSalesSetupForm.companyZip.$invalid
									 || adSalesSetupForm.companyCountry.$invalid">
								Agency Details </a>
						</h4>
					</div>
					<div id="agencyDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.agencyName.$invalid && !adSalesSetupForm.agencyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Name:</label>
									<div class="col-md-6">
										<input type="text" name="agencyName"
											data-ng-model="creditRequest.agencyName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="adSalesSetupForm.agencyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Street
										Address:</label>
									<div class="col-md-6">
										<input type="text" name="agencyStreetAddress"
											data-ng-model="creditRequest.agencyStreetAddress"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.agencyCity.$invalid && !adSalesSetupForm.agencyCity.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency City:</label>
									<div class="col-md-6">
										<input type="text" name="agencyCity"
											data-ng-model="creditRequest.agencyCity"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.agencyCity.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div data-ng-init="getCountryList()"
									class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Country:</label>
									<div class="col-md-6">
										<select id="agencyCountry" name="agencyCountry"
											data-ng-model="creditRequest.agencyCountry"
											data-ng-change="isUSASelectedForAgency(creditRequest.agencyCountry)"
											class='form-control' required="required">
											<option value="">-- Select Country --</option>
											<option data-ng-repeat="countryBO in countryList"
												value="{{countryBO.countryName}}">{{countryBO.countryName}}</option>
										</select>
									</div>
								</div>
							</div>

							<div id="usaStateAgencyId" class="row form-group"
								data-ng-show="isUSAForAgency">
								<div data-ng-init="getUsaStateListForAgency()"
									class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency State:</label>
									<div class="col-md-6">
										<select id="agencyState" name="agencyState"
											data-ng-model="creditRequest.agencyState"
											class='form-control' required="required">
											<option value="">-- Select State --</option>
											<option data-ng-repeat="stateBO in agencyStateList"
												value="{{stateBO.stateName}}">{{stateBO.stateName}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row form-group" data-ng-show="defaultAgencyState"
								class="ng-hide"
								data-ng-class="{ 'has-error' : adSalesSetupForm.agencyState.$invalid && !adSalesSetupForm.agencyState.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency State:</label>
									<div class="col-md-6">
										<input type="text" name="agencyState"
											data-ng-model="creditRequest.agencyState"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="adSalesSetupForm.agencyState.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.agencyZip.$invalid && !adSalesSetupForm.agencyZip.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Zip:</label>
									<div class="col-md-6">
										<input type="text" name="agencyZip"
											data-ng-model="creditRequest.agencyZip"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.agencyZip.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>


							<div class="row form-group">
								<label class="col-md-3 control-label">Agency Billing
									Export Type (OTV/ABCF) - TEC:</label>
								<div class="col-md-6">
									<input type="file" name="aAgencyBillingExport"
										data-ng-model="creditRequest.aAgencyBillingExport"
										class='form-control textboxmargin' />
								</div>

							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.agencyCommissionRate.$invalid && !adSalesSetupForm.agencyCommissionRate.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Commission
										Rate (in %):</label>
									<div class="col-md-6">
										<input type="text" name="agencyCommissionRate"
											data-ng-model="creditRequest.agencyCommissionRate"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9]*$/' />
										<p
											data-ng-show="adSalesSetupForm.agencyCommissionRate.$error.pattern"
											class="help-block">Please enter numbers only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Commission
										Rate Approved by Business Manager?:</label>
									<div class="col-md-7">
										<input type="checkbox" name="isCommissionRateApproved"
											style="margin-left: -316px; margin-top: 4px; height: 25px;"
											data-ng-model="creditRequest.isCommissionRateApproved"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.accountExecutive.$invalid && !adSalesSetupForm.accountExecutive.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Account
										Executive/Category Manager:</label>
									<div class="col-md-6">
										<input type="text" name="accountExecutive"
											data-ng-model="creditRequest.accountExecutive"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p
											data-ng-show="adSalesSetupForm.accountExecutive.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Trade Bill Pay
										Indicator:</label>
									<div class="col-md-7">
										<input type="checkbox" name="isTradeBillPayIndicator"
											style="margin-left: -316px; margin-top: 4px; height: 25px;"
											data-ng-model="creditRequest.isTradeBillPayIndicator"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>

							<a data-toggle="collapse" data-target="#companyDetails"
								data-parent="#agencyDetails" href="#companyDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#agencyDetails').collapse('hide');">Previous </a> <a
								data-toggle="collapse" data-target="#cCollectionDetails"
								data-parent="#agencyDetails" href="#cCollectionDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#agencyDetails').collapse('hide');"
								data-ng-disabled="adSalesSetupForm.agencyName.$invalid || adSalesSetupForm.agencyStreetAddress.$invalid
									 || adSalesSetupForm.agencyCity.$invalid || adSalesSetupForm.agencyState.$invalid || 
									 adSalesSetupForm.agencyZip.$invalid || adSalesSetupForm.agencyCountry.$invalid 
									 || adSalesSetupForm.agencyCommissionRate.$invalid || 
									 adSalesSetupForm.isCommissionRateApproved.$invalid
									 || adSalesSetupForm.accountExecutive.$invalid || 
									 adSalesSetupForm.isTradeBillPayIndicator.$invalid">Next
							</a>
						</div>
					</div>
				</div>
				<div class="panel panel-default" id="cCollectionDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#cCollectionDetails"
								href="#cCollectionDetails" class="collapsed"
								data-ng-disabled="adSalesSetupForm.agencyName.$invalid || adSalesSetupForm.agencyStreetAddress.$invalid
									 || adSalesSetupForm.agencyCity.$invalid || adSalesSetupForm.agencyState.$invalid || 
									 adSalesSetupForm.agencyZip.$invalid || adSalesSetupForm.agencyCountry.$invalid 
									 || adSalesSetupForm.agencyCommissionRate.$invalid || 
									 adSalesSetupForm.isCommissionRateApproved.$invalid
									 || adSalesSetupForm.accountExecutive.$invalid || 
									 adSalesSetupForm.isTradeBillPayIndicator.$invalid">
								Contact Details for collection </a>
						</h4>
					</div>
					<div id="cCollectionDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.cContactPhone.$invalid && !adSalesSetupForm.cContactPhone.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Contact Phone -
										For Collection Purposes:</label>
									<div class="col-md-6">
										<input type="text" name="cContactPhone"
											data-ng-model="creditRequest.cContactPhone"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9-()]*$/' />
										<p
											data-ng-show="adSalesSetupForm.cContactPhone.$error.pattern"
											class="help-block">Please enter valid phone number.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.cContactEmail.$invalid && !adSalesSetupForm.cContactEmail.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Contact Email -
										For Collection Purposes:</label>
									<div class="col-md-6">
										<input type="email" name="cContactEmail"
											data-ng-model="creditRequest.cContactEmail"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="adSalesSetupForm.cContactEmail.$invalid && !adSalesSetupForm.cContactEmail.$pristine"
											class="help-block">Please enter valid email.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.cContactFirstName.$invalid && !adSalesSetupForm.cContactFirstName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Contact First
										Name - For Collection Purposes:</label>
									<div class="col-md-6">
										<input type="text" name="cContactFirstName"
											data-ng-model="creditRequest.cContactFirstName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesSetupForm.cContactFirstName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<a data-toggle="collapse" data-target="#agencyDetails"
								data-parent="#cCollectionDetails" href="#agencyDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#cCollectionDetails').collapse('hide');">Previous
							</a> <a data-toggle="collapse" data-target="#advanceDirectDetails"
								data-parent="#cCollectionDetails" href="#advanceDirectDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#cCollectionDetails').collapse('hide');"
								data-ng-disabled="adSalesSetupForm.cContactPhone.$invalid || adSalesSetupForm.cContactEmail.$invalid
									 || adSalesSetupForm.cContactFirstName.$invalid">Next
							</a>

						</div>
					</div>
				</div>
				<div class="panel panel-default" id="advanceDirectPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#advanceDirectDetails"
								href="#advanceDirectDetails" class="collapsed"
								data-ng-disabled="adSalesSetupForm.cContactPhone.$invalid || adSalesSetupForm.cContactEmail.$invalid
									 || adSalesSetupForm.cContactFirstName.$invalid">
								Advance Cash/Direct </a>
						</h4>
					</div>
					<div id="advanceDirectDetails" class="panel-collapse collapse">
						<div class="panel-body">

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Political Buy?:</label>
									<div class="col-md-7">
										<input type="checkbox" name="isPoliticalBuy"
											style="margin-left: -316px; margin-top: 4px; height: 25px;"
											data-ng-model="creditRequest.isPoliticalBuy"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>
							<div class="row form-group">
								<label class="col-md-2 control-label">Direct?:</label>
								<div class="col-md-7" style="margin-left: -9px;">
									<div class="col-md-2">
										<input type="radio" name="directY" value="1"
											data-ng-model="creditRequest.direct" /> Yes
									</div>
									<div class="col-md-2">
										<input type="radio" name="directN" value="0"
											data-ng-model="creditRequest.direct" /> No
									</div>
								</div>
							</div>

							<div class="row form-group">
								<label class="col-md-2 control-label">Local or
									National?:</label>
								<div class="col-md-7" style="margin-left: -9px;">
									<div class="col-md-2">
										<input type="radio" name="localOrNational" value="1"
											data-ng-model="creditRequest.localOrNational" /> Yes
									</div>
									<div class="col-md-2">
										<input type="radio" name="nonLocalOrNational" value="0"
											data-ng-model="creditRequest.localOrNational" /> No
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Cap Net?:</label>
									<div class="col-md-7">
										<input type="checkbox" name="isCapNet"
											style="margin-left: -316px; margin-top: 4px; height: 25px;"
											data-ng-model="creditRequest.isCapNet"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.capNetAddress.$invalid && !adSalesSetupForm.capNetAddress.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">CaPNet Address:</label>
									<div class="col-md-7">
										<input type="text" name="capNetAddress"
											data-ng-model="creditRequest.capNetAddress"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesSetupForm.capNetAddress.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<a data-toggle="collapse" data-target="#cCollectionDetails"
								data-parent="#advanceDirectDetails" href="#cCollectionDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#advanceDirectDetails').collapse('hide');">Previous
							</a> <a data-toggle="collapse" data-target="#attachment"
								data-parent="#advanceDirectDetails" href="#attachment"
								class="btn btn-primary pull-right continue"
								onclick="$('#advanceDirectDetails').collapse('hide');"
								data-ng-disabled="adSalesSetupForm.isPoliticalBuy.$invalid || adSalesSetupForm.isCapNet.$invalid
									 || adSalesSetupForm.capNetAddress.$invalid">Next
							</a>

						</div>
					</div>
				</div>

				<div class="panel panel-default" id="attachmentPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#attachment"
								href="#attachment" class="collapsed"
								data-ng-disabled="adSalesSetupForm.isPoliticalBuy.$invalid || adSalesSetupForm.isCapNet.$invalid
									 || adSalesSetupForm.capNetAddress.$invalid">
								Attachment </a>
						</h4>
					</div>
					<div id="attachment" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group">
								<label class="col-md-2 control-label">Add Attachment:</label>
								<div class="col-md-7">
									<input type="file" name="attachment"
										data-ng-model="creditRequest.attachment"
										class='form-control textboxmargin' />
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">Web Billing?:</label>
									<div class="col-md-7">
										<input type="checkbox" name="isWebBilling"
											style="margin-left: -316px; margin-top: 4px; height: 25px;"
											data-ng-model="creditRequest.isWebBilling"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>
							<a data-toggle="collapse" data-target="#advanceDirectDetails"
								data-parent="#attachment" href="#advanceDirectDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#attachment').collapse('hide');">Previous </a> <a
								data-toggle="collapse" data-target="#foh"
								data-parent="#attachment" href="#foh"
								class="btn btn-primary pull-right continue"
								onclick="$('#attachment').collapse('hide');"
								data-ng-disabled="adSalesSetupForm.isWebBilling.$invalid">Next
							</a>

						</div>
					</div>
				</div>

				<div class="panel panel-default" id="fohPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#foh" href="#foh"
								class="collapsed"
								data-ng-disabled="adSalesSetupForm.isWebBilling.$invalid">
								FOH Details </a>
						</h4>
					</div>
					<div id="foh" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.fohIdAdv.$invalid && !adSalesSetupForm.fohIdAdv.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">FOH ID #
										(Advertiser):</label>
									<div class="col-md-7">
										<input type="text" name="fohIdAdv"
											data-ng-model="creditRequest.fohIdAdv"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.fohIdAdv.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.fohAgency.$invalid && !adSalesSetupForm.fohAgency.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-2 control-label">FOH ID #
										(Agency):</label>
									<div class="col-md-7">
										<input type="text" name="fohAgency"
											data-ng-model="creditRequest.fohAgency"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.fohAgency.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<a data-toggle="collapse" data-target="#attachment"
								data-parent="#foh" href="#attachment"
								class="btn btn-primary pull-left continue"
								onclick="$('#foh').collapse('hide');">Previous </a> <a
								data-toggle="collapse" data-target="#revenue" data-parent="#foh"
								href="#revenue" class="btn btn-primary pull-right continue"
								onclick="$('#foh').collapse('hide');"
								data-ng-disabled="adSalesSetupForm.fohIdAdv.$invalid || 
								adSalesSetupForm.fohAgency.$invalid">Next
							</a>
						</div>

					</div>
				</div>

				<div class="panel panel-default" id="revenuePanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#revenue" href="#revenue"
								class="collapsed"
								data-ng-disabled="adSalesSetupForm.fohIdAdv.$invalid || 
								adSalesSetupForm.fohAgency.$invalid">
								Revenue Section </a>
						</h4>
					</div>
					<div id="revenue" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.productCode.$invalid && !adSalesSetupForm.productCode.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Product Code:</label>
									<div class="col-md-6">
										<input type="text" name="productCode"
											data-ng-model="creditRequest.productCode"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.productCode.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.revenueCode1.$invalid && !adSalesSetupForm.revenueCode1.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Revenue Code 1:</label>
									<div class="col-md-6">
										<input type="text" name="revenueCode1"
											data-ng-model="creditRequest.revenueCode1"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.revenueCode1.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.revenueCode2.$invalid && !adSalesSetupForm.revenueCode2.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Revenue Code 2:</label>
									<div class="col-md-6">
										<input type="text" name="revenueCode2"
											data-ng-model="creditRequest.revenueCode2"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.revenueCode2.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.revenueCode3.$invalid && !adSalesSetupForm.revenueCode3.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Revenue Code 3:</label>
									<div class="col-md-6">
										<input type="text" name="revenueCode3"
											data-ng-model="creditRequest.revenueCode3"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.revenueCode3.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.priorityCode.$invalid && !adSalesSetupForm.priorityCode.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Priority Code:</label>
									<div class="col-md-6">
										<input type="text" name="priorityCode"
											data-ng-model="creditRequest.priorityCode"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.priorityCode.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.demographic.$invalid && !adSalesSetupForm.demographic.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Demographic:</label>
									<div class="col-md-6">
										<input type="text" name="demographic"
											data-ng-model="creditRequest.demographic"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.demographic.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.dealOrderType.$invalid && !adSalesSetupForm.dealOrderType.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Deal/Order Type -
										Business type (in advertising world):</label>
									<div class="col-md-6">
										<input type="text" name="priorityCode"
											data-ng-model="creditRequest.dealOrderType"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesSetupForm.dealOrderType.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesSetupForm.notes.$invalid && !adSalesSetupForm.notes.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Notes:</label>
									<div class="col-md-6">
										<input type="text" name="notes"
											data-ng-model="creditRequest.notes"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesSetupForm.notes.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<a data-toggle="collapse" data-target="#foh"
								data-parent="#revenue" href="#foh"
								class="btn btn-primary pull-left continue"
								onclick="$('#revenue').collapse('hide');">Previous </a>
						</div>

					</div>

				</div>
			</div>
		</div>
		<div class="panel-footer text-right">
			<button class="btn btn-primary"
				data-ng-disabled="adSalesSetupForm.$invalid || creditRequest.termEndDate < creditRequest.termStartDate">Submit</button>
		</div>
	</form:form>
</body>

</html>
