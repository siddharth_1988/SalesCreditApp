<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<!-- <link rel="stylesheet" type="text/css" href="./resources/css/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="./resources/css/bootstrap-responsive.min.css"></link>
<link rel="stylesheet" type="text/css" href="/resources/css/project_style.css"></link> -->
<!-- <link rel="stylesheet" type="text/css" href="./resources/css/salesapp.css"></link> -->
<!-- <script type="text/javascript" src="./resources/js/jquery-1.9.1.min.js"></script> -->

<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<!-- Hex CSS -->
<link rel="stylesheet" type="text/css"
	href="./resources/css/themes/southpaw.min.css"></link>
</head>

<body>
	<div>
		<tiles:insertAttribute name="header" />
	</div>
	<div class="menuPage">
		<div id="menuItems" class="menuItemBox">
			<tiles:insertAttribute name="menu" />
		</div>
	</div>
	<div class="pageBody">
		<tiles:insertAttribute name="body" />
	</div>
	<div style="clear: both">
		<tiles:insertAttribute name="footer" />
	</div>


</body>
</html>
