<!DOCTYPE html>
<html>
<head>
<title>Sales Credit App</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<!-- Hex CSS -->
<link href="./resources/css/widgets.min.css" rel="stylesheet"
	type="text/css">
<link href="./resources/css/salesapp.css" rel="stylesheet"
	type="text/css">
<link rel="stylesheet" href="https://cdn.rawgit.com/angular-ui/ui-select/master/dist/select.min.css">
   <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">	
	
</head>
<header>
	<div id="main-menu" role="navigation">
		<div id="main-menu-inner">
			<ul class="navigation" id="nav">
				<li id="nav-home"><a href="dashboard"><i
						class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a>
				</li>
				<li id="nav-creditRequest"><a href="creditRequest"><i
						class="menu-icon fa fa-tasks"></i><span class="mm-text">Credit
							Submission</span></a></li>
				<li id="nav-creditRequest"><a href="admin"><i
						class="menu-icon glyphicon glyphicon-user"></i><span class="mm-text">Admin
							Module</span></a></li>
				<li id="nav-help"><a href="help"><i
						class="menu-icon fa fa-question"></i><span class="mm-text">Help</span></a>
				</li>
			</ul>
			<!-- / .navigation -->
		</div>
		<!-- / #main-menu-inner -->
	</div>
	<!-- Hex JS -->
	<%-- <script src="<c:url value="/resources/js/hex.min.js" />"></script> --%>
	<script type="text/javascript" src="./resources/js/jquery-2.1.4.min.js"></script>
	<!-- <script type="text/javascript" src="./resources/js/angular.min.js"></script> -->
	<script type="text/javascript" src="./resources/js/bootstrap.min.js"></script>
	
	<!-- <script src="https://dl.dropboxusercontent.com/u/1004639/stackoverflow/angular-ui-select2.js"></script> -->
	
	<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular-route.js"></script>
 	 <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular-sanitize.js"></script>

  	<!-- ui-select files -->
  	<script src="https://cdn.rawgit.com/angular-ui/ui-select/master/dist/select.min.js"></script>	
  	<script type="text/javascript" src="./resources/js/salesApp.js"></script>
</header>
<body></body>
</html>