<html>
<head>
<link href="./resources/css/widgets.min.css" rel="stylesheet"
	type="text/css">
<link href="http://hex.wds.io/0.7.0/css/rtl.min.css" rel="stylesheet"
	type="text/css">
<link href="http://hex.wds.io/0.7.0/css/themes/iron-patriot.min.css"
	id="theme-css" rel="stylesheet" type="text/css">
<link href="./resources/css/salesapp.css" rel="stylesheet"
	type="text/css">
</head>

	<header>
		<div id="main-navbar" class="navbar navbar-inverse" role="navigation">
			<div class="navbar-inner">
				<!-- Main navbar header -->
				<div class="navbar-header">
					<div>
						<img src="./resources/images/eCredit_logo.jpg" alt="" height="25%"
							width="42%" class="applogo" />
					</div>
				</div>
				<!-- / .navbar-header -->
				<div id="main-navbar-collapse"
					class="collapse navbar-collapse main-navbar-collapse">
					
					<div class="right clearfix">
						<ul class="nav navbar-nav pull-right right-navbar-nav">
							<li><img src="./resources/images/user.png" alt="" width="25%"
								height="25%"> <span><%=session.getAttribute("portalId")%></span>
							</li>
						</ul>
						<!-- / .navbar-nav -->
					</div>
					<!-- / .right -->
				</div>
			</div>
		</div>
	</header>
	<body></body>
</html>