<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="utf-8">
<title>Credit Request</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript" src="./resources/js/salesApp.js"></script>
</head>

<body>
	<form:form name="adSalesForm" class="form-horizontal row-border"
		id="adSalesForm" method="post"
		action="xmlbasedcreditRequestSubmission">
		<div id="custDetails" class="panel form-horizontal">
			<div id="adsales" class="panel-heading">Request Submission</div>
			<div class="panel-body">

				<div class="panel panel-default">
					<div class="panel-heading" >
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#requestorDetails"
								href="#requestorDetails"> Request for Credit </a>
						</h4>
					</div>
					<div id="requestorDetails" class="panel-collapse collapse in">
						<div class="panel-body">

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.requestorName.$invalid && !adSalesForm.requestorName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Requestor Name:</label>
									<div class="col-md-6">
										<input type="text" name="requestorName" id="requestorName"
											data-ng-disabled="true"
											data-ng-model="creditUserRequest.requestorName"
											class='form-control textboxmargin' required
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="adSalesForm.requestorName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.requestorEmail.$invalid && !adSalesForm.requestorEmail.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Requestor Email:</label>
									<div class="col-md-6">
										<input type="email" name="requestorEmail"
											data-ng-disabled="true"
											data-ng-model="creditUserRequest.requestorEmail"
											class='form-control textboxmargin' required />
										<p
											data-ng-show="adSalesForm.requestorEmail.$invalid && !adSalesForm.requestorEmail.$pristine"
											class="help-block">Please enter valid email.</p>
									</div>
								</div>
							</div>
							
							
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aeFirstName.$invalid && !adSalesForm.aeFirstName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Account Executive - First Name:</label>
									<div class="col-md-6">
										<input type="text" name="aeFirstName"
											data-ng-model="creditUserRequest.aeFirstName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.aeFirstName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aeLastName.$invalid && !adSalesForm.aeLastName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Account Executive - Last Name:</label>
									<div class="col-md-6">
										<input type="text" name="aeLastName"
											data-ng-model="creditUserRequest.aeLastName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.aeLastName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aeEmail.$invalid && !adSalesForm.aeEmail.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Account Executive - Email address:</label>
									<div class="col-md-6">
										<input type="email" name="aeEmail"
											data-ng-model="creditUserRequest.aeEmail"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="adSalesForm.aeEmail.$invalid && !adSalesForm.aeEmail.$pristine"
											class="help-block">Please enter valid email.</p>
									</div>
								</div>
							</div>
							
							<a data-toggle="collapse" data-target="#campaignInfo"
								data-parent="#requestorDetails" href="#campaignInfo"
								class="btn btn-primary pull-right continue"
								onclick="$('#requestorDetails').collapse('hide');"
								data-ng-click="saveUserRequesterDetails(creditUserRequest);"
								>Next
							</a>
							<p>{{creditUserRequest.aeEmail}}</p>
							<p>{{creditUserRequest.aeLastName}}</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default" id="campaignInfoPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#campaignInfo"
								href="#campaignInfo" class="collapsed" onclick="">
								Campaign Information </a>
						</h4>
					</div>
					<div id="campaignInfo" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group">
								<div class="col-md-6">
									<div class="form-group no-margin-hr">
										<label class="col-md-6 control-label">Is this request
											URGENT?:</label>
										<div class="col-md-3">
											<input type="checkbox" name="isUrgentRequest"
												data-ng-model="creditRequest.isUrgentRequest"
												class='checkbox' />
										</div>
									</div>
									<p class="alert alert-fail col-md-9 form-group">SLA for this request is
									2-5 business days. The Credit Team will do their best to
									accommodate your urgent need but cannot promise immediate
									processing.</p>
								</div>
								<div class="col-md-6">
									<div class="form-group no-margin-hr required"
										data-ng-show="creditRequest.isUrgentRequest">
										<label class="col-md-3 control-label">Date Needed:</label>
										<div class="col-md-3">
											<div class="input-group date" id="requestNeededByDate">
												<input type="text" name="requestNeededByDate"
													class="form-control textboxmargin" readonly="readonly"
													data-ng-model="creditRequest.requestNeededByDate"
													required="required"> <span
													class="input-group-addon"><span
													class="glyphicon glyphicon-calendar"></span></span>
											</div>

										</div>
									</div>
								</div>
								</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group no-margin-hr required">
										<label class="col-md-6 control-label">Flight Start Date:</label>

										<div class="col-md-3">
											<div class="input-group date" id="termStartDate">
												<input type="text" name="termStartDate"
													class="form-control textboxmargin" readonly="readonly"
													data-ng-model="creditRequest.termStartDate"
													required="required"> <span
													class="input-group-addon"><span
													class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group no-margin-hr required">
										<label class="col-md-3 control-label">Flight End Date:</label>

										<div class="col-md-3"
											data-ng-class="{ 'has-error' : creditRequest.termEndDate < creditRequest.termStartDate  }">
											<div class="input-group date" id="termEndDate">
												<input type="text" name="termEndDate"
													class="form-control textboxmargin" readonly="readonly"
													data-ng-model="creditRequest.termEndDate"
													required="required"> <span
													class="input-group-addon"><span
													class="glyphicon glyphicon-calendar"></span></span>
											</div>
											<p
												data-ng-show="creditRequest.termEndDate < creditRequest.termStartDate"
												class="help-block">Term End Date Should be equal/greater
												than Term Start Date</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Currency for Credit Request:</label>
									<div class="col-md-6">		
										<ui-select ng-model="currencyBO.selected" name="currencyBO"
											class='form-control' style="height:47px" required="required"> 
											<ui-select-match placeholder="Select or search currency">
											 	{{$select.selected.currencyCode}}-{{$select.selected.currencyName}} 	
											 </ui-select-match>
											 <ui-select-choices repeat="currencyBO in creditRequest.currencyBOs | filter: $select.search">
												<div data-ng-bind-html="currencyBO.currencyName + '-' + currencyBO.currencyCode| highlight: $select.search"></div>
											</ui-select-choices> 
										</ui-select>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.requestedCreditLimit.$invalid && !adSalesForm.requestedCreditLimit.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Campaign Amount:</label>
									<div class="col-md-6">
										<input type="text" name="requestedCreditLimit"
											data-ng-model="creditRequest.requestedCreditLimit"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9.]*$/' />
										<p
											data-ng-show="adSalesForm.requestedCreditLimit.$error.pattern"
											class="help-block">Please enter positive number.</p>
									</div>
								</div>
							</div>							
							<!--  
							<div class="row form-group">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Is this request for Licensing?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isLicensingRequest"
											data-ng-model="creditRequest.isLicensingRequest"
											class='checkbox'/>
									</div>
								</div>
							</div>
							-->
							<div class="row form-group no-margin-hr">
								<div>
									<label class="col-md-3 control-label">Is this Cash-in Advance?:</label>
									<div class="col-md-1">
										<input type="checkbox" name="isCashInAdvance"
											data-ng-model="creditRequest.isCashInAdvance"
											class='checkbox'/>
									</div>
								</div>
								<div>
									<label class="col-md-2 control-label">Is this a political buy?:</label>
									<div class="col-md-1">
										<input type="checkbox" name="isPoliticalBuy"
											data-ng-model="creditRequest.isPoliticalBuy"
											class='checkbox'/>
									</div>
								</div>
								<div>
									<label class="col-md-2 control-label">Is this digital buy?:</label>
									<div class="col-md-1">
										<input type="checkbox" name="isDirectBuy"
											data-ng-model="creditRequest.isDirectBuy"
											class='checkbox'/>
									</div>
								</div>
							</div>							
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Is this a Direct Buy?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isThereAgency"
											data-ng-model="creditRequest.isThereAgency"
											class='checkbox'/>
									</div>
								</div>
							</div>
							
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.businessConducted.$invalid && !adSalesForm.businessConducted.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Description of business to be conducted plus any Additional Comments:</label>
									<div class="col-md-6">
										<input type="text" name="businessConducted"
											data-ng-model="creditRequest.businessConducted"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p
											data-ng-show="adSalesForm.businessConducted.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Is Ad/Agency profile Set Up Required?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isAdProfileReq"
											data-ng-model="creditRequest.isAdProfileReq"
											class='checkbox'/>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Is Ad/Agency profile Update needed?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isAdProfileUpdateNeeded"
											data-ng-model="creditRequest.isAdProfileUpdateNeeded"
											class='checkbox'/>
									</div>
								</div>
							</div>
							<div data-ng-show="creditRequest.isAdProfileReq || creditRequest.isAdProfileUpdateNeeded">
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Are Multiple Station Set Ups Required?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isMultipleStationSetupReq"
											data-ng-model="creditRequest.isMultipleStationSetupReq"
											class='checkbox'/>
									</div>
								</div>
							</div>
							
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Is this CAPNET?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isCapNet"
											data-ng-model="creditRequest.isCapNet"
											class='checkbox'/>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.capNetAddress.$invalid && !adSalesForm.capNetAddress.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">CaPNet Address:</label>
									<div class="col-md-6">
										<input type="text" name="capNetAddress"
											data-ng-model="creditRequest.capNetAddress"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.capNetAddress.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.productcode.$invalid && !adSalesForm.productcode.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Product Code:</label>
									<div class="col-md-6">
										<input type="text" name="productcode"
											data-ng-model="creditRequest.productcode"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.productcode.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Is there a digital buy?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isDigitalBuy"
											data-ng-model="creditRequest.isDigitalBuy"
											class='checkbox'/>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.revenueCode1.$invalid && !adSalesForm.revenueCode1.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Revenue Code 1:</label>
									<div class="col-md-6">
										<input type="text" name="revenueCode1"
											data-ng-model="creditRequest.revenueCode1"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.revenueCode1.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.revenueCode2.$invalid && !adSalesForm.revenueCode2.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Revenue Code 2:</label>
									<div class="col-md-6">
										<input type="text" name="revenueCode2"
											data-ng-model="creditRequest.revenueCode2"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.revenueCode2.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.revenueCode3.$invalid && !adSalesForm.revenueCode3.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Revenue Code 3:</label>
									<div class="col-md-6">
										<input type="text" name="revenueCode3"
											data-ng-model="creditRequest.revenueCode3"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.revenueCode3.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.priorityCode.$invalid && !adSalesForm.priorityCode.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Priority Code:</label>
									<div class="col-md-6">
										<input type="text" name="priorityCode"
											data-ng-model="creditRequest.priorityCode"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.priorityCode.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.demographic.$invalid && !adSalesForm.demographic.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Demographic:</label>
									<div class="col-md-6">
										<input type="text" name="demographic"
											data-ng-model="creditRequest.demographic"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.demographic.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.dealType.$invalid && !adSalesForm.dealType.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Deal/Order Type - Business type (in advertising world):</label>
									<div class="col-md-6">
										<input type="text" name="dealType"
											data-ng-model="creditRequest.dealType"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.dealType.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.tradeBillIndicator.$invalid && !adSalesForm.tradeBillIndicator.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Trade Bill Pay Indicator:</label>
									<div class="col-md-6">
										<input type="text" name="tradeBillIndicator"
											data-ng-model="creditRequest.tradeBillIndicator"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p
											data-ng-show="adSalesForm.tradeBillIndicator.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							</div>
							<a data-toggle="collapse" data-target="#requestorDetails"
								data-parent="#campaignInfo" href="#requestorDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#campaignInfo').collapse('hide');">Previous </a>

							<a data-toggle="collapse" data-target="#companyDetails"
								data-parent="#campaignInfo" href="#companyDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#campaignInfo').collapse('hide');">Next
							</a>
						</div>
					</div>
				</div>
				<div class="panel panel-default" id="companyDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#companyDetails"
								href="#companyDetails" class="collapsed"> Advertiser Information </a>
						</h4>
					</div>
					<div id="companyDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.companyName.$invalid && !adSalesForm.companyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Advertiser Legal Name:</label>
									<div class="col-md-6">
										<input type="text" name="companyName"
											data-ng-model="creditRequest.companyName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="adSalesForm.companyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<!-- TODO Mapping new field -->
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.companyName.$invalid && !adSalesForm.companyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Doing business as:</label>
									<div class="col-md-6">
										<input type="text" name="companyName"
											data-ng-model="creditRequest.doingBusinessAs"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="adSalesForm.companyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<!-- TODO Mapping new field -->
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.companyName.$invalid && !adSalesForm.companyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Advertiser Brand/Trade/Product?:</label>
									<div class="col-md-6">
										<input type="text" name="companyName"
											data-ng-model="creditRequest.advtBrandName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="adSalesForm.companyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Advertiser HQ Street Address:</label>
									<div class="col-md-6">
										<input type="text" name="companyStreetAddress"
											data-ng-model="creditRequest.companyStreetAddress"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.companyCity.$invalid && !adSalesForm.companyCity.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Advertiser HQ City:</label>
									<div class="col-md-6">
										<input type="text" name="companyCity"
											data-ng-model="creditRequest.companyCity"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="adSalesForm.companyCity.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div id="usaStateId" class="row form-group" data-ng-show="isUSA">
								<div data-ng-init="getUsaStateList()"
									class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser HQ State:</label>
									<div class="col-md-6">
										<ui-select ng-model="companyState.selected" name="companyState"
											class='form-control' style="height:47px"> 
											<ui-select-match placeholder="Select or search state">
												{{$select.selected.stateName}} </ui-select-match>
											 <ui-select-choices repeat="stateBO in creditRequest.stateBOs | filter: $select.search">
												<div data-ng-bind-html="stateBO.stateName | highlight: $select.search"></div>
											</ui-select-choices> 
										</ui-select>
										
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.companyZip.$invalid && !adSalesForm.companyZip.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Advertiser HQ Zip:</label>
									<div class="col-md-6">
										<input type="text" name="companyZip"
											data-ng-model="creditRequest.companyZip"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesForm.companyZip.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div data-ng-init="getCountryList()"
									class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Advertiser HQ Country:</label>
									<div class="col-md-6">
										<ui-select ng-model="companyCountry.selected" name="companyCountry"
											data-ng-change="isUSASelected(companyCountry.selected)"
											class='form-control' style="height:47px" required="required"> 
											<ui-select-match placeholder="Select or search country">
												{{$select.selected.countryName}} </ui-select-match>
											 <ui-select-choices repeat="countryBO in creditRequest.countryBOs | filter: $select.search">
												<div data-ng-bind-html="countryBO.countryName | highlight: $select.search"></div>
											</ui-select-choices> 
										</ui-select>
										
									</div>
								</div>
							</div>	
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cContactPhone.$invalid && !adSalesForm.cContactPhone.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser Telephone:</label>
									<div class="col-md-6">
										<input type="text" name="cContactPhone"
											data-ng-model="creditRequest.cContactPhone"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9-()]*$/' />
										<p
											data-ng-show="adSalesForm.cContactPhone.$error.pattern"
											class="help-block">Please enter valid phone number.</p>
									</div>
								</div>
							</div>		
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cWebAddress.$invalid && !adSalesForm.cWebAddress.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser Website:</label>
									<div class="col-md-6">
										<input type="text" name="cWebAddress"
											data-ng-model="creditRequest.cWebAddress"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="adSalesForm.cWebAddress.$invalid && !adSalesForm.cWebAddress.$pristine"
											class="help-block">Please enter valid web address.</p>
									</div>
								</div>
							</div>	
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cContactFirstName.$invalid && !adSalesForm.cContactFirstName.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser Contact First Name:</label>
									<div class="col-md-6">
										<input type="text" name="cContactFirstName"
											data-ng-model="creditRequest.cContactFirstName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.cContactFirstName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cContactLastName.$invalid && !adSalesForm.cContactLastName.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser Contact Last Name:</label>
									<div class="col-md-6">
										<input type="text" name="cContactLastName"
											data-ng-model="creditRequest.cContactLastName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.cContactLastName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cContactEmail.$invalid && !adSalesForm.cContactEmail.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser Contact Email:</label>
									<div class="col-md-6">
										<input type="email" name="cContactEmail"
											data-ng-model="creditRequest.cContactEmail"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="adSalesForm.cContactEmail.$invalid && !adSalesForm.cContactEmail.$pristine"
											class="help-block">Please enter valid email.</p>
									</div>
								</div>
							</div>
							<!-- newly added field -->	
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cContactPhone.$invalid && !adSalesForm.cContactPhone.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser Contact Telephone:</label>
									<div class="col-md-6">
										<input type="text" name="cContactPhone"
											data-ng-model="creditRequest.advtContactPhone"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9-()]*$/' />
										<p
											data-ng-show="adSalesForm.cContactPhone.$error.pattern"
											class="help-block">Please enter valid phone number.</p>
									</div>
								</div>
							</div>	
							<!-- TODO New mapping field -->	
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cContactLastName.$invalid && !adSalesForm.cContactLastName.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Advertiser Front of House ID:</label>
									<div class="col-md-6">
										<input type="text" name="cContactLastName"
											data-ng-model="creditRequest.advtFrontHouseID"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.cContactLastName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<!--  
							<div id="stateId" class="row form-group"
								data-ng-show="showdefaultState" class="ng-hide">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Company State:</label>
									<div class="col-md-6">
										<input type="text" name="companyState1"
											data-ng-model="creditRequest.companyState1"
											class='form-control textboxmargin' />
										<p data-ng-show="adSalesForm.companyState1.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							-->
							
							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Is the Advertiser a Franchisee?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isFranchisee"
											data-ng-model="creditRequest.isFranchisee"
											class='checkbox'/>
									</div>
								</div>
							</div>										
							
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cfohIdAdv.$invalid && !adSalesForm.cfohIdAdv.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">FOH ID # (Advertiser):</label>
									<div class="col-md-6">
										<input type="text" name="cfohIdAdv"
											data-ng-model="creditRequest.cfohIdAdv"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesForm.cfohIdAdv.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							
							<a data-toggle="collapse" data-target="#campaignInfo"
								data-parent="#companyDetails" href="#campaignInfo"
								class="btn btn-primary pull-left continue"
								onclick="$('#companyDetails').collapse('hide');">Previous </a> <a
								data-toggle="collapse" data-target="#agencyDetails"
								data-parent="#companyDetails" href="#agencyDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#companyDetails').collapse('hide');">Next
							</a>
						</div>
					</div>
				</div>
				<div class="panel panel-default" id="agencyDetailsPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#agencyDetails"
								href="#agencyDetails" class="collapsed">
								Agency Information </a>
						</h4>
					</div>
					<div id="agencyDetails" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.agencyName.$invalid && !adSalesForm.agencyName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency Legal Name:</label>
									<div class="col-md-6">
										<input type="text" name="agencyName"
											data-ng-model="creditRequest.agencyName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-z\s]+$/i' />
										<p data-ng-show="adSalesForm.agencyName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency HQ Street Address:</label>
									<div class="col-md-6">
										<input type="text" name="agencyStreetAddress"
											data-ng-model="creditRequest.agencyStreetAddress"
											class='form-control textboxmargin' required="required" />
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.agencyCity.$invalid && !adSalesForm.agencyCity.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency HQ City:</label>
									<div class="col-md-6">
										<input type="text" name="agencyCity"
											data-ng-model="creditRequest.agencyCity"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesForm.agencyCity.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div id="usaStateAgencyId" class="row form-group"
								data-ng-show="isUSAForAgency">
								<div data-ng-init="getUsaStateListForAgency()"
									class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency HQ State:</label>
									<div class="col-md-6">
										<select id="agencyState" name="agencyState"
											data-ng-model="creditRequest.agencyState"
											class='form-control' required="required">
											<option value="">-- Select State --</option>
											<option data-ng-repeat="stateBO in agencyStateList"
												value="{{stateBO.stateName}}">{{stateBO.stateName}}</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row form-group" data-ng-show="defaultAgencyState"
								class="ng-hide"
								data-ng-class="{ 'has-error' : adSalesForm.agencyState.$invalid && !adSalesForm.agencyState.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency HQ State:</label>
									<div class="col-md-6">
										<input type="text" name="agencyState"
											data-ng-model="creditRequest.agencyState"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p data-ng-show="adSalesForm.agencyState.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.agencyZip.$invalid && !adSalesForm.agencyZip.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency HQ Zipcode:</label>
									<div class="col-md-6">
										<input type="text" name="agencyZip"
											data-ng-model="creditRequest.agencyZip"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesForm.agencyZip.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div data-ng-init="getCountryList()"
									class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Agency HQ Country:</label>
									<div class="col-md-6">
										<select id="agencyCountry" name="agencyCountry"
											data-ng-model="creditRequest.agencyCountry"
											data-ng-change="isUSASelectedForAgency(creditRequest.agencyCountry)"
											class='form-control' required="required">
											<option value="">-- Select Country --</option>
											<option data-ng-repeat="countryBO in countryList"
												value="{{countryBO.countryName}}">{{countryBO.countryName}}</option>
										</select>
									</div>
								</div>
							</div>							
							<!-- 
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.agencyPhone.$invalid && !adSalesForm.agencyPhone.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Telephone:</label>
									<div class="col-md-6">
										<input type="text" name="agencyPhone"
											data-ng-model="creditRequest.agencyPhone"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9-()]*$/' />
										<p
											data-ng-show="adSalesForm.agencyPhone.$error.pattern"
											class="help-block">Please enter valid phone number.</p>
									</div>
								</div>
							</div>
							 -->
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aContactFirstName.$invalid && !adSalesForm.aContactFirstName.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Contact First Name:</label>
									<div class="col-md-6">
										<input type="text" name="aContactFirstName"
											data-ng-model="creditRequest.aContactFirstName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.aContactFirstName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aContactLastName.$invalid && !adSalesForm.aContactLastName.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Contact Last Name:</label>
									<div class="col-md-6">
										<input type="text" name="aContactLastName"
											data-ng-model="creditRequest.aContactLastName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.aContactLastName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aContactEmail.$invalid && !adSalesForm.aContactEmail.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Contact Email:</label>
									<div class="col-md-6">
										<input type="email" name="aContactEmail"
											data-ng-model="creditRequest.aContactEmail"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="adSalesForm.aContactEmail.$invalid && !adSalesForm.aContactEmail.$pristine"
											class="help-block">Please enter valid email.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.agencyPhone.$invalid && !adSalesForm.agencyPhone.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Contact Telephone:</label>
									<div class="col-md-6">
										<input type="text" name="agencyPhone"
											data-ng-model="creditRequest.agencyPhone"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9-()]*$/' />
										<p
											data-ng-show="adSalesForm.agencyPhone.$error.pattern"
											class="help-block">Please enter valid phone number.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aWebAddress.$invalid && !adSalesForm.aWebAddress.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Website Address:</label>
									<div class="col-md-6">
										<input type="text" name="aWebAddress"
											data-ng-model="creditRequest.aWebAddress"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="adSalesForm.aWebAddress.$invalid && !adSalesForm.aWebAddress.$pristine"
											class="help-block">Please enter valid web address.</p>
									</div>
								</div>
							</div>
							<!-- TODO New mapping field -->	
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.cContactLastName.$invalid && !adSalesForm.cContactLastName.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Front of House ID:</label>
									<div class="col-md-6">
										<input type="text" name="cContactLastName"
											data-ng-model="creditRequest.advtFrontHouseID"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.cContactLastName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.afohIdAdv.$invalid && !adSalesForm.afohIdAdv.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">FOH ID # (Agency):</label>
									<div class="col-md-6">
										<input type="text" name="afohIdAdv"
											data-ng-model="creditRequest.afohIdAdv"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesForm.afohIdAdv.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>
							<div data-ng-show="creditRequest.isAdProfileReq || creditRequest.isAdProfileUpdateNeeded">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.aBillingExportNum.$invalid && !adSalesForm.aBillingExportNum.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Billing Export # for Electronic Invoicing:</label>
									<div class="col-md-6">
										<input type="text" name="aBillingExportNum"
											data-ng-model="creditRequest.aBillingExportNum"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z0-9]*$/' />
										<p data-ng-show="adSalesForm.afohIdAdv.$error.pattern"
											class="help-block">Please enter alphanumeric only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.agencyCommissionRate.$invalid && !adSalesForm.agencyCommissionRate.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Commission Rate (in %):</label>
									<div class="col-md-6">
										<input type="text" name="agencyCommissionRate"
											data-ng-model="creditRequest.agencyCommissionRate"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9]*$/' />
										<p
											data-ng-show="adSalesForm.agencyCommissionRate.$error.pattern"
											class="help-block">Please enter numbers only.</p>
									</div>
								</div>
							</div>

							<div class="row form-group">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Agency Commission Rate Approved by Business Manager?:</label>
									<div class="col-md-6">
										<input type="checkbox" name="isCommissionRateApproved"
											data-ng-model="creditRequest.isCommissionRateApproved"
											class='checkbox' required="required" />
									</div>
								</div>
							</div>
							</div>
						<a data-toggle="collapse" data-target="#agencyDetails"
								data-parent="#agencyDetails" href="#agencyDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#agencyDetails').collapse('hide');">Previous </a> <a
								data-toggle="collapse" data-target="#cCollectionDetails"
								data-parent="#agencyDetails" href="#cCollectionDetails"
								class="btn btn-primary pull-right continue"
								onclick="$('#agencyDetails').collapse('hide');">Next
							</a>
						</div>
					</div>
				</div>
				<div class="panel panel-default" id="alternateContactPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#alternateContact" href="#alternateContact"
								class="collapsed"">
								Alternate Contact </a>
						</h4>
					</div>
					<div id="alternateContact" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.alternateCName.$invalid && !adSalesForm.alternateCName.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Billing Contact Name - (if different for Collection Purposes):</label>
									<div class="col-md-6">
										<input type="text" name="alternateCName"
											data-ng-model="creditRequest.alternateCName"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[a-zA-Z]*$/' />
										<p
											data-ng-show="adSalesForm.alternateCName.$error.pattern"
											class="help-block">Please enter alphabets only.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.alternateCPhone.$invalid && !adSalesForm.alternateCPhone.$pristine }">
								<div class="form-group no-margin-hr">
									<label class="col-md-3 control-label">Company Telephone:</label>
									<div class="col-md-6">
										<input type="text" name="alternateCPhone"
											data-ng-model="creditRequest.alternateCPhone"
											class='form-control textboxmargin' required="required"
											data-ng-pattern='/^[0-9-()]*$/' />
										<p
											data-ng-show="adSalesForm.alternateCPhone.$error.pattern"
											class="help-block">Please enter valid phone number.</p>
									</div>
								</div>
							</div>
							<div class="row form-group"
								data-ng-class="{ 'has-error' : adSalesForm.alternateCEmail.$invalid && !adSalesForm.alternateCEmail.$pristine }">
								<div class="form-group no-margin-hr required">
									<label class="col-md-3 control-label">Billing Contact Email - (if different for Collection Purposes):</label>
									<div class="col-md-6">
										<input type="email" name="alternateCEmail"
											data-ng-model="creditRequest.alternateCEmail"
											class='form-control textboxmargin' required="required" />
										<p
											data-ng-show="adSalesForm.alternateCEmail.$invalid && !adSalesForm.alternateCEmail.$pristine"
											class="help-block">Please enter valid email.</p>
									</div>
								</div>
							</div>
							<a data-toggle="collapse" data-target="#companyDetails"
								data-parent="#agencyDetails" href="#companyDetails"
								class="btn btn-primary pull-left continue"
								onclick="$('#agencyDetails').collapse('hide');">Previous </a> 
							<a
								data-toggle="collapse" data-target="#attachment"
								data-parent="#agencyDetails" href="#attachment"
								class="btn btn-primary pull-right continue"
								onclick="$('#agencyDetails').collapse('hide');">Next
							</a>
						</div>

					</div>

				</div>	
				<div class="panel panel-default" id="attachmentPanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-target="#attachment"
								href="#attachment" class="collapsed"> Attachment </a>
						</h4>
					</div>
					<div id="attachment" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="row form-group">
								<label class="col-md-3 control-label">Would you like to attach any documents to this request?:</label>
								<div class="col-md-6">
									<input type="file" name="attachment"
										data-ng-model="creditRequest.attachment"
										class='form-control textboxmargin' />
								</div>
							</div>
							<a data-toggle="collapse" data-target="#attachment"
								data-parent="#foh" href="#attachment"
								class="btn btn-primary pull-left continue"
								onclick="$('#foh').collapse('hide');">Previous </a>

						</div>
					</div>
				</div>

				
			</div>
		</div>
		<div class="panel-footer text-right">
			<button class="btn btn-primary" 
			data-ng-click="saveUserRequesterDetails(creditUserRequest);" data-parent="#requestorDetails">Submit</button>
<!-- 			<a data-toggle="collapse" data-target="#campaignInfo" -->
<!-- 								data-parent="#requestorDetails" href="#campaignInfo" -->
<!-- 								class="btn btn-primary pull-right continue" -->
<!-- 								onclick="$('#requestorDetails').collapse('hide');" -->
<!-- 								data-ng-click="saveRequesterDetails(creditRequest.requestorName,creditRequest.requestorEmail,creditRequest.requestNeededByDate,creditRequest.aeLastName);" -->
<!-- 								data-ng-disabled="adSalesForm.requestorName.$invalid || adSalesForm.requestorEmail.$invalid -->
<!-- 									 || adSalesForm.requestNeededByDate.$invalid">Next -->
<!-- 							</a> -->
		</div>
	</form:form>
</body>

</html>