<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html data-ng-app="salesCreditApp">
<head lang="en">
<meta charset="utf-8">
<title>Credit Request</title>
<link rel="stylesheet" type="text/css"
	href="./resources/css/salesapp.css"></link>
<script type="text/javascript" src="./resources/js/salesApp.js"></script>
</head>

<body data-ng-controller="CreditRequestController">
	<form:form method="post" action="creditRequestSubmission"
		id="creditRequestForm" class="panel form-horizontal"
		modelAttribute="creditRequest">

		<div id="know" class="panel-heading">Know your Customer</div>
		<div class="panel-body"
			data-ng-init="getCreditRequestDetailsFromServer()">
			<div class="row">
				<div class="border col-md-3">
					<div data-ng-init="getSegmentDataFromServer()"
						class="form-group no-margin-hr required">
						<div class="numberCircle">1</div>
						<label class="control-label" id="segLabel"
							title=" TWDC is currently divided into 6 segments - STUDIO ENTERTAINMENT, MEDIA NETWORKS, CONSUMER PRODUCTS, PARKS AND RESORTS, INTERACTIVE, CORPORATE"
							data-toogle="tooltip" data-placement="bottom">Segment</label> 
							<ui-select ng-model="segment.selected">
								<ui-select-match placeholder="Select or search a segment in the list...">{{$select.selected.segmentName}}</ui-select-match>
								<ui-select-choices repeat="segmentBO in segmentBODatas | filter: $select.search">
								  <div ng-bind-html="segmentBO.segmentName | highlight: $select.search"></div>              
								</ui-select-choices>
							 </ui-select>
							<!-- <select
							id="segment" data-ng-model="itemSegment" 
							data-ng-change="selectedSegment(itemSegment)"
							class='form-control'>
							<option value="0">-- Select Segment Group --</option>
							<option data-ng-repeat="segmentBO in segmentBODatas"
								value="{{segmentBO.segmentId}}">{{segmentBO.segmentName}}</option>
						</select> -->
					</div>
				</div>

				<div class="border col-md-3">
					<div class="form-group no-margin-hr required">
						<div class="numberCircle">2</div>
						<label class="control-label" id="regLabel"
							title="For which region will this customer follow the credit rules and policies?"
							data-toogle="tooltip" data-placement="bottom">Scoring
							Region</label> <select id="region" class='form-control'
							data-ng-model="itemRegion"
							data-ng-change="selectedRegion(itemSegment, itemRegion)">
							<option value="0">-- Select Region --</option>
							<option data-ng-repeat="regionBO in regionBODatas"
								value="{{regionBO.regionId}}">{{regionBO.regionName}}</option>
						</select>
					</div>
				</div>

				<div class="border col-md-3">
					<div class="form-group no-margin-hr required">
						<div class="numberCircle">3</div>
						<label class="control-label" id="comLabel"
							title="The highest level of organization information; a four digit number, usually representing a separate legal entity in SAP.  Generally, numbering formats for company codes by geographic location are as follows:  1xxx - US Entity, 2xxx - Europe, 3xxx - Asia, 4xxx - Latin America, 9xxx - Non-consolidated entities"
							data-toogle="tooltip" data-placement="bottom">Company
							Code</label> <select id="company" data-ng-model="itemCode"
							data-ng-change="selectedCompany(itemSegment, itemRegion, itemCode)"
							class='form-control'>
							<option value="0">-- Select Company Code --</option>
							<option data-ng-repeat="companyBO in companyBODatas"
								value="{{companyBO.companyId}}">{{companyBO.companyCode}}
								{{companyBO.companyName}}</option>
						</select>
					</div>
				</div>
				<div class="border col-md-3">
					<div class="form-group no-margin-hr required">
						<div class="numberCircle">4</div>
						<label class="control-label" id="baLabel"
							title="Segmentation of the TWDC businesses.  The lowest level at which a full balance sheet and P&L can be prepared within Disney."
							data-toogle="tooltip" data-placement="bottom">Business
							Area</label> <select id="businessArea" class='form-control'
							data-ng-model="itemBusinessArea"
							data-ng-change="getBusinessGroup(itemCode, itemBusinessArea)">
							<option value="0">-- Select Business Area --</option>
							<option data-ng-repeat="businessAreaBO in businessAreaBODatas"
								value="{{businessAreaBO.businessAreaId}}">{{businessAreaBO.businessAreaCode}}
								{{businessAreaBO.businessAreaName}}</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</form:form>
	<div data-ng-if="businessGroupIdData== 1 "
		data-ng-include="'adSalesNoSetup.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 2 "
		data-ng-include="'adSalesSetup.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 3 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 4 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 5 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 6 "
		data-ng-include="'emeaAdSales.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 7 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
	<div data-ng-if="businessGroupIdData== 0 "
		data-ng-include="'defaultCreditInfo.jsp'"></div>
</body>

</html>