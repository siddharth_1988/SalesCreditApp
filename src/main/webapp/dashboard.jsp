
	<div id="content-wrapper">		
		<div class="page-header">
			<div class="row">
				<!-- Page header, center on small screens -->
				<h1 class="col-xs-12 col-sm-4 text-center text-left-sm"><i class="fa fa-dashboard page-header-icon"></i> Dashboard</h1>
			</div>
		</div> <!-- / .page-header -->


		<div class="row">
			<div class="col-md-8">

				<div class="stat-panel">
					<div class="stat-row">
						<!-- Small horizontal padding, bordered, without right border, top aligned text -->
						<div class="stat-cell col-sm-4 padding-sm-hr bordered no-border-r valign-top" data-ng-controller="DashboardController" data-ng-init="getDashboardDetails()">
							<!-- Small padding, without top padding, extra small horizontal padding -->
							<h4 class="padding-sm no-padding-t padding-xs-hr"><i class="fa fa-cloud-upload text-primary"></i>&nbsp;&nbsp;Credit Requests</h4>
							<div>	
								<table ng-show="(dashboardBOs | filter:criteria).length" class="table table-bordered table-primary">
									<thead>
									  <tr>
										<th>#</th>
										<th>Request#</th>
										<th>Status</th>
										<th>Raised Date</th>
										<th>SLA</th>
									  </tr>
									  </thead>
									  <tr ng-repeat="dashboard in dashboardBOs">
										<td>{{ $index + 1 }}</td>
										<td>{{ dashboard.creditRequestNumber }}</td>
										<td>{{ dashboard.creditRequestStatus }}</td>
										<td>{{ dashboard.requestRaisedDate | date:'MM/dd/yyyy'}}</td>
										<td>{{ dashboard.slaDate | date:'MM/dd/yyyy'}}</td>
									  </tr>
								</table>
							</div>
						<div ng-show="(!dashboardBOs.length)">
							You have not yet submitted any credit request. <br> You can submit a credit request using Credit Submission option. 
						</div>
						</div> <!-- /.stat-cell -->
						
					</div>
				</div> <!-- /.stat-panel -->
			</div>
		</div>

	</div> <!-- / #content-wrapper -->