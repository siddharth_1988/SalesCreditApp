'use strict';
var app = angular.module('adminApp', [ 'ngSanitize', 'ui.select' ]);
app
		.controller(
				'AdminController',
				function($scope, $http) {
					$scope.baccMap = {};
					$scope.baccMap.selected = {};

					$(document).ready(function() {
						$("#addBaccMapModal").modal('hide');
						//$("#baccMapDetails").hide();
						$("#addStatus").hide();
						$("#editAddStatus").hide();
						$("#editBaccMapGroupModal").modal('hide');
						$("#successStatus").hide();
						$("#editSuccessStatus").hide();
						$("#segmentStatusId").hide();
						$("#regionStatusId").hide();
						$("#companyStatusId").hide();
						$("#businessAreaStatusId").hide();
						$("#editSegmentStatusId").hide();
						$("#editRegionStatusId").hide();
						$("#editCompanyStatusId").hide();
						$("#editBusinessAreaStatusId").hide();
					});

					$scope.getBaccMapGroupData = function() {
						$http({
							method : 'GET',
							url : 'getBaccMapData'
						}).success(function(data, status, headers, config) {
							$scope.baccMapBOData = data;
							$scope.showCompanyTable = true;
							$("#companyDetails").show();
							$scope.sortType = 'segmentBO.segmentName';
							$scope.sortReverse = false;
						}).error(function(data, status, headers, config) {
						});
					}

					$scope.validateIsExistsFields = function(newSegment,
							newRegion, newCompanyName, newBusinessAreaName) {
						if (newSegment != '' && newSegment != undefined) {
							$scope.segmentExists = '';
							$http({
								method : 'POST',
								url : 'isSegmentExists/' + newSegment
							})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.segmentExists = 'The Entered SegmentName already Exists. Please enter a different Name.';
													$("#segmentStatusId")
															.show();
												}
											}).error(
											function(data, status, headers,
													config) {
											});

						} else if (newRegion != '' && newRegion != undefined) {
							$scope.regionExists = '';
							$http({
								method : 'POST',
								url : 'isRegionExists/' + newRegion
							})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.regionExists = 'The Entered RegionName already Exists. Please enter a different Name.';
													$("#regionStatusId").show();
													return;
												}
											}).error(
											function(data, status, headers,
													config) {
											});
						} else if (newCompanyName != ''
								&& newCompanyName != undefined) {
							$scope.companyExists = '';
							$http({
								method : 'POST',
								url : 'isCompanyExists/' + newCompanyName
							})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.companyExists = 'The Entered CompanyName already Exists. Please enter a different Name.';
													$("#companyStatusId")
															.show();
													return;
												}
											}).error(
											function(data, status, headers,
													config) {
											});
						} else if (newBusinessAreaName != ''
								&& newBusinessAreaName != undefined) {
							$scope.businessAreaExists = '';
							$http(
									{
										method : 'POST',
										url : 'isBusinessAreaExists/'
												+ newBusinessAreaName
									})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.businessAreaExists = 'The Entered BusinessArea Name already Exists. Please enter a different Name.';
													$("#businessAreaStatusId")
															.show();
													return;
												}
											}).error(
											function(data, status, headers,
													config) {
											});
						}

					}

					$scope.checkEmptyValues = function(newSegment, newRegion,
							newCompany, newBusinessArea, selectedBaccMap) {
						if (selectedBaccMap.segmentBO == null) {
							$scope.successTextAlert = "Please select a Segment";
							return $scope.successTextAlert;
						}
						if (selectedBaccMap.regionBO == null) {
							$scope.successTextAlert = "Please select a Region";
							return $scope.successTextAlert;
						}
						if (selectedBaccMap.companyBO == null) {
							$scope.successTextAlert = "Please select a Company";
							return $scope.successTextAlert;
						}

						if (selectedBaccMap.businessAreaBO == null) {
							$scope.successTextAlert = "Please select a BusinessArea";
							return $scope.successTextAlert;
						}

						if ($("#newSegment").is(':visible')) {
							if (newSegment == '') {
								$scope.successTextAlert = "New Segment should not be empty!";
								return $scope.successTextAlert;
							}
						}
						if ($("#newRegion").is(':visible')) {
							if (newRegion == '') {
								$scope.successTextAlert = "New Region should not be empty!";
								return $scope.successTextAlert;
							}
						}
						if ($("#newCompany").is(':visible')) {
							if (newCompany == '') {
								$scope.successTextAlert = "New Company should not be empty!";
								return $scope.successTextAlert;
							} else if (newCompany.indexOf("-") == -1) {
								$scope.successTextAlert = "Please enter CompanyCode and Name separated with a '-'";
								return $scope.successTextAlert;
							}
						}
						if ($("#newBusinessArea").is(':visible')) {
							if (newBusinessArea == '') {
								$scope.successTextAlert = "New BusinessArea should not be empty!";
								return $scope.successTextAlert;
							} else if (newBusinessArea.indexOf("-") == -1) {
								$scope.successTextAlert = "Please enter BusinessAreaCode and Name separated with a '-'";
								return $scope.successTextAlert;
							}
						}
						return $scope.successTextAlert;
					}

					$scope.addBaccMapGroupData = function(selectedBaccMap) {

						var urls = '';
						$scope.newCompanyCode = null;
						$scope.newCompanyName = null;
						$scope.newBusinessAreaCode = null;
						$scope.newBusinessAreaName = null;

						$scope.newSegment = null;
						$scope.newRegion = null;
						$scope.newCompany = null;
						$scope.newBusinessArea = null;

						$scope.successTextAlert = '';
						if (selectedBaccMap.segmentBO != null) {
							if (selectedBaccMap.segmentBO.segmentId == 0) {
								$scope.newSegment = $("#newSegment").val();
							}
						}

						if (selectedBaccMap.regionBO != null) {
							if (selectedBaccMap.regionBO.regionId == 0) {
								$scope.newRegion = $("#newRegion").val();
							}
						}

						if (selectedBaccMap.companyBO != null) {
							if (selectedBaccMap.companyBO.companyId == 0) {
								$scope.newCompany = $("#newCompany").val();
								if ($scope.newCompany != null
										|| $scope.newCompany != '') {
									$scope.newCompanyCode = $scope.newCompany
											.split("-")[0];
									$scope.newCompanyName = $scope.newCompany
											.split("-")[1];
								}

							}
						}

						if (selectedBaccMap.businessAreaBO != null) {
							if (selectedBaccMap.businessAreaBO.businessAreaId == 0) {
								$scope.newBusinessArea = $("#newBusinessArea")
										.val();
								if ($scope.newBusinessArea != null
										|| $scope.newBusinessArea != '') {
									$scope.newBusinessAreaCode = $scope.newBusinessArea
											.split("-")[0];
									$scope.newBusinessAreaName = $scope.newBusinessArea
											.split("-")[1];
								}

							}
						}

						$scope.successTextAlert = $scope.checkEmptyValues(
								$scope.newSegment, $scope.newRegion,
								$scope.newCompany, $scope.newBusinessArea,
								selectedBaccMap);
						$scope.validateIsExistsFields($scope.newSegment,
								$scope.newRegion, $scope.newCompanyName,
								$scope.newBusinessAreaName);

						urls = "addBaccMapBaccMapGroupData/"
								+ $scope.newSegment + "/" + $scope.newRegion
								+ "/" + $scope.newCompanyCode + "/"
								+ $scope.newCompanyName + "/"
								+ $scope.newBusinessAreaCode + "/"
								+ $scope.newBusinessAreaName;
						if ($scope.successTextAlert == '') {
							$scope.editMode = false;
							$scope.callAddUrl(urls, selectedBaccMap,$scope.editMode);
						}
						if ($scope.successTextAlert != '') {
							$scope.showSuccessAlert = true;
							$('#addStatus').show();

						}
					}

					$scope.callAddUrl = function(urls, selectedBaccMap,editMode) {
						$http({
							method : 'POST',
							url : urls,
							data : selectedBaccMap
						}).success(function(data, status, headers, config) {
							$scope.baccMapBOData = data;

							$("#newSegment").val('');
							$("#newRegion").val('');
							$("#newCompany").val('');
							$("#newBusinessArea").val('');
							$("#editNewSegment").val('');
							$("#editNewRegion").val('');
							$("#editNewCompany").val('');
							$("#editNewBusinessArea").val('');
							if(editMode == false){
								$scope.successText = "Added Successfully.";
								$("#successStatus").show();
							}else{
								$scope.successText = "Updated Successfully.";
								$("#editSuccessStatus").show();
							}
							
							$("#newSegment").hide();
							$("#newRegion").hide();
							$("#newCompany").hide();
							$("#newBusinessArea").hide();
							$("#editNewSegment").hide();
							$("#editNewRegion").hide();
							$("#editNewCompany").hide();
							$("#editNewBusinessArea").hide();
						}).error(function(data, status, headers, config) {
							$scope.successTextAlert = "Fail to add this time";
						});
					}

					$scope.segment = {};
					$scope.region = {};
					$scope.company = {};
					$scope.businessArea = {};
					$scope.editBaccMap = {};
					$scope.editBaccMap.selected = {};

					$scope.editBaccMapGroup = function(data) {
						$('#editBaccMapGroupModal').modal('show');
						$("#editNewSegment").hide();
						$("#editNewRegion").hide();
						$("#editNewCompany").hide();
						$("#editNewBusinessArea").hide();
						$("#editAddStatus").hide();
						$("#editSuccessStatus").hide();

						$scope.editBaccMap.selected = data;
						$scope.segment.selected = data.segmentBO;
						$scope.region.selected = data.regionBO;
						$scope.company.selected = data.companyBO;
						$scope.businessArea.selected = data.businessAreaBO;

						$http({
							method : 'GET',
							url : 'getBaccMapDetails'
						}).success(function(data, status, headers, config) {
							$scope.creditRequest = data;
						}).error(function(data, status, headers, config) {

						});

					}

					$scope.updateBaccMapGroup = function(selectedBaccMap) {
						
						var urls = '';
						$scope.newCompanyCode = null;
						$scope.newCompanyName = null;
						$scope.newBusinessAreaCode = null;
						$scope.newBusinessAreaName = null;

						$scope.newSegment = null;
						$scope.newRegion = null;
						$scope.newCompany = null;
						$scope.newBusinessArea = null;

						$scope.successTextAlert = '';
						if (selectedBaccMap.segmentBO != null) {
							if (selectedBaccMap.segmentBO.segmentId == 0) {
								$scope.newSegment = $("#editNewSegment").val();
							}
						}

						if (selectedBaccMap.regionBO != null) {
							if (selectedBaccMap.regionBO.regionId == 0) {
								$scope.newRegion = $("#editNewRegion").val();
							}
						}

						if (selectedBaccMap.companyBO != null) {
							if (selectedBaccMap.companyBO.companyId == 0) {
								$scope.newCompany = $("#editNewCompany").val();
								if ($scope.newCompany != null
										|| $scope.newCompany != '') {
									$scope.newCompanyCode = $scope.newCompany
											.split("-")[0];
									$scope.newCompanyName = $scope.newCompany
											.split("-")[1];
								}

							}
						}

						if (selectedBaccMap.businessAreaBO != null) {
							if (selectedBaccMap.businessAreaBO.businessAreaId == 0) {
								$scope.newBusinessArea = $("#editNewBusinessArea")
										.val();
								if ($scope.newBusinessArea != null
										|| $scope.newBusinessArea != '') {
									$scope.newBusinessAreaCode = $scope.newBusinessArea
											.split("-")[0];
									$scope.newBusinessAreaName = $scope.newBusinessArea
											.split("-")[1];
								}

							}
						}

						$scope.successTextAlert = $scope.checkEmptyValuesInEdit(
								$scope.newSegment, $scope.newRegion,
								$scope.newCompany, $scope.newBusinessArea,
								selectedBaccMap);
						$scope.validateIsExistsFieldsInEdit($scope.newSegment,
								$scope.newRegion, $scope.newCompanyName,
								$scope.newBusinessAreaName);

						urls = "updateBaccMapGroupData/"
								+ $scope.newSegment + "/" + $scope.newRegion
								+ "/" + $scope.newCompanyCode + "/"
								+ $scope.newCompanyName + "/"
								+ $scope.newBusinessAreaCode + "/"
								+ $scope.newBusinessAreaName;
						if ($scope.successTextAlert == '') {
							$scope.editMode = true;
							$scope.callAddUrl(urls, selectedBaccMap,$scope.editMode);
						}
						if ($scope.successTextAlert != '') {
							$scope.showSuccessAlert = true;
							$('#editAddStatus').show();

						}

						/*$http({
							method : 'POST',
							url : 'updateBaccMapGroupData',
							data : baccMap
						}).success(function(data, status, headers, config) {
							$scope.baccMapBOData = data;
							//$('#editBaccMapGroupModal').modal('hide');
						}).error(function(data, status, headers, config) {

						});*/
					}

					$scope.deleteBaccMapGroup = function(baccMap) {
						alert(baccMap.baccMapId);
						if (confirm("Are you Sure to Delete?")) {
							$http({
								method : 'POST',
								url : 'deleteBaccMapGroupData',
								data : baccMap
							}).success(function(data, status, headers, config) {
								$scope.baccMapBOData = data;
								$('#editCompanyModal').modal('hide');
							}).error(function(data, status, headers, config) {

							});
						}
					}

					$scope.openAddBaccMapModal = function() {
						$("#newSegment").hide();
						$("#newRegion").hide();
						$("#newCompany").hide();
						$("#newBusinessArea").hide();
						$('#addBaccMapModal').modal('show');
						$("#addStatus").hide();
						$("#successStatus").hide();

						$http({
							method : 'GET',
							url : 'getBaccMapDetails'
						}).success(function(data, status, headers, config) {
							$scope.creditRequest = data;

						}).error(function(data, status, headers, config) {

						});
					}

					$scope.onChangeSegment = function(selectedSegment) {
						$("#addStatus").hide();
						$("#successStatus").hide();

						if (selectedSegment.segmentId == 0) {
							$('#newSegment').show();
						} else {
							$('#newSegment').hide();
						}
					}

					$scope.onChangeRegion = function(selectedRegion) {
						$("#addStatus").hide();
						$("#successStatus").hide();

						if (selectedRegion.regionId == 0) {
							$('#newRegion').show();
						} else {
							$('#newRegion').hide();
						}
					}
					$scope.onChangeCompany = function(selectedCompany) {
						$("#addStatus").hide();
						$("#successStatus").hide();

						if (selectedCompany.companyId == 0) {
							$('#newCompany').show();
						} else {
							$('#newCompany').hide();
						}
					}
					$scope.onChangeBusinessArea = function(selectedBusinessArea) {
						$("#addStatus").hide();
						$("#successStatus").hide();

						if (selectedBusinessArea.businessAreaId == 0) {
							$('#newBusinessArea').show();
						} else {
							$('#newBusinessArea').hide();
						}
					}
					
					$scope.checkEmptyValuesInEdit = function(newSegment, newRegion,
							newCompany, newBusinessArea, selectedBaccMap) {
						if (selectedBaccMap.segmentBO == null) {
							$scope.successTextAlert = "Please select a Segment";
							return $scope.successTextAlert;
						}
						if (selectedBaccMap.regionBO == null) {
							$scope.successTextAlert = "Please select a Region";
							return $scope.successTextAlert;
						}
						if (selectedBaccMap.companyBO == null) {
							$scope.successTextAlert = "Please select a Company";
							return $scope.successTextAlert;
						}

						if (selectedBaccMap.businessAreaBO == null) {
							$scope.successTextAlert = "Please select a BusinessArea";
							return $scope.successTextAlert;
						}

						if ($("#editNewSegment").is(':visible')) {
							if (newSegment == '') {
								$scope.successTextAlert = "New Segment should not be empty!";
								return $scope.successTextAlert;
							}
						}
						if ($("#editNewRegion").is(':visible')) {
							if (newRegion == '') {
								$scope.successTextAlert = "New Region should not be empty!";
								return $scope.successTextAlert;
							}
						}
						if ($("#editNewCompany").is(':visible')) {
							if (newCompany == '') {
								$scope.successTextAlert = "New Company should not be empty!";
								return $scope.successTextAlert;
							} else if (newCompany.indexOf("-") == -1) {
								$scope.successTextAlert = "Please enter CompanyCode and Name separated with a '-'";
								return $scope.successTextAlert;
							}
						}
						if ($("#editNewBusinessArea").is(':visible')) {
							if (newBusinessArea == '') {
								$scope.successTextAlert = "New BusinessArea should not be empty!";
								return $scope.successTextAlert;
							} else if (newBusinessArea.indexOf("-") == -1) {
								$scope.successTextAlert = "Please enter BusinessAreaCode and Name separated with a '-'";
								return $scope.successTextAlert;
							}
						}
						return $scope.successTextAlert;
					}
					
					$scope.validateIsExistsFieldsInEdit = function(newSegment,
							newRegion, newCompanyName, newBusinessAreaName) {
						if (newSegment != '' && newSegment != undefined) {
							$scope.segmentExists = '';
							$http({
								method : 'POST',
								url : 'isSegmentExists/' + newSegment
							})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.segmentExists = 'The Entered SegmentName already Exists. Please enter a different Name.';
													$("#editSegmentStatusId")
															.show();
												}
											}).error(
											function(data, status, headers,
													config) {
											});

						} else if (newRegion != '' && newRegion != undefined) {
							$scope.regionExists = '';
							$http({
								method : 'POST',
								url : 'isRegionExists/' + newRegion
							})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.regionExists = 'The Entered RegionName already Exists. Please enter a different Name.';
													$("#editRegionStatusId").show();
													return;
												}
											}).error(
											function(data, status, headers,
													config) {
											});
						} else if (newCompanyName != ''
								&& newCompanyName != undefined) {
							$scope.companyExists = '';
							$http({
								method : 'POST',
								url : 'isCompanyExists/' + newCompanyName
							})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.companyExists = 'The Entered CompanyName already Exists. Please enter a different Name.';
													$("#editCompanyStatusId")
															.show();
													return;
												}
											}).error(
											function(data, status, headers,
													config) {
											});
						} else if (newBusinessAreaName != ''
								&& newBusinessAreaName != undefined) {
							$scope.businessAreaExists = '';
							$http(
									{
										method : 'POST',
										url : 'isBusinessAreaExists/'
												+ newBusinessAreaName
									})
									.success(
											function(datas, status, headers,
													config) {
												if (datas == true) {
													$scope.businessAreaExists = 'The Entered BusinessArea Name already Exists. Please enter a different Name.';
													$("#editBusinessAreaStatusId")
															.show();
													return;
												}
											}).error(
											function(data, status, headers,
													config) {
											});
						}

					}
					
					$scope.onChangeSegmentEdit = function(selectedSegment) {
						$("#editAddStatus").hide();
						$("#editSuccessStatus").hide();

						if (selectedSegment.segmentId == 0) {
							$('#editNewSegment').show();
						} else {
							$('#editNewSegment').hide();
						}
					}

					$scope.onChangeRegionEdit = function(selectedRegion) {
						$("#editAddStatus").hide();
						$("#editSuccessStatus").hide();

						if (selectedRegion.regionId == 0) {
							$('#editNewRegion').show();
						} else {
							$('#editNewRegion').hide();
						}
					}
					$scope.onChangeCompanyEdit = function(selectedCompany) {
						$("#editAddStatus").hide();
						$("#editSuccessStatus").hide();

						if (selectedCompany.companyId == 0) {
							$('#editNewCompany').show();
						} else {
							$('#editNewCompany').hide();
						}
					}
					$scope.onChangeBusinessAreaEdit = function(selectedBusinessArea) {
						$("#editAddStatus").hide();
						$("#editSuccessStatus").hide();

						if (selectedBusinessArea.businessAreaId == 0) {
							$('#editNewBusinessArea').show();
						} else {
							$('#editNewBusinessArea').hide();
						}
					}

				});