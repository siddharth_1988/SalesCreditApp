var userApp = angular.module('userApp',
		[ 'angularUtils.directives.dirPagination' ]);

userApp.controller('userController', function($scope, $http) {
	$scope.sortType = 'userName'; // set the default sort type
	$scope.sortReverse = false; // set the default sort order
	$scope.searchUser = ''; // set the default search/filter term
	$scope.getUsersDataFromServer = function() {
		$http({
			method : 'GET',
			url : 'getAllUsers'
		}).success(function(data, status, headers, config) {
			$scope.userBODatas = data;
		}).error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
			alert("failure message: " + JSON.stringify({
				data : data
			}));
		});
	}

});

var userManagementApp = angular.module('userManagementApp', []);
userManagementApp.controller('userManagementController',
		function($scope, $http) {
			$scope.getCountryList = function() {
				$http({
					method : 'GET',
					url : 'getCountryList'
				}).success(function(data, status, headers, config) {
					$scope.countryList = data;

				}).error(function(data, status, headers, config) {
				});
			}

		});
