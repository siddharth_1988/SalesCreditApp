'use strict';
var app = angular.module('salesCreditApp', ['ngSanitize','ui.select']);
/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

app.controller('CreditRequestController', function($scope, $http,$filter) {
	$scope.itemList = [];
	$scope.segment = {};
	$scope.region = {};
	$scope.company = {};
	$scope.businessArea = {};
	
	$scope.getSegmentDataFromServer = function() {

		$http({
			method : 'GET',
			url : 'getAllSegments'
		}).success(function(data, status, headers, config) {
			$scope.segmentBODatas = data;
			$scope.cobaListBODatas = '';
			$scope.regionBODatas = '';
			$scope.companyBODatas = '';
			$scope.businessAreaBODatas = '';
			$scope.businessGroupIdData = null;

		}).error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		});
	};

	
	$scope.selectedSegment = function(itemSegment) {
		$http({
			method : 'GET',
			url : 'getCOBAListBySegment/' + itemSegment.segmentId
		}).success(function(data, status, headers, config) {
			$scope.cobaListBODatas = data;
			$scope.creditUserRequest.cobaListBOs = data;
			$scope.creditUserRequest.segmentBO = $scope.segment.selected;
			
			$scope.creditUserRequest.companyBOs = '';
			$scope.creditUserRequest.businessAreaBOs = '';
			
			$scope.region.selected = null;
			$scope.company.selected = null;
			$scope.businessArea.selected = null;
			
			$scope.businessGroupIdData = null;
		}).error(function(data, status, headers, config) {
		});
	}
	$scope.selectedCoba = function() {
		$scope.businessGroupIdData = 1;
	}
	/*$scope.selectedSegment = function(itemSegment) {
		
		$http({
			method : 'GET',
			url : 'getRegionBySegment/' + itemSegment.segmentId
		}).success(function(data, status, headers, config) {
			$scope.regionBODatas = data;
			$scope.creditRequest.regionBOs = data;
			$scope.creditRequest.segmentBO = $scope.segment.selected;
			
			$scope.creditRequest.companyBOs = '';
			$scope.creditRequest.businessAreaBOs = '';
			
			$scope.region.selected = null;
			$scope.company.selected = null;
			$scope.businessArea.selected = null;
			
			$scope.businessGroupIdData = null;
		}).error(function(data, status, headers, config) {
		});
	} */

	$scope.selectedRegion = function(itemSegment, itemRegion) {
		
		$http({
			method : 'GET',
			url : 'getCompanyByRegion/' + itemSegment.segmentId + '/' + itemRegion.regionId
		}).success(function(data, status, headers, config) {
			$scope.companyBODatas = data;
			$scope.creditRequest.companyBOs = data;
			$scope.creditRequest.regionBO = $scope.region.selected;
			
			$scope.creditRequest.businessAreaBOs = '';
			$scope.company.selected = null;
			$scope.businessArea.selected = null;
			$scope.businessGroupIdData = null;
			
		}).error(function(data, status, headers, config) {
		});
	}

	$scope.selectedCompany = function(itemSegment, itemRegion, itemCode) {
		
		$http(
				{
					method : 'GET',
					url : 'getBusinessAreaByCompany/' + itemSegment.segmentId + '/'
							+ itemRegion.regionId + '/' + itemCode.companyId
				}).success(function(data, status, headers, config) {
			$scope.businessAreaBODatas = data;
			$scope.creditRequest.businessAreaBOs = data;
			$scope.creditRequest.companyBO = $scope.company.selected;
			
			$scope.businessArea.selected = null;
			$scope.businessGroupIdData = null;
			
		}).error(function(data, status, headers, config) {
		});
	}

	/*
	 * $scope.selectedBusinessArea = function(itemSegment, itemRegion, itemCode,
	 * itemBusinessArea) { $http( { method : 'GET', url : 'getBusinessArea/' +
	 * itemSegment + '/' + itemRegion + '/' + itemCode + '/' + itemBusinessArea
	 * }).success(function(data, status, headers, config) {
	 * 
	 * }).error(function(data, status, headers, config) { }); }
	 */

	$scope.getBusinessGroup = function(itemCode, itemBusinessArea) {
		$http({
			method : 'GET',
			url : 'getBusinessGroup/' + itemCode.companyId + '/' + itemBusinessArea.businessAreaId
		}).success(function(data, status, headers, config) {
			$scope.businessGroupIdData = data.businessGroupId;
			$scope.creditRequest.businessAreaBO = $scope.businessArea.selected;
			$scope.currentDate = new Date();

		}).error(function(data, status, headers, config) {
			$scope.businessGroupIdData = null;
		});
	}

	$scope.getCountryList = function() {
		$http({
			method : 'GET',
			url : 'getCountryList'
		}).success(function(data, status, headers, config) {
			$scope.creditRequest.countryBOs = data;

		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.getCurrencyList = function() {
		$http({
			method : 'GET',
			url : 'getCurrencyList'
		}).success(function(data, status, headers, config) {
			$scope.creditRequest.currencyBOs = data;

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.getUsaStateList = function() {
		$http({
			method : 'GET',
			url : 'getUsaStateList'
		}).success(function(data, status, headers, config) {
			$scope.creditRequest.stateBOs = data;

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.getUsaStateListForAgency = function() {
		$http({
			method : 'GET',
			url : 'getUsaStateList'
		}).success(function(data, status, headers, config) {
			$scope.creditRequest.stateBOs = data;

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.isUSASelected = function(country) {
		$scope.isUSA = false;
		$scope.showdefaultState = true;
		if (country.countryName == 'USA') {
			$scope.isUSA = true;
			$scope.showdefaultState = false;
		}

	}

	$scope.isUSASelectedForAgency = function(country) {
		$scope.isUSAForAgency = false;
		$scope.defaultAgencyState = true;
		if (country.countryName == 'USA') {
			$scope.isUSAForAgency = true;
			$scope.defaultAgencyState = false;
		}

	}

	$scope.saveRequesterDetails = function(requesterName, requesterEmail,
			requesterDate,aeLastName,creditRequest) {
		
//		$scope.creditRequest={};
		
		console.debug('aeLastName...'+aeLastName);
		$http(
				{
					method : 'POST',
					url : 'saveRequesterDetails/' + requesterName + '/'
							+ requesterEmail + '/' + requesterDate +'/' +aeLastName
				}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.saveUserRequesterDetails = function(creditUserRequest) {
		
		$scope.creditUserRequest={};
		$scope.creditUserRequest.requestorName = creditUserRequest.requestorName;
		$scope.creditUserRequest.requestorEmail = creditUserRequest.requestorEmail;
		$scope.creditUserRequest.aeFirstName = creditUserRequest.aeFirstName;
		$scope.creditUserRequest.aeLastName = creditUserRequest.aeLastName;
		$scope.creditUserRequest.aeEmail = creditUserRequest.aeEmail;
		
		window.alert(creditUserRequest.aeLastName);
		$http(
				{
					method : 'POST',
					url : 'saveUserRequesterDetails',
				    data : angular.toJson($scope.creditUserRequest),
				}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	
	

	$scope.saveBusinessDetails = function(requestedCreditLimit, termStartDate,
			termEndDate) {
		$http(
				{
					method : 'POST',
					url : 'saveBusinessDetails/' + requestedCreditLimit + '/'
							+ termStartDate + '/' + termEndDate
				}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}

	$scope.saveCompanyDetails = function(companyName, companyAddress,
			companyCity, companyCountry, companyState, companyState1,
			companyZip) {
		var url = "";
		
		if(companyState == null){
			url = 'saveCompanyDetails/' + companyName + '/'
			+ companyAddress + '/' + companyCity + '/' 
			+ companyCountry.countryId + '/' + companyCountry.countryName + '/' 
			+ companyState1 + '/' + companyZip;
		}
		else{
			url = 'saveCompanyDetailsForUS/' + companyName + '/'
			+ companyAddress + '/' + companyCity + '/' 
			+ companyCountry.countryId + '/' + companyCountry.countryName + '/' 
			+ companyState.stateId + '/' + companyState.stateName + '/'
			+ companyState1 + '/' + companyZip;
		}
		$http(
				{
					method : 'POST',
					url : url
				}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});	
	}

	$scope.saveAgencyDetails = function(agencyName, address, agencyCity,
			agencyCountry, agencyState, agencyState1, agencyZip,
			accountExecutive) {
		var url = "";
		if(agencyState == null){
			url = 'saveAgencyDetails/' + agencyName + '/' + address
			+ '/' + agencyCity + '/' + agencyCountry.countryId + '/'
			+ agencyCountry.countryName + '/' + agencyState1 + '/'
			+ agencyZip + '/' + accountExecutive;
		}
		else{
			url = 'saveAgencyDetailsForUS/' + agencyName + '/' + address
			+ '/' + agencyCity + '/' + agencyCountry.countryId + '/'
			+ agencyCountry.countryName + '/' + agencyState.stateId + '/'
			+ agencyState.stateName + '/' + agencyState1 + '/'
			+ agencyZip + '/' + accountExecutive;
		}
		
		$http(
				{
					method : 'POST',
					url : url
				}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}

	$scope.saveCollectionDetails = function(phone, email, name) {
		$http({
			method : 'POST',
			url : 'saveCollectionDetails/' + phone + '/' + email + '/' + name
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}

	$scope.saveAdvanceCashDetails = function(cashAdvance, direct) {
		$http({
			method : 'POST',
			url : 'saveAdvanceCashDetails/' + cashAdvance + '/' + direct
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}

	$scope.getCreditRequestDetailsFromServer = function() {
		$scope.companyCountry = {};
		$scope.companyState = {};
		$scope.agencyCountry = {};
		$scope.agencyState = {};
		$scope.currencyBO = {};
		$http({
			method : 'GET',
			url : 'getCreditRequestDetails'
		}).success(function(data, status, headers, config) {
			$scope.creditUserRequest = data;
			$scope.isUSA = false;
			$scope.showdefaultState = false;
			$scope.isUSAForAgency = false;
			$scope.defaultAgencyState = false;
			var myDate = new Date();
			var defaultDate = new Date(myDate);
			defaultDate.setDate(myDate.getDate()+5);
			$scope.nextDay = $filter('date')(defaultDate, 'MM-dd-yyyy');
			if (data.aeFirstName == null || data.aeFirstName == '') {
				data.aeFirstName = data.requestorName;
			}
			if (data.aeEmail == null || data.aeEmail == '') {
				data.aeEmail = data.requestorEmail;
			}
			if(data.companyCountry != null){
				$scope.companyCountry.selected = data.companyCountry;
				if (data.companyCountry.countryName == 'USA') {
						$scope.isUSA = true;
						$scope.showdefaultState = false;
				} else {
						$scope.isUSA = false;
						$scope.showdefaultState = true;
				}
			}
			
			if(data.agencyCountry != null){
				$scope.agencyCountry.selected = data.agencyCountry;
				if (data.agencyCountry.countryName == 'USA') {
						$scope.isUSAForAgency = true;
						$scope.defaultAgencyState = false;
				} else {
						$scope.isUSAForAgency = false;
						$scope.defaultAgencyState = true;
				}
				
			}
			
			if(data.currencyBO != null){
				$scope.currencyBO.selected = data.currencyBO;
			}
			
			$scope.segment.selected = data.segmentBO;
			$scope.region.selected = data.regionBO;
			$scope.company.selected = data.companyBO;
			$scope.businessArea.selected = data.businessAreaBO;
			if(data.businessGroupBO != null){
				$scope.businessGroupIdData = data.businessGroupBO.businessGroupId;
			}
			
			if(data.companyState != null){
				$scope.companyState.selected = data.companyState;
			}
			if(data.agencyState != null ){
				$scope.agencyState.selected = data.agencyState;
			}
			
			
			/*$("#segment").select2("val", "0");
			$("#region").select2("val", "0");
			$("#company").select2("val", "0");
			$("#businessArea").select2("val", "0");*/

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.submitForm = function() {
		$http({
			method : 'POST',
			url : 'xmlbasedcreditRequestSubmission'
		});

	}

});

app.directive('aDisabled', function() {
    return {
        compile: function(tElement, tAttrs, transclude) {
            //Disable ngClick
            tAttrs["ngClick"] = "!("+tAttrs["aDisabled"]+") && ("+tAttrs["ngClick"]+")";

            //Toggle "disabled" to class when aDisabled becomes true
            return function (scope, iElement, iAttrs) {
                scope.$watch(iAttrs["aDisabled"], function(newValue) {
                    if (newValue !== undefined) {
                        iElement.toggleClass("disabled", newValue);
                    }
                });

                //Disable href on click
                iElement.on("click", function(e) {
                    if (scope.$eval(iAttrs["aDisabled"])) {
                        e.preventDefault();
                    }
                });
            };
        }
    };
});   

app.controller('DashboardController', function($scope, $http) {
	$scope.itemList = [];
	$scope.getDashboardDetails = function() {
		$http({
			method : 'GET',
			url : 'getDashboardDetails'
		}).success(function(data, status, headers, config) {
			$scope.dashboardBOs = data;
		}).error(function(data, status, headers, config) {
		});
	};
});
// Search in dropdown
$(document).ready(
		function() {
			var nowDate = new Date();
			var today = new Date(nowDate.getFullYear(), nowDate.getMonth(),
					nowDate.getDate(), 0, 0, 0, 0);
			$('#segLabel').tooltip();
			$('#baLabel').tooltip();
			$('#comLabel').tooltip();
			$('#regLabel').tooltip();
			$.getScript('./resources/js/select2.min.js', function() {
				$.getScript('./resources/js/bootstrap-datepicker.min.js',
						function() {

							$("#segment").select2({

							});

							$("#region").select2({

							});

							$("#company").select2({

							});

							$("#businessArea").select2({

							});

							$("#companyCountry").select2({

							});

							$("#companyState").select2({

							});

							$("#agencyCountry").select2({

							});

							$("#agencyState").select2({

							});

							$('#requestNeededByDate').datepicker({
								format : 'mm-dd-yyyy',
								startDate : today,
								autoclose : true,
							});

							$('#termStartDate').datepicker({
								format : 'mm-dd-yyyy',
								autoclose : true,
							});

							$('#termEndDate').datepicker({
								format : 'mm-dd-yyyy',
								autoclose : true,
							});

						});// script
			});// script
		});
