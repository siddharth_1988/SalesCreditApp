'use strict';
var app = angular.module('salesCreditApp', ['ngRoute','ngSanitize','ui.select']);
/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});
app.config(['$routeProvider', function($routeProvider) {
	   $routeProvider.
	  
	   when('/dashboard', {
	      templateUrl: 'dashboard.jsp', 
	      controller: 'DashboardController'
	   }).
	   
	   when('/creditRequest', {
	      templateUrl: 'creditRequest.jsp', 
	      controller: 'CreditRequestController'
	   }).
	   
	   when('/help', {
		      templateUrl: 'help.jsp', 
		      controller: 'HelpController'
		   }).
	   
	   otherwise({
	      redirectTo: '/dashboard'
	   });
		
	}]);

app.controller('CreditRequestController', function($scope, $http) {
	$scope.itemList = [];
	$scope.getSegmentDataFromServer = function() {
	$scope.segment = {};
		$http({
			method : 'GET',
			url : 'getAllSegments'
		}).success(function(data, status, headers, config) {
			$scope.segmentBODatas = data;
	
			$scope.regionBODatas = '';
			$scope.companyBODatas = '';
			$scope.businessAreaBODatas = '';
			$scope.itemSegment = "0";
			$scope.itemRegion = "0";
			$scope.itemCode = "0";
			$scope.itemBusinessArea = "0";
			$scope.businessGroupIdData = null;			
			$scope.segment.selected = {segmentId: '3', segmentName: 'MEDIA NETWORKS'};		

		}).error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		});
	};

	$scope.selectedSegment = function(itemSegment) {
		$http({
			method : 'GET',
			url : 'getRegionBySegment/' + itemSegment
		}).success(function(data, status, headers, config) {
			$scope.regionBODatas = data;

			$scope.companyBODatas = '';
			$scope.businessAreaBODatas = '';
			$scope.itemRegion = null;
			$scope.itemCode = "0";
			$scope.itemBusinessArea = "0";
			$scope.businessGroupIdData = null;
			

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.selectedRegion = function(itemSegment, itemRegion) {
		$http({
			method : 'GET',
			url : 'getCompanyByRegion/' + itemSegment + '/' + itemRegion
		}).success(function(data, status, headers, config) {
			$scope.companyBODatas = data;

			$scope.businessAreaBODatas = '';
			$scope.itemCode = "0";
			$scope.itemBusinessArea = "0";
			$scope.businessGroupIdData = null;
			

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.selectedCompany = function(itemSegment, itemRegion, itemCode) {
		$http(
				{
					method : 'GET',
					url : 'getBusinessAreaByCompany/' + itemSegment + '/'
							+ itemRegion + '/' + itemCode
				}).success(function(data, status, headers, config) {
			$scope.businessAreaBODatas = data;

			$scope.itemBusinessArea = "0";
			$scope.businessGroupIdData = null;
			

		}).error(function(data, status, headers, config) {
		});
	}

	/*
	 * $scope.selectedBusinessArea = function(itemSegment, itemRegion, itemCode,
	 * itemBusinessArea) { $http( { method : 'GET', url : 'getBusinessArea/' +
	 * itemSegment + '/' + itemRegion + '/' + itemCode + '/' + itemBusinessArea
	 * }).success(function(data, status, headers, config) {
	 * 
	 * }).error(function(data, status, headers, config) { }); }
	 */

	$scope.getBusinessGroup = function(itemCode, itemBusinessArea) {
		$http({
			method : 'GET',
			url : 'getBusinessGroup/' + itemCode + '/' + itemBusinessArea
		}).success(function(data, status, headers, config) {
			$scope.businessGroupIdData = data.businessGroupId;
			$scope.currentDate = new Date();
			$scope.isUSA = false;
			$scope.showdefaultState = false;
			$scope.isUSAForAgency = false;
			$scope.defaultAgencyState = false;
		}).error(function(data, status, headers, config) {
			$scope.businessGroupIdData = null;
		});
	}

	$scope.getCountryList = function() {
		$http({
			method : 'GET',
			url : 'getCountryList'
		}).success(function(data, status, headers, config) {
			$scope.countryList = data;

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.getUsaStateList = function() {
		$http({
			method : 'GET',
			url : 'getUsaStateList'
		}).success(function(data, status, headers, config) {
			$scope.stateList = data;

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.getUsaStateListForAgency = function() {
		$http({
			method : 'GET',
			url : 'getUsaStateList'
		}).success(function(data, status, headers, config) {
			$scope.agencyStateList = data;

		}).error(function(data, status, headers, config) {
		});
	}

	$scope.isUSASelected = function(countryName) {
		$scope.isUSA = false;
		$scope.showdefaultState = true;
		if (countryName == 'USA') {
			$scope.isUSA = true;
			$scope.showdefaultState = false;
		}

	}

	$scope.isUSASelectedForAgency = function(countryName) {
		$scope.isUSAForAgency = false;
		$scope.defaultAgencyState = true;
		if (countryName == 'USA') {
			$scope.isUSAForAgency = true;
			$scope.defaultAgencyState = false;
		}

	}
	
	$scope.saveRequesterDetails = function(requesterName, requesterEmail, requesterDate) {
		$http({
			method : 'GET',
			url : 'saveRequesterDetails/' + requesterName + '/' + requesterEmail+ '/' + requesterDate
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.saveBusinessDetails = function(requestedCreditLimit, termStartDate, termEndDate) {
		$http({
			method : 'GET',
			url : 'saveBusinessDetails/' + requestedCreditLimit + '/' + termStartDate+ '/' + termEndDate
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.saveCompanyDetails = function(companyName, companyAddress, companyCity, companyCountry, companyState, companyState1, companyZip) {
		$http({
			method : 'GET',
			url : 'saveCompanyDetails/' + companyName 
			+ '/' + companyAddress+ '/' + companyCity+ '/' + companyCountry+ '/' + companyState+ '/' + companyState1+ '/' + companyZip
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.saveAgencyDetails = function(agencyName, address, agencyCity, agencyCountry, agencyState, agencyState1, agencyZip, accountExecutive) {
		$http({
			method : 'GET',
			url : 'saveAgencyDetails/' + agencyName 
			+ '/' + address+ '/' + agencyCity+ '/' + agencyCountry+ '/' + agencyState+ '/' + agencyState1+ '/' + agencyZip+ '/' + accountExecutive
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.saveCollectionDetails = function(phone, email, name) {
		$http({
			method : 'GET',
			url : 'saveCollectionDetails/' + phone + '/' + email+ '/' + name
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.saveAdvanceCashDetails = function(cashAdvance, direct) {
		$http({
			method : 'GET',
			url : 'saveAdvanceCashDetails/' + cashAdvance + '/' + direct
		}).success(function(data, status, headers, config) {
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.getCreditRequestDetailsFromServer = function() {
		$http({
			method : 'GET',
			url : 'getCreditRequestDetails'
		}).success(function(data, status, headers, config) {
			$scope.creditRequest = data;
		}).error(function(data, status, headers, config) {
		});
	}
	
	$scope.submitForm = function() {
		$http({
			method : 'POST',
			url : 'xmlbasedcreditRequestSubmission'
		});

	}
});

app.controller('DashboardController', function($scope, $http) {
	$scope.itemList = [];
	$scope.getDashboardDetails = function() {

		$http({
			method : 'GET',
			// FIXME need to remove hardcoded value test.
			url : 'getDashboardDetails/test'
		}).success(function(data, status, headers, config) {
			$scope.dashboardBOs = data;
			
		}).error(function(data, status, headers, config) {
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		});
	};
});

